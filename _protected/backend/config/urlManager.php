<?php 
return [
    'class' => 'yii\web\UrlManager',
    'baseUrl' => getenv('BACKEND_HOST_URL'),
    'showScriptName' => false,
    'enablePrettyUrl' => true,
    'rules' => [
        '<controller:\w+>/<id:\d+>' => '<controller>/create',
    	'<controller:\w+>/<id:\d+>' => '<controller>/view' ,
    	'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>' ,
    	'<controller:\w+>/<action:\w+>' => '<controller>/<action>' ,
    ],
];