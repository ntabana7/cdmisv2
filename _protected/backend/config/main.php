<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

// Added this line
$urlManager = require(__DIR__ . '/urlManager.php');
// Added this line

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],    

    'modules' => [

//Telling tha system that we have created a module called cdproviders
    'cdproviders' => [      
      'class' => 'backend\modules\cdproviders\Settings',
    ],
//Telling tha system that we have created a module called cdproviders
    'internship' => [
      'class' => 'backend\modules\internship\Settings',
    ],

    'cna' => [
       'class' => 'backend\modules\cna\Assessment',
    ],

    'treemanager' =>  [
      'class' => '\kartik\tree\Module',
      // see settings on http://demos.krajee.com/tree-manager#module
    ],

    'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
    ],

    'components' => [
        // here you can set theme used for your backend application 
        // - template comes with: 'default', 'slate', 'spacelab' and 'cerulean'
 
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@webroot/themes/default/views'],
                'baseUrl' => '@web/themes/default',
            ],
        ],

        'user' => [

        ////////////////////////////original codes///////////////

            // 'identityClass' => 'common\models\UserIdentity',
            // 'enableAutoLogin' => true,

        ////////////////////////////Newly added codes///////////////////

            'identityClass' => 'common\models\UserIdentity',
            'enableAutoLogin' => false,
            'enableSession' => true,
            'authTimeout' =>1800,
            'loginUrl' => ['site/login'],
        ],

        ////////////////////////////Newly added codes///////////////////
         //'session' => [
                    //'timeout' => 300,
           // ],

        ////////////////////////////End Newly added codes///////////////////

        'log' => [
            'traceLevel' => YII_DEBUG ? 0 : 0,
            'targets' => [
                'db' => [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'except' => ['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
                    'prefix' => function () {
                        $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;
                        return sprintf('[%s][%s]', Yii::$app->id, $url);
                    },
                    'logVars' => [],
                    'logTable' => '{{%system_log}}'
                ]
            ],
        ],

        // Added this line
         'urlManager' => $urlManager,
         // Added this line

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],//iyi akorade????
    
    ////////////////////////////Newly added codes///////////////////

    'as beforeRequest'=>[
    'class'=>'backend\components\CheckIfLoggedIn',
    'class' => 'yii\filters\AccessControl',
        'rules' => [
        [
        'actions' => ['login', 'error'],
        'allow' => true,
        ],
        [

        'allow' => true,
        'roles' => ['@'],
        ],
        ],
    ],

    ////////////////////////////End Newly added codes///////////////////
    'params' => $params,
];
