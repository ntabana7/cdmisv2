<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Provinces */
?>
<div class="provinces-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProvince',
            'provName',
        ],
    ]) ?>

</div>
