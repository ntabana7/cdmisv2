<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Provinces */

?>
<div class="provinces-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
