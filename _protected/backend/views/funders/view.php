<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Funders */
?>
<div class="funders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idFunder',
            'funderName',
        ],
    ]) ?>

</div>
