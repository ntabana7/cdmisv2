<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Funders */

?>
<div class="funders-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
