<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Commentonprogress */
?>
<div class="commentonprogress-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dataFromPlan' => $dataFromPlan
    ]) ?>

</div>
