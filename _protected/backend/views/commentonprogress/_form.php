<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Commentonprogress */
/* @var $form yii\widgets\ActiveForm */
?>
<h4>Justification on <?= $dataFromPlan->actionDescription;?></h4>
<hr>
<div class="commentonprogress-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idplan')->textInput(['value'=>$dataFromPlan->idplan,'readonly' => true])->label() ?>

    <?= $form->field($model, 'reportedquarter')->dropDownList($model->quarterArray) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file')->fileInput(['maxlength' => true]) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
