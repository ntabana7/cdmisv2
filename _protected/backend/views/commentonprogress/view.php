<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Commentonprogress */
?>
<div class="commentonprogress-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcomment',
            'idplan',
            'reportedquarter',
            'comment:ntext',
            'finalreport',
            'createdon',
            'iduser0.username',
        ],
    ]) ?>

</div>
