<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\models\Funderbyinstitution $model
 */

$this->title = $model->idFunderbyinstitution;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Funderbyinstitutions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funderbyinstitution-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'idFunderbyinstitution',
            'id',
            'idInstit',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->idFunderbyinstitution],
        ],
        'enableEditMode' => true,
    ]) ?>

</div>
