<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Funderbyinstitution $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Funderbyinstitution',
]) . ' ' . $model->idFunderbyinstitution;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Funderbyinstitutions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idFunderbyinstitution, 'url' => ['view', 'id' => $model->idFunderbyinstitution]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="funderbyinstitution-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
