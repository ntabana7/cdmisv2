<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Funderbyinstitution $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Funderbyinstitution',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Funderbyinstitutions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funderbyinstitution-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
