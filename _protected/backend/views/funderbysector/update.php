<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Funderbysector */
?>
<div class="funderbysector-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
