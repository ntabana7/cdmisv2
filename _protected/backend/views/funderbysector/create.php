<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Funderbysector */

?>
<div class="funderbysector-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
