<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Funderbysector */
?>
<div class="funderbysector-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idFunderbysector',
            'idFunder',
            'idInstitSector',
        ],
    ]) ?>

</div>
