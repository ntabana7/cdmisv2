<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\InternshipInternapplication $model
 */

$this->title = $model->idInternapplication;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Internship Internapplications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internship-internapplication-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'idInternapplication',
            'nida',
            'firstname',
            'secondname',
            'email:email',
            'telephone',
            'dob',
            'gender',
            'idQualif',
            'idQualifarea',
            'idGrade',
            'degree',
            'idUniversity',
            'yearofgraduation',
            'accountNbr',
            'idBank',
            'idDistrict',
            'idRecommendation',
            'recommendation',
            'idWorkingexperience',
            'howmanyyears',
            'workingposition',
            'applicationdate',
            'validatingdate',
            'editingdate',
            'placementdate',
            'status',
            'idInstit',
            'reasonnotfinishing',
            'idRetained',
            'idEmployed',
            'employedby',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->idInternapplication],
        ],
        'enableEditMode' => true,
    ]) ?>

</div>
