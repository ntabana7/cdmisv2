<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\InternshipInternapplication $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Internship Internapplication',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Internship Internapplications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internship-internapplication-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
