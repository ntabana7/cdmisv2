<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\InternshipInternapplication $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="internship-internapplication-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'nida' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Nida...', 'maxlength' => 16]],

            'firstname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Firstname...', 'maxlength' => 200]],

            'secondname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Secondname...', 'maxlength' => 200]],

            'email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email...', 'maxlength' => 50]],

            'telephone' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Telephone...', 'maxlength' => 20]],

            'dob' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Dob...', 'maxlength' => 12]],

            'gender' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Gender...', 'maxlength' => 10]],

            'idQualif' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Qualif...']],

            'idQualifarea' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Qualifarea...']],

            'idGrade' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Grade...']],

            'degree' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Degree...', 'maxlength' => 200]],

            'idUniversity' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id University...']],

            'yearofgraduation' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Yearofgraduation...', 'maxlength' => 20]],

            'accountNbr' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Account Nbr...', 'maxlength' => 50]],

            'idBank' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Bank...']],

            'idDistrict' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id District...']],

            'idRecommendation' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Recommendation...']],

            'idWorkingexperience' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Workingexperience...']],

            'applicationdate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Applicationdate...', 'maxlength' => 15]],

            'idInstit' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Instit...']],

            'idRetained' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Retained...']],

            'idEmployed' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Employed...']],

            'howmanyyears' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Howmanyyears...']],

            'reasonnotfinishing' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Reasonnotfinishing...', 'maxlength' => 200]],

            'employedby' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Employedby...', 'maxlength' => 200]],

            'status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Status...', 'maxlength' => 20]],

            'recommendation' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Recommendation...', 'maxlength' => 100]],

            'workingposition' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Workingposition...', 'maxlength' => 100]],

            'validatingdate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Validatingdate...', 'maxlength' => 15]],

            'editingdate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Editingdate...', 'maxlength' => 15]],

            'placementdate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Placementdate...', 'maxlength' => 15]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
