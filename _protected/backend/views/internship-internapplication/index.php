<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\modules\internship\models\InternshipInternapplicationSearch $searchModel
 */

$this->title = Yii::t('app', 'Internship Internapplications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internship-internapplication-index">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Internship Internapplication',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idInternapplication',
            'nida',
            'firstname',
            'secondname',
            'email:email',
//            'telephone', 
//            'dob', 
//            'gender', 
//            'idQualif', 
//            'idQualifarea', 
//            'idGrade', 
//            'degree', 
//            'idUniversity', 
//            'yearofgraduation', 
//            'accountNbr', 
//            'idBank', 
//            'idDistrict', 
//            'idRecommendation', 
//            'recommendation', 
//            'idWorkingexperience', 
//            'howmanyyears', 
//            'workingposition', 
//            'applicationdate', 
//            'validatingdate', 
//            'editingdate', 
//            'placementdate', 
//            'status', 
//            'idInstit', 
//            'reasonnotfinishing', 
//            'idRetained', 
//            'idEmployed', 
//            'employedby', 

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->urlManager->createUrl(['internship-internapplication/view', 'id' => $model->idInternapplication, 'edit' => 't']),
                            ['title' => Yii::t('yii', 'Edit'),]
                        );
                    }
                ],
            ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => true,

        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type' => 'info',
            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),
            'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
    ]); Pjax::end(); ?>

</div>
