<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Clusters */
?>
<div class="clusters-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
