<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Clusters */
?>
<div class="clusters-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCluster',
            'clusterName',
            'title',
            'phone',
            'email:email',
        ],
    ]) ?>

</div>
