<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PrioritySectors */
?>
<div class="priority-sectors-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPriority',
            'priorityName',
        ],
    ]) ?>

</div>
