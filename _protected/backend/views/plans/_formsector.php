<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Programs;
use backend\models\Subprograms;
use backend\models\CbLevels;
use backend\models\CbSublevels;
use backend\models\CbIndicators;
use backend\models\CbTypes;
use backend\models\Skills;
use backend\models\TrainingAreas;
use backend\models\CbQualifications;
use yii\helpers\Url;
use backend\models\Institutions;
use backend\models\InstitSectors;

//This Class is included to allow display of definition
use kartik\popover\PopoverX;
/* @var $this yii\web\View */
/* @var $model backend\models\Plans */
/* @var $form yii\widgets\ActiveForm */
 
$idInstitSector = '<p class="text-justify">' .
'<b>Public Sector:</b>Is any entity under the stewardship of the government.<br><br>

<b>Private Sector:</b> refers to all units of production or establishments owned by either individuals or a group of individuals that are participating in production within Rwanda, and this in contrast with the public sector.<br><br>
<b>Civil society:</b> refers all non-government and non-private organizations that are contributing to improvement of the welfare of Rwandans.
' . 
'</p>';
$idInstit = '<p class="text-justify">' .
'<b>Institution:</b> Establishment, foundation or organization created to pursue a particular type of endeavor. ' . 
'</p>';
// var_dump($idInstitSector);die;
// echo $idInstitSector;die;
//Definitions are written here

$program = '<p class="text-justify">' .
'<b>Programmes:</b> are the major pillars, strategic orientations or targets each institution is delivering on, these are based on your sector and/or institutional roles and responsibilities. Your sector/organisation’s main programs pulled from the Annual Action Plan as per the Budget Call Circular format for public institutions.' . 
'</p>';

$subprogram = '<p class="text-justify">' .
'<b>Sub programs:</b> are sub pillars or smaller targets that are linked to the big programs that all institutions are delivering on, they are more specific to what sectors/institutions are delivering on in line with their mandate. Your sector/organisation’s sub program pulled from the Annual Action Plan as per the Budget Call Circular format for public institutions.' . 
'</p>';

$output = '<p class="text-justify">' .
'<b>Output:</b> this is the short term result/achievement trough the implementation of the capacity building activity you are planning.  It’s taken directly from your Annual Action Plan.' . 
'</p>';

$cbchallenge = '<p class="text-justify">' .
'<b>Capacity Challenge:</b> This is the capacity problem, at different levels (individual, organizational or /and institutional) you currently experience and that prevent you from achieving performance targets. It is that issue at hand that may impede you from achieving your annual targets as institutions or individuals.' . 
'</p>';

$baseline = '<p class="text-justify">' .
'<b>CD Baseline:</b> This refers to the starting point or current situation in the organization. The point from where implementation begins, improvement is judged, or comparison is made.' . 
'</p>';

$level = '<p class="text-justify">' .
'A CD level includes either Institutional, Organizational or Individual.<br><br>

<b>INSTITUTIONAL LEVEL OR ENABLING ENVIRONMENT:</b>refers to the policy, legal and regulatory environment within which organizations and sectors function. These may be described as the formal and informal “rules of the game” that dictate what organizations and sectors can or cannot do, as well as the level of authority and discretion under their mandates.<br><br>

<b>ORGANIZATIONAL CAPABILITIES:</b> refers to the ability of organizations to deliver on their mandates and meet performance targets. Key drivers of organizational capacity include internal policies, organizational arrangements/ structures, operating procedures, processes, work flows and internal frameworks that allow an organization if it is well resourced and aligned to operate and deliver on its mandate as well as soft attributes such as attitudes and shared and values.<br><br>

<b>INDIVIDUAL COMPETENCIES:</b> refers to sufficiency and quality of the workforce i.e. the right numbers of people with the required knowledge, skills, experience, exposure, aptitudes, attitudes and mind-set. ' . 
'</p>';

$sublevel = '<p class="text-justify">' .
'<b>CD Approach:</b> refers to how (process) to develop the capacity of an Institution, organization or individuals . 
'  . 
'</p>';

$indicator = '<p class="text-justify">' .
'<b>CD Indicator:</b> An indicator in general is a specific measure that when tracked systematically over time, indicates progress (or not) toward a specific target. Indicators are traditionally numerical.  An outcome indicator answers the question:<br><br> How will we know success when we see it?<br><br>

<b>QUANTITATIVE INDICATORS</b> can be expressed as a number. For example, the number of civil servants trained by Rwanda Management Institute (RMI), the percentage of staff who understand the vision and mission of the organization among others.<br><br> 
<b>QUALITATIVE INDICATORS</b> on the other hand, indicate the quality of something, and they cannot normally be expressed as a number; for example, ‘improved working relations among staff’; improved staff participation in learning clinics.<br><br>
<b>PERFORMANCE INDICATORS</b> are measures of inputs, processes, outputs, outcomes, and impacts for development projects, programs, or strategies.  Only beneficial when supported with sound data collection, perhaps involving formal surveys, analysis, and review.  Indicators enable us to track progress, demonstrate results, and take corrective action to improve service delivery. 

' . 
'</p>';

$cbtype = '<p class="text-justify">' .
'<b>SPECIALIZED</b> - short courses composed of curriculum units or training modules - after a formal academic training in Civil Engineering, one could then undertake specialized training in Railway; Oil & Gas<br><br>
<b>GENERIC/ SOFT SKILLS</b> – Generic/ Soft skills are ‘transferable’ skills and are seen as part of a suite of skills which in combination optimize an individual’s productivity. They underpin other technical skills, as well as drawing on personal attributes, which affect how effectively skills can be learnt.<br><br>
<b>PROFESSIONAL</b> – training to earn or maintain professional credentials – usually acquired through Continuous Professional Development (CPD) that takes one through different levels.<br><br>
<b>EXPERT</b> – Advanced training level as agreed upon by the Sector e.g. Certified Microsoft IT Expert.<br><br>
<b>ASSOCIATE</b> - Basic training level as agreed upon by the Sector e.g. Associate Microsoft IT Expert

' . 
'</p>';

$skills = '<p class="text-justify">' .
'<b>Skills:</b> is a learned capacity to obtain pre-determined results. E.g.: Technical Skills, Cognitive Skills and Inter-personal skills' . 
'</p>';

$training = '<p class="text-justify">' .
'<b>Training areas:</b>' . 
'</p>';

$qualification = '<p class="text-justify">' .
'<b>Qualification:</b>' . 
'</p>';

$beneficiaries = '<p class="text-justify">' .
'<b>Beneficiaries</b>: Individuals, groups, and or organizations whose situation is supposed to improve through the project activities.  OR the individuals, groups, or organizations, whether targeted or not, that benefit, directly or indirectly, from the development intervention.  Other terms that can be used are <b>reach</b>, <b>target group</b>.' . 
'</p>';

$cbaction = '<p class="text-justify">' .
'<b>CD action:</b> Actions taken or work performed through which inputs, such as training, capacity building funds, technical assistance for example international expert coaches under the strategic capacity building initiative and other types of CD resources are mobilized and applied to produce specific outputs.' . 
'</p>';

$responsable = '<p class="text-justify">' .
'<b>Responsable:</b> refers to individual or organization that has the responsibility to implement the planned CD action.' . 
'</p>';

$stakeholders = '<p class="text-justify">' .
'<b>Key Stakeholder:</b> Key Institutions or other partners who will have a role to play, or will impact, either directly or indirectly, the implementation and effectiveness of the CB action.' . 
'</p>';

$funders = '<p class="text-justify">' .
'<b>Source of fund:</b> Refers to the current or expected source of funding. It is not necessary to have identified sources of funding for all interventions at this stage. E.g JICA, KOICA, SIDA Fund.' . 
'</p>';

$budget = '<p class="text-justify">' .
'<b>Budget:</b> These are the estimated financial resources required to implement the CB action.' . 
'</p>';

$actionplan = '<p class="text-justify">' .
'Action plan: Is a document that lists what steps must be taken in order to achieve a specific goal. Its purpose is to clarify what resources are required to reach the goal, formulate a timeline for when specific tasks need to be completed and determine what resources are required.' . 
'</p>';

?>
<!-- jquery -->
<script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>



 <?php $form = ActiveForm::begin(['options'=> ['enctype'=> 'multipart/form-data']]); ?>
<div class="plans-form">

<!-- boostrap codes -->

<div class="container" style="width:100%">
    <div class="row">
        <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form">
                <div class="tab-content">

                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="step1">



                            <div class="row">
                            <div class="col-md-6"> 

                            <!-- Added Program which was not on the form to allow getting its ID end being used to pick related subprograms-->
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $idInstitSector,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'idInstitSector')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(InstitSectors::sectorss(),'idInstitSector','sectName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select a Sector',
                                'onchange'=>'
                                    $.post("'.Url::to(['institutions/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                         $("select#plans-idinstit" ).html(data);

                                    });
                                    $.post("'.Url::to(['programs/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                         $("select#plans-idprogram" ).html(data);

                                    });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]); 
                            ?>
                            </div>
                            <div class="col-md-6">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $idInstit,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'idInstit')->widget(select2::classname(),[
                            'name' => 'idInstit',
                            'data'=>ArrayHelper::map(Institutions::find()->where('idInstit=0')->all(),'idInstit','institName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select an institution',
                                // 'onchange'=>'
                                //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                //          $("select#select2-plans-idsubprogram-container" ).html(data);
                                //     });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);      
                            ?>
                            </div>
                            
                        </div>











                            <div class="row">
                            <div class="col-md-4"> 

                            <!-- Added Program which was not on the form to allow getting its ID end being used to pick related subprograms-->
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $program,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'idProgram')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(Programs::find()->where('idInstitSector=0')->all(),'idProgram','programName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select a program',
                                'onchange'=>'
                                    $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                         $("select#plans-idsubprogram" ).html(data);
                                    });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]); 
                            ?>
                            </div>
                            <div class="col-md-4">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $subprogram,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'idSubProgram')->widget(select2::classname(),[
                            'name' => 'idSubProgram',
                            'data'=>ArrayHelper::map(Subprograms::find()->where('idSubProgram=0')->all(),'idSubProgram','subProgramName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select a Sub program',
                                // 'onchange'=>'
                                //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                //          $("select#select2-plans-idsubprogram-container" ).html(data);
                                //     });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);      
                            ?>
                            </div>
                            <div class="col-md-4">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $output,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'output')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">                            
                            <div class="col-md-6">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $cbchallenge,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'challenge')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $baseline,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'baseline')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                        </ul>                       
                    </div>

                    <!-- Start step 2 -->

                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div class="step2">
                            <div class="step_21">
                            <div class="row">
                            <div class="col-md-3">                            
                               <!-- Added level which was not on the form to allow getting its ID end being used to pick related sublevel-->
                            
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $level,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->

                            <?= $form->field($model, 'idLevel')->widget(select2::classname(),[
                                                    'data'=>ArrayHelper::map(CbLevels::find()->all(),'idLevel','level'),
                                                    'theme' => Select2::THEME_KRAJEE, 
                                                    'options'=>[
                                                        'placeholder'=>'Select CB level',
                                                        'onchange'=>'
                                                            
                                                            $.post("'.Url::to(['cb-sublevels/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                                                 $("select#plans-idsublevel" ).html(data);
                                                            });'
                                                    ],
                                                    'language' => 'en',
                                                    'pluginOptions'=>['alloweClear'=>true],
                                                    ]);

                               
                            ?>
                            </div>
                           
                            <div class="col-md-3">                            
                               <!-- Added level which was not on the form to allow getting its ID end being used to pick related sublevel-->
                             <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $sublevel,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->                         

                            <?= $form->field($model, 'idSubLevel')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(CbSublevels::find()->where('idSubLevel=0')->all(),'idSubLevel','subLevel'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select CB Sub level',
                                'onchange'=>'

                                // To get dropdownlist for courses

                                   var id = document.getElementById("plans-idsublevel").value;
                                   if(id == 2){
                                    $("#course" ).show();
                                    $("#quarter1" ).show();
                                    $("#skills" ).show();
                                    $("#qualif" ).show();
                                    $("#quarter2" ).hide();

                                   }else if(id == 3){
                                    $("#course" ).show();
                                    $("#quarter1" ).hide();
                                    $("#skills" ).show();
                                    $("#qualif" ).show();
                                    $("#quarter2" ).show();

                                   }
                                    else{
                                    $("#course" ).hide();
                                    $("#skills" ).hide();
                                    $("#qualif" ).hide();
                                    $("#quarter1" ).hide();
                                    $("#quarter2" ).show();
                                   }
                                    
                                    $.post("'.Url::to(['cb-indicators/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                         $("select#plans-idindicator" ).html(data);
                                    });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>
                            </div>

                             <div class="col-md-3"> 
                             <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $indicator,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition --> 
                            <?= $form->field($model, 'idIndicator')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(CbIndicators::find()->where('idIndicator=0')->all(),'idIndicator','indicator'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select CB indicator',
                                // 'onchange'=>'
                                //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                //          $("select#select2-plans-idsubprogram-container" ).html(data);
                                //     });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>
                            </div>
                             <div class="col-md-3"> 
                             <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $cbtype,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->                           
                            <?= $form->field($model, 'idCbType')->dropDownList(ArrayHelper::map(CbTypes::find()->all(),'idCbType','cbType'),[ 'prompt'=>'Select CB type',
                            'language' => 'en',
                            ]);
                            ?>
                            </div>                           
                            </div>

                            <!-- Second row step 2 -->
                            <div class="row">
                             <div class="col-md-4" id='skills' style='display:none'>
                             <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $skills,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?> 
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'idSkills')->widget(select2::classname(),[
                                                    'data'=>ArrayHelper::map(Skills::find()->all(),'idSkills','skills'),
                                                    'theme' => Select2::THEME_KRAJEE, 
                                                    'options'=>[
                                                        'placeholder'=>'Select Skills',
                                                        'onchange'=>'
                                                            
                                                            $.post("'.Url::to(['training-areas/lists', 'id'=> '']).'"+$(this).val()+","+$("#plans-idcbtype").val()+","+$("#plans-idinstitsector").val(),function(data){
                                                                 $("select#plans-idtraining" ).html(data);
                                                              
                                                            
                                                            });'
                                                    ],
                                                    'language' => 'en',
                                                    'pluginOptions'=>['alloweClear'=>true],
                                                    ]);

                               
                            ?>
                            </div>
                            <div class="col-md-4" id='course' style='display:none'> 
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $training,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?> 
                            <!--End Button allowing the display of Definition -->

                            <?= $form->field($model, 'idTraining')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(TrainingAreas::find()->where('idTraining=0')->all(),'idTraining','courseName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select training area',
                                // 'onchange'=>'
                                //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                //          $("select#select2-plans-idsubprogram-container" ).html(data);
                                //     });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>
                            </div>
                            
                            <div class="col-md-4" id='qualif' style='display:none'>
                            <!--Button allowing the display of Definition -->
                           <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $qualification,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'idQualif')->dropDownList(ArrayHelper::map(CbQualifications::find()->all(),'idQualif','qualif'),[ 'prompt'=>'Select CB qualification',
                            'language' => 'en',
                            ]);
                            ?>
                            </div>
                            </div>

                            <!-- Third row step 2-->
                            <div class="row">
                            <div class="col-md-6">                            
                               <!-- Added level which was not on the form to allow getting its ID end being used to pick related sublevel-->
                            <!--Button allowing the display of Definition -->
                             <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $beneficiaries,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <span style="color:red"><?= Yii::$app->session->getFlash('beneficiaire');?></span> 
                            <?= $form->field($model, 'nbrbeneficiaries')->textInput() ?>
                            </div>
                            <div class="col-md-6">                            
                            <!-- Added level which was not on the form to allow getting its ID end being used to pick related sublevel-->
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $cbaction,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->

                            <?= $form->field($model, 'actionDescription')->textInput(['maxlength' => true]) ?>
                            </div>                          
                            </div>

                            <!-- 4th row step2 -->
                            <div class="row">
                            <div class="col-md-6">                            
                            <!-- <?= $form->field($model, 'generalNeeds')->textInput(['maxlength' => true]) ?> -->
                            </div>
                            <div class="col-md-6">                            
                            <!-- <?= $form->field($model, 'specificNeeds')->textInput(['maxlength' => true]) ?> -->
                            </div>                                                             
                            </div>

                            <!-- 5th row step2 -->
                            <div class="row">
                            <div id='quarter1' style='display:none'>
                            <div class="col-md-3"> 
                            <span><b><?= $model->getAttributeLabel('quarters'); ?></b></span>                    
                            <?= $form->field($model, 'male1')->textInput(['placeholder'=>'male','style'=>'width:49%'])->label(false) ?>
                            <?= $form->field($model, 'female1')->textInput(['placeholder'=>'female','style'=>'width:49%'])->label(false)  ?>
                            </div>
                            <div class="col-md-3"> 
                            <span><b><?= $model->getAttributeLabel('quarters2'); ?></b></span>                   
                            <?= $form->field($model, 'male2')->textInput(['placeholder'=>'male','style'=>'width:49%'])->label(false) ?>
                            <?= $form->field($model, 'female2')->textInput(['placeholder'=>'female','style'=>'width:49%'])->label(false)  ?>
                            </div>
                            <div class="col-md-3">                        
                            <span><b><?= $model->getAttributeLabel('quarters3'); ?></b></span>                    
                            <?= $form->field($model, 'male3')->textInput(['placeholder'=>'male','style'=>'width:49%'])->label(false) ?>
                            <?= $form->field($model, 'female3')->textInput(['placeholder'=>'female','style'=>'width:49%'])->label(false)  ?>
                            </div>
                            <div class="col-md-3">                        
                            <span><b><?= $model->getAttributeLabel('quarters4'); ?></b></span>                   
                            <?= $form->field($model, 'male4')->textInput(['placeholder'=>'male','style'=>'width:49%'])->label(false) ?>
                            <?= $form->field($model, 'female4')->textInput(['placeholder'=>'female','style'=>'width:49%'])->label(false)  ?>
                            </div> 
                            </div> 

                            <div id='quarter2' style='display:none'>
                            <div class="col-md-3"> 
                            <span><b><?= $model->getAttributeLabel('quarters'); ?></b></span>
                            <?= $form->field($model, 'qter1')->checkbox(['value'=>'Quarter1'])->label(''); ?>
                            </div>
                            <div class="col-md-3"> 
                            <span><b><?= $model->getAttributeLabel('quarters2'); ?></b></span>
                            <?= $form->field($model, 'qter2')->checkbox(['value'=>'Quarter2'])->label(''); ?>
                            
                            </div> 
                            <div class="col-md-3"> 
                            <span><b><?= $model->getAttributeLabel('quarters3'); ?></b></span> 
                            <?= $form->field($model, 'qter3')->checkbox(['value'=>'Quarter3'])->label(''); ?>
                            </div>
                            <div class="col-md-3">  
                            <span><b><?= $model->getAttributeLabel('quarters4'); ?></b></span>  
                            <?= $form->field($model, 'qter4')->checkbox(['value'=>'Quarter4'])->label(''); ?>
                            </div> 
                            </div>

                            </div>

                            <!-- 6th row step2 -->
                            <div class="row">
                            <div class="col-md-6">                            
                            
                            </div>
                            <div class="col-md-6">                            
                            
                            </div>                                                            
                            </div>
                            </div>
                            <div class="step-22">
                            
                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <!-- End Start step 2 -->

                    <div class="tab-pane" role="tabpanel" id="step3">
                        <div class="step33">
                        <h5><strong><!-- CB Resources --></strong></h5>
                        <hr>
                            <div class="row mar_ned">
                            <div class="col-md-4">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $responsable,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'responsable')->textInput(['maxlength' => true]) ?> 
                            </div> 
                            <div class="col-md-4">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $stakeholders,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'stakeholders[]')            
                            ->dropDownList($model->StakeholdersListDropdown,
                            [
                            'multiple'=>'multiple'
                            //'class'=>'chosen-select input-md required',              
                            ]             
                            )->label("Stakeholders"); 
                            ?>  
                            </div> 
                            <div class="col-md-4"> 
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $funders,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            

                            <?= $form->field($model, 'funders[]')            
                            ->dropDownList($model->FundersListDropdown,
                            [
                            'multiple'=>'multiple'
                            //'class'=>'chosen-select input-md required',              
                            ]             
                            )->label("Funders"); 
                            ?>  


                            </div>                  
                            </div>
                            <div class="row mar_ned">
                            <div class="col-md-4">
                            <!--Button allowing the display of Definition -->
                            <?=
                            // right
                             PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $budget,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?= $form->field($model, 'cost')->textInput(['placeholder'=>'Rwf','maxlength' => true]) ?>
                            </div> 
                            <div class="col-md-4">

                            <!--Button allowing the display of Definition -->
                            <?php
                            if($model->hiddingUploadFile() == 0 ){
                            // right
                            echo PopoverX::widget([
                                'header' => 'Definition',
                                'placement' => PopoverX::ALIGN_RIGHT,
                                'content' => $actionplan,
                                'footer' => '',
                                'toggleButton' => ['label'=>'<span class="label label-info">i</span>'],
                            ]);
                            ?>
                            <!--End Button allowing the display of Definition -->
                            <?php
                            

                            echo $form->field($model, 'file')->fileInput() ;
                            }
                            ?>  
                            </div> 
                            <div class="col-md-4">
                            <!-- <?= $form->field($model, 'cost')->textInput(['maxlength' => true]) ?> -->
                            </div> 
                            <div class="col-md-4 col-xs-3">
                                    <!-- <p align="right"><stong>Date of birth</stong></p> -->
                            </div>
                            <div class="col-md-8 col-xs-9">
                            <div class="row">
                                 
                            </div>
                            <div class="row mar_ned">
                   
                            </div>
                            <div class="row mar_ned">
                               
                            </div>
                            <div class="row mar_ned">
                                
                            </div>
                            <div class="row mar_ned">
                              
                            </div>
                            <div class="row mar_ned">
                            <div class="col-md-4 col-xs-3">
                                <!-- <p align="right"><stong>Total Experience</stong></p> -->
                            </div>
                            <div class="col-md-8 col-xs-9">
                            <div class="row">
                                
                            </div>
                            </div>
                            </div>
                            <div class="row mar_ned">
                               
                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                            <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">
                        <div class="step44">
                    <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg col-md-4' : 'btn btn-primary']) ?>
                    </div>
                    <?php } ?>
                            
                          
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>
  
  

    <?php ActiveForm::end(); ?>
    
</div>

<script type="text/javascript">
    $(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {       
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');
    
    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});
    
    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');
    
    // The Accordion Effect
    $('.accordion-header').click(function () {
        if($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
        
        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });
    
    return false;
});
</script>

<!-- CSS -->

<style>
.length{width: 274px;}


/*added CSS codes*/

.wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #fff;
    border: 2px solid #5bc0de;
    
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 50px;
}

.wizard h3 {
    margin-top: 0;
}
.step1 .row {
    margin-bottom:10px;
      padding-right:10px;
        padding-left:10px;
}
.step_21 {
    border :1px solid #eee;
    border-radius:5px;
    padding:10px;
}
.step33 {
    border:1px solid #ccc;
    border-radius:5px;
    padding-left:10px;
    margin-bottom:10px;
    padding-right:10px;
}
.step44 {
   margin : -33px 0px 0px 242px;
}
.dropselectsec {
    width: 68%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
}
.dropselectsec1 {
    width: 74%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
}
.mar_ned {
    margin-bottom:10px;
}
.wdth {
    width:25%;
}
.birthdrop {
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    width: 16%;
    outline: 0;
    font-weight: normal;
}


/* according menu */
#accordion-container {
    font-size:13px
}
.accordion-header {
    font-size:13px;
    background:#ebebeb;
    margin:5px 0 0;
    padding:7px 20px;
    cursor:pointer;
    color:#fff;
    font-weight:400;
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border-radius:5px
}
.unselect_img{
    width:18px;
    -webkit-user-select: none;  
    -moz-user-select: none;     
    -ms-user-select: none;      
    user-select: none; 
}
.active-header {
    -moz-border-radius:5px 5px 0 0;
    -webkit-border-radius:5px 5px 0 0;
    border-radius:5px 5px 0 0;
    background:#F53B27;
}
.active-header:after {
    content:"\f068";
    font-family:'FontAwesome';
    float:right;
    margin:5px;
    font-weight:400
}
.inactive-header {
    background:#333;
}
.inactive-header:after {
    content:"\f067";
    font-family:'FontAwesome';
    float:right;
    margin:4px 5px;
    font-weight:400
}
.accordion-content {
    display:none;
    padding:20px;
    background:#fff;
    border:1px solid #ccc;
    border-top:0;
    -moz-border-radius:0 0 5px 5px;
    -webkit-border-radius:0 0 5px 5px;
    border-radius:0 0 5px 5px
}
.accordion-content a{
    text-decoration:none;
    color:#333;
}
.accordion-content td{
    border-bottom:1px solid #dcdcdc;
}



@media( max-width : 585px ) {

    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}
</style>
