<?php
use yii\helpers\Url;

return [

    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '10px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idplan',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstit',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idSubProgram',        
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'output',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'challenge',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'baseline',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idSubLevel',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'nbrbeneficiaries',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'actionDescription',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idCbType',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'generalNeeds',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'specificNeeds',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idIndicator',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'quarter1',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'quarter2',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'quarter3',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'quarter4',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'responsable',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'stakeholders',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'cost',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'funders',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'status',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   