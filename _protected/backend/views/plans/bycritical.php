<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\PlansSearch $searchModel
 */

$this->title = Yii::t('app', 'Total budget by critical or none critical skills report');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plans-index">
    <div class="page-header">
            <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Plans',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'idplan',
            // 'idInstitSector',
            // 'value'=>'idInstit0.institName',
            // 'value'=>'idInstit0.idInstitSector0.sectName',
            [
                'attribute'=>'idSkills',
                'value'=>'idSkills0.skills',
            ],
            [
                'attribute'=>'idLevel',
                'value'=>'idSubLevel0.idLevel0.level', 
            ],
            [
                'attribute'=>'idSubLevel',
                'value'=>'idSubLevel0.subLevel',
            ],
            // [
            //     'attribute'=>'idTraining',
            //     'value'=>'idTraining0.courseName',
            // ],
            'cost', 

        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,

        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class=""></i>', ['create'], ['class' => '']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['bycritical'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
