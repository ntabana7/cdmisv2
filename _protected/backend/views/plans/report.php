<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\PlansSearch $searchModel
 */

$this->title = Yii::t('app', 'Total planned budget by sector report');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plans-index">
    <div class="page-header">
            <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Plans',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'panel'=>['type'=>'primary', 'heading'=>'Grid Grouping Example'],
        'columns' => [
             //['class' => 'yii\grid\SerialColumn'],

            // 'idplan',
        
            [
                'attribute'=>'idInstitSector',
                // 'value'=>'idInstit0.idInstitSector0.sectName',
                'group'=>true,  // enable grouping
                'groupedRow'=>true,                    // move grouped column to a single grouped row
                'groupOddCssClass'=>'kv-grouped-row',  // configure odd group cell css class
                'groupEvenCssClass'=>'kv-grouped-row', // configure even group cell css class
                'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns'=>[[0,0]], // columns to merge in summary
                        'content'=>[             // content to show in each summary cell
                            1=>'total('.$model->idInstit0->idInstitSector0->sectName.')',
                            3=>GridView::F_SUM,
                            4=>GridView::F_SUM,
                            //6=>GridView::F_SUM,
                        ],
                        'contentFormats'=>[      // content reformatting for each summary cell
                            3=>['format'=>'number', 'decimals'=>0],
                            4=>['format'=>'number', 'decimals'=>0],
                            //6=>['format'=>'number', 'decimals'=>2],
                        ],
                        'contentOptions'=>[      // content html attributes for each summary cell
                            1=>['style'=>'font-variant:small-caps'],
                            3=>['style'=>'text-align:right'],
                            4=>['style'=>'text-align:right'],
                            //6=>['style'=>'text-align:right'],
                        ],
                        // html attributes for group summary row
                        'options'=>['class'=>'danger','style'=>'font-weight:bold;']
                    ];
                }
            ],            
            
            [
                'attribute'=>'idInstit',
                'value'=> 'idInstit0.institName',
            ],  
            [
                'attribute'=>'idSubLevel',
                'value'=>'idSubLevel0.subLevel',
    
                //'group'=>true
            ],
           
            [
            'attribute'=>'nbrbeneficiaries',
            'pageSummary'=>true,
            'hAlign'=> 'right'
            ],
            [
            'attribute'=>'cost',
            'pageSummary'=>true,
            'hAlign'=> 'right'
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class=""></i>', ['create'], ['class' => '']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['report'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
