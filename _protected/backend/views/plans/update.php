<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Plans */

$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = $this->title;
if(Yii::$app->user->can('cluster')){ 
	$frm = '_formsector';
}elseif (Yii::$app->user->can('institution')){
	$frm = '_form';
}else{
	$frm = '_formfunder';
}
?>

<div class="plans-create">
   <div class="page-header">
            <h1 style="color:;"><?= 'Hi '.Html::encode(Yii::$app->user->identity->username).'' ?></h1>
    </div>
    <?= $this->render($frm, [
        'model' => $model,
    ]) ?>
</div>
