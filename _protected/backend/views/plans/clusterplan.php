<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\models\PlansSearch $searchModel
 */

$this->title = Yii::t('app', 'Plans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plans-index">
    <div class="page-header">

<!-- For exporting whatever you want in excel sheet about the CB plan   -->
<?php
    $gridColumns = [
    // ['class' => 'yii\grid\SerialColumn'],

            // 'idplan',
            
            [
                'attribute'=>'idInstitSector',
                'value'=>'idInstitSector0.sectName',
            ],
            [
                'attribute'=>'idInstit',
                'value'=>'idInstit0.institName',
            ],
            [
                'attribute'=>'idSubProgram',
                'value'=>'idSubProgram0.subProgramName',
            ],
            
            'output',
            'challenge',
            'baseline', 
        
            [
                'attribute'=>'idSubLevel',
                'value'=>'idSubLevel0.subLevel',
            ],
           
           'nbrbeneficiaries', 
           'actionDescription', 
            [
                'attribute'=>'idCbType',
                'value'=>'idCbType0.cbType',
            ],             
//            'generalNeeds', 
//            'specificNeeds', 
            [
                'attribute'=>'idIndicator',
                'value'=>'idIndicator0.indicator',
            ],
            // [
            //     'attribute'=>'quarterMale1',
            //     'value'=>'quarterMale1',
            // ],
            'quarterMale1',           
            'quarterFemale1',
            'quarterMale2',           
            'quarterFemale2',
            'quarterMale3',           
            'quarterFemale3',
            'quarterMale4',           
            'quarterFemale4',
            'nbrbeneficiaries',            
            'responsable', 
            'stakeholders', 
            'cost', 
            'funders', 
//          'status', 
];

// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);
?>
<!--End For exporting whatever you want in excel sheet about the CB plan   -->
            <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Plans',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>




    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
          'rowOptions'=>function($model){
            if($model->status == 2){
                return ['class' => 'success'];
            }
            elseif($model->status == 3){
                return ['class' => 'danger'];
            }elseif($model->status == 1){
                return ['class' => 'warning'];
            }
        },
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'idplan',
            
            [
                'attribute'=>'idInstitSector',
                'value'=>'idInstitSector0.sectName',
            ],
            [
                'attribute'=>'idInstit',
                'value'=>'idInstit0.institName',
            ],
            [
                'attribute'=>'idSubProgram',
                'value'=>'idSubProgram0.subProgramName',
            ],
            
            // 'output',
            // 'challenge',
//          'baseline', 
        
            [
                'attribute'=>'idSubLevel',
                'value'=>'idSubLevel0.subLevel',
            ],
           
//            'nbrbeneficiaries', 
//            'actionDescription', 
            // [
            //     'attribute'=>'idCbType',
            //     'value'=>'idCbType0.cbType',
            // ],  
//            'generalNeeds', 
//            'specificNeeds', 
            // [
            //     'attribute'=>'idIndicator',
            //     'value'=>'idIndicator0.indicator',
            // ],
            // [
            //     'attribute'=>'quarterMale1',
            //     'value'=>'quarterMale1',
            // ],
            // 'quarterMale1',           
            // 'quarterFemale1',
            // 'quarterMale2',           
            // 'quarterFemale2',
            // 'quarterMale3',           
            // 'quarterFemale3',
            // 'quarterMale4',           
            // 'quarterFemale4',
            // 'nbrbeneficiaries',
            // 'quarters3', 
            // 'quarters4', 
//            'responsable', 
//            'stakeholders', 
            [
            'attribute'=>'cost',
            'format'=>['decimal',0] ,
            ],
            [
            'attribute'=>'grantedAmount',
            'format'=>['decimal',0] ,
            ],   
//            'funders', 
           'statuslabel', 
            [
            'attribute'=>'datasource', 
            'width'=>'250px',
            'value'=>function ($model, $key, $index, $widget) { 
                return $model->datasource;
            },
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>[0=>'From Institution',1=>'From DP',2=>'From Sector'], 
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'filterInputOptions'=>['placeholder'=>'----']
            ],
            [
            'attribute'=>'submittedon', 
            'width'=>'250px',
            'value'=>function ($model, $key, $index, $widget) { 
                return $model->submittedon;
            },
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>[2017=>'2017-2018',2018=>'2018-2019'], 
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'filterInputOptions'=>['placeholder'=>'----']
            ],




            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view} {update} ',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['plans/decision','id' => $model->idplan]), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',            
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['clusterplan'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
