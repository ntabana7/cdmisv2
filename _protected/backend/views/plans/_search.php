<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\models\PlansSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="plans-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idplan') ?>

    <?= $form->field($model, 'idInstit') ?>

    <?= $form->field($model, 'idSubProgram') ?>

    <?= $form->field($model, 'output') ?>

    <?= $form->field($model, 'challenge') ?>

    <?php // echo $form->field($model, 'baseline') ?>

    <?php // echo $form->field($model, 'idSubLevel') ?>

    <?php // echo $form->field($model, 'nbrbeneficiaries') ?>

    <?php // echo $form->field($model, 'actionDescription') ?>

    <?php // echo $form->field($model, 'idCbType') ?>

    <?php // echo $form->field($model, 'generalNeeds') ?>

    <?php // echo $form->field($model, 'specificNeeds') ?>

    <?php // echo $form->field($model, 'idIndicator') ?>

    <?php // echo $form->field($model, 'quarters') ?>

    <?php // echo $form->field($model, 'responsable') ?>

    <?php // echo $form->field($model, 'stakeholders') ?>

    <?php // echo $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'funders') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
