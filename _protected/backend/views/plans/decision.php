<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Plans */
?>
<div class="plans-update">

    <?= $this->render('_decisionform', [
        'model' => $model,
    ]) ?>

</div>