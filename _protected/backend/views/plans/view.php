<?php

use yii\widgets\DetailView;
use backend\models\Subprograms;
use backend\models\Programs;
/* @var $this yii\web\View */
/* @var $model backend\models\Plans */
?>
<div class="plans-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'idplan',
            'idInstit0.institName',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idProgram',
            // 'label'=>'Program',
            'value'=>$model->idSubProgram0->idProgram0->programName,            
            ],
            'idSubProgram0.subProgramName',
            'output',
            'challenge',
            'baseline',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idSubLevel',
            'label'=>'CD approach',
            'value'=>$model->idSubLevel0->subLevel,            
            ],
            'nbrbeneficiaries',
            'actionDescription',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idCbType',
            'label'=>'CD type',
            'value'=>$model->idCbType0->cbType,            
            ],
            // 'idCbType0.cbType',
            // 'generalNeeds',
            // 'specificNeeds',
            'idIndicator0.indicator',
            'quarterMale1',           
            'quarterFemale1',
            'quarterMale2',           
            'quarterFemale2',
            'quarterMale3',           
            'quarterFemale3',
            'quarterMale4',           
            'quarterFemale4',
            'responsable',
            'stakeholders',
            [
            'attribute'=>'cost',
            'format'=>['decimal',0] ,
            ],
            [
            'attribute'=>'grantedAmount',
            'format'=>['decimal',0] ,
            ],         
            'funders',            
            'statuslabel',
            'reasonnotapproved',
        ],
    ]) ?>

</div>
