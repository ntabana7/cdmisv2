<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Reasonnotapproved;

/* @var $this yii\web\View */
/* @var $model backend\models\Clusters */
/* @var $form yii\widgets\ActiveForm */
$arrayDecision = [
        [ 'id'=> 0, 'value' => 'Pending'],
        [ 'id'=> 1, 'value' => 'In process'],
        [ 'id'=> 2, 'value' => 'Approved'],
        [ 'id'=> 3, 'value' => 'Not approved']
];
?>

<div class="clusters-form">

    <?php $form = ActiveForm::begin(); ?>

  
    <?= $form->field($model, 'status')->widget(select2::classname(),[
                                                    'data'=>ArrayHelper::map($arrayDecision,'id','value'),
                                                    'theme' => Select2::THEME_KRAJEE, 
                                                    'options'=>[
                                                        'placeholder'=>'Select CB level',
                                                        'onchange'=>'

                                                         // To get dropdownlist for grantedAmount

                                                           var id = document.getElementById("plans-status").value;

                                                           if(id == 2){
                                                            $("#grantedAmount").show();
                                                            $("#reasonnotapproved").hide();

                                                           }
                                                           else if(id == 3){
                                                            $("#reasonnotapproved").show();
                                                            $("#grantedAmount").hide();
                                                            document.getElementById("plans-grantedamount").value = 0;

                                                           }
                                                           else{
                                                            $("#grantedAmount").hide();
                                                            $("#reasonnotapproved").hide();
                                                            
                                                            document.getElementById("plans-grantedamount").value = 0;
                                                            
                                                           }'
                                                    ],
                                                    'language' => 'en',
                                                    'pluginOptions'=>['alloweClear'=>true],
                                                    ]);

                               
                            ?>

    <?= $form->field($model, 'cost')->textInput(['maxlength' => true,'readonly'=>true]) ?>
    <div id='grantedAmount' style='display:none'>
    <?= $form->field($model, 'grantedAmount')->textInput(['maxlength' => true]) ?>
    </div>
    <div id='reasonnotapproved' style='display:none'>
    <?= $form->field($model, 'reasonnotapproved')->widget(select2::classname(),[
                                                    'data'=>ArrayHelper::map(Reasonnotapproved::find()->all(),'reason','reason'),
                                                    'theme' => Select2::THEME_KRAJEE, 
                                                    'options'=>[
                                                        'placeholder'=>'Select reason',
                                                        'onchange'=>''
                                                        
                                                    ],
                                                    'language' => 'en',
                                                    'pluginOptions'=>['alloweClear'=>true],
                                                    ]);

                               
                            ?>
    </div>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
