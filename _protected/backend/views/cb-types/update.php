<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CbTypes */
?>
<div class="cb-types-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
