<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CbTypes */
?>
<div class="cb-types-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCbType',
            'cbType',
        ],
    ]) ?>

</div>
