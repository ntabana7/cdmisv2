<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Quarters */
?>
<div class="quarters-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idquarter',
            'quarter',
            'period',
        ],
    ]) ?>

</div>
