<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Quarters */

?>
<div class="quarters-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
