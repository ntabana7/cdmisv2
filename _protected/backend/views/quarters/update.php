<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Quarters */
?>
<div class="quarters-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
