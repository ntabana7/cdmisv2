<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CbLevels */
?>
<div class="cb-levels-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLevel',
            'level',
        ],
    ]) ?>

</div>
