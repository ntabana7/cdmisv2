<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CbLevels */
?>
<div class="cb-levels-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
