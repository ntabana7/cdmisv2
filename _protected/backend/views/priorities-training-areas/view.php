<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PrioritiesTrainingAreas */
?>
<div class="priorities-training-areas-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTraining',
            'idSkills',
            'idInstitSector',
            'idCbType',
            'priorityName',
        ],
    ]) ?>

</div>
