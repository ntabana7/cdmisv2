<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\InstitSectors;
use backend\models\Skills;
use backend\models\CbTypes;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\PrioritiesTrainingAreas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="priorities-training-areas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idInstitSector')->dropDownList(ArrayHelper::map(InstitSectors::find()->all(),'idInstitSector','sectName'),[ 'prompt'=>'Select Sector',
                            'language' => 'en',
                            ]);

    ?>

    <!-- <?= $form->field($model, 'idSkills')->textInput() ?>  -->
    <?= $form->field($model, 'idSkills')->dropDownList(ArrayHelper::map(Skills::find()->all(),'idSkills','skills'),[ 'prompt'=>'Select Skills',
                            'language' => 'en',
                            ]);

    ?>   

    <!-- <?= $form->field($model, 'idCbType')->textInput() ?> -->
    <?= $form->field($model, 'idCbType')->dropDownList(ArrayHelper::map(CbTypes::find()->all(),'idCbType','cbType'),[ 'prompt'=>'Select CB Type',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'priorityName')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
