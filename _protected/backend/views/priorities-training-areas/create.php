<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PrioritiesTrainingAreas */

?>
<div class="priorities-training-areas-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
