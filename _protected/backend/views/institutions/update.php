<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Institutions */
?>
<div class="institutions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
