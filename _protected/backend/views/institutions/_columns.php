<?php
use yii\helpers\Url;
use yii\helpers\Html;
if(Yii::$app->user->can('admin')){    
 $_column =   [
        // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idInstit',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstitSector',
        'value'=>'idInstitSector0.sectName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'institName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstitType',
        'value'=>'idInstitType0.institType',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idDistrict',
        'value'=>'idDistrict0.distName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'focalpointName',
    ],
    [
        'label'=>Yii::t('app','Create').' '.Yii::t('app','User'),
        'format' => 'raw',
        
        'value'=>function ($data) {
            return Html::a(Yii::t('app','Create login'),['user/create','iduser'=>$data["idInstit"],'email'=>$data["email"]]);
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'FocalPersonalPosition',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'phone',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'email',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idCluster',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   
}else if(Yii::$app->user->can('cluster')){
     $_column =   [
        // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idInstit',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstitSector',
        'value'=>'idInstitSector0.sectName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'institName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstitType',
        'value'=>'idInstitType0.institType',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idDistrict',
        'value'=>'idDistrict0.distName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'focalpointName',
    ],
    [
        'label'=>Yii::t('app','Create').' '.Yii::t('app','User'),
        'format' => 'raw',
        
        'value'=>function ($data) {
            // return Html::a(Yii::t('app','Create login'),['user/create','iduser'=>$data["idInstit"]]);
            return Html::a(Yii::t('app','Create login'),['user/create','iduser'=>$data["idInstit"],'email'=>$data["email"]]);
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'FocalPersonalPosition',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'phone',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'email',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idCluster',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view} {update}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        // 'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
        //                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        //                   'data-request-method'=>'post',
        //                   'data-toggle'=>'tooltip',
        //                   'data-confirm-title'=>'Are you sure?',
        //                   'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];


}else{

     $_column =   [
        // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idInstit',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstitSector',
        'value'=>'idInstitSector0.sectName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'institName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstitType',
        'value'=>'idInstitType0.institType',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idDistrict',
        'value'=>'idDistrict0.distName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'focalpointName',
    ],
    [
        'label'=>Yii::t('app','Create').' '.Yii::t('app','User'),
        'format' => 'raw',
        
        'value'=>function ($data) {
            // return Html::a(Yii::t('app','Create login'),['user/create','iduser'=>$data["idInstit"]]);
            return Html::a(Yii::t('app','Create login'),['user/create','iduser'=>$data["idInstit"],'email'=>$data["email"]]);
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'FocalPersonalPosition',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'phone',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'email',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idCluster',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        // 'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        // 'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
        //                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        //                   'data-request-method'=>'post',
        //                   'data-toggle'=>'tooltip',
        //                   'data-confirm-title'=>'Are you sure?',
        //                   'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];
}

return $_column;







