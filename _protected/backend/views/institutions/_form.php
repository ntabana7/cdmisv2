<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\InstitSectors;
use backend\models\Districts;
use backend\models\Clusters;
use backend\models\InstitTypes;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Institutions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institutions-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'idInstitSector')->textInput() ?> -->
    <?= $form->field($model, 'idInstitSector')->dropDownList(ArrayHelper::map(InstitSectors::find()->all(),'idInstitSector','sectName'),[ 'prompt'=>'Select Sector',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'institName')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'idInstitType')->textInput() ?> -->
    <?= $form->field($model, 'idInstitType')->dropDownList(ArrayHelper::map(InstitTypes::find()->all(),'idInstitType','institType'),[ 'prompt'=>'Select institution type',
                            'language' => 'en',
                            ]);

    ?>
    


    <!-- <?= $form->field($model, 'idDistrict')->textInput() ?> -->
    <?= $form->field($model, 'idDistrict')->dropDownList(ArrayHelper::map(Districts::find()->all(),'idDistrict','distName'),[ 'prompt'=>'Select District',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'focalpointName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'FocalPersonalPosition')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'idCluster')->textInput() ?> -->
    <?= $form->field($model, 'idCluster')->dropDownList(ArrayHelper::map(Clusters::find()->all(),'idCluster','clusterName'),[ 'prompt'=>'Select Cluster',
                            'language' => 'en',
                            ]);

    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
