<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Institutions */
?>
<div class="institutions-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idInstit',
            'idInstitSector',
            'institName',
            'idInstitType',
            'idDistrict',
            'focalpointName',
            'FocalPersonalPosition',
            'phone',
            'email:email',
            'idCluster',
        ],
    ]) ?>

</div>
