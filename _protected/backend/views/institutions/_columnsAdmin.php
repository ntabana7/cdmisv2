<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [

    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'institName',
    ],

    [
        'class'=>'\common\grid\ProgressColumn',
        'attribute' => 'instititional',
        'label'=>Yii::t('app','Institution'),
        'format' => 'raw',
        'hAlign' => 'center',        
    ],

    [
        'class'=>'\common\grid\ProgressColumn',
        'attribute' => 'organisational',
        'label'=>Yii::t('app','Organisational'),
        'format' => 'raw',
        'hAlign' => 'center',       
    ],

    [
        'class'=>'\common\grid\ProgressColumn',
        'attribute' => 'individual',
        'label'=>Yii::t('app','Individual'),
        'format' => 'raw',
        'hAlign' => 'center',      
    ],

    [
        'label'=>Yii::t('app','View Details'),
        'format' => 'raw',
        'vAlign' => 'middle',
        'hAlign' => 'center',
        'width' => '30px',
        'value'=>function ($data) {
            return Html::a(Yii::t('app','<i class="fa fa-eye" aria-hidden="true"></i>'),['/cna/assessment/institutional','idInstitution'=>$data["idInstit"]]);
        },
    ],



];   
