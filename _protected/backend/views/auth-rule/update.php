<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AuthRule */
?>
<div class="auth-rule-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
