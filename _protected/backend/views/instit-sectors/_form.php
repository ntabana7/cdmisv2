<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\PrioritySectors;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\InstitSectors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="instit-sectors-form">
    <?php $form = ActiveForm::begin(); ?>
    <!-- <?= $form->field($model, 'idPriority')->textInput() ?> --> 
    <?= $form->field($model, 'idPriority')->dropDownList(ArrayHelper::map(PrioritySectors::find()->all(),'idPriority','priorityName'),[ 'prompt'=>'Select a Sub Level',
                            'language' => 'en',
                         	]);

  	?>

    <?= $form->field($model, 'sectName')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
