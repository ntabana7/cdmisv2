<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\InstitSectors */
?>
<div class="instit-sectors-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idInstitSector',
            'idPriority',
            'sectName',
        ],
    ]) ?>

</div>
