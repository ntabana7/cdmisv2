<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\InstitSectors */
?>
<div class="instit-sectors-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
