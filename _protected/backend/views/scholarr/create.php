<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Scholarr */

?>
<div class="scholarr-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelseducationdetail'=>$modelseducationdetail,
    ]) ?>
</div>
