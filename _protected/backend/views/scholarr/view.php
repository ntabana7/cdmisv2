<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Scholarr */
?>
<div class="scholarr-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'passport_id',
            'familyname',
            'firstname',
            'fathername',
            'mothername',
            'gender',
            'birthdate',
            'emailaddress:email',
            'telephone',
            'emproymentstatus',
            'position',
            'instutition',
            'location',
        ],
    ]) ?>

</div>
