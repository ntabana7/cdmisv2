<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\User */

?>
<div class="user-create">
    <?= $this->render('_form', [
            'user' => $user,
            'role' => $role,
        ]) ?>
</div>
