<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Fiscalyear */
?>
<div class="fiscalyear-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idFiscalyear',
            'startdate',
            'enddate',
        ],
    ]) ?>

</div>
