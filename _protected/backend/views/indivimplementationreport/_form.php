<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Indivimplementationreport */
/* @var $form yii\widgets\ActiveForm */
?>
<h4>Report on <?= $dataFromPlan->idTraining0->courseName;?> training </h4>
<hr>
<div class="indivimplementationreport-form">

    <?php $form = ActiveForm::begin(); ?>
<div class='row'>
    <div class="col-md-6">
    <?= $form->field($model, 'idplan')->textInput(['value'=>$dataFromPlan->idplan,'readonly' => true])->label() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->dropDownList($model->genderArray) ?>

     
    
</div>
<div class="col-md-6">

    <?= $form->field($model, 'periodoftraining')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reportedquarter')->dropDownList($model->quarterArray) ?>

    <?= $form->field($model, 'amountspent')->textInput() ?>

    <?= $form->field($model, 'placeoftraining')->textInput(['maxlength' => true]) ?>
</div>
 
</div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
