<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Indivimplementationreport */
?>
<div class="indivimplementationreport-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dataFromPlan' => $dataFromPlan
    ]) ?>

</div>
