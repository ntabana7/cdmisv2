<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Indivimplementationreport */

?>
<div class="indivimplementationreport-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dataFromPlan'=>$dataFromPlan, //getting array containing data from Plan to the create page from controller
    ]) ?>
</div>
