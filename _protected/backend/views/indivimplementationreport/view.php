<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Indivimplementationreport */
?>
<div class="indivimplementationreport-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idIndivreport',
            'idplan',
            'name',
            'position',
            'gender',
            'periodoftraining',
            'reportedquarter',
            'amountspent',
            'placeoftraining',
            'createdon',
            'iduser0.username',
        ],
    ]) ?>

</div>
