<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Subprograms */
?>
<div class="subprograms-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idSubProgram',
            'idProgram',
            'subProgramName',
        ],
    ]) ?>

</div>
