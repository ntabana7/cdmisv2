<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingAreas */
?>
<div class="training-areas-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
