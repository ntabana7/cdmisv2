<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Districts */
?>
<div class="districts-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idDistrict',
            'idProvince',
            'distName',
        ],
    ]) ?>

</div>
