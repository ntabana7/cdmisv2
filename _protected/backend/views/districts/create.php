<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Districts */

?>
<div class="districts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
