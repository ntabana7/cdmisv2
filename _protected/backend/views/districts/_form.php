<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Provinces;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Districts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="districts-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'idProvince')->textInput() ?> -->
    <?= $form->field($model, 'idProvince')->dropDownList(ArrayHelper::map(Provinces::find()->all(),'idProvince','provName'),[ 'prompt'=>'Select Province',
                            'language' => 'en',
                         	]);

  	?>

    <?= $form->field($model, 'distName')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
