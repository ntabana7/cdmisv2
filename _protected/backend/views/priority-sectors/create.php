<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PrioritySectors */

?>
<div class="priority-sectors-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
