<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Orginstimplementationreport */
?>
<div class="orginstimplementationreport-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idorginst',
            'idplan',
            'reportedquarter',
            'amountspent',
            'createdon',
            'iduser0.username',
        ],
    ]) ?>

</div>
