<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Orginstimplementationreport */
?>
<div class="orginstimplementationreport-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dataFromPlan' => $dataFromPlan
    ]) ?>

</div>
