<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Orginstimplementationreport */

?>
<div class="orginstimplementationreport-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dataFromPlan'=>$dataFromPlan, //getting array containing data from Plan to the create page from controller
    ]) ?>
</div>
