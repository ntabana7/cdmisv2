<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CbSublevels;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\CbIndicators */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cb-indicators-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idSubLevel')->dropDownList(ArrayHelper::map(CbSublevels::find()->all(),'idSubLevel','subLevel'),[ 'prompt'=>'Select a Sub Level',
                            'language' => 'en',
                         	]);

  	?>

  	<?php /*$form->field($model, 'idSubLevel')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(CbSublevels::find()->all(),'idSubLevel','subLevel'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select a Sub Level',
                                'onchange'=>'
                                    $.post("'.Url::to(['farmers/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                         $("select#collectionproducts-farmer" ).html(data);
                                    });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       */
	?>

    <?= $form->field($model, 'indicator')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
