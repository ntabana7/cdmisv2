<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CbIndicators */
?>
<div class="cb-indicators-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idIndicator',
            'idSubLevel',
            'indicator',
        ],
    ]) ?>

</div>
