<?php
use yii\helpers\Url;
if(Yii::$app->user->can('admin')){
 $_column =   [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
       // To hide the colunm I don't need to see on the form

       // [
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'idIndicator',
    //],
    [
        'class'=>'\kartik\grid\DataColumn',
        //get fieldname from another table
        'attribute' => 'idSubLevel',
        'value'=>'idSubLevel0.subLevel',
    ],
    [

    // if you want to edit directly certain content use the key word "Editable"
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'indicator',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   
}else if(Yii::$app->user->can('cluster')){

$_column = [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
       // To hide the colunm I don't need to see on the form

       // [
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'idIndicator',
    //],
    [
        'class'=>'\kartik\grid\DataColumn',
        //get fieldname from another table
        'attribute' => 'idSubLevel',
        'value'=>'idSubLevel0.subLevel',
    ],
    [

    // if you want to edit directly certain content use the key word "Editable"
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'indicator',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view} {update}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
   
    ],

];   
}else{
    $_column = [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
       // To hide the colunm I don't need to see on the form

       // [
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'idIndicator',
    //],
    [
        'class'=>'\kartik\grid\DataColumn',
        //get fieldname from another table
        'attribute' => 'idSubLevel',
        'value'=>'idSubLevel0.subLevel',
    ],
    [

    // if you want to edit directly certain content use the key word "Editable"
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'indicator',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        // 'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
   
    ],

]; 

}

return $_column;