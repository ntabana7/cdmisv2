<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CbIndicators */

?>
<div class="cb-indicators-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
