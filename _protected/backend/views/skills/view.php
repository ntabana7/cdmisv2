<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Skills */
?>
<div class="skills-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idSkills',
            'skills',
        ],
    ]) ?>

</div>
