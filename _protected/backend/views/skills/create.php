<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Skills */

?>
<div class="skills-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
