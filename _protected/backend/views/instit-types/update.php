<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\InstitTypes */
?>
<div class="instit-types-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
