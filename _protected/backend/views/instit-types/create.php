<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\InstitTypes */

?>
<div class="instit-types-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
