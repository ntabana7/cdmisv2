<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\InstitTypes */
?>
<div class="instit-types-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idInstitType',
            'institType',
        ],
    ]) ?>

</div>
