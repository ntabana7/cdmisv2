<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Stakeholders */
?>
<div class="stakeholders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idStakeholder',
            'StakeholderName',
        ],
    ]) ?>

</div>
