<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Stakeholders */

?>
<div class="stakeholders-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
