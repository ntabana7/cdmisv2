<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Reasonnotapproved */
?>
<div class="reasonnotapproved-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idreason',
            'reason',
        ],
    ]) ?>

</div>
