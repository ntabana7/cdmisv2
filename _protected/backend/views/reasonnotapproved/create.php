<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Reasonnotapproved */

?>
<div class="reasonnotapproved-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
