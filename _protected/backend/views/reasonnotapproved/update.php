<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Reasonnotapproved */
?>
<div class="reasonnotapproved-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
