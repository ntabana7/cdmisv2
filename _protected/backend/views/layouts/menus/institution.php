 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //  ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                   
                    [
                        'label' => 'CNA Module Menu',
                        'icon' => 'folder-open',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Start', 'icon' => 'play', 'url' => ['/cna/assessment/instruction'],],
                            ['label' => 'Institutional', 'icon' => 'list-ol', 'url' => ['/cna/assessment/institutional'],],
                            ['label' => 'organisational', 'icon' => 'list-ul', 'url' => ['/cna/assessment/organisational-assessment'],],
                            ['label' => 'Individual', 'icon' => 'list', 'url' => ['/cna/assessment/individual-assessment'],],
                        ], 
                    ],
                    
                    [
                        'label' => 'CD Plan Module Menu',
                        'icon' => 'folder-open-o',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Individual', 'icon' => 'list-alt', 'url' => ['/cna/assessment/plan-individual'],], 
                            ['label' => 'organisational', 'icon' => 'list-alt', 'url' => ['/cna/assessment/plan-organisational'],], 
                            ['label' => 'Institutional', 'icon' => 'list-alt', 'url' => ['/cna/assessment/plan-institutional'],], 
                            ['label' => 'CB Plans', 'icon' => 'fa fa-circle-o', 'url' => ['/plans'],],
                            ['label' => 'Report the implemenation plan', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/approvedplan'],],
                            [

                            //end 
                            ],                                

                        // end items RBAc
                        ],
                    
                    // end second after items1
                    ],

                    [
                        'label' => 'Internship Module Menu',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Interns request', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequest'],],
                            ['label' => 'Request Details', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequestdetails'],],
                            ['label' => 'Given interns', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/deployedininstitution'],],  
                        ], 
                    ],

                    [
                        'label' => 'Reports',
                        'icon' => 'bars',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Total planned budget by sector', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report'],],
                            ['label' => 'Total planned budget by CB level', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report2'],],
                            ['label' => 'Planned budget by critical or non critical', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/bycritical'],],
                            [

                            //end 
                            ],                                

                        // end items RBAc
                        ],
                    
                    // end second after items1
                    ],

                
                // end items1
                ],
           // end first
            ]
        ) ?>



