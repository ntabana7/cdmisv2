 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    // ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    

                            // end items RBAC                 

            
                    [
                        'label' => 'Internship Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Dashboard', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/dashboardinterns'],],
                            ['label' => 'Apprenticeship', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/'],],
                            ['label' => 'Intern applications', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/internshipofficerindex'],],
                            ['label' => 'Waiting applicants', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/validatedinterns'],],
                            ['label' => 'Deployed interns', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/deployedinterns'],],
                            ['label' => 'Unavailable interns', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/unavalableinterns'],],
                            ['label' => 'Interns request', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequest'],],
                            ['label' => 'Request Details (Ongoing)', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequestdetails/internshipofficerindex'],], 
                            ['label' => 'Request Details (Done)', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequestdetails/internshipofficerdenodeployment'],],                     
                            ['label' => 'Banks', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-bank'],],
                            ['label' => 'Universities', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-universities'],],
                            
                           
                        ], 
                    ],
                ],
           // end first
            ]
        ) ?>

