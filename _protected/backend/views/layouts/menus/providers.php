 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //  ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'CD Providers Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Beneficiaries', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-beneficiaries'],],
                            ['label' => 'Beneficiaries enrollment', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-beneficiariesenrollment'],],
                            ['label' => 'Provider Infrastructure', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-infrastructure'],],
                            ['label' => 'Providers services', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-services'],],
                            ['label' => 'Trainers', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-trainer'],],
                            ['label' => 'Training areas by Provider', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-trainingareasbyprovider'],],
                            ['label' => 'Trainer by course', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-whotrainwhat'],],
                            
                           
                        ], 
                    
                    // // end second after items1
                    ],


                
                // end items1
                ],
           // end first
            ]
        ) ?>



