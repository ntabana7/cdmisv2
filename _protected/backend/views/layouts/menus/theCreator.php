 <?php 
 use backend\models\SystemLog;
 $logs = (SystemLog::find()->count() > 0) ? ' ('.SystemLog::find()->count().')' : '';
 ?>
 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'gears', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'bug', 'url' => ['/debug']],
                    ['label' => Yii::t('app','Cache'), 'icon' => 'fa fa-angle-double-right', 'url' => ['/cache/index'],],
                    ['label' => 'Fiscal Years', 'icon' => 'calendar-plus-o', 'url' => ['/fiscal-years']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'RBAC',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Auth Assignment', 'icon' => 'circle-o', 'url' => ['/auth-assignment'],],
                            ['label' => 'Auth Item', 'icon' => 'circle-o', 'url' => ['/auth-item'],],
                            ['label' => 'Auth Item Child', 'icon' => 'circle-o', 'url' => ['/auth-item-child'],],
                            ['label' => 'Auth Rules', 'icon' => 'circle-o', 'url' => ['/auth-rule'],],
                            ['label' => 'users', 'icon' => 'circle-o', 'url' => ['/user'],],
                            // [

                            //     //'label' => 'Level One',
                            //     //'icon' => 'fa fa-circle-o',
                            //     //'url' => '#',
                            //     //'items' => [
                            //        // ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                            //        // [
                            //          //   'label' => 'Level Two',
                            //           //  'icon' => 'fa fa-circle-o',
                            //           //  'url' => '#',
                            //           //  'items' => [
                            //           //      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                            //           //      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                            //          //   ],
                            //         //],
                            //    //],
                            
                            // //end 
                            // ],
                                                      
                        ],
                    ],

                            // end items RBAC
                    [
                        'label' => 'CNA Module Menu',
                        'icon' => 'folder-open',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Assessments', 'icon' => 'list-ol', 'url' => ['/institutions/admin'],],
                            ['label' => 'Question Section', 'icon' => 'puzzle-piece', 'url' => ['/cna/sections/index'],],
                            ['label' => 'Create Question', 'icon' => 'question', 'url' => ['/cna/questions/index'],],
                            ['label' => 'Create Question Options', 'icon' => 'filter', 'url' => ['/cna/question-options/index'],],
                            ['label' => 'Input Types', 'icon' => 'keyboard-o', 'url' => ['/cna/input-types/index'],],
                       
                            // [

                            // //end 
                            // ],
                        ],
                    ],

                    [
                        'label' => 'CD Plan Module Menu',
                        'icon' => 'folder-open-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'CB Plans', 'icon' => 'circle-o', 'url' => ['/plans'],],
                            ['label' => 'CB Plans Implemenation', 'icon' => 'circle-o', 'url' => ['/'],],
                            ['label' => 'CB Indicators', 'icon' => 'circle-o', 'url' => ['/cb-indicators'],],
                            ['label' => 'CB levels', 'icon' => 'circle-o', 'url' => ['/cb-levels'],],
                            ['label' => 'CB Sub levels', 'icon' => 'circle-o', 'url' => ['/cb-sublevels'],],
                            ['label' => 'CB Types', 'icon' => 'circle-o', 'url' => ['/cb-types'],],
                            ['label' => 'Institutions', 'icon' => 'circle-o', 'url' => ['/institutions'],],
                            ['label' => 'Institution sectors', 'icon' => 'circle-o', 'url' => ['/instit-sectors'],],
                            ['label' => 'Institution types', 'icon' => 'circle-o', 'url' => ['/instit-types'],],
                            
                            ['label' => 'Priority sectors', 'icon' => 'circle-o', 'url' => ['/priority-sectors'],],
                            ['label' => 'Clusters', 'icon' => 'circle-o', 'url' => ['/clusters'],],
                            ['label' => 'Provinces', 'icon' => 'circle-o', 'url' => ['/provinces'],],
                            ['label' => 'Districts', 'icon' => 'circle-o', 'url' => ['/districts'],],                 
                            ['label' => 'Programs', 'icon' => 'circle-o', 'url' => ['/programs'],],
                            ['label' => 'Sub programs', 'icon' => 'circle-o', 'url' => ['/subprograms'],],
                            ['label' => 'Stakeholders', 'icon' => 'circle-o', 'url' => ['/stakeholders'],],
                            ['label' => 'Funders', 'icon' => 'circle-o', 'url' => ['/funders'],],
                            ['label' => 'Skills', 'icon' => 'circle-o', 'url' => ['/skills'],],
                            ['label' => 'Training areas', 'icon' => 'circle-o', 'url' => ['/training-areas'],],
                            ['label' => 'Priority Training areas', 'icon' => 'circle-o', 'url' => ['/priorities-training-areas'],],
                            ['label' => 'Qualifications', 'icon' => 'circle-o', 'url' => ['/cb-qualifications'],],
                        ], 
                    ],

                    [
                        'label' => 'CD Providers Module Menu',
                        'icon' => 'folder',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Beneficiaries', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-beneficiaries'],],
                            ['label' => 'Beneficiaries enrollment', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-beneficiariesenrollment'],],
                            ['label' => 'Provider Infrastructure', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-infrastructure'],],
                            ['label' => 'Qualification areas', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-qualificationarea'],],
                            ['label' => 'Method of evaluation', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-evaluation'],],
                            ['label' => 'Training methodology', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-methodology'],],
                            ['label' => 'Teaching language', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-language'],],
                            ['label' => 'Providers', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-provider'],],
                            ['label' => 'Providers geo-location', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-providerbase'],],
                            ['label' => 'Providers category', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-providercategory'],],
                            ['label' => 'Providers type', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-providertype'],],
                            ['label' => 'Providers services', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-services'],],
                            ['label' => 'Trainers', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-trainer'],],
                            ['label' => 'Training areas by Provider', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-trainingareasbyprovider'],],
                            ['label' => 'Trainer by course', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-whotrainwhat'],],
                            ['label' => 'working positions', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-workingposition'],],
                            ['label' => 'Countries', 'icon' => 'circle-o', 'url' => ['/cdproviders/provider-country'],],
                            
                           
                        ], 
                    ],
                    
                    [
                        'label' => 'Internship Module Menu',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Dashboard', 'icon' => 'circle-o', 'url' => ['/internship/internshipinternapplication/dashboardinterns'],],
                            ['label' => 'Apprenticeship', 'icon' => 'circle-o', 'url' => ['/internship/'],],
                            ['label' => 'Intern applications', 'icon' => 'circle-o', 'url' => ['/internship/internshipinternapplication'],],
                            ['label' => 'Interns request', 'icon' => 'circle-o', 'url' => ['/internship/internship-institutionsrequest'],],
                            ['label' => 'Request Details', 'icon' => 'circle-o', 'url' => ['/internship/internship-institutionsrequestdetails'],],
                            ['label' => 'Banks', 'icon' => 'circle-o', 'url' => ['/internship/internship-bank'],],
                            ['label' => 'Universities', 'icon' => 'circle-o', 'url' => ['/internship/internship-universities'],],
                            
                           
                        ], 
                    ],

                    [
                        'label' => Yii::t('app','Logs').$logs, 
                        'icon' => 'fa fa-angle-double-right', 
                        'url' => ['/log/index'],
                    ],

                    // end items Menu

                    // [
                    //     'label' => 'Reports',
                    //     'icon' => 'fa fa-share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Total planned budget by sector', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report'],],
                    //         ['label' => 'Total planned budget by CB level', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report2'],],
                    //         ['label' => 'Planned budget by critical or non critical', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/bycritical'],],
                    //         // ['label' => 'CB levels', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-levels'],],
                    //         // ['label' => 'CB Sub levels', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-sublevels'],],
                    //         // ['label' => 'CB Types', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-types'],],
                    //         // ['label' => 'Institutions', 'icon' => 'fa fa-circle-o', 'url' => ['/institutions'],],
                    //         // ['label' => 'Institution sectors', 'icon' => 'fa fa-circle-o', 'url' => ['/instit-sectors'],],
                    //         // ['label' => 'Institution types', 'icon' => 'fa fa-circle-o', 'url' => ['/instit-types'],],
                            
                    //         // ['label' => 'Priority sectors', 'icon' => 'fa fa-circle-o', 'url' => ['/priority-sectors'],],
                    //         // ['label' => 'Clusters', 'icon' => 'fa fa-circle-o', 'url' => ['/clusters'],],
                    //         // ['label' => 'Provinces', 'icon' => 'fa fa-circle-o', 'url' => ['/provinces'],],
                    //         // ['label' => 'Districts', 'icon' => 'fa fa-circle-o', 'url' => ['/districts'],],                 
                    //         // ['label' => 'Programs', 'icon' => 'fa fa-circle-o', 'url' => ['/programs'],],
                    //         // ['label' => 'Sub programs', 'icon' => 'fa fa-circle-o', 'url' => ['/subprograms'],],
                    //         // ['label' => 'Stakeholders', 'icon' => 'fa fa-circle-o', 'url' => ['/stakeholders'],],
                    //         // ['label' => 'Funders', 'icon' => 'fa fa-circle-o', 'url' => ['/funders'],],
                    //         // ['label' => 'Skills', 'icon' => 'fa fa-circle-o', 'url' => ['/skills'],],
                    //         // ['label' => 'Training areas', 'icon' => 'fa fa-circle-o', 'url' => ['/training-areas'],],
                    //         // ['label' => 'Qualifications', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-qualifications'],],     
                    //         // [

                    //         // //end 
                    //         // ], 
                    //     ],
                    // ],                
                // end items Reports


                ],
           // end first
            ]
        ) ?>

