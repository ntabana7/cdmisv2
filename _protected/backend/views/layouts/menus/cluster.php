 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //  ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                     [
                        'label' => 'CNA Module Menu',
                        'icon' => 'folder-open',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Assessments', 'icon' => 'list-ol', 'url' => ['/institutions/admin'],],
                       
                            // [

                            // //end 
                            // ],
                        ],
                    ],
                    [
                        'label' => 'App Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'CB Plans', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/clusterindex'],],
                            ['label' => 'Priority Training areas', 'icon' => 'fa fa-circle-o', 'url' => ['/priorities-training-areas'],],
                            ['label' => 'CB Plans Implemenation', 'icon' => 'fa fa-circle-o', 'url' => ['/'],],
                            [

                            //end 
                            ],                                

                        // end items RBAc
                        ],
                    
                    // // end second after items1
                    ],
                    [
                        'label' => 'Reports',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Total planned budget by sector', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report'],],
                            ['label' => 'Total planned budget by CB level', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report2'],],
                            ['label' => 'Planned budget by critical or non critical', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/bycritical'],],
                            [

                            //end 
                            ],                                

                        // end items RBAc
                        ],
                    
                    // end second after items1
                    ],

                
                // end items1
                ],
           // end first
            ]
        ) ?>



