 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //  ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                   
                    [
                        'label' => 'CNA Module Menu',
                        'icon' => 'folder-open',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Institutional', 'icon' => 'list-ol', 'url' => ['/cna/assessment/approve-institutional'],],
                            ['label' => 'organisational', 'icon' => 'list-ul', 'url' => ['/cna/assessment/approve-organisational'],],
                            ['label' => 'Individual', 'icon' => 'list', 'url' => ['/cna/assessment/approve-individual'],],

                        ], 
                    ],                
                // end items1
                ],
           // end first
            ]
        ) ?>



