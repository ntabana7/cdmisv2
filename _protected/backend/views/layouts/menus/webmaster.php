 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //  ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                   
                    [
                        'label' => 'Questions',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Questions', 'icon' => 'fa fa-circle-o', 'url' => ['/cna/questions/index'],],
                        ], 
                    ],
                    
                    [
                        'label' => 'CD Plan Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'CB Plans', 'icon' => 'fa fa-circle-o', 'url' => ['/plans'],],
                            ['label' => 'Report the implemenation plan', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/approvedplan'],],
                            [

                            //end 
                            ],                                

                        // end items RBAc
                        ],
                    
                    // end second after items1
                    ],

                    [
                        'label' => 'Internship Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Interns request', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequest'],],
                            ['label' => 'Request Details', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequestdetails'],],
                            ['label' => 'Given interns', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/deployedininstitution'],],  
                        ], 
                    ],

                    [
                        'label' => 'Reports',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Total planned budget by sector', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report'],],
                            ['label' => 'Total planned budget by CB level', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report2'],],
                            ['label' => 'Planned budget by critical or non critical', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/bycritical'],],
                            [

                            //end 
                            ],                                

                        // end items RBAc
                        ],
                    
                    // end second after items1
                    ],

                
                // end items1
                ],
           // end first
            ]
        ) ?>



