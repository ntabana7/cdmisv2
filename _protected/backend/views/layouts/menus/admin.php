 <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //  ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                     [
                        'label' => 'CNA Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'Assessments', 'icon' => 'fa fa-circle-o', 'url' => ['/institutions/admin'],],
                            ['label' => 'Question Section', 'icon' => 'fa fa-circle-o', 'url' => ['/cna/sections/index'],],
                            ['label' => 'Create Question', 'icon' => 'fa fa-circle-o', 'url' => ['/cna/questions/index'],],
                            ['label' => 'Create Question Options', 'icon' => 'fa fa-circle-o', 'url' => ['/cna/question-options/index'],],
                            ['label' => 'Input Types', 'icon' => 'fa fa-circle-o', 'url' => ['/cna/input-types/index'],],
                       
                            // [

                            // //end 
                            // ],
                        ],
                    ],
                    [
                        'label' => 'CD Plan Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [

                            ['label' => 'CB Plans', 'icon' => 'fa fa-circle-o', 'url' => ['/plans'],],
                            ['label' => 'CB Plans Implemenation', 'icon' => 'fa fa-circle-o', 'url' => ['/'],],
                            ['label' => 'CB Indicators', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-indicators'],],
                            ['label' => 'CB levels', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-levels'],],
                            ['label' => 'CB Sub levels', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-sublevels'],],
                            ['label' => 'CB Types', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-types'],],
                            ['label' => 'Institutions', 'icon' => 'fa fa-circle-o', 'url' => ['/institutions'],],
                            ['label' => 'Institution sectors', 'icon' => 'fa fa-circle-o', 'url' => ['/instit-sectors'],],
                            ['label' => 'Institution types', 'icon' => 'fa fa-circle-o', 'url' => ['/instit-types'],],
                            
                            ['label' => 'Priority sectors', 'icon' => 'fa fa-circle-o', 'url' => ['/priority-sectors'],],
                            ['label' => 'Clusters', 'icon' => 'fa fa-circle-o', 'url' => ['/clusters'],],
                            ['label' => 'Provinces', 'icon' => 'fa fa-circle-o', 'url' => ['/provinces'],],
                            ['label' => 'Districts', 'icon' => 'fa fa-circle-o', 'url' => ['/districts'],],                 
                            ['label' => 'Programs', 'icon' => 'fa fa-circle-o', 'url' => ['/programs'],],
                            ['label' => 'Sub programs', 'icon' => 'fa fa-circle-o', 'url' => ['/subprograms'],],
                            ['label' => 'Stakeholders', 'icon' => 'fa fa-circle-o', 'url' => ['/stakeholders'],],
                            ['label' => 'Funders', 'icon' => 'fa fa-circle-o', 'url' => ['/funders'],],
                            ['label' => 'Skills', 'icon' => 'fa fa-circle-o', 'url' => ['/skills'],], 
                            ['label' => 'Training areas', 'icon' => 'fa fa-circle-o', 'url' => ['/training-areas'],],
                            ['label' => 'Priority Training areas', 'icon' => 'fa fa-circle-o', 'url' => ['/priorities-training-areas'],], 
                            ['label' => 'Qualifications', 'icon' => 'fa fa-circle-o', 'url' => ['/cb-qualifications'],],                         
                            // [

                            // //end 
                            // ],
                        ],
                    ],
                // end items RBAC

                      [
                        'label' => 'CD Providers Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Beneficiaries', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-beneficiaries'],],
                            ['label' => 'Beneficiaries enrollment', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-beneficiariesenrollment'],],                            
                            ['label' => 'Provider Infrastructure', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-infrastructure'],],
                            ['label' => 'Qualification areas', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-qualificationarea'],],
                            ['label' => 'Method of evaluation', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-evaluation'],],
                            ['label' => 'Training methodology', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-methodology'],],
                            ['label' => 'Teaching language', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-language'],],
                            ['label' => 'Providers', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-provider'],],
                            ['label' => 'Providers geo-location', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-providerbase'],],
                            ['label' => 'Providers category', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-providercategory'],],
                            ['label' => 'Providers type', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-providertype'],],
                            ['label' => 'Providers services', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-services'],],
                            ['label' => 'Trainers', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-trainer'],],
                            ['label' => 'Training areas by Provider', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-trainingareasbyprovider'],],
                            ['label' => 'Trainer by course', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-whotrainwhat'],],
                            ['label' => 'working positions', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-workingposition'],],
                            ['label' => 'Countries', 'icon' => 'fa fa-circle-o', 'url' => ['/cdproviders/provider-country'],],
                            
                           
                        ], 
                    ],

                     [
                        'label' => 'Internship Module Menu',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Dashboard', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication/dashboardinterns'],],
                            ['label' => 'Apprenticeship', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/'],],
                            ['label' => 'Intern applications', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internshipinternapplication'],],
                            ['label' => 'Interns request', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequest'],],
                            ['label' => 'Request Details', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-institutionsrequestdetails'],],
                            ['label' => 'Banks', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-bank'],],
                            ['label' => 'Universities', 'icon' => 'fa fa-circle-o', 'url' => ['/internship/internship-universities'],],
                            
                           
                        ], 
                    ],

                    // [
                    //     'label' => 'Reports',
                    //     'icon' => 'fa fa-share',
                    //     'url' => '#',
                    //     'items' => [

                    //         ['label' => 'Total planned budget by sector', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report'],],
                    //         ['label' => 'Total planned budget by CB level', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/report2'],],
                    //         ['label' => 'Planned budget by critical or non critical', 'icon' => 'fa fa-circle-o', 'url' => ['/plans/bycritical'],],                            
                    //         // [

                    //         // //end 
                    //         // ],
                    //     ],
                    // ],


              
                ],
            ]
        ) ?>

