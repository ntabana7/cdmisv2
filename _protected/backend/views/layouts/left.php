<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user1.png" class="img-circle" alt="User Image"/>

                     <!-- This pic was removed above, it is for user who logged in -->
                     <!--user2-160x160.jpg-->

            </div>
            <div class="pull-left info">
                <p><?php echo Yii::$app->user->identity->username;?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php 

            if (Yii::$app->user->can('theCreator'))
            {
              include('menus/theCreator.php');  
          
            }
             // display Login page to guests of the site
            elseif (Yii::$app->user->can('admin')) 
            {
                include('menus/admin.php'); 
            }            
            
            elseif (Yii::$app->user->can('cluster')) 
            {
                include('menus/cluster.php');  
            }
            // display Login page to guests of the site
            elseif (Yii::$app->user->can('approver')) 
            {
              include('menus/institutionapprover.php');   
            }
            // display Login page to guests of the site
            elseif (Yii::$app->user->can('institution')) 
            {
              include('menus/institution.php');   
            }
            elseif (Yii::$app->user->can('funder')) 
            {
              include('menus/funder.php');   
            }
            elseif (Yii::$app->user->can('funderboss')) 
            {
              include('menus/funderboss.php');   
            }
               // display Login page to guests of the site
            elseif (Yii::$app->user->can('supervisor')) 
            {
                include('menus/supervisor.php');  
            }

            elseif (Yii::$app->user->can('provider')) 
            {
                include('menus/providers.php');  
            }
            elseif (Yii::$app->user->can('internshipofficer')) 
            {
                include('menus/internshipofficer.php');  
            }
            elseif (Yii::$app->user->can('intern')) 
            {
                include('menus/intern.php');  
            }
        ?>
           </section>

</aside>
