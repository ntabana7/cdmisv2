<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AuthItemChild */

?>
<div class="auth-item-child-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
