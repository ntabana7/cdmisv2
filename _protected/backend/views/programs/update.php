<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Programs */
?>
<div class="programs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
