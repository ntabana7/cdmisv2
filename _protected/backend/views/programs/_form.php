<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\InstitSectors;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Programs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="programs-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'idInstitSector')->textInput() ?> -->
    <?= $form->field($model, 'idInstitSector')->dropDownList(ArrayHelper::map(InstitSectors::find()->all(),'idInstitSector','sectName'),[ 'prompt'=>'Select Sector',
                            'language' => 'en',
                         	]);

  	?>

    <?= $form->field($model, 'programName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
