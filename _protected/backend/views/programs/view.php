<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Programs */
?>
<div class="programs-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProgram',
            'idInstitSector',
            'programName',
            'status',
        ],
    ]) ?>

</div>
