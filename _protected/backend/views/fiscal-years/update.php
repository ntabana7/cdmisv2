<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FiscalYears */
?>
<div class="fiscal-years-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
