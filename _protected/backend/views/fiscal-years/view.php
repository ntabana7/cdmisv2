<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FiscalYears */
?>
<div class="fiscal-years-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fiscalyear',
            'current',
        ],
    ]) ?>

</div>
