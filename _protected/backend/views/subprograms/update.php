<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Subprograms */
?>
<div class="subprograms-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
