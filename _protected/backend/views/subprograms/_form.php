<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Programs;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Subprograms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subprograms-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'idProgram')->textInput() ?> -->
    <?= $form->field($model, 'idProgram')->dropDownList(ArrayHelper::map(Programs::find()->all(),'idProgram','programName'),[ 'prompt'=>'Select Program',
                            'language' => 'en',
                         	]);

  	?>

    <?= $form->field($model, 'subProgramName')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
