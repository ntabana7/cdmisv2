<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Scholardetails */
?>
<div class="scholardetails-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'scholar_id',
            'universityattended',
            'universitycountry',
            'sponsorhip',
            'othersponsorship',
            'qualification',
            'qualificationarea',
            'startingdate',
            'endingdate',
        ],
    ]) ?>

</div>
