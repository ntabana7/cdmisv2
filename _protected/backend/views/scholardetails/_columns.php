<?php
use yii\helpers\Url;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'scholar_id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'universityattended',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'universitycountry',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sponsorhip',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'othersponsorship',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'qualification',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'qualificationarea',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'startingdate',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'endingdate',
    ],
    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to([$action,'id'=>$key]);
    //     },
    //     'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>'Are you sure?',
    //                       'data-confirm-message'=>'Are you sure want to delete this item'], 
    // ],

];   