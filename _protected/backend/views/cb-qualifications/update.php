<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CbQualifications */
?>
<div class="cb-qualifications-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
