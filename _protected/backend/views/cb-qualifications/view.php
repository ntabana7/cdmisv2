<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CbQualifications */
?>
<div class="cb-qualifications-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idQualif',
            'qualif',
        ],
    ]) ?>

</div>
