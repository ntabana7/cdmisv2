<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CbLevels;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\CbSublevels */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cb-sublevels-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'idLevel')->textInput() ?> -->
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idLevel')->dropDownList(ArrayHelper::map(CbLevels::find()->all(),'idLevel','level'),[ 'prompt'=>'Select CB Level',
                            'language' => 'en',
                         	]);

  	?>

    <?= $form->field($model, 'subLevel')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
