<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CbSublevels */
?>
<div class="cb-sublevels-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
