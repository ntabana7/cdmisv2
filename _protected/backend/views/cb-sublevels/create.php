<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CbSublevels */

?>
<div class="cb-sublevels-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
