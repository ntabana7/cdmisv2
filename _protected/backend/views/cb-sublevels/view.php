<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CbSublevels */
?>
<div class="cb-sublevels-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idSubLevel',
            'idLevel',
            'subLevel',
        ],
    ]) ?>

</div>
