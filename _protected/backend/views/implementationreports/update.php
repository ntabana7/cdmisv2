<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Implementationreports $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Implementationreports',
]) . ' ' . $model->idImplementation;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Implementationreports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idImplementation, 'url' => ['view', 'id' => $model->idImplementation]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="implementationreports-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
