<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\models\Implementationreports $model
 * @var yii\widgets\ActiveForm $form
 */


?>
<div class='well'>
<table class='table'>
<th>id Plan</th>
<th>Plan</th>
<tr>
    
    <td>

        <?= $data->output?>
    </td>
    <td>
        <?= $data->idSubLevel?>
    </td>
</tr>v
</table>
    
</div>
<hr>
<div class="implementationreports-form well">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'idplan' => ['type' => Form::INPUT_HIDDEN, 'options' => ['value' =>$_GET['id']],'label'=>''],

            'Quarter' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Quarter...']],

            'nbrbeneficiaries' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Nbrbeneficiaries...']],

            'female' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Female...']],

            'male' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Male...']],


            'comment' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Comment...','rows' => 6]],

            'file' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter File...', 'maxlength' => 200]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
