<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\models\Implementationreports $model
 */

$this->title = $model->idImplementation;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Implementationreports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="implementationreports-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'idImplementation',
            'idplan',
            'Quarter',
            'nbrbeneficiaries',
            'female',
            'male',
            [
                'attribute' => 'createdon',
                'format' => [
                    'date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date']))
                        ? Yii::$app->modules['datecontrol']['displaySettings']['date']
                        : 'd-m-Y'
                ],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATE
                ]
            ],
            'comment:ntext',
            'file',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->idImplementation],
        ],
        'enableEditMode' => true,
    ]) ?>

</div>
