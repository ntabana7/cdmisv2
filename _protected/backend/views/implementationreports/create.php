<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\models\Implementationreports $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Implementationreports',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Implementationreports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="implementationreports-create">

    <?= $this->render('_form', [
        'model' => $model,
         'data' => $data
    ]) ?>

</div>
