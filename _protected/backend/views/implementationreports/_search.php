<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\models\ImplementationreportsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="implementationreports-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idImplementation') ?>

    <?= $form->field($model, 'idplan') ?>

    <?= $form->field($model, 'Quarter') ?>

    <?= $form->field($model, 'nbrbeneficiaries') ?>

    <?= $form->field($model, 'female') ?>

    <?php // echo $form->field($model, 'male') ?>

    <?php // echo $form->field($model, 'createdon') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'file') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
