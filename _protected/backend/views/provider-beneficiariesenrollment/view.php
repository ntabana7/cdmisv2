<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\modules\cdproviders\models\ProviderBeneficiariesenrollment $model
 */

$this->title = $model->idEnrollment;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Provider Beneficiariesenrollments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-beneficiariesenrollment-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'idEnrollment',
            'idProvider',
            'idBeneficiary',
            'idInstit',
            'idWorkingposition',
            'idSubLevel',
            'idTraining',
            'startingdate',
            'endingdate',
            'idQualif',
            'costoftraining',
            'idCountry',
            'funder',
            'status',
            'reasonnotcompleted',
            'yearofcd',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->idEnrollment],
        ],
        'enableEditMode' => true,
    ]) ?>

</div>
