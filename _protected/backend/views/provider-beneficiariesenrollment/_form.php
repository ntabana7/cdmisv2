<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\modules\cdproviders\models\ProviderBeneficiariesenrollment $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="provider-beneficiariesenrollment-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'idProvider' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Provider...']],

            'idBeneficiary' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Beneficiary...']],

            'idInstit' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Instit...']],

            'idWorkingposition' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Workingposition...']],

            'idSubLevel' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Sub Level...']],

            'idTraining' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Training...']],

            'startingdate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Startingdate...', 'maxlength' => 100]],

            'endingdate' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Endingdate...', 'maxlength' => 100]],

            'idQualif' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Qualif...']],

            'costoftraining' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Costoftraining...', 'maxlength' => 100]],

            'idCountry' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Id Country...']],

            'funder' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Funder...', 'maxlength' => 100]],

            'yearofcd' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Yearofcd...', 'maxlength' => 100]],

            'status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Status...']],

            'reasonnotcompleted' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Reasonnotcompleted...', 'maxlength' => 100]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
