<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\cdproviders\models\ProviderBeneficiariesenrollment $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Provider Beneficiariesenrollment',
]) . ' ' . $model->idEnrollment;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Provider Beneficiariesenrollments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEnrollment, 'url' => ['view', 'id' => $model->idEnrollment]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="provider-beneficiariesenrollment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
