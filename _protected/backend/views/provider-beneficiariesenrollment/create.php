<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\cdproviders\models\ProviderBeneficiariesenrollment $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Provider Beneficiariesenrollment',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Provider Beneficiariesenrollments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provider-beneficiariesenrollment-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
