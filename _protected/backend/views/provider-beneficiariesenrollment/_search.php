<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\modules\cdproviders\models\ProviderBeneficiariesenrollmentSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="provider-beneficiariesenrollment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idEnrollment') ?>

    <?= $form->field($model, 'idProvider') ?>

    <?= $form->field($model, 'idBeneficiary') ?>

    <?= $form->field($model, 'idInstit') ?>

    <?= $form->field($model, 'idWorkingposition') ?>

    <?php // echo $form->field($model, 'idSubLevel') ?>

    <?php // echo $form->field($model, 'idTraining') ?>

    <?php // echo $form->field($model, 'startingdate') ?>

    <?php // echo $form->field($model, 'endingdate') ?>

    <?php // echo $form->field($model, 'idQualif') ?>

    <?php // echo $form->field($model, 'costoftraining') ?>

    <?php // echo $form->field($model, 'idCountry') ?>

    <?php // echo $form->field($model, 'funder') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'reasonnotcompleted') ?>

    <?php // echo $form->field($model, 'yearofcd') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
