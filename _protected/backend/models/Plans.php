<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%plans}}".
 *
 * @property integer $idplan
 * @property integer $idInstit
 * @property integer $idSubProgram
 * @property string $output
 * @property string $challenge
 * @property integer $baseline
 * @property integer $idSubLevel
 * @property integer $nbrbeneficiaries
 * @property string $actionDescription
 * @property integer $idCbType
 * @property string $generalNeeds
 * @property string $specificNeeds
 * @property integer $idIndicator
 * @property string $quarters
 * @property string $responsable
 * @property string $stakeholders
 * @property string $cost
 * @property string $funders
 * @property integer $status
 *
 * @property Institutions $idInstit0
 * @property Subprograms $idSubProgram0
 * @property CbSublevels $idSubLevel0
 * @property CbTypes $idCbType0
 * @property CbIndicators $idIndicator0
 */
class Plans extends \yii\db\ActiveRecord
{
    
    public $idProgram;
    public $idLevel;    
    public $file;
    public $institName;  
    public $female1;
    public $male1;
    public $female2;
    public $male2;
    public $female3;
    public $male3;
    public $female4;
    public $male4;
    public $idIndicators;
    public $idCbTypes;
    public $qter1;
    public $qter2;
    public $qter3;
    public $qter4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%plans}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProgram','idSubProgram', 'output', 'challenge', 'baseline', 'idSubLevel', 'nbrbeneficiaries', 'actionDescription', 'idCbType', 'idIndicator', 'responsable', 'stakeholders', 'cost', 'funders', 'status','idLevel'], 'required'],


            [['idInstitSector','idInstit', 'idSubProgram', 'baseline', 'idSubLevel', 'idCbType', 'idIndicator', 'status','male1','female1','male2','female2','male3','female3','male4','female4','nbrbeneficiaries','idSkills','idTraining','idQualif','datasource'], 'integer'],
            [['cost','grantedAmount'], 'double'],
            [['file'], 'file','maxSize' => 1024 * 1024 * 10, 'on'=>'create'],
            //[['nbrbeneficiaries'], 'verifyTheCard'],
            [['output', 'challenge','actionDescription','actionplan','institName'], 'string', 'max' => 255],
            // [['generalNeeds', 'specificNeeds'], 'string', 'max' => 150],
            [['qter1','qter2','qter3','qter4'], 'string', 'max' => 20],
            [['responsable'], 'string', 'max' => 100],  
            [['reasonnotapproved'], 'string', 'max' => 200],           
            [['idInstit'], 'exist', 'skipOnError' => true, 'targetClass' => Institutions::className(), 'targetAttribute' => ['idInstit' => 'idInstit']],
            [['idLevel','idSubProgram'], 'exist', 'skipOnError' => true, 'targetClass' => Subprograms::className(), 'targetAttribute' => ['idSubProgram' => 'idSubProgram']],
            [['idSubLevel'], 'exist', 'skipOnError' => true, 'targetClass' => CbSublevels::className(), 'targetAttribute' => ['idSubLevel' => 'idSubLevel']],
            [['idCbType'], 'exist', 'skipOnError' => true, 'targetClass' => CbTypes::className(), 'targetAttribute' => ['idCbType' => 'idCbType']],
            [['idIndicator'], 'exist', 'skipOnError' => true, 'targetClass' => CbIndicators::className(), 'targetAttribute' => ['idIndicator' => 'idIndicator']],
        
        ];
    }

    public function getQuarterMale1(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters);

            return $data[0];
        }else{
            return $this->quarters;
        }
        
    }

    public function getQuarterFemale1(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters);
            return $data[1];
        }else{
            return $this->quarters;
        }
        
    }
    public function getQuarterMale2(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters2);

            return $data[0];
        }else{
            return $this->quarters2;
        }
        
    }

    public function getQuarterFemale2(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters2);
            return $data[1];
        }else{
            return $this->quarters2;
        }
        
    }
    public function getQuarterMale3(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters3);

            return $data[0];
        }else{
            return $this->quarters3;
        }
        
    }

    public function getQuarterFemale3(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters3);
            return $data[1];
        }else{
            return $this->quarters3;
        }
        
    }
    public function getQuarterMale4(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters4);

            return $data[0];
        }else{
            return $this->quarters4;
        }
        
    }

    public function getQuarterFemale4(){

        if($this->idSubLevel == 2){
            $data = explode(',',$this->quarters4);
            return $data[1];
        }else{
            return $this->quarters4;
        }
        
    }

    public function hiddingUploadFile(){
        $obj = new Fiscalyear;
        
        if($this->find()->where('idInstit='.Yii::$app->user->identity->userFrom)->count() > 0){

        $submittedDate = $this->find()->select('submittedon')->where('idInstit='.Yii::$app->user->identity->userFrom)->orderBy('idplan DESC')->LIMIT(1)->one()->submittedon;
        if(!empty($submittedDate)){
            $check = $obj->checkValidity($submittedDate);
            return $check;
        }

        return 1;
        }

        return 0;
        
    }
    // public function getQuarter2(){
    //     if($this->idSubLevel == 2){
    //         $data = explode(',',$this->quarters2);

    //     //return 'Male('.$data[0].') , Female('.$data[1].')';

    //         return $data[0].' Male(s) , '.$data[1].' Female(s)';
    //     }else{
    //         return $this->quarters2;
    //     }
        
    // }


    // public function getQuarter3(){
    //     if($this->idSubLevel == 2){
    //         $data = explode(',',$this->quarters3);

    //     //return 'Male('.$data[0].') , Female('.$data[1].')';

    //         return $data[0].' Male(s) , '.$data[1].' Female(s)';
    //     }else{
    //         return $this->quarters3;
    //     }
        
    // }


    // public function getQuarter4(){
    //     if($this->idSubLevel == 2){
    //         $data = explode(',',$this->quarters4);

    //     //return 'Male('.$data[0].') , Female('.$data[1].')';

    //         return $data[0].' Male(s) , '.$data[1].' Female(s)';
    //     }else{
    //         return $this->quarters4;
    //     }
        
    // }
    public function verifyTheCard($attribute){

        $total = $this->male1+$this->male2+$this->male3+$this->male4+$this->female1+$this->female2+$this->female3+$this->female4;
        if($total != (int)$this->nbrbeneficiaries){
            // var_dump($attribute);die;
            $this->addError($attribute, Yii::t('app', ' can not be blank.Please scan the card'));         
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idplan' => Yii::t('app', 'Idplan'),
            'idInstitSector' => Yii::t('app', 'Sector'),
            'idInstit' => Yii::t('app', 'Institution'),
            'idProgram' => Yii::t('app', 'Program'),
            'idSubProgram' => Yii::t('app', 'Sub Program'),
            'output' => Yii::t('app', 'Output'),
            'challenge' => Yii::t('app', 'CD Challenge'),
            'baseline' => Yii::t('app', 'CD Baseline'),
            'idLevel' => Yii::t('app', 'CD Level'),
            'idSubLevel' => Yii::t('app', 'CD Approach'),
            'nbrbeneficiaries' => Yii::t('app', 'Number of beneficiaries'),
            'actionDescription' => Yii::t('app', 'CD Action'),
            'idCbType' => Yii::t('app', 'CD Type'),
            'generalNeeds' => Yii::t('app', 'General Needs'),
            'specificNeeds' => Yii::t('app', 'Specific Needs'),
            'idIndicator' => Yii::t('app', 'Indicator'),
            'quarters' => Yii::t('app', 'Quarter 1'),
            'quarters2' => Yii::t('app', 'Quarter 2'),
            'quarters3' => Yii::t('app', 'Quarter 3'),
            'quarters4' => Yii::t('app', 'Quarter 4'),
            'responsable' => Yii::t('app', 'Responsable'),
            'stakeholders' => Yii::t('app', 'Stakeholders'),
            'cost' => Yii::t('app', 'Budget needed'),
            'funders' => Yii::t('app', 'Funders'),
            'status' => Yii::t('app', 'Status'),
            'file' => Yii::t('app', 'Action Plan'),
            'idSkills' => Yii::t('app', 'Skills'),
            'idQualif' => Yii::t('app', 'Qualification'),
            'idTraining' => Yii::t('app', 'Training area'),
            'quarterMale1' => Yii::t('app', 'Quarter 1 (Male)'),
            'quarterFemale1' => Yii::t('app', 'Quarter 1 (Female)'),
            'quarterMale2' => Yii::t('app', 'Quarter 2 (Male)'),
            'quarterFemale2' => Yii::t('app', 'Quarter 2 (Female)'),
            'quarterMale3' => Yii::t('app', 'Quarter 3 (Male)'),
            'quarterFemale3' => Yii::t('app', 'Quarter 3 (Female)'),
            'quarterMale4' => Yii::t('app', 'Quarter 4 (Male)'),
            'quarterFemale4' => Yii::t('app', 'Quarter 4 (Females)'),
            'grantedAmount' => Yii::t('app', 'Granted Amount'),
            'statuslabel' => Yii::t('app', 'Status'),
            'reasonnotapproved' => Yii::t('app', 'The reason the plan not approved'),
            'datasource' => Yii::t('app', 'Data source'),
            'submittedon' => Yii::t('app', 'Fiscal year'),

            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstit0()
    {
        return $this->hasOne(Institutions::className(), ['idInstit' => 'idInstit']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProgram0()
    {
        return $this->hasOne(Programs::className(), ['idProgram' => 'idProgram']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubProgram0()
    {
        return $this->hasOne(Subprograms::className(), ['idSubProgram' => 'idSubProgram']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubLevel0()
    {
        return $this->hasOne(CbSublevels::className(), ['idSubLevel' => 'idSubLevel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCbType0()
    {
        return $this->hasOne(CbTypes::className(), ['idCbType' => 'idCbType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIndicator0()
    {
        return $this->hasOne(CbIndicators::className(), ['idIndicator' => 'idIndicator']);
    }

    public function getStakeholdersListDropdown()
    {   
        $listCategory   = Stakeholders::find()->select('StakeholderName')->all();
        $list   = ArrayHelper::map( $listCategory,'StakeholderName','StakeholderName');

        return $list;
    }

    public function getFundersListDropdown()
    {   
        $listfunders   = Funders::find()->select('funderName')->all();
        $listt   = ArrayHelper::map( $listfunders,'funderName','funderName');

        return $listt;
    }

     public function getFunder()
    {   
        return Funders::find()->where('idfunder=:funderid',['funderid'=>Yii::$app->user->identity->userFrom])->one()->funderName;
    }

// This functions have been created after Gii generartion


    public function getIdTraining0()
    {
        return $this->hasOne(TrainingAreas::className(), ['idTraining' => 'idTraining']);
    }

    public function getIdSkills0()
    {
        return $this->hasOne(Skills::className(), ['idSkills' => 'idSkills']);
    }

    public function getIdIdQualif0()
    {
        return $this->hasOne(CbQualifications::class, ['idQualif' => 'idQualif']);
    }

    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    public function getStatuslabel()
    {

        if($this->status == 1)
            return 'In process';
        elseif($this->status == 2)
            return 'Approved';
        elseif($this->status == 3)
            return 'Not approved';
        else
            return 'Pending' ;

        
    }

    
}
