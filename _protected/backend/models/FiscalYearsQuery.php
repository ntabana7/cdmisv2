<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[FiscalYears]].
 *
 * @see FiscalYears
 */
class FiscalYearsQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[current]]=1');
    }

    /**
     * @inheritdoc
     * @return FiscalYears[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FiscalYears|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
