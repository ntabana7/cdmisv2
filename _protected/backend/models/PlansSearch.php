<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Plans;
use backend\models\Subprograms;
/**
 * PlansSearch represents the model behind the search form about `backend\models\Plans`.
 */
class PlansSearch extends Plans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idplan','baseline', 'nbrbeneficiaries', 'status' ,'datasource'], 'integer'],
            [['output', 'challenge', 'idSubLevel','idProgram','actionDescription', 'generalNeeds', 'specificNeeds', 'quarters', 'responsable', 'stakeholders', 'funders','datasource', 'idInstit', 'idSubProgram', 'idCbType', 'idIndicator','idInstitSector','idTraining','submittedon'], 'safe'],
            [['cost','grantedAmount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->can('admin')){
            $query = Plans::find()->orderBy('idInstitSector');
        }

        elseif(Yii::$app->user->can('cluster')){
           $idinstitutions = Institutions::find()->select('idInstit')->where('idCluster=:u',['u'=>Yii::$app->user->identity->userFrom])->all();
           $ids = array(); 
            foreach($idinstitutions as $ar) {    
                $ids[] = $ar['idInstit'];
            }
            $idInstits = implode(',', $ids);
           $query = Plans::find()->where('plans.idInstit IN('.$idInstits.')');
        }
        elseif(Yii::$app->user->can('funder')){

            $idinstit=Funderbyinstitution::find()->select('idInstit')->where('id=:u',['u'=>Yii::$app->user->identity->id])->one();

            $funder_name = Funders::find()->select('funderName')->where('idfunder=:funderid',['funderid'=>Yii::$app->user->identity->userFrom])->one();


            $query = Plans::find()->where('funders=:u',['u'=>$funder_name['funderName']])
                                  ->AndWhere('plans.idInstit=:x',['x'=>$idinstit['idInstit']])
                                    //->AndWhere('datasource = 1')
                                   ->orderBy('idInstitSector'); 
        }
        elseif(Yii::$app->user->can('funderboss')){
            $funder_name = Funders::find()->select('funderName')->where('idfunder=:funderid',['funderid'=>Yii::$app->user->identity->userFrom])->one();


            $query = Plans::find()->where('funders=:u',['u'=>$funder_name['funderName']])
                                    //->AndWhere('datasource = 1')
                                   ->orderBy('idInstitSector'); 
        }
        else{
            
           // $query = Plans::find()->where('idInstit='.Yii::$app->user->identity->userFrom.' and status = 0 and  year(submittedon)= year(now())')
           //          ->orWhere('idInstit='.Yii::$app->user->identity->userFrom.' and status <> 0 and year(submittedon) <> year(now())')
           //          ->orderBy('status'); 

            $query = Plans::find()->where('plans.idInstit=:u',['u'=>Yii::$app->user->identity->userFrom])->orderBy(['submittedon' => SORT_ASC]); 
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith('idSubLevel0');
        $query->joinWith('idInstitSector0');
        $query->joinWith('idInstit0');
        $query->joinWith('idSubProgram0');        
        // ->joinWith('idSubProgram0')->joinWith('idProgram0');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idplan' => $this->idplan,
            // 'idInstit' => $this->idInstit,
            // 'idSubProgram' => $this->idSubProgram,
            // 'idSProgram' => $this->idSubProgram,
            'baseline' => $this->baseline,
            'nbrbeneficiaries' => $this->nbrbeneficiaries,
            'idCbType' => $this->idCbType,
            'idIndicator' => $this->idIndicator,
            'cost' => $this->cost,
            'grantedAmount' => $this->grantedAmount,
            'status' => $this->status,
            'datasource' => $this->datasource,
        ]);

        $query->andFilterWhere(['like', 'output', $this->output])
            ->andFilterWhere(['like', 'challenge', $this->challenge])
            ->andFilterWhere(['like', 'actionDescription', $this->actionDescription])
            ->andFilterWhere(['like', 'generalNeeds', $this->generalNeeds])
            ->andFilterWhere(['like', 'specificNeeds', $this->specificNeeds])
            ->andFilterWhere(['like', 'quarters', $this->quarters])            
            ->andFilterWhere(['like', 'subLevel', $this->idSubLevel]) //from 
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'stakeholders', $this->stakeholders])
            ->andFilterWhere(['like', 'programs.idProgram', $this->stakeholders])
            ->andFilterWhere(['like', 'funders', $this->funders])
            ->andFilterWhere(['like', 'submittedon', $this->submittedon])
            ->andFilterWhere(['like', 'instit_sectors.sectName', $this->idInstitSector])
            ->andFilterWhere(['like', 'institutions.institName', $this->idInstit])
            ->andFilterWhere(['like', 'subprograms.subProgramName', $this->idSubProgram]);

        return $dataProvider;
    }

    
    public function searchApprovedPlan($params)
    {
        if(Yii::$app->user->can('institution')){
            $query = Plans::find()->where('idInstit=:u',['u'=>Yii::$app->user->identity->userFrom])
                                   ->andWhere('status = 2')->orderBy('idInstitSector'); 
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith('idSubLevel0');
        //$query->joinWith('idSubLevel0');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idplan' => $this->idplan,
            'idInstit' => $this->idInstit,
            'idSubProgram' => $this->idSubProgram,
            'baseline' => $this->baseline,
            'nbrbeneficiaries' => $this->nbrbeneficiaries,
            'idCbType' => $this->idCbType,
            'idIndicator' => $this->idIndicator,
            'cost' => $this->cost,
            'grantedAmount' => $this->grantedAmount,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'output', $this->output])
            ->andFilterWhere(['like', 'challenge', $this->challenge])
            ->andFilterWhere(['like', 'actionDescription', $this->actionDescription])
            ->andFilterWhere(['like', 'generalNeeds', $this->generalNeeds])
            ->andFilterWhere(['like', 'specificNeeds', $this->specificNeeds])
            ->andFilterWhere(['like', 'quarters', $this->quarters])            
            ->andFilterWhere(['like', 'subLevel', $this->idSubLevel]) //from 
            ->andFilterWhere(['like', 'responsable', $this->responsable])
            ->andFilterWhere(['like', 'stakeholders', $this->stakeholders])
            ->andFilterWhere(['like', 'funders', $this->funders])
            ->andFilterWhere(['like', 'submittedon', $this->submittedon]);

        return $dataProvider;
    }
}
