<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Subprograms;

/**
 * SubprogramsSearch represents the model behind the search form about `backend\models\Subprograms`.
 */
class SubprogramsSearch extends Subprograms
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idSubProgram', 'idProgram'], 'integer'],
            [['subProgramName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subprograms::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idSubProgram' => $this->idSubProgram,
            'idProgram' => $this->idProgram,
        ]);

        $query->andFilterWhere(['like', 'subProgramName', $this->subProgramName]);

        return $dataProvider;
    }
}
