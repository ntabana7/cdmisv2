<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%funders}}".
 *
 * @property integer $idFunder
 * @property string $funderName
 */
class Funders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%funders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['funderName'], 'required'],
            [['funderName'], 'string', 'max' => 100],
            [['funderName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFunder' => Yii::t('app', 'Id Funder'),
            'funderName' => Yii::t('app', 'Funder'),
        ];
    }
}
