<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%programs}}".
 *
 * @property integer $idProgram
 * @property integer $idInstitSector
 * @property string $programName
 * @property string $status
 *
 * @property InstitSectors $idInstitSector0
 * @property Subprograms[] $subprograms
 */
class Programs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%programs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInstitSector', 'programName', 'status'], 'required'],
            [['idInstitSector'], 'integer'],
            [['status'], 'string'],
            [['programName'], 'string', 'max' => 200],
            [['programName'], 'unique'],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProgram' => Yii::t('app', 'Id Program'),
            'idInstitSector' => Yii::t('app', 'Sector'),
            'programName' => Yii::t('app', 'Program'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubprograms()
    {
        return $this->hasMany(Subprograms::className(), ['idProgram' => 'idProgram']);
    }
}
