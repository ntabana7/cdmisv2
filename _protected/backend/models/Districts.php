<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%districts}}".
 *
 * @property integer $idDistrict
 * @property integer $idProvince
 * @property string $distName
 *
 * @property Provinces $idProvince0
 * @property Institutions[] $institutions
 */
class Districts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%districts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvince', 'distName'], 'required'],
            [['idProvince'], 'integer'],
            [['distName'], 'string', 'max' => 100],
            [['distName'], 'unique'],
            [['idProvince'], 'exist', 'skipOnError' => true, 'targetClass' => Provinces::className(), 'targetAttribute' => ['idProvince' => 'idProvince']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDistrict' => Yii::t('app', 'Id District'),
            'idProvince' => Yii::t('app', 'Province'),
            'distName' => Yii::t('app', 'District Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvince0()
    {
        return $this->hasOne(Provinces::className(), ['idProvince' => 'idProvince']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutions()
    {
        return $this->hasMany(Institutions::className(), ['idDistrict' => 'idDistrict']);
    }
}
