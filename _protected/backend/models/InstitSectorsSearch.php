<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\InstitSectors;

/**
 * InstitSectorsSearch represents the model behind the search form about `backend\models\InstitSectors`.
 */
class InstitSectorsSearch extends InstitSectors
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInstitSector'], 'integer'],
            [['sectName','idPriority'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InstitSectors::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith('idPriority0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInstitSector' => $this->idInstitSector,
            // 'idPriority' => $this->idPriority,
        ]);

        $query->andFilterWhere(['like', 'sectName', $this->sectName])
              ->andFilterWhere(['like', 'priorityName', $this->idPriority]);

        return $dataProvider;
    }
}
