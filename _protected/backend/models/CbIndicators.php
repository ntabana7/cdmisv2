<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cb_indicators".
 *
 * @property integer $idIndicator
 * @property integer $idSubLevel
 * @property string $indicator
 *
 * @property CbSublevels $idSubLevel0
 * @property Plans[] $plans
 */
class CbIndicators extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cb_indicators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idSubLevel', 'indicator'], 'required'],
            [['idSubLevel'], 'integer'],
            [['indicator'], 'string', 'max' => 100],
            [['indicator'], 'unique'],
            [['idSubLevel'], 'exist', 'skipOnError' => true, 'targetClass' => CbSublevels::className(), 'targetAttribute' => ['idSubLevel' => 'idSubLevel']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idIndicator' => Yii::t('app', 'Id Indicator'),
            'idSubLevel' => Yii::t('app', 'CB Sub level')
,            'indicator' => Yii::t('app', 'Indicator'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubLevel0()
    {
        return $this->hasOne(CbSublevels::className(), ['idSubLevel' => 'idSubLevel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['idIndicator' => 'idIndicator']);
    }
}
