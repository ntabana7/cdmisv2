<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Provinces;

/**
 * ProvincesSearch represents the model behind the search form about `backend\models\Provinces`.
 */
class ProvincesSearch extends Provinces
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvince'], 'integer'],
            [['provName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Provinces::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idProvince' => $this->idProvince,
        ]);

        $query->andFilterWhere(['like', 'provName', $this->provName]);

        return $dataProvider;
    }
}
