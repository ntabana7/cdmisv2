<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%implementationreports}}".
 *
 * @property integer $idImplementation
 * @property integer $idplan
 * @property integer $Quarter
 * @property integer $nbrbeneficiaries
 * @property integer $female
 * @property integer $male
 * @property string $createdon
 * @property string $comment
 * @property string $file
 *
 * @property Plans $idplan0
 */
class Implementationreports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%implementationreports}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quarter', 'nbrbeneficiaries', 'female', 'male', 'comment', 'file'], 'required'],
            [['idplan', 'Quarter', 'nbrbeneficiaries', 'female', 'male'], 'integer'],
            [['createdon'], 'safe'],
            [['comment'], 'string'],
            [['file'], 'string', 'max' => 200],
            [['idplan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['idplan' => 'idplan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idImplementation' => Yii::t('app', 'Id Implementation'),
            'idplan' => Yii::t('app', 'Idplan'),
            'Quarter' => Yii::t('app', 'Quarter'),
            'nbrbeneficiaries' => Yii::t('app', 'Nbrbeneficiaries'),
            'female' => Yii::t('app', 'Female'),
            'male' => Yii::t('app', 'Male'),
            'createdon' => Yii::t('app', 'Createdon'),
            'comment' => Yii::t('app', 'Comment'),
            'file' => Yii::t('app', 'File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdplan0()
    {
        return $this->hasOne(Plans::className(), ['idplan' => 'idplan']);
    }
}
