<?php

namespace backend\models;

use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use \backend\modules\cna\models\QuestionOptions;
/**
 * Account form
 */
class challengeForm
{
    const SIZE = '100%';


    private static function inputText($form, $model, $name){
        return $form->field($model, $name)->textInput(['style'=> 'width:'.static::SIZE, ])->label();
    }

    private static function inputTextHidden($form, $model, $name){
        
            return $form->field($model, $name)->textInput(
                    [ 
                        'style'=> 'display:none;width:'.static::SIZE,
                        'id' => 'challenge_hidden',
                        'placeholder' => 'specify:',
                        'value' => 'Yes'
                    ])->label(false);
    }

    private static function inputTextMultiple($model,$addDependentAnswer){
        
        return  \kartik\tabs\TabsX::widget([
                    'items' => $addDependentAnswer,
                    'position' => \kartik\tabs\TabsX::POS_ABOVE,
                    'encodeLabels' => false,
                    'id' => 'challenge',
                    'pluginOptions' => [
                        'bordered' => true,
                        'sideways' => true,
                        'enableCache' => false,
                    ],
                    'headerOptions' => ['style' => 'display:block']
                ]);
    }

    private static function dropDownList($form, $model, $name){
        $questions = ArrayHelper::map(QuestionOptions::getQuestionOptions($model->questions->id), 'question_option', 'question_option');
        if($model->action_status == 0) {
            $questions = array_merge(['' => 'Select'],$questions);
        }

        return $form->field($model, $name)
                ->dropDownList(
                $questions,
                [
                    'style'=> 'width:'.static::SIZE,
                    'id' => 'assessment',
                    'required' => false,
                    'onchange'=>' 
                        document.getElementById("challenge_hidden").style.display = "none";
                        document.getElementById("challenge_hidden").value = "";

                        var other = "Other";
                        if($(this).val().toLowerCase() == other.toLowerCase()){
                            document.getElementById("challenge_hidden").style.display = "block";
                        }
                    '
                ])
                ->label();  
    }

    private static function textarea($form, $model, $name ){
        return $form->field($model, $name)
                ->textInput(
                    [ 
                        'style'=>'width:'.static::SIZE, 
                    ])
                ->label();
    }

    private static function radioList($form, $model,$name ){
        $questions = ArrayHelper::map(QuestionOptions::getQuestionOptions($model->questions->id), 'question_option', 'question_option');
        if($model->challenge_status == 0) {
            $questions = array_merge(['' => 'Select'],$questions);
        }

        return $form->field($model, $name)
                ->dropDownList(
                $questions,
                [
                    'style'=>'width:'.static::SIZE,
                    'onchange'=>' 

                        var dependent_question_needed = '.$model->questions->challenge_dependent_question_needed.';
                        var condition_answer = "'.$model->questions->challengeDependentQuestion->condition_answer.'";
                        var dependent_question = "'.$model->questions->dependentQuestion->dependent_question.'";

                        document.getElementById("dependent_question").innerHTML = "";
                        document.getElementById("add_challenge_dependent_answer_'.$model->id.'").style.display = "none";
                        var extent = "To some extent";

                        if(dependent_question_needed){                     
                            if($(this).val().toLowerCase() == condition_answer.toLowerCase() || $(this).val().toLowerCase() == extent.toLowerCase()){
                                document.getElementById("dependent_question").innerHTML = dependent_question;
                                document.getElementById("add_challenge_dependent_answer_'.$model->id.'").style.display = "block";
                            }   
                        }
                    '
                ])
                ->label();  
                
    }

    private static function checkboxList($form, $model, $name){
        return $form->field($model, $name)
                ->dropDownList(
                ArrayHelper::map(QuestionOptions::getQuestionOptions($model->id), 'question_option', 'question_option'),
                [
                    'style'=>'width:'.static::SIZE,
                    'prompt'=>$placeholder,
                    //($question->required_question) ? 'required' : ''
                ])
                ->label();  
    }

    public static function assessment($form, $model, $challenge, $addDependentAnswer = null){

        switch ($model->questions->challengeType->type) {

            case 'inputtext':
                echo static::inputText($form, $model, $challenge);
            break;
            
            case 'select':
                echo static::dropDownList($form ,$model, $challenge );
                echo static::inputTextHidden($form, $model, 'other_challenge');
            break;
            
            case 'textarea':
                echo static::textarea($form, $model, $challenge);
            break;
            
            case 'radio':
                echo static::radioList($form, $model, $challenge);
                echo '<div style="display:none" id="add_challenge_dependent_answer_'.$model->id.'">';
                echo '<span id="dependent_question"></span>';
                echo static::inputTextMultiple($model, $addDependentAnswer);
                echo '</div>';
            break;
            
            case 'checkbox':
                echo static::checkboxList($form, $model, $challenge);
            break;
            
            case 'addmore':
                echo static::inputTextHidden($form, $model, $challenge);
                echo static::inputTextMultiple($model, $addDependentAnswer);
            break;
            
            default:
                echo 'This type is not supported '.$model->questions->inputtype0->type;
                break;
            }
    }

}
?>