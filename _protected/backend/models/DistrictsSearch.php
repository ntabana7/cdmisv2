<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Districts;
use backend\models\Provinces;

/**
 * DistrictsSearch represents the model behind the search form about `backend\models\Districts`.
 */
class DistrictsSearch extends Districts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDistrict'], 'integer'],
            [['distName','idProvince'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Districts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables Provinces and Districts
        $query->joinWith('idProvince0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idDistrict' => $this->idDistrict,
        ]);

        $query->andFilterWhere(['like', 'distName', $this->distName])
              ->andFilterWhere(['like', 'provName', $this->idProvince]);

        return $dataProvider;
    }
}
