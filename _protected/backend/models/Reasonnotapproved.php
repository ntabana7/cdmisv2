<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%reasonnotapproved}}".
 *
 * @property integer $idreason
 * @property string $reason
 */
class Reasonnotapproved extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reasonnotapproved}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason'], 'required'],
            [['reason'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idreason' => Yii::t('app', 'Idreason'),
            'reason' => Yii::t('app', 'Reason'),
        ];
    }
}
