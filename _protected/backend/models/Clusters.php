<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%clusters}}".
 *
 * @property integer $idCluster
 * @property string $clusterName
 * @property string $title
 * @property string $phone
 * @property string $email
 *
 * @property Institutions[] $institutions
 */
class Clusters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%clusters}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clusterName', 'title', 'phone', 'email'], 'required'],
            [['clusterName', 'title', 'email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCluster' => Yii::t('app', 'Id Cluster'),
            'clusterName' => Yii::t('app', 'Cluster Name'),
            'title' => Yii::t('app', 'Title'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutions()
    {
        return $this->hasMany(Institutions::className(), ['idCluster' => 'idCluster']);
    }
}
