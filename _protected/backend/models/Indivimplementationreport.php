<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%indivimplementationreport}}".
 *
 * @property integer $idIndivreport
 * @property integer $idplan
 * @property string $name
 * @property string $position
 * @property string $gender
 * @property string $periodoftraining
 * @property string $reportedquarter
 * @property double $amountspent
 * @property string $placeoftraining
 * @property string $createdon
 *
 * @property Plans $idplan0
 */
class Indivimplementationreport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%indivimplementationreport}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idplan', 'name', 'position', 'gender', 'periodoftraining', 'reportedquarter', 'amountspent', 'placeoftraining'], 'required'],
            [['idplan','id'], 'integer'],
            [['amountspent'], 'number'],
            [['createdon'], 'safe'],
            [['name', 'position', 'periodoftraining', 'reportedquarter'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 6],
            [['placeoftraining'], 'string', 'max' => 200],
            [['idplan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['idplan' => 'idplan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idIndivreport' => Yii::t('app', 'Id Indivreport'),
            'idplan' => Yii::t('app', 'Idplan'),
            'name' => Yii::t('app', 'Name'),
            'position' => Yii::t('app', 'Position'),
            'gender' => Yii::t('app', 'Gender'),
            'periodoftraining' => Yii::t('app', 'Period of training'),
            'reportedquarter' => Yii::t('app', 'Reported quarter'),
            'amountspent' => Yii::t('app', 'Amount spent'),
            'placeoftraining' => Yii::t('app', 'Place of training'),
            'createdon' => Yii::t('app', 'Created on'),
            'id' => Yii::t('app', 'Created by'),
        ];
    }

    public function getGenderArray()
    {
        $genderArray = [
            '' => Yii::t('app','Select Gender'),
            'male'     => Yii::t('app','Male'),
            'female' => Yii::t('app','Female'),
        ];

        return $genderArray;
    }

    public function getQuarterArray()
    {
        $quarterArray = [
            '' => Yii::t('app','Select Quarter'),
            'Quarter 1'     => Yii::t('app','Quarter 1'),
            'Quarter 2' => Yii::t('app','Quarter 2'),
            'Quarter 3'     => Yii::t('app','Quarter 3'),
            'Quarter 4' => Yii::t('app','Quarter 4'),
        ];

        return $quarterArray;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdplan0()
    {
        return $this->hasOne(Plans::className(), ['idplan' => 'idplan']);
    }

    public function getIduser0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
