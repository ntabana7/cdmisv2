<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%orginstimplementationreport}}".
 *
 * @property integer $idorginst
 * @property integer $idplan
 * @property double $amountspent
 * @property string $createdon
 *
 * @property Plans $idplan0
 */
class Orginstimplementationreport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orginstimplementationreport}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idplan', 'amountspent','reportedquarter'], 'required'],
            [['idplan','id'], 'integer'],
            [['amountspent'], 'number'],
            [['createdon'], 'safe'],
            [['idplan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['idplan' => 'idplan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idorginst' => Yii::t('app', 'Id'),
            'idplan' => Yii::t('app', 'Idplan'),
            'reportedquarter' => Yii::t('app', 'Reported quarter'),
            'amountspent' => Yii::t('app', 'Amount spent'),
            'createdon' => Yii::t('app', 'Created on'),
            'id' => Yii::t('app', 'Created by'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdplan0()
    {
        return $this->hasOne(Plans::className(), ['idplan' => 'idplan']);
    }
    public function getIduser0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    public function getQuarterArray()
    {
        $quarterArray = [
            '' => Yii::t('app','Select Quarter'),
            'Quarter 1'     => Yii::t('app','Quarter 1'),
            'Quarter 2' => Yii::t('app','Quarter 2'),
            'Quarter 3'     => Yii::t('app','Quarter 3'),
            'Quarter 4' => Yii::t('app','Quarter 4'),
        ];

        return $quarterArray;
    }
}
