<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%instit_sectors}}".
 *
 * @property integer $idInstitSector
 * @property integer $idPriority
 * @property string $sectName
 *
 * @property PrioritySectors $idPriority0
 * @property Institutions[] $institutions
 * @property Programs[] $programs
 */
class InstitSectors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%instit_sectors}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPriority', 'sectName'], 'required'],
            [['idPriority'], 'integer'],
            [['sectName'], 'string', 'max' => 100],
            [['sectName'], 'unique'],
            [['idPriority'], 'exist', 'skipOnError' => true, 'targetClass' => PrioritySectors::className(), 'targetAttribute' => ['idPriority' => 'idPriority']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInstitSector' => Yii::t('app', 'Id Instit Sector'),
            'idPriority' => Yii::t('app', 'Priority'),
            'sectName' => Yii::t('app', 'Sector'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPriority0()
    {
        return $this->hasOne(PrioritySectors::className(), ['idPriority' => 'idPriority']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutions()
    {
        return $this->hasMany(Institutions::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrograms()
    {
        return $this->hasMany(Programs::className(), ['idInstitSector' => 'idInstitSector']);
    }

    static public function sectors(){

                               
        return InstitSectors::find()->where(['IN', 'idInstitSector',Funderbysector::find()->select('idInstitSector')->where('idFunder=:u',['u'=>Yii::$app->user->identity->userFrom])])->all();
    }

    static public function sectorss(){

                               
        return InstitSectors::find()->where(['IN','idInstitSector',Institutions::find()->select('idInstitSector')->distinct()->where(['IN','idInstit',Institutions::find()->select('idInstit')->distinct()->where('idCluster=:u',['u'=>Yii::$app->user->identity->userFrom])])])->all();
    }
}


