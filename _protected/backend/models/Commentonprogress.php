<?php

namespace backend\models;

use Yii;
use common\models\User;


/**
 * This is the model class for table "{{%commentonprogress}}".
 *
 * @property integer $idcomment
 * @property integer $idplan
 * @property string $reportedquarter
 * @property string $comment
 * @property string $finalreport
 * @property string $createdon
 * @property integer $id
 *
 * @property Plans $idplan0
 */
class Commentonprogress extends \yii\db\ActiveRecord
{

    public $file;
    public $institName;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%commentonprogress}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idplan', 'reportedquarter', 'comment', 'finalreport', 'id'], 'required'],
            [['idplan', 'id'], 'integer'],
            [['comment'], 'string'],
            [['createdon'], 'safe'],
            [['reportedquarter'], 'string', 'max' => 100],
            [['finalreport'], 'string', 'max' => 200],
            [['idplan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['idplan' => 'idplan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcomment' => Yii::t('app', 'Idcomment'),
            'idplan' => Yii::t('app', 'Idplan'),
            'reportedquarter' => Yii::t('app', 'Reported quarter'),
            'comment' => Yii::t('app', 'Justification'),
            'finalreport' => Yii::t('app', 'Final report'),
            'createdon' => Yii::t('app', 'Created on'),
            'id' => Yii::t('app', 'Created by'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdplan0()
    {
        return $this->hasOne(Plans::className(), ['idplan' => 'idplan']);
    }

    public function getIduser0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    public function getQuarterArray()
    {
        $quarterArray = [
            '' => Yii::t('app','Select Quarter'),
            'Quarter 1'     => Yii::t('app','Quarter 1'),
            'Quarter 2' => Yii::t('app','Quarter 2'),
            'Quarter 3'     => Yii::t('app','Quarter 3'),
            'Quarter 4' => Yii::t('app','Quarter 4'),
        ];

        return $quarterArray;
    }
}
