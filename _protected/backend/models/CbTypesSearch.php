<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CbTypes;

/**
 * CbTypesSearch represents the model behind the search form about `backend\models\CbTypes`.
 */
class CbTypesSearch extends CbTypes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCbType'], 'integer'],
            [['cbType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CbTypes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCbType' => $this->idCbType,
        ]);

        $query->andFilterWhere(['like', 'cbType', $this->cbType]);

        return $dataProvider;
    }
}
