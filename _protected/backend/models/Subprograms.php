<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%subprograms}}".
 *
 * @property integer $idSubProgram
 * @property integer $idProgram
 * @property string $subProgramName
 *
 * @property Plans[] $plans
 * @property Programs $idProgram0
 */
class Subprograms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subprograms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProgram', 'subProgramName'], 'required'],
            [['idProgram'], 'integer'],
            [['subProgramName'], 'string', 'max' => 200],
            [['subProgramName'], 'unique'],
            [['idProgram'], 'exist', 'skipOnError' => true, 'targetClass' => Programs::className(), 'targetAttribute' => ['idProgram' => 'idProgram']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idSubProgram' => Yii::t('app', 'Id Sub Program'),
            'idProgram' => Yii::t('app', 'Program'),
            'subProgramName' => Yii::t('app', 'Sub Program'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['idSubProgram' => 'idSubProgram']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProgram0()
    {
        return $this->hasOne(Programs::className(), ['idProgram' => 'idProgram']);
    }
}
