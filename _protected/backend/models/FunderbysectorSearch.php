<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Funderbysector;

/**
 * FunderbysectorSearch represents the model behind the search form about `backend\models\Funderbysector`.
 */
class FunderbysectorSearch extends Funderbysector
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idFunderbysector', 'idFunder', 'idInstitSector'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Funderbysector::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idFunderbysector' => $this->idFunderbysector,
            'idFunder' => $this->idFunder,
            'idInstitSector' => $this->idInstitSector,
        ]);

        return $dataProvider;
    }
}
