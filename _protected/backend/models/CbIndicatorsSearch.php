<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CbIndicators;

/**
 * CbIndicatorsSearch represents the model behind the search form about `backend\models\CbIndicators`.
 */
class CbIndicatorsSearch extends CbIndicators
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idIndicator'], 'integer'],
            [['idSubLevel','indicator'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(yii::$app->user->can('theCreator'))
            $query = CbIndicators::find();
        else
            $query = CbIndicators::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // join tables cbIndicators and sublevels
        $query->joinWith('idSubLevel0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idIndicator' => $this->idIndicator,
            
        ]);
                // Added the last line 

        $query->andFilterWhere(['like', 'indicator', $this->indicator])
                ->andFilterWhere(['like', 'cb_sublevels.subLevel', $this->idSubLevel]);

        return $dataProvider;
    }
}
