<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Institutions;

/**
 * InstitutionsSearch represents the model behind the search form about `backend\models\Institutions`.
 */
class InstitutionsSearch extends Institutions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInstit', 'idInstitSector', 'idInstitType', 'idDistrict', 'idCluster'], 'integer'],
            [['institName', 'focalpointName', 'FocalPersonalPosition', 'phone', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Institutions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInstit' => $this->idInstit,
            'idInstitSector' => $this->idInstitSector,
            'idInstitType' => $this->idInstitType,
            'idDistrict' => $this->idDistrict,
            'idCluster' => $this->idCluster,
        ]);

        $query->andFilterWhere(['like', 'institName', $this->institName])
            ->andFilterWhere(['like', 'focalpointName', $this->focalpointName])
            ->andFilterWhere(['like', 'FocalPersonalPosition', $this->FocalPersonalPosition])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
