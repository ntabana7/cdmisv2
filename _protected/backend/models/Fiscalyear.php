<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%fiscalyear}}".
 *
 * @property integer $idFiscalyear
 * @property string $startdate
 * @property string $enddate
 */
class Fiscalyear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fiscalyear}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startdate', 'enddate'], 'required'],
            [['startdate', 'enddate'], 'safe'],
        ];
    }

    public function fiscalYears(){
        $years = [
        "fiscalyear" =>"2016/07/01 - 2017/06/30"
        ];

        return $years;
    }
    public function checkValidity($submittedDate){
        /*$count = $this->find()->where("".date('Y-m-d',strtotime($submittedDate)) ." between startdate and enddate")->orderBy('idFiscalyear DESC')->limit(1)->count();*/
       // var_dump($this->fiscalYears());die;
        foreach($this->fiscalYears() as $year){
            $yearArray = explode('-',$year);
            if(strtotime($submittedDate) >= strtotime($yearArray[0]) and strtotime($submittedDate) <= strtotime($yearArray[1]) ){
                return 1;
            }
        }

        return 0;
           
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFiscalyear' => Yii::t('app', 'Id Fiscalyear'),
            'startdate' => Yii::t('app', 'Startdate'),
            'enddate' => Yii::t('app', 'Enddate'),
        ];
    }
}
