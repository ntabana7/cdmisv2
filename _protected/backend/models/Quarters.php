<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quarters}}".
 *
 * @property integer $idquarter
 * @property string $quarter
 * @property string $period
 *
 * @property Commentonprogress[] $commentonprogresses
 * @property Indivimplementationreport[] $indivimplementationreports
 * @property Orginstimplementationreport[] $orginstimplementationreports
 */
class Quarters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%quarters}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quarter', 'period'], 'required'],
            [['quarter'], 'string', 'max' => 10],
            [['period'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idquarter' => Yii::t('app', 'Idquarter'),
            'quarter' => Yii::t('app', 'Quarter'),
            'period' => Yii::t('app', 'Period'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentonprogresses()
    {
        return $this->hasMany(Commentonprogress::className(), ['idquarter' => 'idquarter']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndivimplementationreports()
    {
        return $this->hasMany(Indivimplementationreport::className(), ['idquarter' => 'idquarter']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrginstimplementationreports()
    {
        return $this->hasMany(Orginstimplementationreport::className(), ['idquarter' => 'idquarter']);
    }
}
