<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%funderbyinstitution}}".
 *
 * @property integer $idFunderbyinstitution
 * @property integer $id
 * @property integer $idInstit
 *
 * @property Institutions $idInstit0
 */
class Funderbyinstitution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%funderbyinstitution}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idInstit'], 'required'],
            [['id', 'idInstit'], 'integer'],
            [['idInstit'], 'exist', 'skipOnError' => true, 'targetClass' => Institutions::className(), 'targetAttribute' => ['idInstit' => 'idInstit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFunderbyinstitution' => Yii::t('app', 'Id Funderbyinstitution'),
            'id' => Yii::t('app', 'ID'),
            'idInstit' => Yii::t('app', 'Id Instit'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstit0()
    {
        return $this->hasOne(Institutions::className(), ['idInstit' => 'idInstit']);
    }
    public function getIdUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
