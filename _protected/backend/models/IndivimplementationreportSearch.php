<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Indivimplementationreport;

/**
 * IndivimplementationreportSearch represents the model behind the search form about `backend\models\Indivimplementationreport`.
 */
class IndivimplementationreportSearch extends Indivimplementationreport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idIndivreport', 'idplan'], 'integer'],
            [['name', 'position', 'gender', 'periodoftraining', 'reportedquarter', 'placeoftraining', 'createdon'], 'safe'],
            [['amountspent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Indivimplementationreport::find();

        //added this code to limit people to see their own data on implementation report
        $query->joinWith('idplan0');
        //End added this code to limit people to see their own data on implementation report

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idIndivreport' => $this->idIndivreport,
            'idplan' => $this->idplan,
            'amountspent' => $this->amountspent,
            'createdon' => $this->createdon,

            //added this code to limit people to see their own data on implementation report
            'idInstit' => Yii::$app->user->identity->userFrom,
            //End added this code to limit people to see their own data on implementation report
        ]);


        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'periodoftraining', $this->periodoftraining])
            ->andFilterWhere(['like', 'reportedquarter', $this->reportedquarter])
            ->andFilterWhere(['like', 'placeoftraining', $this->placeoftraining]);

        return $dataProvider;
    }
}
