<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%cb_types}}".
 *
 * @property integer $idCbType
 * @property string $cbType
 *
 * @property Plans[] $plans
 */
class CbTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cb_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cbType'], 'required'],
            [['cbType'], 'string', 'max' => 25],
            [['cbType'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCbType' => Yii::t('app', 'Id Cb Type'),
            'cbType' => Yii::t('app', 'Cb Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['idCbType' => 'idCbType']);
    }
}
