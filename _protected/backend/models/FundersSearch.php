<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Funders;

/**
 * FundersSearch represents the model behind the search form about `backend\models\Funders`.
 */
class FundersSearch extends Funders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idFunder'], 'integer'],
            [['funderName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Funders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idFunder' => $this->idFunder,
        ]);

        $query->andFilterWhere(['like', 'funderName', $this->funderName]);

        return $dataProvider;
    }
}
