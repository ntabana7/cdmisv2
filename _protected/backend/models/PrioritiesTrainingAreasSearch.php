<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PrioritiesTrainingAreas;

/**
 * PrioritiesTrainingAreasSearch represents the model behind the search form about `backend\models\PrioritiesTrainingAreas`.
 */
class PrioritiesTrainingAreasSearch extends PrioritiesTrainingAreas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTraining'], 'integer'],
            [['priorityName', 'idSkills', 'idInstitSector', 'idCbType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PrioritiesTrainingAreas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // join tables cbIndicators and sublevels
        $query->joinWith('idSkills0')
              ->joinWith('idInstitSector0')
              ->joinWith('idCbType0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTraining' => $this->idTraining,
            // 'idSkills' => $this->idSkills,
            // 'idInstitSector' => $this->idInstitSector,
            // 'idCbType' => $this->idCbType,
        ]);

        $query->andFilterWhere(['like', 'priorityName', $this->priorityName])
              ->andFilterWhere(['like', 'skills.skills', $this->idSkills])
              ->andFilterWhere(['like', 'instit_sectors.sectName', $this->idInstitSector])
              ->andFilterWhere(['like', 'cb_types.cbType', $this->idCbType]);

        return $dataProvider;
    }
}
