<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%priorities_training_areas}}".
 *
 * @property integer $idTraining
 * @property integer $idSkills
 * @property integer $idInstitSector
 * @property integer $idCbType
 * @property string $priorityName
 *
 * @property Skills $idSkills0
 * @property InstitSectors $idInstitSector0
 * @property CbTypes $idCbType0
 */
class PrioritiesTrainingAreas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%priorities_training_areas}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idSkills', 'idInstitSector', 'idCbType', 'priorityName'], 'required'],
            [['idSkills', 'idInstitSector', 'idCbType'], 'integer'],
            [['priorityName'], 'string', 'max' => 200],
            [['idSkills'], 'exist', 'skipOnError' => true, 'targetClass' => Skills::className(), 'targetAttribute' => ['idSkills' => 'idSkills']],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
            [['idCbType'], 'exist', 'skipOnError' => true, 'targetClass' => CbTypes::className(), 'targetAttribute' => ['idCbType' => 'idCbType']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTraining' => Yii::t('app', 'Id Training'),
            'idSkills' => Yii::t('app', 'Skills type'),
            'idInstitSector' => Yii::t('app', 'Sector'),
            'idCbType' => Yii::t('app', 'Cb Type'),
            'priorityName' => Yii::t('app', 'Priority Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSkills0()
    {
        return $this->hasOne(Skills::className(), ['idSkills' => 'idSkills']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCbType0()
    {
        return $this->hasOne(CbTypes::className(), ['idCbType' => 'idCbType']);
    }
}
