<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TrainingAreas;

/**
 * TrainingAreasSearch represents the model behind the search form about `backend\models\TrainingAreas`.
 */
class TrainingAreasSearch extends TrainingAreas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTraining'], 'integer'],
            [['idSkills','idInstitSector','courseName', 'idCbType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    // public function search($params)
    // {
    //     $query = TrainingAreas::find();

    //     $dataProvider = new ActiveDataProvider([
    //         'query' => $query,
    //     ]);

    //     $this->load($params);

    //     if (!$this->validate()) {
    //         // uncomment the following line if you do not want to return any records when validation fails
    //         // $query->where('0=1');
    //         return $dataProvider;
    //     }

    //     $query->andFilterWhere([
    //         'idTraining' => $this->idTraining,
    //         'idSkills' => $this->idSkills,
    //         'idInstitSector' => $this->idInstitSector,
    //         'idCbType' => $this->idCbType,
    //     ]);

    //     $query->andFilterWhere(['like', 'courseName', $this->courseName]);

    //     return $dataProvider;
    // }

    public function search($params)
    {
        // if(yii::$app->user->can('theCreator'))
        //     $query = TrainingAreas::find();
        // else
        //     $query = TrainingAreas::find();
        $query = TrainingAreas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idInstitSector0');
        $this->load($params);
        

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTraining' => $this->idTraining,
            'idSkills' => $this->idSkills,
            'idInstitSector' => $this->idInstitSector,
            'idCbType' => $this->idCbType,
        ]);
        // Added the last line 

        $query->andFilterWhere(['like', 'courseName', $this->courseName])
              ->andFilterWhere(['like', 'instit_sectors.sectName', $this->idInstitSector]);

        return $dataProvider;
    }
}
