<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Stakeholders;

/**
 * StakeholdersSearch represents the model behind the search form about `backend\models\Stakeholders`.
 */
class StakeholdersSearch extends Stakeholders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idStakeholder'], 'integer'],
            [['StakeholderName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stakeholders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idStakeholder' => $this->idStakeholder,
        ]);

        $query->andFilterWhere(['like', 'StakeholderName', $this->StakeholderName]);

        return $dataProvider;
    }
}
