<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "skills".
 *
 * @property integer $idSkills
 * @property string $skills
 *
 * @property TrainingAreas[] $trainingAreas
 */
class Skills extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skills'], 'required'],
            [['skills'], 'string', 'max' => 100],
            [['skills'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idSkills' => Yii::t('app', 'Id Skills'),
            'skills' => Yii::t('app', 'Skills'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingAreas()
    {
        return $this->hasMany(TrainingAreas::className(), ['idSkills' => 'idSkills']);
    }
}
