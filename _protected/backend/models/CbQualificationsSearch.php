<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CbQualifications;

/**
 * CbQualificationsSearch represents the model behind the search form about `backend\models\CbQualifications`.
 */
class CbQualificationsSearch extends CbQualifications
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idQualif'], 'integer'],
            [['qualif'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CbQualifications::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idQualif' => $this->idQualif,
        ]);

        $query->andFilterWhere(['like', 'qualif', $this->qualif]);

        return $dataProvider;
    }
}
