<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cb_qualifications".
 *
 * @property integer $idQualif
 * @property string $qualif
 */
class CbQualifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cb_qualifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['qualif'], 'required'],
            [['qualif'], 'string', 'max' => 100],
            [['qualif'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idQualif' => Yii::t('app', 'Id Qualif'),
            'qualif' => Yii::t('app', 'Qualification'),
        ];
    }
}
