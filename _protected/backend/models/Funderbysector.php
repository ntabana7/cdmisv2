<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%funderbysector}}".
 *
 * @property integer $idFunderbysector
 * @property integer $idFunder
 * @property integer $idInstitSector
 *
 * @property InstitSectors $idInstitSector0
 * @property Funders $idFunder0
 */
class Funderbysector extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%funderbysector}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idFunder', 'idInstitSector'], 'required'],
            [['idFunder', 'idInstitSector'], 'integer'],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
            [['idFunder'], 'exist', 'skipOnError' => true, 'targetClass' => Funders::className(), 'targetAttribute' => ['idFunder' => 'idFunder']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFunderbysector' => Yii::t('app', 'Id Funderbysector'),
            'idFunder' => Yii::t('app', 'Id Funder'),
            'idInstitSector' => Yii::t('app', 'Id Instit Sector'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFunder0()
    {
        return $this->hasOne(Funders::className(), ['idFunder' => 'idFunder']);
    }
}
