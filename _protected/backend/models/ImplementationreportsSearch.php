<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Implementationreports;

/**
 * ImplementationreportsSearch represents the model behind the search form about `backend\models\Implementationreports`.
 */
class ImplementationreportsSearch extends Implementationreports
{
    public function rules()
    {
        return [
            [['idImplementation', 'idplan', 'Quarter', 'nbrbeneficiaries', 'female', 'male'], 'integer'],
            [['createdon', 'comment', 'file'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Implementationreports::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idImplementation' => $this->idImplementation,
            'idplan' => $this->idplan,
            'Quarter' => $this->Quarter,
            'nbrbeneficiaries' => $this->nbrbeneficiaries,
            'female' => $this->female,
            'male' => $this->male,
            'createdon' => $this->createdon,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'file', $this->file]);

        return $dataProvider;
    }
}
