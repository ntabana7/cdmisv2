<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Funderbyinstitution;

/**
 * FunderbyinstitutionSearch represents the model behind the search form about `backend\models\Funderbyinstitution`.
 */
class FunderbyinstitutionSearch extends Funderbyinstitution
{
    public function rules()
    {
        return [
            [['idFunderbyinstitution', 'id', 'idInstit'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Funderbyinstitution::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idFunderbyinstitution' => $this->idFunderbyinstitution,
            'id' => $this->id,
            'idInstit' => $this->idInstit,
        ]);

        return $dataProvider;
    }
}
