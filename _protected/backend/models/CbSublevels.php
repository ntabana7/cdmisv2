<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%cb_sublevels}}".
 *
 * @property integer $idSubLevel
 * @property integer $idLevel
 * @property string $subLevel
 *
 * @property CbIndicators[] $cbIndicators
 * @property CbLevels $idLevel0
 * @property Plans[] $plans
 */
class CbSublevels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cb_sublevels}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idLevel', 'subLevel'], 'required'],
            [['idLevel'], 'integer'],
            [['subLevel'], 'string', 'max' => 100],
            [['subLevel'], 'unique'],
            [['idLevel'], 'exist', 'skipOnError' => true, 'targetClass' => CbLevels::className(), 'targetAttribute' => ['idLevel' => 'idLevel']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idSubLevel' => Yii::t('app', 'Id Sub Level'),
            'idLevel' => Yii::t('app', 'CB Level'),
            'subLevel' => Yii::t('app', 'CB Sub Level'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCbIndicators()
    {
        return $this->hasMany(CbIndicators::className(), ['idSubLevel' => 'idSubLevel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLevel0()
    {
        return $this->hasOne(CbLevels::className(), ['idLevel' => 'idLevel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['idSubLevel' => 'idSubLevel']);
    }
}
