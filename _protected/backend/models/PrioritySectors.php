<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%priority_sectors}}".
 *
 * @property integer $idPriority
 * @property string $priorityName
 *
 * @property InstitSectors[] $institSectors
 */
class PrioritySectors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%priority_sectors}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priorityName'], 'required'],
            [['priorityName'], 'string', 'max' => 100],
            [['priorityName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPriority' => Yii::t('app', 'Id Priority'),
            'priorityName' => Yii::t('app', 'Priority Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitSectors()
    {
        return $this->hasMany(InstitSectors::className(), ['idPriority' => 'idPriority']);
    }
}
