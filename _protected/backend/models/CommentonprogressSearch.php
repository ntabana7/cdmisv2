<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Commentonprogress;

/**
 * CommentonprogressSearch represents the model behind the search form about `backend\models\Commentonprogress`.
 */
class CommentonprogressSearch extends Commentonprogress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcomment', 'idplan', 'id'], 'integer'],
            [['reportedquarter', 'comment', 'finalreport', 'createdon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Commentonprogress::find();

        //added this code to limit people to see their own data on implementation report
        $query->joinWith('idplan0');
        //End added this code to limit people to see their own data on implementation report

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idcomment' => $this->idcomment,
            'idplan' => $this->idplan,
            'createdon' => $this->createdon,
            
            //added this code to limit people to see their own data on implementation report
            'idInstit' => Yii::$app->user->identity->userFrom,
            //End added this code to limit people to see their own data on implementation report
        ]);

        $query->andFilterWhere(['like', 'reportedquarter', $this->reportedquarter])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'finalreport', $this->finalreport]);

        return $dataProvider;
    }
}
