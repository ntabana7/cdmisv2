<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%stakeholders}}".
 *
 * @property integer $idStakeholder
 * @property string $StakeholderName
 */
class Stakeholders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stakeholders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['StakeholderName'], 'required'],
            [['StakeholderName'], 'string', 'max' => 100],
            [['StakeholderName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idStakeholder' => Yii::t('app', 'Id Stakeholder'),
            'StakeholderName' => Yii::t('app', 'Stakeholder'),
        ];
    }
}
