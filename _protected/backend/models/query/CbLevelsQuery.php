<?php

namespace backend\models\query;

/**
 * This is the ActiveQuery class for [[\backend\models\query\CbLevels]].
 *
 * @see \backend\models\query\CbLevels
 */
class CbLevelsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\query\CbLevels[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\query\CbLevels|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function isInstitutional(){
        $this->andWhere(['slug'=> 'institutional']);
        return $this;
    }

    public function isOrganisational(){
        $this->andWhere(['slug'=> 'organisational']);
        return $this;
    }

    public function isIndividual(){
        $this->andWhere(['slug'=> 'individual']);
        return $this;
    }
}
