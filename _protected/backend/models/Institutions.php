<?php

namespace backend\models;

use Yii;
use backend\modules\cna\models\base\Assessment;
/**
 * This is the model class for table "{{%institutions}}".
 *
 * @property integer $idInstit
 * @property integer $idInstitSector
 * @property string $institName
 * @property integer $idInstitType
 * @property integer $idDistrict
 * @property string $focalpointName
 * @property string $FocalPersonalPosition
 * @property string $phone
 * @property string $email
  * @property string $approver_email
 * @property integer $idCluster
 *
 * @property InstitSectors $idInstitSector0
 * @property InstitTypes $idInstitType0
 * @property Districts $idDistrict0
 * @property Clusters $idCluster0
 * @property Plans[] $plans
 */
class Institutions extends \yii\db\ActiveRecord
{
    public $instititional  = 3;
    public $organisational = 2;
    public $individual     = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%institutions}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInstitSector', 'institName', 'idInstitType', 'idDistrict', 'focalpointName', 'FocalPersonalPosition', 'phone', 'email', 'idCluster'], 'required'],
            [['idInstitSector', 'idInstitType', 'idDistrict', 'idCluster','organisational','organisational','individual'], 'integer'],
            [['institName', 'focalpointName', 'FocalPersonalPosition', 'phone', 'email','approver_email'], 'string', 'max' => 100],
            [['institName'], 'unique'],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
            [['idInstitType'], 'exist', 'skipOnError' => true, 'targetClass' => InstitTypes::className(), 'targetAttribute' => ['idInstitType' => 'idInstitType']],
            [['idDistrict'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['idDistrict' => 'idDistrict']],
            [['idCluster'], 'exist', 'skipOnError' => true, 'targetClass' => Clusters::className(), 'targetAttribute' => ['idCluster' => 'idCluster']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInstit' => Yii::t('app', 'Id Instit'),
            'idInstitSector' => Yii::t('app', 'Sector'),
            'institName' => Yii::t('app', 'Institution Name'),
            'idInstitType' => Yii::t('app', 'Institution Type'),
            'idDistrict' => Yii::t('app', 'District'),
            'focalpointName' => Yii::t('app', 'Focalpoint Name'),
            'FocalPersonalPosition' => Yii::t('app', 'Focal Personal Position'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'approver_email' => Yii::t('app', 'Approver Email'),
            'idCluster' => Yii::t('app', 'Cluster'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitType0()
    {
        return $this->hasOne(InstitTypes::className(), ['idInstitType' => 'idInstitType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDistrict0()
    {
        return $this->hasOne(Districts::className(), ['idDistrict' => 'idDistrict']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCluster0()
    {
        return $this->hasOne(Clusters::className(), ['idCluster' => 'idCluster']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['idInstit' => 'idInstit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessment()
    {
        return $this->hasMany(Assessment::className(), ['idInstit' => 'institution_id']);
    }

    public static function InstitutionName($id){
        return static::find()->where(['idInstit' => $id])->one()->institName;
    }

    public static function institutionEmail(){
        $email = static::find()->where(['idInstit' => Yii::$app->user->identity->userFrom])->one()->email;
        return (!empty($email))? $email: Yii::$app->user->identity->email;
    }

    public static function institutionApproverEmail(){
        return static::find()->where(['idInstit' => Yii::$app->user->identity->userFrom])->one()->approver_email;
    }

    public static function clusterEmail(){
        $idCluster = static::find()->where(['idInstit' => Yii::$app->user->identity->userFrom])->one()->idCluster;
        return \backend\models\Clusters::find()->where(['idCluster' => $idCluster])->one()->email;
    }
    
}
