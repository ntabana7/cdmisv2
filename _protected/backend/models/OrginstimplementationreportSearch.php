<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Orginstimplementationreport;

/**
 * OrginstimplementationreportSearch represents the model behind the search form about `backend\models\Orginstimplementationreport`.
 */
class OrginstimplementationreportSearch extends Orginstimplementationreport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idorginst', 'idplan'], 'integer'],
            [['amountspent'], 'number'],
            [['createdon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orginstimplementationreport::find();
        $query->joinWith('idplan0');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idorginst' => $this->idorginst,
            'idplan' => $this->idplan,
            'amountspent' => $this->amountspent,
            'createdon' => $this->createdon,
            'idInstit' => Yii::$app->user->identity->userFrom,
        ]);

        return $dataProvider;
    }
}
