<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%fiscal_years}}".
 *
 * @property integer $id
 * @property integer $fiscalyear
 * @property integer $current
 */
class FiscalYears extends \yii\db\ActiveRecord
{   
    /**
     * @var $fiscalYearStartMonth string - represents the month the fiscal year begins in, MM format
     */
    protected static $fiscalYearStartMonth = "07";

    /**
     * @var $fiscalYearStartDay string - represents the day the fiscal year begins on, DD format
     */
    protected static $fiscalYearStartDay = "01";

    /**
     * @var $fiscalYearEndMonth string - represents the month the fiscal year end in, MM format
     */
    protected static $fiscalYearEndMonth = "06";

    /**
     * @var $fiscalYearEndDay string - represents the day the fiscal year ends on, DD format
     */
    protected static $fiscalYearEndDay = "30";


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fiscal_years}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fiscalyear'], 'required'],
            [['fiscalyear', 'current'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fiscalyear' => Yii::t('app', 'Fiscalyear'),
            'current' => Yii::t('app', 'Current'),
        ];
    }

    /**
     * @inheritdoc
     * @return FiscalYearsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FiscalYearsQuery(get_called_class());
    }

     /**
     * the current year public static function
     * Returns the start date of any Fiscal Year in YYYY format
     * Defaults to the current system's Fiscal Year
     *
     * @param $year integer | null
     * @return string
     */
    public static function getCurrentYear(){
        return FiscalYears::find()->active()->cache(7200)->one()->fiscalyear;
    }

    /**
     * startDate() public static function
     * Returns the start date of any Fiscal Year in YYYY-MM-DD format
     * Defaults to the current system's Fiscal Year
     *
     * @param $year integer | null
     * @return string
     */
    public static function startDate($year=null)
    { 
        if(is_null($year)){
            $year = FiscalYears::getCurrentYear();
        }
        return $year ."-". FiscalYears::startMonthDay("-");
    }

    /**
     * endDate() public static function
     * Returns the end date of any Fiscal Year in YYYY-MM-DD format
     * Defaults to the current system's Fiscal Year
     *
     * @param $year integer | null
     * @return string
     */
    public static function endDate($year=null)
    {
        if(is_null($year)){
            $year = FiscalYears::getCurrentYear();
        }
        return ($year + 1) ."-". FiscalYears::endMonthDay("-");
    }

    /**
     * startMonthDay() public static method
     * Takes a deliminator, defaults to "", returns the starting month and day in MMDD format
     * deliminator if passed will deliminate the month and day
     *
     * @param string $deliminator
     * @return string
     */
    public static function startMonthDay($deliminator="")
    {
        return FiscalYears::$fiscalYearStartMonth .$deliminator. FiscalYears::$fiscalYearStartDay;
    }

    /**
     * endMonthDay() public static method
     * Takes a deliminator, defaults to "", returns the ending month and day in MMDD format
     * deliminator if passed will deliminate the month and day
     *
     * @param string $deliminator
     * @return string
     */
    public static function endMonthDay($deliminator="")
    {
        return FiscalYears::$fiscalYearEndMonth .$deliminator. FiscalYears::$fiscalYearEndDay;
    }

    /**
     * checkFiscalYearHasPassed() protected static method
     * Determines if we have passed the last date in the set fiscal year.
     *
     * @return bool
     */
    protected static function checkFiscalYearHasPassed()
    {
        $now = strtotime(date("Y-m-d"));
        $lastDateOfFiscalYear = strtotime(FiscalYears::endDate());

        if($now > $lastDateOfFiscalYear){
            return true;
        }
        return false;
    }
}
