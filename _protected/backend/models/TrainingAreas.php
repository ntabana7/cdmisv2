<?php

namespace backend\models;

use Yii;
use backend\modules\cdproviders\models\ProviderTrainingareasbyprovider;

/**
 * This is the model class for table "training_areas".
 *
 * @property integer $idTraining
 * @property integer $idSkills
 * @property integer $idInstitSector
 * @property integer $idCbType
 * @property string $courseName
 *
 * @property Skills $idSkills0
 * @property InstitSectors $idInstitSector0
 * @property CbTypes $idCbType0
 */
class TrainingAreas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idSkills', 'idInstitSector', 'idCbType', 'courseName'], 'required'],
            [['idSkills', 'idInstitSector', 'idCbType'], 'integer'],
            [['courseName'], 'string', 'max' => 100],
            [['idSkills'], 'exist', 'skipOnError' => true, 'targetClass' => Skills::className(), 'targetAttribute' => ['idSkills' => 'idSkills']],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
            [['idCbType'], 'exist', 'skipOnError' => true, 'targetClass' => CbTypes::className(), 'targetAttribute' => ['idCbType' => 'idCbType']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTraining' => Yii::t('app', 'Id Training'),
            'idSkills' => Yii::t('app', 'Skills'),
            'idInstitSector' => Yii::t('app', 'Sector'),
            'idCbType' => Yii::t('app', 'Cb Type'),
            'courseName' => Yii::t('app', 'Training area'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSkills0()
    {
        return $this->hasOne(Skills::className(), ['idSkills' => 'idSkills']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCbType0()
    {
        return $this->hasOne(CbTypes::className(), ['idCbType' => 'idCbType']);
    }

    static public function trainings()

    {

        return TrainingAreas::find()->where(['IN','idTraining',ProviderTrainingareasbyprovider::find()->select('idTraining')->distinct()->where(['IN','idProvider',ProviderTrainingareasbyprovider::find()->select('idProvider')->distinct()->where('idProvider=:u',['u'=>Yii::$app->user->identity->userFrom])])])->all();
        
    }


    
}
