<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%provinces}}".
 *
 * @property integer $idProvince
 * @property string $provName
 *
 * @property Districts[] $districts
 */
class Provinces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provinces}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provName'], 'required'],
            [['provName'], 'string', 'max' => 100],
            [['provName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProvince' => Yii::t('app', 'Id Province'),
            'provName' => Yii::t('app', 'Prov Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(Districts::className(), ['idProvince' => 'idProvince']);
    }
}
