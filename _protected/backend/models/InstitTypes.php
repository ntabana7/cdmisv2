<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%instit_types}}".
 *
 * @property integer $idInstitType
 * @property string $institType
 *
 * @property Institutions[] $institutions
 */
class InstitTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%instit_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['institType'], 'required'],
            [['institType'], 'string', 'max' => 100],
            [['institType'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInstitType' => Yii::t('app', 'Id Instit Type'),
            'institType' => Yii::t('app', 'Institution Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutions()
    {
        return $this->hasMany(Institutions::className(), ['idInstitType' => 'idInstitType']);
    }
}
