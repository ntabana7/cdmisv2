<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%dpbyinstitution}}".
 *
 * @property integer $iddpbyinstitution
 * @property integer $id
 * @property integer $idInstit
 *
 * @property Institutions $idInstit0
 * @property User $id0
 */
class Dpbyinstitution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dpbyinstitution}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idInstit'], 'required'],
            [['id', 'idInstit'], 'integer'],
            [['idInstit'], 'exist', 'skipOnError' => true, 'targetClass' => Institutions::className(), 'targetAttribute' => ['idInstit' => 'idInstit']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iddpbyinstitution' => Yii::t('app', 'Iddpbyinstitution'),
            'id' => Yii::t('app', 'ID'),
            'idInstit' => Yii::t('app', 'Id Instit'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstit0()
    {
        return $this->hasOne(Institutions::className(), ['idInstit' => 'idInstit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
