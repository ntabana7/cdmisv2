<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%cb_levels}}".
 *
 * @property integer $idLevel
 * @property string $level
 * @property string $slug
 * @property CbSublevels[] $cbSublevels
 */
class CbLevels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cb_levels}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level'], 'required'],
            [['level','slug'], 'string', 'max' => 25],
            [['level'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLevel' => Yii::t('app', 'Id Level'),
            'level' => Yii::t('app', 'CB Level'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCbSublevels()
    {
        return $this->hasMany(CbSublevels::className(), ['idLevel' => 'idLevel']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\query\LevelsQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \backend\models\query\CbLevelsQuery(get_called_class());
        return $query->where(['cb_levels.deleted_by' => 0])->cache(7200);
    }
}
