<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipBank */
?>
<div class="internship-bank-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idBank',
            'bank',
        ],
    ]) ?>

</div>
