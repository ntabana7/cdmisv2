<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipBank */
?>
<div class="internship-bank-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
