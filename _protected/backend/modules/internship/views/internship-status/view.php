<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipStatus */
?>
<div class="internship-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idStatus',
            'status',
        ],
    ]) ?>

</div>
