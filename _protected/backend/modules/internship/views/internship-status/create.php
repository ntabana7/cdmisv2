<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipStatus */

?>
<div class="internship-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
