<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\Internshipinternapplication $model
 */

// $this->title = $model->idInternapplication;
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Internshipinternapplications'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="internshipinternapplication-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            // 'idInternapplication',
            'nida',
            'firstname',
            'secondname',
            'email:email',
            'telephone',
            'dob',
            [
            'attribute'=>'idGender',
            'value'=>$model->idGender0->gender,
            ],
            [
            'attribute'=>'idQualif',
            'value'=>$model->idQualif0->qualif,
            ],
            [
            'attribute'=>'idQualifarea',
            'value'=>$model->idQualifarea0->area,
            ],
            [
            'attribute'=>'idGrade',
            'value'=>$model->idGrade0->grade,
            ],
            'degree',
            [
            'attribute'=>'idUniversity',
            'value'=>$model->idUniversity0->university,
            ],
            'yearofgraduation',
            'accountNbr',
            [
            'attribute'=>'idBank',
            'value'=>$model->idBank0->bank,
            ],
            'idDistrict',
            // [
            // 'attribute'=>'idDistrict',
            // 'value'=>$model->idDistrict0->distName,
            // ],
            [
                'attribute' => 'applicationdate',
                'format' => [
                    'datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime']))
                        ? Yii::$app->modules['datecontrol']['displaySettings']['datetime']
                        : 'd-m-Y H:i:s A'
                ],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATETIME
                ]
            ],
            // [
            //     'attribute' => 'validatingdate',
            //     'format' => [
            //         'datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime']))
            //             ? Yii::$app->modules['datecontrol']['displaySettings']['datetime']
            //             : 'd-m-Y H:i:s A'
            //     ],
            //     'type' => DetailView::INPUT_WIDGET,
            //     'widgetOptions' => [
            //         'class' => DateControl::classname(),
            //         'type' => DateControl::FORMAT_DATETIME
            //     ]
            // ],
            // 'iduser',
            [
            'attribute'=>'idInstit',
            'value'=>$model->idInstit0->institName,
            ],
            [
                'attribute' => 'placementdate',
                'format' => [
                    'datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime']))
                        ? Yii::$app->modules['datecontrol']['displaySettings']['datetime']
                        : 'd-m-Y H:i:s A'
                ],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATETIME
                ]
            ],
            // 'iduserplacement',
            'status',
            [
            'attribute'=>'idStatus',
            'value'=>$model->idStatus0->status,
            ],
            'reasonnotfinishing',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->idInternapplication],
        ],
        'enableEditMode' => true,
    ]) ?>

</div>
