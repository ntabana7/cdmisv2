<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\Internshipinternapplication $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Internshipinternapplication',
]) . ' ' . $model->idInternapplication;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Internshipinternapplications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idInternapplication, 'url' => ['view', 'id' => $model->idInternapplication]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="internshipinternapplication-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
