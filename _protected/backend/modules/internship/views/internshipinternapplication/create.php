<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\Internshipinternapplication $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Internshipinternapplication',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Internshipinternapplications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internshipinternapplication-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
