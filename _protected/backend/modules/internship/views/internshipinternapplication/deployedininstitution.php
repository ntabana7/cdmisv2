<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\editable\Editable;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\modules\internship\models\InternshipinternapplicationSearch $searchModel
 */

// $this->title = Yii::t('app', 'Internshipinternapplications');
// $this->params['breadcrumbs'][] = $this->title;
$institName=Yii::$app->user->identity->username;
// echo $institName;die;
echo "<h2>Deployed interns</h2>" ;
?>
<div class="internshipinternapplication-index">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Internshipinternapplication',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'idInternapplication',            
            'firstname',
            'secondname',
            'nida',
            'email:email',
           'telephone', 
//            'dob', 
//            'idGender', 
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idQualif',
            'value'=>'idQualif0.qualif',
            ],
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idQualifarea',
            'value'=>'idQualifarea0.area',
            ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'idGrade',
            // 'value'=>'idGrade0.grade',
            // ], 
//            'degree', 
//            'idUniversity', 
//            'yearofgraduation', 
//            'accountNbr', 
//            'idBank', 
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'idDistrict',
            // 'value'=>'idDistrict0.distName',
            // ], 
//            ['attribute' => 'applicationdate','format' => ['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            ['attribute' => 'validatingdate','format' => ['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            'iduser', 
//            'idInstit', 
            'status',
            // ['attribute' => 'placementdate','format' => ['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 

            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'placementdate',
            ],
//            'iduserplacement',


            // [
            // 'class'=>'\kartik\grid\EditableColumn',
            // 'attribute'=>'status', 
            // 'width'=>'100px',
            // 'editableOptions' => function($oModel) {
            //   return [
            //     'inputType' => Editable::INPUT_DROPDOWN_LIST,
            //     'data' => ["waitingList" => 'Waiting list',"Notavailable" => 'Not available',"Deployed" => 'Deployed'],
            //     'displayValueConfig'=> [
            //         'waitingList' => '<i class="glyphicon glyphicon-thumbs-up"></i> Waiting list',
            //         'Notavailable' => '<i class="glyphicon glyphicon-hourglass"></i> Not available',
            //         'Deployed' => '<i class="glyphicon glyphicon-hourglass"></i> Deployed',               
            //         ],
            //       ];
            //   }

            // ],
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idInstit',
            'value'=>'idInstit0.institName',
            ],
            
//            'idStatus', 
//            'reasonnotfinishing', 

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            Yii::$app->urlManager->createUrl(['internship/internshipinternapplication/view', 'id' => $model->idInternapplication, 'edit' => 't']),
                            ['title' => Yii::t('yii', 'Edit'),]
                        );
                    }
                ],
            ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => true,

        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type' => 'info',
            // 'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),
            // 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
    ]); Pjax::end(); ?>

</div>
