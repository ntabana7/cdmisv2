<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\modules\internship\models\InternshipinternapplicationSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="internshipinternapplication-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idInternapplication') ?>

    <?= $form->field($model, 'nida') ?>

    <?= $form->field($model, 'firstname') ?>

    <?= $form->field($model, 'secondname') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'dob') ?>

    <?php // echo $form->field($model, 'idGender') ?>

    <?php // echo $form->field($model, 'idQualif') ?>

    <?php // echo $form->field($model, 'idQualifarea') ?>

    <?php // echo $form->field($model, 'idGrade') ?>

    <?php // echo $form->field($model, 'degree') ?>

    <?php // echo $form->field($model, 'idUniversity') ?>

    <?php // echo $form->field($model, 'yearofgraduation') ?>

    <?php // echo $form->field($model, 'accountNbr') ?>

    <?php // echo $form->field($model, 'idBank') ?>

    <?php // echo $form->field($model, 'idDistrict') ?>

    <?php // echo $form->field($model, 'idHome') ?>

    <?php // echo $form->field($model, 'applicationdate') ?>

    <?php // echo $form->field($model, 'validatingdate') ?>

    <?php // echo $form->field($model, 'iduser') ?>

    <?php // echo $form->field($model, 'idInstit') ?>

    <?php // echo $form->field($model, 'placementdate') ?>

    <?php // echo $form->field($model, 'startingdate') ?>

    <?php // echo $form->field($model, 'endingdate') ?>

    <?php // echo $form->field($model, 'iduserplacement') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'idStatus') ?>

    <?php // echo $form->field($model, 'reasonnotfinishing') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
