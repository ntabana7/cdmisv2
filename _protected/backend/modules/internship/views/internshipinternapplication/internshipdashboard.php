<?php

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
?>

<!-- Dashboard -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <!-- Dashboard -->
        <small></small>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <b>Status as per<?php echo " today on " . date("d/m/Y") . "<br>";?></b>
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-person-add"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Applied Interns</span>
              <span class="info-box-number"><?= count($appliedinterns); ?><small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion-person-stalker"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Interns on pending list</span>
              <span class="info-box-number"><?= count($internsonpendinglist); ?></span>
              <a href="internshipofficerindex" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion-person-stalker"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Interns on waiting list</span>
              <span class="info-box-number"><?= count($internsonwaitinglist); ?></span>
              <a href="validatedinterns" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion-person-stalker"></i></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Deployed interns</span>
              <span class="info-box-number"><?= count($deployedinterns); ?></span>
              <a href="deployedinterns" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><b>CHARTS</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>


            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
              <div class="col-md-3">
                <section class="">
              <!-- Custom tabs (Charts with tabs)-->
              <?php
                echo Highcharts::widget([
                   'options' => [
                      'title' => ['text' => 'Gender'],
                      'xAxis' => [
                         'categories' => ['Female', 'Male']
                      ],
                      'yAxis' => [
                         'title' => ['text' => 'Number of interns depending on gender']
                      ],
                      'series' => [
                         [
                            'type' => 'column',
                            'name' => 'Gender',
                            'data' => [
                                [
                                  'name' => 'Female',
                                  'y' => $female,
                                ],
                                [
                                  'name' => 'Male',
                                  'y' => $male,
                                ],
                            ],
                            'size' => 200,
                            'dataLabels' => [
                                'enabled' => true,
                            ],
                          ],
                      ]
                   ]
                ]);
              ?>
              <!-- /.nav-tabs-custom -->
            </section>                  
                </div>
                <div class="col-md-3">
                <section class="">
              <!-- Custom tabs (Charts with tabs)-->
              <?php
                echo Highcharts::widget([
                   'options' => [
                      'title' => ['text' => 'Education level'],
                      'xAxis' => [
                         'categories' => ['Masters', 'Bachelor', 'Engineering','Diploma','Technicians','Artisans']
                      ],
                      'yAxis' => [
                         'title' => ['text' => 'Interns qualifications']
                      ],
                      'series' => [
                         [
                            'type' => 'column',
                            'name' => 'Interns qualification',
                            'data' => [
                                [
                                  'name' => 'Masters',
                                  'y' => $masters,
                                ],
                                [
                                  'name' => 'Bachelors',
                                  'y' => $bachelors,
                                ],
                                [
                                  'name' => 'Engineering',
                                  'y' => $engineers,
                                ],
                                [
                                  'name' => 'Diploma',
                                  'y' => $diploma,
                                ],
                                [
                                  'name' => 'Technicians',
                                  'y' => $technicians,
                                ],
                                [
                                  'name' => 'Artisans',
                                  'y' => $artisans,
                                ],
                            ],
                            'size' => 300,
                            'dataLabels' => [
                                'enabled' => true,
                            ],
                          ],
                      ]
                   ]
                ]);
              ?>
              <!-- /.nav-tabs-custom -->
            </section>                  
                </div>
                <div class="col-md-3">
                <section class="">
              <!-- Custom tabs (Charts with tabs)-->
              <?php
                echo Highcharts::widget([
                   'options' => [
                      'title' => ['text' => 'Sectors contribution'],
                      'xAxis' => [
                         'categories' => ['Public', 'Private', 'Civil Society']
                      ],
                      'yAxis' => [
                         'title' => ['text' => 'Number of job seekers']
                      ],
                      'series' => [
                         [
                            'type' => 'pie',
                            'name' => 'Sector',
                            'data' => [
                                [
                                  'name' => 'Public',
                                  'y' => 65,
                                ],
                                [
                                  'name' => 'Private',
                                  'y' => 30,
                                ],
                                [
                                  'name' => 'Civil Society',
                                  'y' => 5,
                                ],
                            ],
                            'size' => 130,
                            'dataLabels' => [
                                'enabled' => true,
                            ],
                          ],
                      ]
                   ]
                ]);
              ?>
              <!-- /.nav-tabs-custom -->
            </section>
            </div> 
                <div class="col-md-3">
                <section class="">
              <!-- Custom tabs (Charts with tabs)-->
              <?php
                echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'Combination chart',
        ],
        'xAxis' => [
            'categories' => ['2013', '2014', '2015', '2016', '2017'],
        ],
        'labels' => [
            'items' => [
                [
                    'html' => 'Sector contribution',
                    'style' => [
                        'left' => '50px',
                        'top' => '18px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'Agriculture',
                'data' => [3, 2, 1, 3, 4],
            ],
            [
                'type' => 'column',
                'name' => 'ICT',
                'data' => [2, 3, 5, 7, 6],
            ],
            [
                'type' => 'column',
                'name' => 'Health',
                'data' => [4, 3, 3, 9, 0],
            ],
            [
                'type' => 'spline',
                'name' => 'Average',
                'data' => [3, 2.67, 3, 6.33, 3.33],
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                    'fillColor' => 'white',
                ],
            ],
            [
                'type' => 'pie',
                'name' => 'Total consumption',
                'data' => [
                    [
                        'name' => 'Agriculture',
                        'y' => 13,
                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                    ],
                    [
                        'name' => 'ICT',
                        'y' => 23,
                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                    ],
                    [
                        'name' => 'Health',
                        'y' => 19,
                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
                    ],
                ],
                'center' => [1000, 1500],
                'size' => 100,
                'showInLegend' => false,
                'dataLabels' => [
                    'enabled' => false,
                ],
            ],
        ],
    ]
]);
              ?>
              <!-- /.nav-tabs-custom -->
            </section>
                                  
                </div>                               
            <!-- /.col -->
              </div>
              <!-- /.row -->
              
              <div class="row">
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <h3><center><?= count($nbrofinstitutionusingapp); ?></center></h3>
                  <div class="knob-label">Number of Institutions using the system so far</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                <h3><center><?=$nbrofinternsrequested;?></center></h3> 
                  <div class="knob-label">Number of interns requested by Institutions so far</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center">
                  <h3><center><?=$nbrofinternsprovided; ?></center></h3> 
                  <div class="knob-label">Number of interns requested by Institutions provided</div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            
            </div>
            <!-- ./box-body -->


            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-black"><i class="fa fa-caret-up"></i> 100%</span>
                    <h5 class="description-header text-black"><?= count($requestfrominstitutions); ?></h5>
                    <span class="description-text text-black">TOTAL REQUESTS FROM INSTITUTIONS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-red"><i class="fa fa-caret-up"></i><?php echo round($pendingrequestpercentage,1); ?>%</span>
                    <h5 class="description-header text-red"><?= count($requestpending); ?></h5>
                    <span class="description-text text-red">PENDING REQUEST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i><?php echo round($inprogressquestpercentage,1); ?>%</span>
                    <h5 class="description-header text-yellow"><?= count($requestinprogress); ?></h5>
                    <span class="description-text text-yellow">IN PROGRESS REQUEST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i><?php echo round($donequestpercentage,1); ?>%</span>
                    <h5 class="description-header text-green"><?= count($requestdone); ?></h5>
                    <span class="description-text text-green">COMPLETED REQUEST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Main row -->

      <!-- /.row -->
    </section>
    <!-- /.content -->
 <!--  </div>