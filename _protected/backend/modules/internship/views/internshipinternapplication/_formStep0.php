    <!-- Start step 0 -->
        <div class="tab-pane active" role="tabpanel" id="step0">
        <div class="step0">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-"></i></h4></div>
        <!-- envelope -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 1, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsaccount[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'username',
                    'email',
                    'userFrom',
                    'password',
                    'status',


                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsaccount as $i => $modelaccount): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Create username and password</h3>
                        <div class="pull-right">
                            <button type="button" class=""><i class=""></i></button>
                            <button type="button" class=""><i class=""></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelaccount->isNewRecord) {
                                echo Html::activeHiddenInput($modelaccount, "[{$i}]id");
                            }
                        ?>                        
                        <div class="row">
                            <div class="col-sm-6">
                            <?= $form->field($modelaccount, "[{$i}]username")->textInput(['maxlength' => true]) ?>   
                            </div>                       
                            <div class="col-sm-6">
                            <?= $form->field($modelaccount, "[{$i}]password_hash")->textInput(['maxlength' => true]) ?>
                             </div>
                        </div>                        
                        </div><!-- .row -->
 
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
                        </div>                       

        </div>
        <ul class="list-inline pull-right">
            <li><button type="button" class="btn btn-primary next-step">continue</button></li>
        </ul>                       
                    

        </div>
        <!-- End Start step 0 -->