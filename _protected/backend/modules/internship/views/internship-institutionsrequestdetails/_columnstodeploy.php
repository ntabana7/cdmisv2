<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idRequestdetails',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idRequest',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstit',
        'value'=>'idRequest0.idInstit0.institName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'applicationdate',
        'value'=>'idRequest0.applicationdate',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'requestednumber',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'suppliednumber',
    ],
    'status',
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idQualif',
        'value'=>'idQualif0.qualif',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idQualifarea',
        'value'=>'idQualifarea0.area',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idGrade',
        'value'=>'idGrade0.grade',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'workingplace',
    ],
    
    [
        'label'=>Yii::t('app','Deploy').' '.Yii::t('app','Interns'),
        'format' => 'raw',
        
        'value'=>function ($data) {
            return Html::a(Yii::t('app','Deploy requested interns'),['/internship/internshipinternapplication/deployinterns','idRequestdetails'=>$data["idRequestdetails"],'idRequest'=>$data["idRequest"],'idQualif'=>$data["idQualif"],'idQualifarea'=>$data["idQualifarea"],'idGrade'=>$data["idGrade"],'idInstit'=>$data->idRequest0->idInstit]);
        },
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   