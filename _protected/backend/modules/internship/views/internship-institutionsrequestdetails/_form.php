<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipInstitutionsrequestdetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="internship-institutionsrequestdetails-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idRequest')->textInput() ?>

    <?= $form->field($model, 'requestednumber')->textInput() ?>

    <?= $form->field($model, 'suppliednumber')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idQualif')->textInput() ?>

    <?= $form->field($model, 'idQualifarea')->textInput() ?>

    <?= $form->field($model, 'idGrade')->textInput() ?>

    <?= $form->field($model, 'workingplace')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
