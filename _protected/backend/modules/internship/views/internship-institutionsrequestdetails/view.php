<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipInstitutionsrequestdetails */
?>
<div class="internship-institutionsrequestdetails-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'idRequestdetails',
            // 'idRequest',
        'idRequest0.idInstit0.institName',
        'idRequest0.applicationdate',
        'requestednumber',
        'suppliednumber',
        'status',
        // 'idQualif',
        'idQualifarea0.area',
            'idGrade0.grade',
        'workingplace',
        ],
    ]) ?>

</div>
