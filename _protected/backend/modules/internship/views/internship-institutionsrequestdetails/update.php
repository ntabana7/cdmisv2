<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipInstitutionsrequestdetails */
?>
<div class="internship-institutionsrequestdetails-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
