<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipInstitutionsrequest */
?>
<div class="internship-institutionsrequest-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsrequestdetail'=>$modelsrequestdetail,
    ]) ?>

</div>
