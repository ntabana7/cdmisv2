<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use backend\modules\internship\models\InternshipGrade;
use backend\models\CbQualifications;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipInstitutionsrequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="internship-institutionsrequest-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'requestedfor')->textInput(['readonly' => true, 'value' => '6']) ?>


    <div class='row'>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-"></i>Interns request details</h4></div>
        <!-- envelope -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsrequestdetail[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'requestednumber',
                    'idQualifarea',
                    'idGrade',
                    'workingplace',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsrequestdetail as $i => $modelrequestdetail): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Interns request details</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelrequestdetail->isNewRecord) {
                                echo Html::activeHiddenInput($modelrequestdetail, "[{$i}]id");
                            }
                        ?>                        
                        <div class="row">
                            <div class="col-sm-6">
                            <?= $form->field($modelrequestdetail, "[{$i}]requestednumber")->textInput(['maxlength' => true]) ?>   
                            </div>                       
                            <div class="col-sm-6">
                            <?= $form->field($modelrequestdetail, "[{$i}]idQualif")->dropDownList(ArrayHelper::map(CbQualifications::find()->where(['IN', 'idQualif', [2,3,4,5,6,10,11]])->all(),'idQualif','qualif'),[ 'prompt'=>'Select qualification',
                            'language' => 'en',
                            ]);

                             ?>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                            <?= $form->field($modelrequestdetail, "[{$i}]idQualifarea")->dropDownList(ArrayHelper::map(ProviderQualificationarea::find()->all(),'idQualifarea','area'),[ 'prompt'=>'Select qualification area',
                            'language' => 'en',
                            ]);

                             ?>                                
                            </div>                        
                             <div class="col-sm-6">
                            <?= $form->field($modelrequestdetail, "[{$i}]idGrade")->dropDownList(ArrayHelper::map(InternshipGrade::find()->all(),'idGrade','grade'),[ 'prompt'=>'Select grade',
                            'language' => 'en',
                            ]);

                             ?>    
                            </div>
                            <div class="col-sm-6">
                            <?= $form->field($modelrequestdetail, "[{$i}]workingplace")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        </div><!-- .row -->
 
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>        
    </div> 
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
