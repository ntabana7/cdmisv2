<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipInstitutionsrequest */
?>
<div class="internship-institutionsrequest-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'idRequest',
            'idInstit0.institName',
            'applicationdate',
            'responsedate',
            'requestedfor',
            'status',
            'demandednumber',
            'suppliednumber',
        ],
    ]) ?>

</div>
