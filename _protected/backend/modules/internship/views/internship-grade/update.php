<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipGrade */
?>
<div class="internship-grade-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
