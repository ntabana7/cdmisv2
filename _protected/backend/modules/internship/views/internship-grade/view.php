<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipGrade */
?>
<div class="internship-grade-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idGrade',
            'grade',
        ],
    ]) ?>

</div>
