<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipGender */
?>
<div class="internship-gender-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idGender',
            'gender',
        ],
    ]) ?>

</div>
