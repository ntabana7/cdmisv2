<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipGender */

?>
<div class="internship-gender-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
