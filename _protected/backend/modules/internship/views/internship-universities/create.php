<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipUniversities */

?>
<div class="internship-universities-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
