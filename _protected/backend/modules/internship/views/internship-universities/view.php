<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\internship\models\InternshipUniversities */
?>
<div class="internship-universities-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idUniversity',
            'university',
        ],
    ]) ?>

</div>
