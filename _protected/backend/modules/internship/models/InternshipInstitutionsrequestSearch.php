<?php

namespace backend\modules\internship\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\internship\models\InternshipInstitutionsrequest;

/**
 * InternshipInstitutionsrequestSearch represents the model behind the search form about `backend\modules\internship\models\InternshipInstitutionsrequest`.
 */
class InternshipInstitutionsrequestSearch extends InternshipInstitutionsrequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRequest', 'idInstit', 'requestedfor', 'demandednumber', 'suppliednumber'], 'integer'],
            [['applicationdate', 'responsedate', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = InternshipInstitutionsrequest::find();
        if(Yii::$app->user->can('internshipofficer')){
            $query = InternshipInstitutionsrequest::find();
        }elseif(Yii::$app->user->can('institution')){
            $query = InternshipInstitutionsrequest::find()->where('idInstit=:u',['u'=>Yii::$app->user->identity->userFrom]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idRequest' => $this->idRequest,
            'idInstit' => $this->idInstit,
            'requestedfor' => $this->requestedfor,
            'demandednumber' => $this->demandednumber,
            'suppliednumber' => $this->suppliednumber,
        ]);

        $query->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'responsedate', $this->responsedate])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
