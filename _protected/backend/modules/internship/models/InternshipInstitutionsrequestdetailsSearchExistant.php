<?php

namespace backend\modules\internship\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\internship\models\InternshipInstitutionsrequestdetails;

/**
 * InternshipInstitutionsrequestdetailsSearch represents the model behind the search form about `backend\modules\internship\models\InternshipInstitutionsrequestdetails`.
 */
class InternshipInstitutionsrequestdetailsSearch extends InternshipInstitutionsrequestdetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRequestdetails', 'idRequest', 'requestednumber','suppliednumber'], 'integer'],
            [['workingplace','idQualif','idQualifarea', 'idGrade','idInstit','applicationdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->can('internshipofficer')){
            $query = InternshipInstitutionsrequestdetails::find();
        }elseif(Yii::$app->user->can('institution')){
            $query = InternshipInstitutionsrequestdetails::find()->where('idInstit=:u',['u'=>Yii::$app->user->identity->userFrom]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        // join tables cbIndicators and sublevels
        $query->joinWith('idRequest0')
              ->joinWith('idQualif0')
              ->joinWith('idQualifarea0')
              ->joinWith('idGrade0');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idRequestdetails' => $this->idRequestdetails,
            'idRequest' => $this->idRequest,
            'requestednumber' => $this->requestednumber,
        ]);

        $query->andFilterWhere(['like', 'workingplace', $this->workingplace])
              ->andFilterWhere(['like', 'qualif', $this->idQualif])
              ->andFilterWhere(['like', 'area', $this->idQualifarea])
              ->andFilterWhere(['like', 'grade', $this->idGrade])
              ->andFilterWhere(['like', 'institName.idInstitt', $this->idRequest])
              ->andFilterWhere(['like', 'applicationdate', $this->idRequest]);

        return $dataProvider;
    }
}
