<?php

namespace backend\modules\internship\models;

use Yii;
use backend\models\Institutions;

/**
 * This is the model class for table "{{%internship_institutionsrequest}}".
 *
 * @property integer $idRequest
 * @property integer $idInstit
 * @property string $applicationdate
 * @property string $responsedate
 * @property integer $requestedfor
 * @property string $status
 * @property integer $demandednumber
 * @property integer $suppliednumber
 *
 * @property Institutions $idInstit0
 * @property InternshipInstitutionsrequestdetails[] $internshipInstitutionsrequestdetails
 */
class InternshipInstitutionsrequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_institutionsrequest}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requestedfor'], 'required'],
            [['idInstit', 'requestedfor', 'demandednumber', 'suppliednumber'], 'integer'],
            [['applicationdate', 'responsedate'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 50],
            [['idInstit'], 'exist', 'skipOnError' => true, 'targetClass' => Institutions::className(), 'targetAttribute' => ['idInstit' => 'idInstit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRequest' => Yii::t('app', 'Id Request'),
            'idInstit' => Yii::t('app', 'Requesting Institution'),
            'applicationdate' => Yii::t('app', 'Application date'),
            'responsedate' => Yii::t('app', 'Response date'),
            'requestedfor' => Yii::t('app', 'Requested for how long?'),
            'status' => Yii::t('app', 'Status'),
            'demandednumber' => Yii::t('app', 'Requested number'),
            'suppliednumber' => Yii::t('app', 'Supplied number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstit0()
    {
        return $this->hasOne(Institutions::className(), ['idInstit' => 'idInstit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternshipInstitutionsrequestdetails()
    {
        return $this->hasMany(InternshipInstitutionsrequestdetails::className(), ['idRequest' => 'idRequest']);
    }
}
