<?php

namespace backend\modules\internship\models;

use Yii;

/**
 * This is the model class for table "{{%internship_universities}}".
 *
 * @property integer $idUniversity
 * @property string $university
 *
 * @property InternshipInternapplication[] $internshipInternapplications
 */
class InternshipUniversities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_universities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['university'], 'required'],
            [['university'], 'string', 'max' => 100],
            [['university'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUniversity' => Yii::t('app', 'Id University'),
            'university' => Yii::t('app', 'University'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternshipInternapplications()
    {
        return $this->hasMany(InternshipInternapplication::className(), ['idUniversity' => 'idUniversity']);
    }
}
