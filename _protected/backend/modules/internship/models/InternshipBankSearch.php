<?php

namespace backend\modules\internship\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\internship\models\InternshipBank;

/**
 * InternshipBankSearch represents the model behind the search form about `backend\modules\internship\models\InternshipBank`.
 */
class InternshipBankSearch extends InternshipBank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idBank'], 'integer'],
            [['bank'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InternshipBank::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'bank', $this->bank]);

        return $dataProvider;
    }
}
