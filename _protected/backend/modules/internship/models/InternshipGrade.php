<?php

namespace backend\modules\internship\models;

use Yii;

/**
 * This is the model class for table "{{%internship_grade}}".
 *
 * @property integer $idGrade
 * @property string $grade
 *
 * @property InternshipIternapplication[] $internshipIternapplications
 */
class InternshipGrade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_grade}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade'], 'required'],
            [['grade'], 'string', 'max' => 100],
            [['grade'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idGrade' => Yii::t('app', 'Id Grade'),
            'grade' => Yii::t('app', 'Grade'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternshipIternapplications()
    {
        return $this->hasMany(InternshipIternapplication::className(), ['idGrade' => 'idGrade']);
    }
}
