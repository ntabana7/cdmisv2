<?php

namespace backend\modules\internship\models;

use Yii;

/**
 * This is the model class for table "{{%internship_status}}".
 *
 * @property integer $idStatus
 * @property string $status
 */
class InternshipStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string', 'max' => 50],
            [['status'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idStatus' => Yii::t('app', 'Id Status'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
