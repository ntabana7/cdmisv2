<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "js_main_jobseekers".
 *
 * @property integer $pk_jobseeker
 * @property string $idcardno
 * @property string $fname
 * @property string $lname
 * @property string $dateofbirth
 * @property integer $fk_gender
 * @property string $fk_nationality
 * @property integer $fk_maritalstatus
 * @property integer $fk_disabled
 * @property integer $fk_disability
 * @property integer $fk_province
 * @property integer $fk_district
 * @property integer $fk_geosector
 * @property string $phone
 * @property string $email
 * @property string $photo
 * @property string $photoname
 * @property string $phototype
 * @property integer $photosize
 * @property string $whodidit
 * @property string $whencreated
 * @property string $whenlastupdated
 * @property integer $fk_active
 *
 * @property CenterRelAchievements[] $centerRelAchievements
 * @property SGender $fkGender
 * @property SCountrycodeIso3166 $fkNationality
 * @property SNoYes $fkDisabled
 * @property SNoYes $fkActive
 * @property SSectors $fkGeosector
 * @property SDistricts $fkDistrict
 * @property SDisability $fkDisability
 * @property JsRelCv[] $jsRelCvs
 * @property JsRelEducation[] $jsRelEducations
 * @property JsRelEmployment[] $jsRelEmployments
 * @property JsRelLanguage[] $jsRelLanguages
 * @property JsRelPublication[] $jsRelPublications
 * @property JsRelTraining[] $jsRelTrainings
 */
class JsMainJobseekers extends \yii\db\ActiveRecord
{
    public $passportno;
    public $verifyCode;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'js_main_jobseekers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname', 'lname', 'dateofbirth', 'fk_gender', 'fk_nationality', 'fk_maritalstatus', 'fk_disabled', 'fk_district','phone','email','verifyCode'], 'required'],
            ['idcardno', 'required', 'when'=> function($model){
                return $model->fk_nationality=='646';
            }],
            [['dateofbirth', 'whencreated', 'whenlastupdated'], 'safe'],
            [['fk_gender', 'fk_maritalstatus', 'fk_disabled', 'fk_disability', 'fk_province', 'fk_district', 'fk_geosector', 'photosize', 'fk_active'], 'integer'],
            [['photo'], 'string'],
            [['idcardno'], 'string', 'max' => 17],
            [['fname', 'lname'], 'string', 'max' => 30],
            [['email'], 'email'],
            [['fk_nationality'], 'string', 'max' => 3],
            [['passportno','phone', 'whodidit'], 'string', 'max' => 45],
            [['photoname', 'phototype'], 'string', 'max' => 100],
            [['idcardno'], 'unique'],
            [['fk_gender'], 'exist', 'skipOnError' => true, 'targetClass' => SGender::className(), 'targetAttribute' => ['fk_gender' => 'pk_gender']],
            [['fk_nationality'], 'exist', 'skipOnError' => true, 'targetClass' => SCountrycodeIso3166::className(), 'targetAttribute' => ['fk_nationality' => 'cc_iso3166']],
            [['fk_disabled'], 'exist', 'skipOnError' => true, 'targetClass' => SNoYes::className(), 'targetAttribute' => ['fk_disabled' => 'pk_no_yes']],
            [['fk_active'], 'exist', 'skipOnError' => true, 'targetClass' => SNoYes::className(), 'targetAttribute' => ['fk_active' => 'pk_no_yes']],
            [['fk_geosector'], 'exist', 'skipOnError' => true, 'targetClass' => SSectors::className(), 'targetAttribute' => ['fk_geosector' => 'pk_sector']],
            [['fk_district'], 'exist', 'skipOnError' => true, 'targetClass' => SDistricts::className(), 'targetAttribute' => ['fk_district' => 'pk_district']],
            [['fk_disability'], 'exist', 'skipOnError' => true, 'targetClass' => SDisability::className(), 'targetAttribute' => ['fk_disability' => 'pk_disability']],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pk_jobseeker' => 'Job seeker number',
            'idcardno' => 'Rwanda ID Card No',
            'fname' => 'First name',
            'lname' => 'Last name',
            'dateofbirth' => 'Date of birth',
            'fk_gender' => 'Gender',
            'fk_nationality' => 'Nationality',
            'fk_maritalstatus' => 'Marital status',
            'fk_disabled' => 'Do you have any disability?',
            'fk_disability' => 'Disability',
            'fk_province' => 'Province',
            'fk_district' => 'District',
            'fk_geosector' => 'Geographic sector',
            'phone' => 'Phone',
            'email' => 'Email',
            'photo' => 'Photo',
            'photoname' => 'Photo name',
            'phototype' => 'Photo type',
            'photosize' => 'Photo size',
            'whodidit' => 'Who did it?',
            'whencreated' => 'When created',
            'whenlastupdated' => 'When last updated',
            'fk_active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenterRelAchievements()
    {
        return $this->hasMany(CenterRelAchievements::className(), ['fk_idcardno' => 'idcardno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkGender()
    {
        return $this->hasOne(SGender::className(), ['pk_gender' => 'fk_gender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkMaritalstatus()
    {
        return $this->hasOne(SMaritalstatus::className(), ['pk_maritalstatus' => 'fk_maritalstatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkNationality()
    {
        return $this->hasOne(SCountrycodeIso3166::className(), ['cc_iso3166' => 'fk_nationality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkDisabled()
    {
        return $this->hasOne(SNoYes::className(), ['pk_no_yes' => 'fk_disabled']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkActive()
    {
        return $this->hasOne(SNoYes::className(), ['pk_no_yes' => 'fk_active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkGeosector()
    {
        return $this->hasOne(SSectors::className(), ['pk_sector' => 'fk_geosector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkProvince()
    {
        return $this->hasOne(SProvinces::className(), ['pk_province' => 'fk_province']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkDistrict()
    {
        return $this->hasOne(SDistricts::className(), ['pk_district' => 'fk_district']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkDisability()
    {
        return $this->hasOne(SDisability::className(), ['pk_disability' => 'fk_disability']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJsRelCvs()
    {
        return $this->hasMany(JsRelCv::className(), ['fk_jobseeker' => 'pk_jobseeker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJsRelEducations()
    {
        return $this->hasMany(JsRelEducation::className(), ['fk_jobseeker' => 'pk_jobseeker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJsRelEmployments()
    {
        return $this->hasMany(JsRelEmployment::className(), ['fk_jobseeker' => 'pk_jobseeker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJsRelLanguages()
    {
        return $this->hasMany(JsRelLanguage::className(), ['fk_jobseeker' => 'pk_jobseeker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJsRelPublications()
    {
        return $this->hasMany(JsRelPublication::className(), ['fk_jobseeker' => 'pk_jobseeker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJsRelTrainings()
    {
        return $this->hasMany(JsRelTraining::className(), ['fk_jobseeker' => 'pk_jobseeker']);
    }
}