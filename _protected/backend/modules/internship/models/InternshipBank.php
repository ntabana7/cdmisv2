<?php

namespace backend\modules\internship\models;

use Yii;

/**
 * This is the model class for table "{{%internship_bank}}".
 *
 * @property integer $idBank
 * @property string $bank
 *
 * @property InternshipIternapplication[] $internshipIternapplications
 */
class InternshipBank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_bank}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank'], 'required'],
            [['bank'], 'string', 'max' => 100],
            [['bank'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBank' => Yii::t('app', 'Id Bank'),
            'bank' => Yii::t('app', 'Bank'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternshipIternapplications()
    {
        return $this->hasMany(InternshipIternapplication::className(), ['idBank' => 'idBank']);
    }
}
