<?php

namespace backend\modules\internship\models;

use Yii;

/**
 * This is the model class for table "{{%internship_gender}}".
 *
 * @property integer $idGender
 * @property string $gender
 *
 * @property Internshipinternapplication[] $internshipinternapplications
 */
class InternshipGender extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_gender}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender'], 'required'],
            [['gender'], 'string', 'max' => 50],
            [['gender'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idGender' => Yii::t('app', 'Id Gender'),
            'gender' => Yii::t('app', 'Gender'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternshipinternapplications()
    {
        return $this->hasMany(Internshipinternapplication::className(), ['idGender' => 'idGender']);
    }
}
