<?php

namespace backend\modules\internship\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\internship\models\InternshipGender;

/**
 * InternshipGenderSearch represents the model behind the search form about `backend\modules\internship\models\InternshipGender`.
 */
class InternshipGenderSearch extends InternshipGender
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idGender'], 'integer'],
            [['gender'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InternshipGender::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idGender' => $this->idGender,
        ]);

        $query->andFilterWhere(['like', 'gender', $this->gender]);

        return $dataProvider;
    }
}
