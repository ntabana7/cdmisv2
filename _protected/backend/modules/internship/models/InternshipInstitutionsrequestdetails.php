<?php

namespace backend\modules\internship\models;

use Yii;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use backend\models\CbQualifications;

/**
 * This is the model class for table "{{%internship_institutionsrequestdetails}}".
 *
 * @property integer $idRequestdetails
 * @property integer $idRequest
 * @property integer $requestednumber
 * @property integer $suppliednumber
 * @property string $status
 * @property integer $idQualif
 * @property integer $idQualifarea
 * @property integer $idGrade
 * @property string $workingplace
 *
 * @property InternshipInstitutionsrequest $idRequest0
 * @property ProviderQualificationarea $idQualifarea0
 * @property InternshipGrade $idGrade0
 * @property CbQualifications $idQualif0
 */
class InternshipInstitutionsrequestdetails extends \yii\db\ActiveRecord
{

    public $idInstit;
    public $applicationdate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internship_institutionsrequestdetails}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requestednumber', 'idQualif', 'idQualifarea', 'idGrade', 'workingplace'], 'required'],
            [['idRequest', 'requestednumber', 'suppliednumber', 'idQualif', 'idQualifarea', 'idGrade'], 'integer'],
            [['status'], 'string', 'max' => 50],
            [['workingplace'], 'string', 'max' => 100],
            [['idRequest'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipInstitutionsrequest::className(), 'targetAttribute' => ['idRequest' => 'idRequest']],
            [['idQualifarea'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderQualificationarea::className(), 'targetAttribute' => ['idQualifarea' => 'idQualifarea']],
            [['idGrade'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipGrade::className(), 'targetAttribute' => ['idGrade' => 'idGrade']],
            [['idQualif'], 'exist', 'skipOnError' => true, 'targetClass' => CbQualifications::className(), 'targetAttribute' => ['idQualif' => 'idQualif']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRequestdetails' => Yii::t('app', 'Id Requestdetails'),
            'idRequest' => Yii::t('app', 'Id Request'),
            'requestednumber' => Yii::t('app', 'Requested number of Interns'),
            'suppliednumber' => Yii::t('app', 'Supplied number'),
            'status' => Yii::t('app', 'Status'),
            'idQualif' => Yii::t('app', 'Qualification'),
            'idQualifarea' => Yii::t('app', 'Qualification area'),
            'idGrade' => Yii::t('app', 'Grade'),
            'workingplace' => Yii::t('app', 'Working place'),
            'idInstit' => Yii::t('app', 'Request from'),
            'applicationdate' => Yii::t('app', 'Request of'),
            'idInstit' => Yii::t('app', 'Request from'),
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRequest0()
    {
        return $this->hasOne(InternshipInstitutionsrequest::className(), ['idRequest' => 'idRequest']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualifarea0()
    {
        return $this->hasOne(ProviderQualificationarea::className(), ['idQualifarea' => 'idQualifarea']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGrade0()
    {
        return $this->hasOne(InternshipGrade::className(), ['idGrade' => 'idGrade']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualif0()
    {
        return $this->hasOne(CbQualifications::className(), ['idQualif' => 'idQualif']);
    }
}
