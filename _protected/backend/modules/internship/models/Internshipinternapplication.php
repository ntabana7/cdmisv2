<?php

namespace backend\modules\internship\models;

use Yii;
use backend\models\CbQualifications;
use backend\models\Districts;
use backend\models\Institutions;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "{{%internshipinternapplication}}".
 *
 * @property integer $idInternapplication
 * @property string $nida
 * @property string $firstname
 * @property string $secondname
 * @property string $email
 * @property string $telephone
 * @property string $dob
 * @property integer $idGender
 * @property integer $idQualif
 * @property integer $idQualifarea
 * @property integer $idGrade
 * @property string $degree
 * @property integer $idUniversity
 * @property string $yearofgraduation
 * @property string $accountNbr
 * @property integer $idBank
 * @property integer $idDistrict
 * @property integer $idHome
 * @property string $applicationdate
 * @property string $validatingdate
 * @property integer $iduser
 * @property integer $idInstit
 * @property string $placementdate
 * @property string $startingdate
 * @property string $endingdate
 * @property integer $iduserplacement
 * @property string $status
 * @property integer $idStatus
 * @property string $reasonnotfinishing
 *
 * @property CbQualifications $idQualif0
 * @property Institutions $idInstit0
 * @property Districts $idHome0
 * @property ProviderQualificationarea $idQualifarea0
 * @property InternshipGrade $idGrade0
 * @property InternshipUniversities $idUniversity0
 * @property InternshipBank $idBank0
 * @property Districts $idDistrict0
 * @property InternshipStatus $idStatus0
 * @property InternshipGender $idGender0
 */
class Internshipinternapplication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%internshipinternapplication}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nida', 'firstname', 'secondname', 'email', 'telephone', 'dob', 'idGender', 'idQualif', 'idQualifarea', 'idGrade', 'degree', 'idUniversity', 'yearofgraduation', 'accountNbr', 'idBank', 'idDistrict', 'idHome'], 'required'],
            [['idGender', 'idQualif', 'idQualifarea', 'idGrade', 'idUniversity', 'idBank','idHome', 'iduser', 'idInstit', 'iduserplacement', 'idStatus'], 'integer'],
            [['applicationdate', 'validatingdate', 'placementdate', 'startingdate', 'endingdate'], 'safe'],
            [['nida'], 'string', 'max' => 16],
            [['firstname', 'secondname', 'degree', 'reasonnotfinishing'], 'string', 'max' => 200],
            [['email', 'accountNbr'], 'string', 'max' => 50],
            [['telephone', 'yearofgraduation', 'status'], 'string', 'max' => 20],
            [['dob'], 'string', 'max' => 12],
            [['nida'], 'unique'],
            [['email'], 'unique'],
            [['telephone'], 'unique'],
            [['accountNbr'], 'unique'],
            [['degree'], 'file', 'extensions'=>'jpg, png, pdf'],
            [['idQualif'], 'exist', 'skipOnError' => true, 'targetClass' => CbQualifications::className(), 'targetAttribute' => ['idQualif' => 'idQualif']],
            [['idInstit'], 'exist', 'skipOnError' => true, 'targetClass' => Institutions::className(), 'targetAttribute' => ['idInstit' => 'idInstit']],
            [['idHome'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['idHome' => 'idDistrict']],
            [['idQualifarea'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderQualificationarea::className(), 'targetAttribute' => ['idQualifarea' => 'idQualifarea']],
            [['idGrade'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipGrade::className(), 'targetAttribute' => ['idGrade' => 'idGrade']],
            [['idUniversity'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipUniversities::className(), 'targetAttribute' => ['idUniversity' => 'idUniversity']],
            [['idBank'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipBank::className(), 'targetAttribute' => ['idBank' => 'idBank']],
            // [['idDistrict'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['idDistrict' => 'idDistrict']],
            [['idStatus'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipStatus::className(), 'targetAttribute' => ['idStatus' => 'idStatus']],
            [['idGender'], 'exist', 'skipOnError' => true, 'targetClass' => InternshipGender::className(), 'targetAttribute' => ['idGender' => 'idGender']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInternapplication' => Yii::t('app', 'Id Internapplication'),
            'nida' => Yii::t('app', 'National ID number'),
            'firstname' => Yii::t('app', 'First name'),
            'secondname' => Yii::t('app', 'Second name'),
            'email' => Yii::t('app', 'Email'),
            'telephone' => Yii::t('app', 'Telephone'),
            'dob' => Yii::t('app', 'Date of birth'),
            'idGender' => Yii::t('app', 'Gender'),
            'idQualif' => Yii::t('app', 'Qualification'),
            'idQualifarea' => Yii::t('app', 'Qualification area'),
            'idGrade' => Yii::t('app', 'Grade'),
            'degree' => Yii::t('app', 'Degree'),
            'idUniversity' => Yii::t('app', 'University'),
            'yearofgraduation' => Yii::t('app', 'Year of graduation'),
            'accountNbr' => Yii::t('app', 'Account number'),
            'idBank' => Yii::t('app', 'Bank name'),
            'idDistrict' => Yii::t('app', 'Preferably I may work from'),
            'applicationdate' => Yii::t('app', 'Applicationdate'),
            'validatingdate' => Yii::t('app', 'Validatingdate'),
            'iduser' => Yii::t('app', 'Who valided the application'),
            'idInstit' => Yii::t('app', 'Hosting Institution'),
            'placementdate' => Yii::t('app', 'Placement date'),
            'iduserplacement' => Yii::t('app', 'Who placed the intern'),
            'status' => Yii::t('app', 'Application status'),
            'idStatus' => Yii::t('app', 'Status of Intern in hosting Institution'),
            'idHome' => Yii::t('app', 'Where do you live currently?'),
            'startingdate' => Yii::t('app', 'Starting date of internship in hosting Institution'),
            'endingdate' => Yii::t('app', 'Expected ending date of internship in hosting Institution'),
            'reasonnotfinishing' => Yii::t('app', 'Reason not finishing'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualif0()
    {
        return $this->hasOne(CbQualifications::className(), ['idQualif' => 'idQualif']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstit0()
    {
        return $this->hasOne(Institutions::className(), ['idInstit' => 'idInstit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdHome0()
    {
        return $this->hasOne(Districts::className(), ['idDistrict' => 'idHome']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualifarea0()
    {
        return $this->hasOne(ProviderQualificationarea::className(), ['idQualifarea' => 'idQualifarea']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGrade0()
    {
        return $this->hasOne(InternshipGrade::className(), ['idGrade' => 'idGrade']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUniversity0()
    {
        return $this->hasOne(InternshipUniversities::className(), ['idUniversity' => 'idUniversity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBank0()
    {
        return $this->hasOne(InternshipBank::className(), ['idBank' => 'idBank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDistrict0()
    {
        return $this->hasOne(Districts::className(), ['idDistrict' => 'idDistrict']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus0()
    {
        return $this->hasOne(InternshipStatus::className(), ['idStatus' => 'idStatus']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGender0()
    {
        return $this->hasOne(InternshipGender::className(), ['idGender' => 'idGender']);
    }

    public function getDistrictsListDropdown()
    {   
        $listdistricts   = Districts::find()->select('distName')->all();
        $listt   = ArrayHelper::map( $listdistricts,'distName','distName');

        return $listt;
    }
}
