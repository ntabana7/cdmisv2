<?php

namespace backend\modules\internship\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\internship\models\InternshipUniversities;

/**
 * InternshipUniversitiesSearch represents the model behind the search form about `backend\modules\internship\models\InternshipUniversities`.
 */
class InternshipUniversitiesSearch extends InternshipUniversities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUniversity'], 'integer'],
            [['university'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InternshipUniversities::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idUniversity' => $this->idUniversity,
        ]);

        $query->andFilterWhere(['like', 'university', $this->university]);

        return $dataProvider;
    }
}
