<?php

namespace backend\modules\internship\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\internship\models\Internshipinternapplication;

/**
 * InternshipinternapplicationSearch represents the model behind the search form about `backend\modules\internship\models\Internshipinternapplication`.
 */
class InternshipinternapplicationSearch extends Internshipinternapplication
{
    public function rules()
    {
        return [
            [['idInternapplication'], 'integer'],
            [['nida', 'firstname', 'secondname', 'email', 'telephone', 'dob', 'degree', 'yearofgraduation', 'accountNbr', 'applicationdate', 'validatingdate', 'placementdate', 'status', 'reasonnotfinishing', 'idGender', 'idQualif', 'idQualifarea', 'idGrade', 'idUniversity', 'idBank', 'idDistrict', 'iduser', 'idInstit', 'iduserplacement', 'idStatus'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        if(Yii::$app->user->can('admin')){
            // $query = InternshipInternapplication::find()->orderBy('idInstitSector');
            $query = Internshipinternapplication::find();
        }elseif(Yii::$app->user->can('internshipofficer')){
           $query = Internshipinternapplication::find()->where('status = "Pending"'); 
        }else{
            $query = Internshipinternapplication::find()->where('nida=:u',['u'=>Yii::$app->user->identity->userFrom]);
        }        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idDistrict0')
        ->joinWith('idQualif0')
        ->joinWith('idQualifarea0')
        ->joinWith('idGrade0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternapplication' => $this->idInternapplication,
            // 'idQualif' => $this->idQualif,
            // 'idQualifarea' => $this->idQualifarea,
            // 'idGrade' => $this->idGrade,
            'idUniversity' => $this->idUniversity,
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'gender', $this->idGender])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'yearofgraduation', $this->yearofgraduation])
            ->andFilterWhere(['like', 'accountNbr', $this->accountNbr])
            ->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'validatingdate', $this->validatingdate])
            ->andFilterWhere(['like', 'placementdate', $this->placementdate])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reasonnotfinishing', $this->reasonnotfinishing])
            ->andFilterWhere(['like', 'districts.distName', $this->idDistrict])
            ->andFilterWhere(['like', 'qualif', $this->idQualif])
            ->andFilterWhere(['like', 'area', $this->idQualifarea])
            ->andFilterWhere(['like', 'grade', $this->idGrade]);

        return $dataProvider;
    }

    
    //I added this function for displaying only validated interns on waiting list

    public function searchUnavailabeinterns($params)
    {


        if(Yii::$app->user->can('internshipofficer')){            
            $query = Internshipinternapplication::find()->Where('status = "Notavailable"');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idDistrict0')
        ->joinWith('idQualif0')
        ->joinWith('idQualifarea0')
        ->joinWith('idGrade0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternapplication' => $this->idInternapplication,
            // 'idQualif' => $this->idQualif,
            // 'idQualifarea' => $this->idQualifarea,
            // 'idGrade' => $this->idGrade,
            'idUniversity' => $this->idUniversity,
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'gender', $this->idGender])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'yearofgraduation', $this->yearofgraduation])
            ->andFilterWhere(['like', 'accountNbr', $this->accountNbr])
            ->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'validatingdate', $this->validatingdate])
            ->andFilterWhere(['like', 'placementdate', $this->placementdate])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reasonnotfinishing', $this->reasonnotfinishing])
            ->andFilterWhere(['like', 'districts.distName', $this->idDistrict])
            ->andFilterWhere(['like', 'qualif', $this->idQualif])
            ->andFilterWhere(['like', 'area', $this->idQualifarea])
            ->andFilterWhere(['like', 'grade', $this->idGrade]);

        return $dataProvider;
    }
    
    public function searchValidatedinterns($params)
    {


        if(Yii::$app->user->can('internshipofficer')){            
            $query = Internshipinternapplication::find()->Where('status = "WaitingList"');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idDistrict0')
        ->joinWith('idQualif0')
        ->joinWith('idQualifarea0')
        ->joinWith('idGrade0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternapplication' => $this->idInternapplication,
            // 'idQualif' => $this->idQualif,
            // 'idQualifarea' => $this->idQualifarea,
            // 'idGrade' => $this->idGrade,
            'idUniversity' => $this->idUniversity,
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'gender', $this->idGender])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'yearofgraduation', $this->yearofgraduation])
            ->andFilterWhere(['like', 'accountNbr', $this->accountNbr])
            ->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'validatingdate', $this->validatingdate])
            ->andFilterWhere(['like', 'placementdate', $this->placementdate])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reasonnotfinishing', $this->reasonnotfinishing])
            ->andFilterWhere(['like', 'districts.distName', $this->idDistrict])
            ->andFilterWhere(['like', 'qualif', $this->idQualif])
            ->andFilterWhere(['like', 'area', $this->idQualifarea])
            ->andFilterWhere(['like', 'grade', $this->idGrade]);

        return $dataProvider;
    }

    public function searchDeployedinterns($params)
    {


        if(Yii::$app->user->can('internshipofficer')){            
            $query = Internshipinternapplication::find()->Where('status = "Deployed"');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idDistrict0')
        ->joinWith('idQualif0')
        ->joinWith('idQualifarea0')
        ->joinWith('idGrade0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternapplication' => $this->idInternapplication,
            // 'idQualif' => $this->idQualif,
            // 'idQualifarea' => $this->idQualifarea,
            // 'idGrade' => $this->idGrade,
            'idUniversity' => $this->idUniversity,
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'gender', $this->idGender])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'yearofgraduation', $this->yearofgraduation])
            ->andFilterWhere(['like', 'accountNbr', $this->accountNbr])
            ->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'validatingdate', $this->validatingdate])
            ->andFilterWhere(['like', 'placementdate', $this->placementdate])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reasonnotfinishing', $this->reasonnotfinishing])
            ->andFilterWhere(['like', 'districts.distName', $this->idDistrict])
            ->andFilterWhere(['like', 'qualif', $this->idQualif])
            ->andFilterWhere(['like', 'area', $this->idQualifarea])
            ->andFilterWhere(['like', 'grade', $this->idGrade]);

        return $dataProvider;
    }

    public function searchDeployedininstitution($params)
    {

        $idinstit=Yii::$app->user->identity->userFrom;
        // echo $idinstit;die;
        if(Yii::$app->user->can('institution')){            
            $query = Internshipinternapplication::find()->Where('status = "Deployed"')
                                                        ->andWhere('idInstit="'.$idinstit.'"');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idDistrict0')
        ->joinWith('idQualif0')
        ->joinWith('idQualifarea0')
        ->joinWith('idGrade0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternapplication' => $this->idInternapplication,
            // 'idQualif' => $this->idQualif,
            // 'idQualifarea' => $this->idQualifarea,
            // 'idGrade' => $this->idGrade,
            'idUniversity' => $this->idUniversity,
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'gender', $this->idGender])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'yearofgraduation', $this->yearofgraduation])
            ->andFilterWhere(['like', 'accountNbr', $this->accountNbr])
            ->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'validatingdate', $this->validatingdate])
            ->andFilterWhere(['like', 'placementdate', $this->placementdate])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reasonnotfinishing', $this->reasonnotfinishing])
            ->andFilterWhere(['like', 'districts.distName', $this->idDistrict])
            ->andFilterWhere(['like', 'qualif', $this->idQualif])
            ->andFilterWhere(['like', 'area', $this->idQualifarea])
            ->andFilterWhere(['like', 'grade', $this->idGrade]);

        return $dataProvider;
    }

    public function searchDeployinterns($params)
    {


    if(Yii::$app->user->can('internshipofficer')){    
    
        if(isset($_GET['idQualif']) && isset($_GET['idQualifarea']) && isset($_GET['idGrade'])){

            $query = Internshipinternapplication::find()->Where('internshipinternapplication.idQualif = "'.$_GET["idQualif"].'" ' )
                                                        ->andWhere('internshipinternapplication.idQualifarea = "'.$_GET["idQualifarea"].'" ' )
                                                        ->andWhere('internshipinternapplication.idGrade = "'.$_GET["idGrade"].'" ' )
                                                        ->andWhere('internshipinternapplication.status = "WaitingList" ' )
                                                        ->limit(20);
        } 

    }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // join tables cbIndicators and sublevels
        $query->joinWith('idDistrict0')
        ->joinWith('idQualif0')
        ->joinWith('idQualifarea0')
        ->joinWith('idGrade0');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternapplication' => $this->idInternapplication,
            // 'idQualif' => $this->idQualif,
            // 'idQualifarea' => $this->idQualifarea,
            // 'idGrade' => $this->idGrade,
            'idUniversity' => $this->idUniversity,
            'idBank' => $this->idBank,
        ]);

        $query->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'gender', $this->idGender])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'yearofgraduation', $this->yearofgraduation])
            ->andFilterWhere(['like', 'accountNbr', $this->accountNbr])
            ->andFilterWhere(['like', 'applicationdate', $this->applicationdate])
            ->andFilterWhere(['like', 'validatingdate', $this->validatingdate])
            ->andFilterWhere(['like', 'placementdate', $this->placementdate])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'reasonnotfinishing', $this->reasonnotfinishing])
            ->andFilterWhere(['like', 'districts.distName', $this->idDistrict])
            ->andFilterWhere(['like', 'qualif', $this->idQualif])
            ->andFilterWhere(['like', 'area', $this->idQualifarea])
            ->andFilterWhere(['like', 'grade', $this->idGrade]);

        return $dataProvider;
    }

}
