<?php

namespace backend\modules\internship\controllers;

use Yii;
use backend\modules\internship\models\InternshipInstitutionsrequest;
use backend\modules\internship\models\InternshipInstitutionsrequestSearch;
use backend\modules\internship\models\InternshipInstitutionsrequestdetails;
use backend\modules\cdproviders\models\Model;
use backend\models\CbQualifications;
use backend\models\Institutions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * InternshipInstitutionsrequestController implements the CRUD actions for InternshipInstitutionsrequest model.
 */
class InternshipInstitutionsrequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InternshipInstitutionsrequest models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new InternshipInstitutionsrequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single InternshipInstitutionsrequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "InternshipInstitutionsrequest #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new InternshipInstitutionsrequest model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('internshipofficer')){ 

            throw new NotFoundHttpException('You are not supposed to submit the CB plan');

        }elseif(Yii::$app->user->can('institution')){
        $request = Yii::$app->request;
        $model = new InternshipInstitutionsrequest();
        $modelsrequestdetail = [new InternshipInstitutionsrequestdetails]; 

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Interns request",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelsrequestdetail'=>(empty($modelsrequestdetail)) ? [new InternshipInstitutionsrequestdetails ]: $modelsrequestdetail
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){


            $modelsrequestdetail = Model::createMultiple(InternshipInstitutionsrequestdetails::classname());
            Model::loadMultiple($modelsrequestdetail, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsrequestdetail) && $valid;
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->applicationdate=date('Y-m-d H:i:s');
                    $model->idInstit = Yii::$app->user->identity->userFrom;
                    if ($flag = $model->save(false)) {
                        $total = 0;
                        foreach ($modelsrequestdetail as $modelrequestdetail) 
                        {
                            $modelrequestdetail->idRequest = $model->idRequest;
                            $total+= $modelrequestdetail->requestednumber;
                            if (! ($flag = $modelrequestdetail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                  
                        $this->updatedemandednumber($model->idRequest,$total);
                    }
                    if ($flag) {
                        $transaction->commit();
                    return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new InternshipInstitutionsrequest",
                    'content'=>'<span class="text-success">Create InternshipInstitutionsrequest success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];  
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }else{           
                return [
                    'title'=> "Create new InternshipInstitutionsrequest",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelsrequestdetail'=>(empty($modelsrequestdetail)) ? [new InternshipInstitutionsrequestdetails ]: $modelsrequestdetail
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }


        }


        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idRequest]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'modelsrequestdetail'=>(empty($modelsrequestdetail)) ? [new InternshipInstitutionsrequestdetails ]: $modelsrequestdetail
                ]);
            }
        }
       
    }
}

    /**
     * Updates an existing InternshipInstitutionsrequest model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update InternshipInstitutionsrequest #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "InternshipInstitutionsrequest #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update InternshipInstitutionsrequest #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idRequest]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing InternshipInstitutionsrequest model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function updatedemandednumber($id,$demandednumber)
    {
        
        $model = $this->findModel($id);       
        $model->demandednumber = $demandednumber;
        $model->save(false);
    }

    /**
     * Delete an existing InternshipInstitutionsrequest model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing InternshipInstitutionsrequest model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the InternshipInstitutionsrequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InternshipInstitutionsrequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InternshipInstitutionsrequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
