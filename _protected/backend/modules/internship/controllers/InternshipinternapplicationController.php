<?php

namespace backend\modules\internship\controllers;

use Yii;
use backend\modules\internship\models\Internshipinternapplication;
use backend\modules\internship\models\InternshipinternapplicationSearch;
use backend\modules\internship\models\InternshipInstitutionsrequest;
use backend\modules\internship\models\InternshipInstitutionsrequestdetails;
use common\models\User;
use nenad\passwordStrength\PasswordInput;
use backend\modules\cdproviders\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

// added classes

use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * InternshipinternapplicationController implements the CRUD actions for Internshipinternapplication model.
 */
class InternshipinternapplicationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Internshipinternapplication models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new InternshipinternapplicationSearch;
    //     $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

    //     return $this->render('index', [
    //         'dataProvider' => $dataProvider,
    //         'searchModel' => $searchModel,
    //     ]);
    // }

    public function actionIndex()
    {    
        if(Yii::$app->user->can('admin')){ 
        $searchModel = new InternshipinternapplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    // I added this function for displaying only validated interns on waiting list 

    public function actionInternindex()
    {    
        if(Yii::$app->user->can('intern')){ 
        $searchModel = new InternshipinternapplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('internindex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }   

    public function validateInterns($id)
    {
        $model = $this->findModel($id);       
        $model->iduser = Yii::$app->user->identity->id;
        $model->validatingdate= date('Y-m-d H:i:s');
        $model->save(false);

    }

    public function actionInternshipofficerindex()
    {    
        if(Yii::$app->user->can('internshipofficer')){ 
        $searchModel = new InternshipinternapplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        //For Editable Status 
        if(Yii::$app->request->post('hasEditable'))
        {
            $idInternapplication=Yii::$app->request->post('editableKey');
            $status= Internshipinternapplication::findOne($idInternapplication);

            $out= Json::encode(['output'=>'','message'=>'']);
            $post=[];
            $posted=current($_POST['Internshipinternapplication']);
            $post['Internshipinternapplication']=$posted;
            if($status->load($post)){

                // $status->save();

                // added if condition
                if($status->save()){
                    $this->validateInterns($idInternapplication);                   
                }

            }
                echo $out;
                return;

        }

        return $this->render('pendinglist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    public function actionUnavalableinterns()
    {   

        if(Yii::$app->user->can('internshipofficer')){ 
        $searchModel = new InternshipinternapplicationSearch();        
        $dataProvider = $searchModel->searchUnavailabeinterns(Yii::$app->request->queryParams);

          //For Editable Status 
        if(Yii::$app->request->post('hasEditable'))
        {
            $idInternapplication=Yii::$app->request->post('editableKey');
            $status= Internshipinternapplication::findOne($idInternapplication);

            $out= Json::encode(['output'=>'','message'=>'']);
            $post=[];
            $posted=current($_POST['Internshipinternapplication']);
            $post['Internshipinternapplication']=$posted;
            if($status->load($post)){
                
                $status->save();

                // added if condition
                // if($status->save()){
                //     $this->validateInterns($idInternapplication);                   
                // }

            }
                echo $out;
                return;

        }

        //I replaced approvedplan by waitinglist

        return $this->render('unavailablelist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }
    

    public function actionDashboardinterns()
    {
        if(Yii::$app->user->can('internshipofficer')){
        $pendinglist           = new Internshipinternapplication();
        $internshipapplication = new Internshipinternapplication();
        $waitinglist           = new Internshipinternapplication();
        $deployed              = new Internshipinternapplication();        
        $requests              = new InternshipInstitutionsrequest();
        $gender                = new Internshipinternapplication();
        $qualification         = new Internshipinternapplication();
        $institutions          = new InternshipInstitutionsrequest();
        $interns               = new InternshipInstitutionsrequestdetails();

        


        $internsonpendinglist=$pendinglist     ->find()->where('status = "Pending"')->all();
        $appliedinterns=$internshipapplication ->find()->All();        
        $internsonwaitinglist=$waitinglist->find()->Where('status="WaitingList"')->all();
        $deployedinterns=$deployed->find()->Where('status = "Deployed"')->all();
        $requestfrominstitutions=$requests->find()->All();
        $requestpending=$requests->find()->Where('status="Pending"')->All();
        $requestinprogress=$requests->find()->Where('status="InProgress"')->All();
        $requestdone=$requests->find()->Where('status="Done"')->All();
        $pendingrequestpercentage=(count($requestpending)/count($requestfrominstitutions))*100;
        $inprogressquestpercentage=(count($requestinprogress)/count($requestfrominstitutions))*100;
        $donequestpercentage=(count($requestdone)/count($requestfrominstitutions))*100;
        $male=count($gender->find()->Where('idGender="1"')->all());
        $female=count($gender->find()->Where('idGender="2"')->all());
        $engineers=count($qualification->find()->Where('idQualif="6"')->all());
        $masters=count($qualification->find()->Where('idQualif="2"')->all());
        $bachelors=count($qualification->find()->Where('idQualif="3"')->all());
        $technicians=count($qualification->find()->Where('idQualif="4"')->all());
        $artisans=count($qualification->find()->Where('idQualif="5"')->all());
        $diploma=count($qualification->find()->Where('idQualif="10"')->all());
        $nbrofinstitutionusingapp=$institutions->find()->select('idInstit')->distinct()->All();
        $nbrofinternsrequested=$interns->find()->sum('requestednumber');
        $nbrofinternsprovided=$interns->find()->sum('suppliednumber');


        return $this->render('internshipdashboard',[
                'appliedinterns' => $appliedinterns,
                'internsonpendinglist' => $internsonpendinglist,
                'internsonwaitinglist' => $internsonwaitinglist,
                'deployedinterns' => $deployedinterns,
                'requestfrominstitutions'=>$requestfrominstitutions,
                'requestpending'=>$requestpending,
                'requestinprogress'=>$requestinprogress,
                'requestdone'=>$requestdone,
                'pendingrequestpercentage'=>$pendingrequestpercentage,
                'inprogressquestpercentage'=>$inprogressquestpercentage,
                'donequestpercentage'=>$donequestpercentage,
                'male'=>$male,
                'female'=>$female,
                'engineers'=>$engineers,
                'masters'=>$masters,
                'bachelors'=>$bachelors,
                'technicians'=>$technicians,
                'artisans'=>$artisans,
                'diploma'=>$diploma,
                'nbrofinstitutionusingapp'=>$nbrofinstitutionusingapp,
                'nbrofinternsrequested'=>$nbrofinternsrequested,
                'nbrofinternsprovided'=>$nbrofinternsprovided,
            ]);
        }else{

            throw new NotFoundHttpException('You dont have access to this dashboard');

        }
        

    }

    public function actionValidatedinterns()
    {   

        if(Yii::$app->user->can('internshipofficer')){ 
        $searchModel = new InternshipinternapplicationSearch();        
        $dataProvider = $searchModel->searchValidatedinterns(Yii::$app->request->queryParams);

          //For Editable Status 
        if(Yii::$app->request->post('hasEditable'))
        {
            $idInternapplication=Yii::$app->request->post('editableKey');
            $status= Internshipinternapplication::findOne($idInternapplication);

            $out= Json::encode(['output'=>'','message'=>'']);
            $post=[];
            $posted=current($_POST['Internshipinternapplication']);
            $post['Internshipinternapplication']=$posted;
            if($status->load($post)){
                
                $status->save();

                // added if condition
                // if($status->save()){
                //     $this->validateInterns($idInternapplication);                   
                // }

            }
                echo $out;
                return;

        }

        //I replaced approvedplan by waitinglist

        return $this->render('waitinglist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }


    // I added this function for displaying only validated interns on waiting list    

    public function deployInterns($id,$idInst,$idRequest)
    {
        $model = $this->findModel($id);  
        $requestModel = InternshipInstitutionsrequest::findOne($idRequest);  

        $model->iduserplacement = Yii::$app->user->identity->id;
        $model->placementdate= date('Y-m-d H:i:s');
        $model->idInstit = $idInst;
        if($model->save(false)){
            if($requestModel->demandednumber > $requestModel->suppliednumber){  
                if(($requestModel->demandednumber -1) == $requestModel->suppliednumber){
                    $requestModel->suppliednumber += 1;
                    $requestModel->status = 'Done';
                    $requestModel->responsedate=date('Y-m-d H:i:s'); 
                }else{
                    $requestModel->suppliednumber += 1;
                    $requestModel->status = 'In Progress';
                    $requestModel->responsedate=date('Y-m-d H:i:s'); 
                    
                }  

                $requestModel->save(false);        
                
            }
        }

    }

    public function updatesuppliednumberrequestdetails($id,$idRequestdetails)
    {
        $model = $this->findModel($id); 
        $requestdetailsModel = InternshipInstitutionsrequestdetails::findOne($idRequestdetails);

            
    if($model->save(false)){
        if($requestdetailsModel->requestednumber > $requestdetailsModel->suppliednumber){  
if(($requestdetailsModel->requestednumber -1) == $requestdetailsModel->suppliednumber){
                    $requestdetailsModel->suppliednumber += 1;
                    $requestdetailsModel->status = 'Done'; 
                }else{
                    $requestdetailsModel->suppliednumber += 1;
                    $requestdetailsModel->status = 'In Progress'; 
                    
                }  

                $requestdetailsModel->save(false);        
                
            }
        }

    }

    public function actionDeployinterns()
    {   

        if(Yii::$app->user->can('internshipofficer')){        
        
            // echo $_GET['idRequestdetails'];die;

        $searchModel = new InternshipinternapplicationSearch();        
        $dataProvider = $searchModel->searchDeployinterns(Yii::$app->request->queryParams);
       

          //For Editable Status 
        if(Yii::$app->request->post('hasEditable'))
        {
            $idInternapplication=Yii::$app->request->post('editableKey');
            $status= Internshipinternapplication::findOne($idInternapplication);

            $out= Json::encode(['output'=>'','message'=>'']);
            $post=[];
            $posted=current($_POST['Internshipinternapplication']);
            $post['Internshipinternapplication']=$posted;
            if($status->load($post)){
                
                // $status->save();

                // added if condition
                if($status->save()){                
                $this->deployInterns($idInternapplication,$_GET['idInstit'],$_GET['idRequest']); 
                $this->updatesuppliednumberrequestdetails($idInternapplication,$_GET['idRequestdetails']);
                                   
                }

            }
                echo $out;
                return;

        }

        //I replaced approvedplan by waitinglist

        return $this->render('potentialcandidateslist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    public function actionDeployedininstitution()
    {    
        if(Yii::$app->user->can('institution')){ 
        $searchModel = new InternshipinternapplicationSearch();
        $dataProvider = $searchModel->searchDeployedininstitution(Yii::$app->request->queryParams);

        return $this->render('deployedininstitution', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    public function actionDeployedinterns()
    {   

        if(Yii::$app->user->can('internshipofficer')){ 
        $searchModel = new InternshipinternapplicationSearch();        
        $dataProvider = $searchModel->searchDeployedinterns(Yii::$app->request->queryParams);

          //For Editable Status 
        if(Yii::$app->request->post('hasEditable'))
        {
            $idInternapplication=Yii::$app->request->post('editableKey');
            $status= Internshipinternapplication::findOne($idInternapplication);

            $out= Json::encode(['output'=>'','message'=>'']);
            $post=[];
            $posted=current($_POST['Internshipinternapplication']);
            $post['Internshipinternapplication']=$posted;
            if($status->load($post)){
                
                $status->save();

                // added if condition
                // if($status->save()){
                //     $this->validateInterns($idInternapplication);                   
                // }

            }
                echo $out;
                return;

        }

        //I replaced approvedplan by waitinglist

        return $this->render('deployedlist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    /**
     * Displays a single Internshipinternapplication model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idInternapplication]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new Internshipinternapplication model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
// public function actionCreate()
// {
//     $model = new Internshipinternapplication;
//     $modelsaccount = [new User];

//     if ($model->load(Yii::$app->request->post()) && $model->save()) 
//     {              

//     $modelsaccount = Model::createMultiple(User::classname());
//     Model::loadMultiple($modelsaccount, Yii::$app->request->post());

//     // validate all models
//     $valid = $model->validate();
//     $valid = Model::validateMultiple($modelsaccount) && $valid;
    
//     if ($valid) {
//         $transaction = \Yii::$app->db->beginTransaction();
//         try {
//             if ($flag = $model->save(false)) {
//                 foreach ($modelsaccount as $modelaccount) 
//                 {
//                     $modelaccount->userFrom = $model->nida;
//                     $modelaccount->email = $model->email;
//                     $modelaccount->status = 10;
//                     $modelaccount->auth_key ="AZ1g_F51dUKmT-WwOV_pG2FF5bWOCWGu";
//                     if (! ($flag = $modelaccount->save(false))) {
//                         $transaction->rollBack();
//                         break;
//                     }
//                 }
//             }
//             if ($flag) {
//                 $transaction->commit();
//                 return $this->redirect(['view', 'id' => $model->idInternapplication]);
//             }
//         } catch (Exception $e) {
//             $transaction->rollBack();
//         }
//     }

//     } else {
//         return $this->render('create', [
//             'model' => $model,
//             'modelsaccount'=>(empty($modelsaccount)) ? [new User]: $modelsaccount
//         ]);
//     }
// }  
 public function actionCreate()
    {
        $model = new Internshipinternapplication;

        if ($model->load(Yii::$app->request->post())) {

            
            $image= UploadedFile::getInstance($model,'degree');
            $imageName = $model->firstname.'_'.$model->secondname.'_'.'degree'.'_'.'Intern';
            $image->saveAs(Yii::getAlias('@upload').'/'. $imageName);
            $model->degree=$imageName;
            $model->save();




            // echo '<pre>';
            // print_r($image);
            // exit();
            // echo '<pre>';



            // $imageName = Yii::$app->user->identity->username.'_'.'Actionplan'.'_'.$model->submittedon; 
            //         // echo  Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension;die;                      
            //         $model->file->saveAs(Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension);
            //         $extension = $model->file->extension;
                 
            //         $model->actionplan=$imageName.'.'.$extension;


            

            return $this->redirect(['view', 'id' => $model->idInternapplication]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }  

    /**
     * Updates an existing Internshipinternapplication model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idInternapplication]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Internshipinternapplication model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Internshipinternapplication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Internshipinternapplication the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Internshipinternapplication::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
