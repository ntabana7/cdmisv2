<?php

namespace frontend\controllers;

use Yii;
use app\models\JsMainJobseekers;
use app\models\JsMainJobseekersSearch;
use app\models\JsRelEducation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\SignupForm;
use yii\web\UploadedFile;
use app\models\SDistricts;
use app\models\SSectors;
use app\models\SDisability;
use common\models\MetaGroupmembers;

/**
 * JsMainJobseekersController implements the CRUD actions for JsMainJobseekers model.
 */
class JsMainJobseekersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JsMainJobseekers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JsMainJobseekersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all JsMainJobseekers models.
     * @return mixed
     */
    

    


    public function actionReport()
    {
        $searchModel = new JsMainJobseekersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }







    /**
     * Displays a single JsMainJobseekers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // $education=JsRelEducation::findModel()
        //             ->where(['fk_jobseeker' => $id])
        //             ->orderBy(['fk_qualification' =>SORT_DESC]);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JsMainJobseekers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JsMainJobseekers();
        $signup = new SignupForm();

        $model->whodidit=$model->email;

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['view', 'id' => $model->pk_jobseeker]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'signup'=> $signup,
            ]);
        }
    }

    /**
     * Creates a new JsMainJobseekers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $this->layout = 'login';

        $model = new JsMainJobseekers();
        //$model = new JsMainJobseekers(['scenario' => 'foreign']);

        $signup = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $signup->load(Yii::$app->request->post()))
        {
           
            if($model->passport)
                $model->idcradno=$model->passportno;

            if($model->photo)
            {
                $model->photo = UploadedFile::getInstance($model, 'photo');
                $model->photoname=$model->photo->name;
                $model->phototype=$model->photo->type;
                $model->photosize=$model->photo->size;
                $model->photo=file_get_contents($model->photo->tempName);
            }

            $model->whodidit=$model->email;
            $model->save();

            $signup->username=$model->email;
            $signup->email=$model->email;

            if ($user = $signup->signup())
            {
                $groupMember=new MetaGroupmembers();

                $groupMember->fk_group=2;
                $groupMember->fk_user=$user->id;
                $groupMember->save();

                return $this->render('registered');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'signup' => $signup,
            ]);
        }
    }

    /**
     * Updates an existing JsMainJobseekers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pk_jobseeker]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing JsMainJobseekers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JsMainJobseekers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JsMainJobseekers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JsMainJobseekers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDistricts($id)
    {
        $countDistricts= SDistricts::find()
            ->where(['fk_province'=>$id])
            ->count();
       
        $districts= SDistricts::find()
            ->where(['fk_province'=>$id])
            ->orderBy('dist_name')
            ->all();

        echo '<option>Select district</option>';
       
        if($countDistricts>0)
        {
            foreach($districts as $districts)
            {
                echo"<option value='".$districts->pk_district."'>".$districts->dist_name."</option>";
            }
        }
        else {
            echo"<option>-</option>";
        }
    }
   
    public function actionSectors($id)
    {
        $countSectors= SSectors::find()
        ->where(['fk_district'=>$id])
        ->count();
   
        $sectors= SSectors::find()
        ->where(['fk_district'=>$id])
        ->orderBy('sector_name')
        ->all();

        echo '<option>Select geographic sector</option>';
   
        if($countSectors>0)
        {
            foreach($sectors as $sectors)
            {
                echo"<option value='".$sectors->pk_sector."'>".$sectors->sector_name."</option>";
            }
        }
        else {
            echo"<option>-</option>";
        }
    }

    public function actionDisability($id)
    {

        if($id==1)
        {
            echo"<option value=0>No disability</option>";
        }
        if($id!=1)
        {
            $countDisa= SDisability::find()
                        ->where(['<>','pk_disability',0])
                        ->count();

            $disability= SDisability::find()
                        ->where(['!=','pk_disability',0])
                        ->All();

            if($countDisa > 0)
            foreach($disability as $disability)
            {
                echo"<option value='".$disability->pk_disability."'>".$disability->disability."</option>";
            }
        }
    }
} 