<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\Sections as BaseSections;

/**
 * This is the model class for table "sections".
 */
class Sections extends BaseSections
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['section_name', 'levels_id'], 'required'],
            [['levels_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['section_name'], 'string', 'max' => 200]
        ]);
    }
	
}
