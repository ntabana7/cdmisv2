<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\ChallengeDependentQuestion as BaseDependentQuestion;

/**
 * This is the model class for table "dependent_question".
 */
class ChallengeDependentQuestion extends BaseDependentQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['dependent_question'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['condition_answer'], 'string', 'max' => 4],
            [['dependent_question'], 'string', 'max' => 200]
        ]);
    }
	
}
