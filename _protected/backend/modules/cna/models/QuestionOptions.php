<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\QuestionOptions as BaseQuestionOptions;

/**
 * This is the model class for table "question_options".
 */
class QuestionOptions extends BaseQuestionOptions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['question_option', 'questions_id'], 'required'],
            [['questions_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['question_option'], 'string', 'max' => 50]
        ]);
    }
	
}
