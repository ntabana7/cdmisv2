<?php

namespace backend\modules\cna\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\Sections]].
 *
 * @see \backend\modules\cna\models\query\Sections
 */
class SectionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\Sections[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\Sections|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byLevel($level)
    {
        $this->andWhere(['{{%sections}}.levels_id' => $level]);
        return $this;
    }

}
