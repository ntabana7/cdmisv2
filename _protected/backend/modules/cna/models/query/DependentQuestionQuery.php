<?php

namespace backend\modules\cna\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\DependentQuestion]].
 *
 * @see \backend\modules\cna\models\query\DependentQuestion
 */
class DependentQuestionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\DependentQuestion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\DependentQuestion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
