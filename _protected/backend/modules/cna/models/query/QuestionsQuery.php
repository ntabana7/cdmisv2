<?php

namespace backend\modules\cna\models\query;
use backend\modules\cna\models\Sections;
/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\Questions]].
 *
 * @see \backend\modules\cna\models\query\Questions
 */
class QuestionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\Questions[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\Questions|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

   /**
    * Displays questions by level(Institutional , Organisational or Individual).
    * @param integer $level_id
    * @return mixed
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
   
    public function byLevel($level_id)
    {
        $section_ids = Sections::find()->select('id')->where(['levels_id' => $level_id])->all();
        $ids = [];
        foreach($section_ids as $row){
            $ids[] = $row['id'];  
        }
        $this->andWhere(['IN', '{{%questions}}.sections_id',$ids]);
        return $this;
    }

    public function available()
    {
        $this->andWhere(['{{%questions}}.status' => 1]);
        return $this;
    }

    public function bySection(){
        $this->groupBy('sections_id');
        return $this;
    }
}