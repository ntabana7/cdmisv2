<?php

namespace backend\modules\cna\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\InputTypes]].
 *
 * @see \backend\modules\cna\models\query\InputTypes
 */
class InputTypesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\InputTypes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\InputTypes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\InputTypes|array|null
     */
    public function sortByTitle()
    {
        $this->orderBy(['title' => SORT_ASC]);
        return $this;
    }
}
