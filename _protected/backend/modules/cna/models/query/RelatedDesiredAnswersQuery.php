<?php

namespace backend\modules\cna\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\RelatedDesiredAnswers]].
 *
 * @see \backend\modules\cna\models\query\RelatedDesiredAnswers
 */
class RelatedDesiredAnswersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedDesiredAnswers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedDesiredAnswers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
