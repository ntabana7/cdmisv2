<?php

namespace backend\modules\cna\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\RelatedCurrentAnswers]].
 *
 * @see \backend\modules\cna\models\query\RelatedCurrentAnswers
 */
class RelatedCurrentAnswersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedCurrentAnswers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedCurrentAnswers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
