<?php

namespace backend\modules\cna\models\query;

use backend\modules\cna\models\base\Assessment;
use backend\models\FiscalYears;
use yii;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\Assessment]].
 *
 * @see \backend\modules\cna\models\query\Assessment
 */
class AssessmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\Assessment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\Assessment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    
    /**
    *Get assessment by level (Institutional , Organisational or Indivudual)
    *@return \backend\modules\cna\models\query\Assessment|array|null
    */
    public function byLevel($idLevel){
        $this->andWhere(['{{%assessment}}.level'=> $idLevel]);
        return $this;
    }

    /**
    * Order assessment by current situation status
    * @return \backend\modules\cna\models\query\Assessment|array|null
    */
    public function orberByCurrentStatus(){
        $this->orderBy(['action_status' => SORT_ASC]);
        return $this;
    }

    /**
    * Get assessment by institution
    * @return \backend\modules\cna\models\query\Assessment|array|null
    */
    public function byInstitution($idInstitution = null ){

        if (Yii::$app->user->can('cluster')){
             $this->andWhere(['institution_id' => $idInstitution]);  
             return $this; 

        }
        elseif(Yii::$app->user->can('approver')){

            $this->andWhere(['institution_id' => Yii::$app->user->identity->userFrom])
                 ->andWhere(['pre_submitted' => 1]); 

            return $this;       

        }elseif(Yii::$app->user->can('institution')){

             $this->andWhere(['institution_id' => Yii::$app->user->identity->userFrom]);  

             return $this;     

        }else{

            return $this;   

        }
    }

    // public function isAdmin(){
       
    //    if (Yii::$app->user->can('theCreator') || Yii::$app->user->can('admin') ){
    //          return $this;   
    //     }

    // }

   /**
    * Current assessement year
    * @return \backend\modules\cna\models\query\Assessment|array|null
    */

   public function isCurrent(){
        $this->andWhere(['between', 'created_at', FiscalYears::startdate(),FiscalYears::enddate() ]);
        return $this;
   }

   /**
    * submitted assessement
    * @return \backend\modules\cna\models\query\Assessment|array|null
    */

    public function isSubmitted(){
        $this->andWhere(['submission' => 1]);
        return $this;
    }

    /**
    * pre submitted assessement
    * @return \backend\modules\cna\models\query\Assessment|array|null
    */

    public function isPreSubmitted(){
        $this->andWhere(['pre_submitted' => 1]);
        return $this;
    }

    /**
    * approved assessement
    * @return \backend\modules\cna\models\query\Assessment|array|null
    */

    public function isApproved(){
        $this->andWhere(['approved' => 1]);
        return $this;
    }
}
