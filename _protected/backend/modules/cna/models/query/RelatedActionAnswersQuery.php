<?php

namespace backend\modules\cna\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\cna\models\query\RelatedActionAnswers]].
 *
 * @see \backend\modules\cna\models\query\RelatedActionAnswers
 */
class RelatedActionAnswersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedActionAnswers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedActionAnswers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
