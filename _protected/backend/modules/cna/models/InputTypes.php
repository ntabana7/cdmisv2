<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\InputTypes as BaseInputTypes;

/**
 * This is the model class for table "input_types".
 */
class InputTypes extends BaseInputTypes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['type','title'], 'string', 'max' => 50]
        ]);
    }
	
}
