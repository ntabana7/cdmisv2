<?php

namespace backend\modules\cna\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cna\models\Assessment;

/**
 * backend\modules\cna\models\search\AssessmentSearch represents the model behind the search form about `backend\modules\cna\models\Assessment`.
 */
 class AssessmentSearch extends Assessment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'questions_id', 'level', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['current_situation', 'desired_capacity', 'capacity_building_action', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$level=null,$idInstitution=null)
    {
        if(!is_null($level)){
            $query = Assessment::find()->byLevel($level)->byInstitution($idInstitution)->isCurrent()->orberByCurrentStatus();
        }else{
             $query = Assessment::find()->byInstitution($idInstitution)->isCurrent();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'questions_id' => $this->questions_id,
            'level' => $this->level,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'current_situation', $this->current_situation])
            ->andFilterWhere(['like', 'desired_capacity', $this->desired_capacity])
            ->andFilterWhere(['like', 'capacity_building_action', $this->capacity_building_action]);

        return $dataProvider;
    }     
}
