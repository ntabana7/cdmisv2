<?php

namespace backend\modules\cna\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cna\models\QuestionOptions;

/**
 * backend\modules\cna\models\search\QuestionOptionsSearch represents the model behind the search form about `backend\modules\cna\models\QuestionOptions`.
 */
 class QuestionOptionsSearch extends QuestionOptions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'questions_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['question_option', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuestionOptions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'questions_id' => $this->questions_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'question_option', $this->question_option]);

        return $dataProvider;
    }
}
