<?php

namespace backend\modules\cna\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cna\models\Questions;

/**
 * backend\modules\cna\models\search\QuestionsSearch represents the model behind the search form about `backend\modules\cna\models\Questions`.
 */
 class QuestionsSearch extends Questions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sections_id', 'dependent_question_id', 'inputtype', 'action_type', 'desired_type', 'status', 'position', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['question', 'dependent_question_needed', 'slug','required_question', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sections_id' => $this->sections_id,
            'dependent_question_id' => $this->dependent_question_id,
            'inputtype' => $this->inputtype,
            'action_type' => $this->action_type,
            'desired_type' => $this->desired_type,
            'status' => $this->status,
            'position' => $this->position,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'dependent_question_needed', $this->dependent_question_needed])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'required_question', $this->required_question]);

        return $dataProvider;
    }
}
