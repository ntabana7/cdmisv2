<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\Questions as BaseQuestions;

/**
 * This is the model class for table "questions".
 */
class Questions extends BaseQuestions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sections_id', 'question', 'inputtype','action_type', 'challenge_type','desired_type', 'status'], 'required'],
            [['sections_id', 'dependent_question_id', 'desired_dependent_question_id', 'challenge_dependent_question_id', 'action_dependent_question_id', 'inputtype', 'challenge_type','action_type', 'desired_type', 'status', 'position', 'created_by', 'updated_by', 'deleted_by','challenge_dependent_question_needed','desired_dependent_question_needed','action_dependent_question_needed'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['question'], 'string', 'max' => 200],
            [['slug'], 'string', 'max' => 30],
            [['dependent_question_needed', 'required_question'], 'string', 'max' => 1]
        ]);
    }
	
}
