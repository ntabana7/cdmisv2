<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\Question as BaseQuestion;

/**
 * This is the model class for table "questions".
 */
class Question extends BaseQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['question', 'sections_id', 'status', 'inputtype'], 'required'],
            [['sections_id','dependent_question_id', 'desired_dependent_question_id', 'challenge_dependent_question_id', 'action_dependent_question_id', 'parent_id', 'created_by', 'deleted_by', 'position', 'status', 'inputtype'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['question', 'if_applicable'], 'string', 'max' => 200]
        ]);
    }
	
}
