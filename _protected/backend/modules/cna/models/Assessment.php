<?php

namespace backend\modules\cna\models;

use Yii;
use \backend\modules\cna\models\base\Assessment as BaseAssessment;
use backend\modules\cna\models\base\relatedCurrentAnswers;
use backend\modules\cna\models\base\RelatedDesiredAnswers;
use backend\modules\cna\models\base\RelatedChallengeAnswers;
use backend\modules\cna\models\base\RelatedActionAnswers;

/**
 * This is the model class for table "assert(assertion)ssment".
 */
class Assessment extends BaseAssessment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['questions_id'], 'required'],
            [['questions_id', 'institution_id','level','current_status','desired_status','challenge_status','action_status','approved','pre_submitted','submission','planned','created_by', 'updated_by','deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['slug_question'], 'string','max' => 30],
            [['current_situation'], 'string', 'max' => 200],
            [['desired_capacity','challenge', 'capacity_building_action','comments'], 'string', 'max' => 255]
        ]);
    }
	
    public static function numberQuestionBySection($idLevel){
        $query = new \yii\db\Query;
        $data = $query->select(['count(*) AS numbereofquestions, section_name'])  
                ->from('sections')
                ->leftJoin('questions', 'sections.id = questions.sections_id')
                ->andWhere(['sections.levels_id' =>$idLevel])
                ->andWhere(['questions.status' => 1])
                ->groupBy('sections_id')
                ->all();

        return $data;
    }

    private static function getAssessement($level_id,$idInstitution=null){
        return Assessment::find()->byInstitution($idInstitution)->byLevel($level_id)->isCurrent();
    }

    private static function answeredCurrentQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['current_status' => 1])->count();
    }

    private static function answeredDesiredQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['desired_status' => 1])->count();
    }

    private static function answeredChallengedQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['challenge_status' => 1])->count();
    }

    private static function answeredActionQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['action_status' => 1])->count();
    }

    private static function totalQuestions($level_id,$idInstitution=null){
        $total = Assessment::find()->byInstitution($idInstitution)->byLevel($level_id)->isCurrent()->count();
        return ($total == 0) ? 1 : $total;
    }

    private static function getCurrentPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredCurrentQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

    private static function getDesiredPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredDesiredQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

     private static function getChallengedPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredChallengedQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

    private static function getActionPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredActionQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

    private static function submissionQuestions($level_id,$idInstitution=null){
        return Assessment::find()->byInstitution($idInstitution)->byLevel($level_id)->isCurrent()->andWhere(['submission' => 1])->count();
    }

    private static function answeredApprovedQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['approved' => 1])->count();
    }


    private static function answeredPendingQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['approved' => 0])->count();
    }

    private static function answeredRejectedQuestions($level_id,$idInstitution=null){
        return static::getAssessement($level_id,$idInstitution)->andWhere(['approved' => 2])->count();
    }

    private static function getSubmissionPercentage($level_id,$idInstitution=null){
       return round(((100 * static::submissionQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

     private static function getApprovedPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredApprovedQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

    private static function getPendingPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredPendingQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

     private static function getRejectedPercentage($level_id,$idInstitution=null){
       return round(((100 * static::answeredRejectedQuestions($level_id,$idInstitution)) / static::totalQuestions($level_id,$idInstitution) ) );
    }

    public static function progressClass($number){
        if($number == 100) {
            return["" , "progress-bar-success"];
        }
        elseif($number >= 70 && $number <= 99) {
            return ["active" , "progress-bar-info"];
        }
        elseif($number < 70 && $number >= 50){
             return ["active" , "progress-bar-warning"]; 
        }else{
             return ["active" , "progress-bar-danger"];
        }
    }

    public static function progress($level_id,$idInstitution=null) {
        $numberCurrent    = static::getCurrentPercentage($level_id,$idInstitution);
        $numberDesired    = static::getDesiredPercentage($level_id,$idInstitution);
        $numberChallenged = static::getChallengedPercentage($level_id,$idInstitution);
        $numberAction     = static::getActionPercentage($level_id,$idInstitution);

        list($activeCurrent , $classCurrent)       = static::progressClass($numberCurrent);
        list($activeDesired , $classDesired)       = static::progressClass($numberDesired);
        list($activeChallenged , $classChallenged) = static::progressClass($numberChallenged);
        list($activeAction , $classAction)         = static::progressClass($numberAction);

        return $progress = [
            'label'   => ['current' => $numberCurrent , 'desired' => $numberDesired , 'challenge' => $numberChallenged ,'action' => $numberAction],
            'percent' => ['current' => $numberCurrent , 'desired' => $numberDesired , 'challenge' => $numberChallenged ,'action' => $numberAction],
            'class'   => ['current' => $classCurrent  , 'desired' => $classDesired  , 'challenge' => $classChallenged  ,'action' => $classAction ],
            'active'  => ['current' => $activeCurrent , 'desired' => $activeDesired , 'challenge' => $activeChallenged ,'action' => $activeAction]
        ];
    }

    public static function progressApproval($level_id) {
        $numberSubmitted = static::getSubmissionPercentage($level_id);
        $numberApproved  = static::getApprovedPercentage($level_id);
        $numberPending   = static::getPendingPercentage($level_id);
        $numberRejected  = static::getRejectedPercentage($level_id);        

        list($activeSubmitted , $classSubmitted)= static::progressClass($numberSubmitted);
        list($activeApproved , $classApproved)  = static::progressClass($numberApproved);
        list($activePending , $classPending)    = static::progressClass($numberPending);
        list($activeRejected , $classRejected)  = static::progressClass($numberRejected);


        return $progress = [
            'label'   => ['submitted' => $numberSubmitted, 'approved' => $numberApproved , 'pending' => $numberPending , 'rejected' => $numberRejected ],
            'percent' => ['submitted' => $numberSubmitted, 'approved' => $numberApproved , 'pending' => $numberPending , 'rejected' => $numberRejected ],
            'class'   => ['submitted' => $classSubmitted, 'approved' => $classApproved  , 'pending' => $classPending  , 'rejected' => $classRejected  ],
            'active'  => ['submitted' => "", 'approved' => "" , 'pending' => "" , 'rejected' => "" ]
        ];
    }

    private static function answeredQuestion($level_id,$idInstitution=null){
        $answered = static::answeredCurrentQuestions($level_id,$idInstitution) + static::answeredDesiredQuestions($level_id,$idInstitution) + static::answeredChallengedQuestions($level_id,$idInstitution) + static::answeredActionQuestions($level_id,$idInstitution);       

        return $answered / 4;
    }

    public static function percentages($level_id,$idInstitution){
        return number_format((static::answeredQuestion($level_id,$idInstitution) / static::totalQuestions($level_id,$idInstitution) ) * 100,0);
    }

    public static function challenges($idassessment){
        $individual = Assessment::find()->where(['id' => $idassessment])->isCurrent()->one();

        $data = [];
        foreach($individual->relatedCurrentAnswers as $get){ 
            $desired   = RelatedDesiredAnswers::find()
                                ->where(['assessment_id' => $idassessment , 'specific_area' => $get->specific_area])
                                ->one();

            $challenge = RelatedChallengeAnswers::find()
                                ->where(['assessment_id' => $idassessment , 'specific_area' => $get->specific_area])
                                ->one();

            $action    = RelatedActionAnswers::find()
                                ->where(['assessment_id' => $idassessment , 'specific_area' => $get->specific_area])
                                ->one();

            $data[] = [
                    'challenge'        => (!empty($challenge->challenge)) ?$challenge->challenge:$individual->challenge,
                    'basiline'         => (!empty($get->baseline)) ? $get->baseline : '',
                    'nbrbeneficiaries' => (!empty($desired->nbrbeneficiaries)) ? $desired->nbrbeneficiaries: '',
                    'action_taken'     => (!empty($desired->nbrbeneficiaries)) ? $action->action_taken : $individual->capacity_building_action,
                    'assessment_id'    => $idassessment,
            ];
        }

        if(empty($data)){
            $data[] = [
                    'challenge'        => $individual->challenge,
                    'basiline'         => 0,
                    'nbrbeneficiaries' => 0,
                    'action_taken'     => $individual->capacity_building_action,
                    'assessment_id'    => $idassessment,
            ];
        }

        return $data;
    }

}
