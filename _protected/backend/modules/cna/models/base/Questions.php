<?php

namespace backend\modules\cna\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "{{%questions}}".
 *
 * @property integer $id
 * @property integer $sections_id
 * @property string $question
 * @property string $slug
 * @property integer $dependent_question_needed
 * @property integer $desired_dependent_question_needed
 * @property integer $action_dependent_question_needed
 * @property integer $challenge_dependent_question_needed
 * @property integer $dependent_question_id
 * @property integer $desired_dependent_question_id
 * @property integer $action_dependent_question_id
 * @property integer $challenge_dependent_question_id
 * @property integer $inputtype
 * @property integer $action_type
 * @property integer $challenge_type
 * @property integer $desired_type
 * @property integer $required_question
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 *
 * @property \backend\modules\cna\models\Assessment[] $assessments
 * @property \backend\modules\cna\models\QuestionOptions[] $questionOptions
 * @property \backend\modules\cna\models\Sections $sections
 * @property \backend\modules\cna\models\InputTypes $inputtype0
 * @property \backend\modules\cna\models\InputTypes $desiredType
 * @property \backend\modules\cna\models\InputTypes $challengeType
 * @property \backend\modules\cna\models\InputTypes $actionType
 * @property \backend\modules\cna\models\DependentQuestion $dependentQuestion
 */
class Questions extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;
    public  $level;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'sections',
            'inputtype0',
            'desiredType',
            'actionType',
            //'assessments',
            'challengeType',
            'questionOptions',
            'dependentQuestion',
            'actionDependentQuestion',
            'desiredDependentQuestion',
            'challengeDependentQuestion'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sections_id', 'question', 'inputtype'  , 'challenge_type', 'action_type', 'desired_type', 'status'], 'required'],
            [['sections_id', 'dependent_question_id', 'desired_dependent_question_id', 'challenge_dependent_question_id', 'action_dependent_question_id', 'inputtype'  , 'challenge_type', 'action_type', 'desired_type', 'status', 'position', 'created_by', 'updated_by', 'deleted_by','challenge_dependent_question_needed','desired_dependent_question_needed','action_dependent_question_needed','level'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['question'], 'string', 'max' => 200],
            [['slug'], 'string', 'max' => 30],
            [['dependent_question_needed', 'required_question'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'sections_id' => Yii::t('backend', 'Sections ID'),
            'question' => Yii::t('backend', 'Question'),
            'slug' => Yii::t('backend', 'Slug'),
            'dependent_question_needed' => Yii::t('backend', 'Dependent Question Needed'),
            'dependent_question_id' => Yii::t('backend', 'Dependent Question'),
            'desired_dependent_question_id' => Yii::t('backend', 'Desired Dependent Question'),
            'challenge_dependent_question_id' => Yii::t('backend', 'challenge Dependent Question'),
            'action_dependent_question_id' => Yii::t('backend', 'Action Dependent Question'),
            'inputtype' => Yii::t('backend', 'Current Type'),
            'action_type' => Yii::t('backend', 'Action Type'),
            'desired_type' => Yii::t('backend', 'Desired Type'),
            'challenge_type' => Yii::t('backend', 'Challenge Type'),
            'required_question' => Yii::t('backend', 'Required Question'),
            'status' => Yii::t('backend', 'Status'),
            'position' => Yii::t('backend', 'Position'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessments()
    {
        return $this->hasMany(\backend\modules\cna\models\Assessment::className(), ['questions_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionOptions()
    {
        return $this->hasMany(\backend\modules\cna\models\QuestionOptions::className(), ['questions_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDependentQuestion()
    {
        return $this->hasOne(\backend\modules\cna\models\DependentQuestion::className(), ['id' => 'dependent_question_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesiredDependentQuestion()
    {
        return $this->hasOne(\backend\modules\cna\models\DesiredDependentQuestion::className(), ['id' => 'desired_dependent_question_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getChallengeDependentQuestion()
    {
        return $this->hasOne(\backend\modules\cna\models\ChallengeDependentQuestion::className(), ['id' => 'challenge_dependent_question_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionDependentQuestion()
    {
        return $this->hasOne(\backend\modules\cna\models\ActionDependentQuestion::className(), ['id' => 'action_dependent_question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasOne(\backend\modules\cna\models\Sections::className(), ['id' => 'sections_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInputtype0()
    {
        return $this->hasOne(\backend\modules\cna\models\InputTypes::className(), ['id' => 'inputtype']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesiredType()
    {
        return $this->hasOne(\backend\modules\cna\models\InputTypes::className(), ['id' => 'desired_type']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionType()
    {
        return $this->hasOne(\backend\modules\cna\models\InputTypes::className(), ['id' => 'action_type']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getChallengeType()
    {
        return $this->hasOne(\backend\modules\cna\models\InputTypes::className(), ['id' => 'challenge_type']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\QuestionsQuery the active query used by this AR class.
     */
    public static function find()
    { 
        $query = new \backend\modules\cna\models\query\QuestionsQuery(get_called_class());
        return $query->where(['questions.deleted_by' => 0]);//->cache(7200);
    }
}
