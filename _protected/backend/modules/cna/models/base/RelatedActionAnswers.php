<?php

namespace backend\modules\cna\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "{{%related_action_answers}}".
 *
 * @property integer $id
 * @property integer $assessment_id
 * @property string $specific_area
 * @property integer $action
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 *
 * @property \backend\modules\cna\models\Assessment $assessment
 */
class RelatedActionAnswers extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'assessment'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['assessment_id'], 'required'],
            [['assessment_id', 'specific_area', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['action_taken'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%related_action_answers}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'assessment_id' => Yii::t('backend', 'Assessment ID'),
            'specific_area' => Yii::t('backend', 'Specific Area'),
            'action_taken' => Yii::t('backend', 'Action'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessment()
    {
        return $this->hasOne(\backend\modules\cna\models\Assessment::className(), ['id' => 'assessment_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\RelatedActionAnswersQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \backend\modules\cna\models\query\RelatedActionAnswersQuery(get_called_class());
        return $query->where(['related_action_answers.deleted_by' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualificationArea()
    {
        return $this->hasOne(\backend\modules\cdproviders\models\ProviderQualificationarea::className(), ['idQualifarea' => 'specific_area']);
    }
}
