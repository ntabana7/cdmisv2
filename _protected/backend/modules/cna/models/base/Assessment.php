<?php

namespace backend\modules\cna\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "{{%assessment}}".
 *
 * @property integer $id
 * @property integer $questions_id
 * @property string $slug_question
 * @property string $current_situation
 * @property string $desired_capacity
 * @property string $challenge
 * @property string $capacity_building_action
 * @property integer $level
 * @property integer $institution_id
 * @property integer $current_status
 * @property integer $desired_status
 * @property integer $challenge_status
 * @property integer $action_status
 * @property integer $approved
 * @property integer $pre_submitted
 * @property integer $submission
 * @property integer $planned
 * @property integer $comments
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 *
 * @property \backend\modules\cna\models\Questions $questions
 * @property \backend\models\CbLevels $level0
 * @property \common\models\User $institution
 * @property \backend\modules\cna\models\RelatedActionAnswers[] $relatedActionAnswers
 * @property \backend\modules\cna\models\RelatedChallengeAnswers[] $relatedChallengeAnswers
 * @property \backend\modules\cna\models\RelatedCurrentAnswers[] $relatedCurrentAnswers
 * @property \backend\modules\cna\models\RelatedDesiredAnswers[] $relatedDesiredAnswers
 */
class Assessment extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;
    public $other_current;
    public $other_desired;
    public $other_challenge;
    public $other_action;
    const PRE_SUBMITTED = 1;
    const SUBMITTED     = 1;
    const APPROVED      = 1;
    const EVENT_UNIT_ASSESSMENT_SUBMITTED = 'submit-unit-assessment';
    const EVENT_DG_ASSESSMENT_RECEIVED    = 'received-assessment-dg';
    const EVENT_DG_ASSESSMENT_SUBMITTED   = 'submit-dg-assessment';
    const EVENT_UNIT_ASSESSMENT_RECEIVED  = 'received-assessment-unit';

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'questions',
            'level0',
            'institution',
            'relatedActionAnswers',
            'relatedCurrentAnswers',
            'relatedDesiredAnswers',
            'relatedChallengeAnswers',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questions_id'], 'required'],
            [['questions_id', 'institution_id','level','current_status','desired_status','challenge_status','action_status','approved','pre_submitted','submission','planned','created_by', 'updated_by','deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['slug_question'], 'string', 'max' => 30],
            [['current_situation','other_current','other_desired','other_challenge','other_action'], 'string', 'max' => 200],
            [['desired_capacity', 'challenge', 'capacity_building_action','comments'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%assessment}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'questions_id' => Yii::t('backend', 'Questions ID'),
            'slug_question' => Yii::t('backend', 'Slug Question'),
            'current_situation' => Yii::t('backend', 'Current Situation'),
            'desired_capacity' => Yii::t('backend', 'Desired Capacity'),
            'challenge' => Yii::t('backend', 'Challenge'),
            'capacity_building_action' => Yii::t('backend', 'Action Plan'),
            'level' => Yii::t('backend', 'Level'),
            'institution_id' => Yii::t('backend', 'Institution ID'),
            'current_status' => Yii::t('backend', 'Current Status'),
            'desired_status' => Yii::t('backend', 'Desired Status'),
            'challenge_status' => Yii::t('backend', 'Challenge Status'),
            'action_status' => Yii::t('backend', 'Action Status'),
            'approved' => Yii::t('backend', 'Approved'),
            'pre_submitted' => Yii::t('backend', 'Pre Submitted'),
            'submission' => Yii::t('backend', 'Submission'),
            'planned' => Yii::t('backend', 'Planned'),
            'comments' => Yii::t('backend', 'Comments'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasOne(\backend\modules\cna\models\Questions::className(), ['id' => 'questions_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel0()
    {
        return $this->hasOne(\backend\models\CbLevels::className(), ['idLevel' => 'level']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'institution_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedActionAnswers()
    {
        return $this->hasMany(\backend\modules\cna\models\base\RelatedActionAnswers::className(), ['assessment_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedChallengeAnswers()
    {
        return $this->hasMany(\backend\modules\cna\models\base\RelatedChallengeAnswers::className(), ['assessment_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedCurrentAnswers()
    {
        return $this->hasMany(\backend\modules\cna\models\base\RelatedCurrentAnswers::className(), ['assessment_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedDesiredAnswers()
    {
        return $this->hasMany(\backend\modules\cna\models\base\RelatedDesiredAnswers::className(), ['assessment_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \backend\modules\cna\models\query\AssessmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \backend\modules\cna\models\query\AssessmentQuery(get_called_class());
        return $query->where(['assessment.deleted_by' => 0]);
    }

    /*
    *Notification email for institution after submitting assessment
    *
    */
    public function unitAssessmentSubmissionNotification($event)
    {
        return Yii::$app->mailer->compose(['html'=>'unitassessmentnotification'], ['model' => '1'])
            ->setTo($this->institutionEmail())
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Assessment')
            ->send();
    }

    /*
    * When institution has been submit the assessment , the approver (DG) should be get this email
    *
    */
    public function dgAssessmentReceivedNotification($event)
    {   
        return Yii::$app->mailer->compose(['html'=>'assessmentnotificationreceiveddg'], ['model' => '1'])
            ->setTo($this->institutionApproverEmail())
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Assessment')
            ->send();
    }

    /*
    * Notification email for DG or approver after submitting assessment
    *
    */
    public function dgAssessmentSubmissionNotification($event)
    {
        return Yii::$app->mailer->compose(['html'=>'dgassessmentnotification'], ['model' => '1'])
            ->setTo($this->institutionApproverEmail())
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Assessment')
            ->send();
    }

    public function unitAssessmentReceivedNotification($event)
    {   //$event->sender->email_to
        return Yii::$app->mailer->compose(['html'=>'assessmentnotificationreceivedunit'], ['model' => '1'])
            ->setTo($this->institutionEmail())
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Assessment')
            ->send();
    }

     public function init(){
        $this->on(self::EVENT_UNIT_ASSESSMENT_SUBMITTED, [$this, 'unitAssessmentSubmissionNotification']);
        $this->on(self::EVENT_DG_ASSESSMENT_RECEIVED,    [$this, 'dgAssessmentReceivedNotification']);
        $this->on(self::EVENT_DG_ASSESSMENT_SUBMITTED,   [$this, 'dgAssessmentSubmissionNotification']);
        $this->on(self::EVENT_UNIT_ASSESSMENT_RECEIVED,  [$this, 'unitAssessmentReceivedNotification']);
    }

    /*
    * Institution Email
    */
    private function institutionEmail(){
        return 'udenyse@gmail.com';// \backend\models\Institutions::institutionEmail();
    }

    /*
    * Institution => DG or Approver Email
    */
    private function institutionApproverEmail(){
        return 'udenyse@gmail.com';// \backend\models\Institutions::institutionApproverEmail();
    }

    /*
    * institution=> cluster email
    */
    private function clusterEmail(){
        return 'udenyse@gmail.com';// \backend\models\Institutions::clusterEmail();
    }
}
