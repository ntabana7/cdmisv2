<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\InputTypes */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="input-types-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Input Types').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">         
            <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'title',
        'type',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="container row">
<?php
if($providerQuestions->totalCount){
    $gridColumnQuestions = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'question',
            [
                'attribute' => 'sections.id',
                'label' => Yii::t('backend', 'Sections')
            ],
            'required_question',
            'position',
            'status',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerQuestions,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-questions']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend', 'Questions')),
        ],
        'columns' => $gridColumnQuestions
    ]);
}
?>

    </div>
</div>
