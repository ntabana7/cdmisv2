<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\InputTypes */

$this->title = Yii::t('backend', 'Create Input Types');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="input-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
