<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cna\models\search\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Progress;
use kartik\export\ExportMenu;
use common\grid\ApprovedColumn;

$this->title = Yii::t('backend', 'Assessment');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";

$preview = "$('.preview-button').click(function(){
    $('.preview-form').toggle(1000);
    return false;
});";

$this->registerJs($search);
$this->registerJs($preview);
?>
<div class="assessment-index">
    <table class='table table-striped table-bordered'>
        <th style="text-align: center">Submission</th>
        <th style="text-align: center">Approved</th>
        <th style="text-align: center">No Approved</th>
        <th style="text-align: center">Rejected</th>
        <tr>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['submitted'].'%',
                            'percent' => $progress['percent']['submitted'],
                            'barOptions' => ['class' => $progress['class']['submitted']],
                            'options' => ['class' => $progress['active']['submitted'].' progress-striped']
                    ]);
                ?> 
            </td>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['approved'].'%',
                            'percent' => $progress['percent']['approved'],
                            'barOptions' => ['class' => $progress['class']['approved']],
                            'options' => ['class' => $progress['active']['approved'].' progress-striped']
                    ]);
                ?> 
            </td>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['pending'].'%',
                            'percent' => $progress['percent']['pending'],
                            'barOptions' => ['class' => $progress['class']['pending']],
                            'options' => ['class' => $progress['active']['pending'].' progress-striped']
                    ]);
                ?> 
            </td>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['rejected'].'%',
                            'percent' => $progress['percent']['rejected'],
                            'barOptions' => ['class' => $progress['class']['rejected']],
                            'options' => ['class' => $progress['active']['rejected'].' progress-striped']
                    ]);
                ?> 
            </td>
        </tr>
    </table>
    <hr>
    <p>
        <?= Html::a(Yii::t('backend', 'Submission'), '#', ['class' => 'btn btn-success preview-button']) ?>
        <?= Html::a(Yii::t('backend', 'Advanced Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <hr>
    <div class="search-form" style="display:none">
        <?=  $this->render('_searchIndex', ['model' => $searchModel]); ?>
    </div>

    <div class="preview-form" style="display:none">
        <?=  $this->render('_previewSubmission', ['previews' => $previews , 'level' => $level]); ?>
    </div>

    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'questions_id',
            'label' => Yii::t('backend', 'Questions'),
            'value' => function($model){                   
                return $model->questions->question;                   
            },
        'width' => '55%',
        ],
        [
            'class' => 'common\grid\ApprovedColumn',
            'attribute' => 'approved', 
            'label' => Yii::t('backend', 'Approved'),
            'readonly' => function($model, $key, $index, $widget) {
                // do not allow editing of inactive records
                if($model->submission == 1) return true;
            },
            'editableOptions' => [
                'header' => 'Approved', 
                'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                'data' => [ 0 => 'Pending' , 1 => 'Approved' , 2 => 'Rejected'],
            ],
            'hAlign' => 'center', 
            'width' => '10%',
        ],

        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'comments', 
            'value' => function($model){                   
                return (strlen($model->comments) < 40) ? $model->comments : substr($model->comments, 0,40).' ...';                  
            },
            'label' => Yii::t('backend', 'Comments'),
            'hAlign' => 'left', 
            'width' => '25%',
               'editableOptions' => [
                    'size' => 'lg',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    'placement' => 'left',
                    'valueIfNull' => 'No comment'
                ],
        ],
    

        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'submission', 
            'label' => Yii::t('backend', 'Submitted'),
            'hAlign' => 'left', 
            'width' => '10%',
        ],
    
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-assessment']],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
