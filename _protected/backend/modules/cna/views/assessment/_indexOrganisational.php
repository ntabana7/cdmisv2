<?php 
use \yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;

$start = "$('#start-button-".$model->id."').click(function(){
    $('#start-form-".$model->id."').toggle(1);
    return false;
});";

$this->registerJs($start); 
?>
<style>
    .modal-content {
        width: 900px;
        margin-left: -50px;
  
    }

</style>
<div class="well col-sm-10">
    <div class="col-sm-9">
        <h4><?= Html::a(Html::encode($model->questions->position.'. '.$model->questions->question), ['view', 'id' => $model->id]) ?></h4>
    </div>
    <div class="col-sm-1 pull-right">
        <?php 
            if ($model->pre_submitted == 1) {

                if($model->submission == 1){
                    echo  Html::a(Yii::t('app', 'Rejected').'<i class="fa fa-level-down" aria-hidden="true"></i>', ['update', 'id' => $model->id], 
                    [
                        'class' => 'btn btn-danger' , 
                        'id' => 'start-button-'.$model->id,
                        'style' => 'margin-left: 80px;style="margin-top: 15px'
                    ]);  
                } 
                else{
                    if($model->approved == 2) {
                    Modal::begin([
                      'header' => '<b>'.$model->questions->id.'. '.$model->questions->question.'</b>',
                      'toggleButton' => ['class' => 'btn btn-danger','label' => '<i class="fa fa-plus" aria-hidden="true"></i>'],
                      'class' => 'modal-lg' , 
                  ]);
                     
                    echo $this->render('_form', [ 
                        'model' => $model , 
                        'url' => 'assessment/update-organisational',
                        'answer' => true,
                        'total_number' => false,
                        'baseline' => false,
                        'specific_area' => false,
                        'nbrbeneficiaries' => false,
                        'challenge' => true,
                        'action_taken' => true,
                    ]); 
                     
                    Modal::end();    
                    }
                    else if($model->approved == 1) {
                        echo '<span class="btn btn-success">Approved <i class="fa fa-check"></i></span>';
                    }else {
                        echo '<span class="btn btn-warning">Pending <i class="fa fa-clock-o" aria-hidden="true"></i></span>';
                    }
                }          
            }else{
               if (Yii::$app->user->can('cluster')){
                    echo '<span class="btn btn-success" style = "margin-left: 120px;margin-top: -3px">New </span>';  
                }else{
                   Modal::begin([
                          'header' => '<b>'.$model->questions->id.'. '.$model->questions->question.'</b>',
                          'toggleButton' => ['class' => 'btn btn-primary','label' => '<i class="fa fa-plus" aria-hidden="true"></i>'],
                          'class' => 'modal-lg' , 
                      ]);
                     
                    echo $this->render('_form', [ 
                        'model' => $model , 
                        'url' => 'assessment/update-organisational',
                        'answer' => true,
                        'total_number' => false,
                        'baseline' => false,
                        'specific_area' => false,
                        'nbrbeneficiaries' => false,
                        'challenge' => true,
                        'action_taken' => true,
                    ]); 
                     
                    Modal::end();    
                }
                
            }
        
        ?>
    </div>
    
    <br><br>
    <?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'current_situation',
        'desired_capacity',
        'challenge',
        'capacity_building_action',
        [
            'class' => 'kartik\grid\DataColumn',
            'attribute' => 'comments', 
            'value' => function($model){                   
                return (strlen($model->comments) < 100) ? $model->comments : substr($model->comments, 0,100).' ...';                  
            },
        ]
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>

<div class="col-sm-2">
    
    <div class='row' style="text-align: center;">

       <?php if( $model->current_status == 1) 
         { 
            echo "<span class='label label-primary'>Current: </span>"."<span class='label label-primary'>Answered</span>";
        }else{
            echo "<span class='label label-danger'> Current: </span>"."<span class='label label-danger'>Not Yet</span>";
        }?>
    </div>
    <hr>
    <div class='row' style="text-align: center;">
         
       <?php if( $model->desired_status == 1) 
        { 
            echo "<span class='label label-primary'>Desired: </span>"."<span class='label label-primary'>Answered</span>";
        }else{
            echo "<span class='label label-danger'>Desired:</span>"."<span class='label label-danger'>Not Yet</span>";
        }?>
    </div>
    <hr>
    <div class='row' style="text-align: center;">
         
       <?php if( $model->challenge_status == 1) 
        { 
            echo "<span class='label label-primary'>Challenge: </span>"."<span class='label label-primary'>Answered</span>";
        }else{
            echo "<span class='label label-danger'>Challenge:</span>"."<span class='label label-danger'>Not Yet</span>";
        }?>
    </div>
    <hr>
    <div class='row' style="text-align: center;">
         
       <?php if( $model->action_status == 1) 
        { 
            echo "<span class='label label-primary'>Action: </span>"."<span class='label label-primary'>Answered</span>";
        }else{
            echo "<span class='label label-danger'>Action:</span>"."<span class='label label-danger'>Not Yet</span>";
        }?>
    </div>
    <hr>

    <div class='row' style="text-align: center;">
        <span class='label label-default'><?= $model->level0->level ?></span>
    </div>
    <hr>
</div>


