<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="assessment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'current_situation')->textInput(['maxlength' => true, 'value' => $model->questions->question , 'readonly' =>true]) ?>

    <?= $form->field($model, 'current_situation')->textInput(['maxlength' => true, 'placeholder' => 'Current Situation']) ?>

    <?= $form->field($model, 'desired_capacity')->textInput(['maxlength' => true, 'placeholder' => 'Desired Capacity']) ?>

    <?= $form->field($model, 'capacity_building_action')->textInput(['maxlength' => true, 'placeholder' => 'Capacity Building Action']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
