<div class="form-group" id="add-current-dependent-answers">
<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use backend\models\base\CBLevels;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'DependentAnswers',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'answer' => ['type' => TabularForm::INPUT_TEXT],
        'staff_number' => ['type' => TabularForm::INPUT_TEXT],
        'specific_area' => [
                            'type' => TabularForm::INPUT_DROPDOWN_LIST ,
                            'items'=>ArrayHelper::map(CbLevels::find()->orderBy('level')->asArray()->all(), 'level', 'level'),
                    ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend', 'Delete'), 'onClick' => 'delRowDependentAnswers(' . $key . '); return false;', 'id' => 'dependent-answers-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend', 'Add Dependent Answers'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowDependentAnswers()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

