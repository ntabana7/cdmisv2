<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = Yii::t('backend', 'Assessment');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<style>
    .cellContainer {
      width: 27%;
      float: left;
    }
</style>
<div class="container" >

    <h3 style="text-align: center"><u>Start Assessment</u></h3>    

        <div class='well cellContainer' style="margin: 150px 0 0 5px;height: 200px;">
            <div style='margin-left:4  0px'>
                <h2>Institutional</h2>
                <a href="<?= Url::to(['/cna/assessment/create'])?>" class='btn btn-sm btn-success   col-sm' style="margin:10px 0 0 20px"> 
                    Start <i class="fa fa-play"></i>
                </a>
            </div>
        </div>

        <div class='well cellContainer' style="margin: 150px 0 0 5px;height: 200px;">
            <div style='margin-left:40px'>
                <h2>Organisational</h2>
                <a href="<?= Url::to(['/cna/assessment/create-organisational'])?>" class='btn btn-sm btn-info col-sm' style="margin:10px 0 0 50px"> 
                    Start <i class="fa fa-play"></i>
                </a>
            </div>
        </div>

        <div class='well cellContainer' style="margin: 150px 0 0 5px;height: 200px;">
            <div style='margin-left: 40px'>
                <h2>Individual</h2>
                <a href="<?= Url::to(['/cna/assessment/create-individual'])?>" class='btn btn-sm btn-warning col-sm' style="margin:10px 0 0 50px"> 
                    start <i class="fa fa-play"></i>
                </a>
            </div>
        </div>
</div>


    <!-- <div class='well'>
            <p><b>1.<u> Puporse</u> :</b></p>
            <p>
                This template is designed to assist institutions assess the capacities they need to implement a policy priority
            </p>
    </div>

    <div class='well'>
            <p><b>2<u>. Approach/methodology</u>:</b></p>
            <p>
                A small task force should be put in place, composed of the 
                <b><i>planner, HR specialist, M&E specialist, technical experts and budget officer </i></b>
                are expected to undertake this assessment.
            </p>
            <p> 
                The work of this task force/sub-committee shall be linked to the <b><i>internal planning process of your institution</i></b>.
            </p>
            <p>
                The work of this group shall be coordinated by the <b><i>director of planning or his/her equivalent in your institution</i></b>.</p>
            <p>
                In cases where a group of institutions within <b><i>a sector or sub-sector</i></b> are involved in delivery on a policy priority, each institution should fill this form and the results aggregated by the lead ministry in close collaboration with PSCSBS.
            </p>
    </div>

    <div class="well">
            <p><b>3. <u>Scope</u>:</b></p>
            <p>
             The assessment should look at all three levels of capacity, beginning at the <code>Institutional level</code>, then going on to the <code>Organisational level</code> and finally ,<code> Individual level</code>. 
            </p>
            <p>
                Each level of capacity has its own <code>form</code>.
            </p>
    </div>

    <div class="well">
        <p>
            <b>
                4. <u>Process</u>: 
            </b>
        </p>

        <p>
            <i>The template has <code>five parts</code>, as follows:</i>
        </p>
            <hr>
        
        <p>
            <b>Part 1:</b>
        </p>
        <p>
            Questions
        </p>
            <hr>

        <p>
            <b>Part 2:</b>
        </p>
        <p>
            what is the <b>current situation</b>: this refers to what exists within your institution at the time of the assessment. 
        </p>
            
        <p>
            This information will establish the baseline for future assessments, based on a three-year cycle for carrying out 
            capacity needs assessment.
        </p>
    
        <p>
            Various options are provided for each question, please select the most appropriate box for your institution.
        </p>
            <hr>
        <p>
            <b>Part 3:</b>
        </p>
        <p>
         what is the <b><u>desired situation</u></b> to be able to deliver on the policy priority:</p>
         <p> 
            what capacities should your institution have to implement the policy priority. 
        </p>
            <hr>
        
        <p>
            <b>Part 4:</b>
        </p>
        <p>
            <b><u>capacity needs:</u></b> comparison between the desired situation and the current situation.
        </p>
            <hr>
        <p>
            <b>Part 5:</b>
        </p>
        <p>
            <b><u>capacity building actions:</u></b> the actions required to address the capacity needs.
        </p>
        <p>
            These actions will form the basis for the Capacity Building Plan (tool 6.1.).
        </p>

    </div>

    <div class="well">
        <p><b> 5. <u>General comments</u>:</b></p>
        <p>
            a) Please draw on any previous <u>Capacity Needs Assessment</u> that have been conducted in your institution, as well as other reports such as: 
        <ul>
            <li><b>audit reports</b></li>
            <li><b>evaluation reports</b></li> 
            <li><b>procurement reports</b></li> 
            <li><b>relevant parliamentary reports and</b></li> 
            <li><b>others</b></li>
        </ul>
        </p>
        <p>
            b) Please be precise and avoid vague formulations when filling part 3, 4 and 5 (e.g. do not write 
            <b>a new regulatory framework is needed</b>, but 
            <b>a new regulation on tariff harmonisation is needed</b>; do not write 
            <b>training for procurement unit</b>, but 
            <b>a training of procurement officer in contract negotiation</b>.
        </p>
        <p>
            c) Focus on what is most essential to delivery on the policy priority.  A line is provided for any additional issue you might want to raise.
        </p>
        <p>
            d) Note that the 
            <code>current situation</code>, 
            <code>desired situation</code>, 
            <code>capacity needs</code> and 
            <code>capacity actions</code> may go beyond what is under the direct control of your institution.
        </p>
        <p>
             They should however be identified when filling this template.
        </p>
    </div>

    <div class="well btn-toolbar"  >
        <a href="create"         class='btn btn-lg  btn-success  col-lg-4'> Institutional  </a>
        <a href="#organational"  class='btn btn-lg btn-info      col-lg-3'> Organisational </a>
        <a href="#individual"    class='btn btn-lg btn-warning   col-lg-4'> Individual     </a>
    </div>
 -->
</div>
