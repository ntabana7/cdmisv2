<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'DependentAnswers', 
        'relID' => 'dependent-answers', 
        'value' => \yii\helpers\Json::encode($model->dependentAnswers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
echo $model->questions->inputtype0->type;
?>
<div class="assessment-form col-lg-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'current_situation')->textInput(['maxlength' => true, 'placeholder' => 'Current Situation']) ?>

    <?= $form->field($model, 'desired_capacity')->textInput(['maxlength' => true, 'placeholder' => 'Desired Capacity']) ?>

    <?= $form->field($model, 'capacity_building_action')->textInput(['maxlength' => true, 'placeholder' => 'Capacity Building Action']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'DependentAnswers')),
            'content' => $this->render('_formDependentAnswers', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->dependentAnswers),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
