<?php 
    use yii\helpers\Url;
?>
<div class="well" style='background-color: white'>
    <div class='row'>
        <div class='col-lg-12'>
            <h1><center><u>List of Challenges</u></center></h1>
            <center><b><i>(Institutional , Organational ,Individual)</i></b></center>
        </div>
        <hr>
        <div class="col-lg-12">
            <table class='table table-bordered table-striped'>    
                <tr>
                    <td>#</td>
                    <th>Challenge</th>
                    <th>Basiline</th>
                    <th>Number of benificiaries</th>
                    <th>Action Taken</th>
                    <th>Plan</th>
                </tr>                  
                </thead>
                <tbody>
                <?php  foreach ($challenges as $key => $row) {?>
                <?php $assessment = implode(",",$row);?>
                <tr>
                    <td><?= $key + 1; ?></td>
                    <td><?= $row['challenge']; ?></td>
                    <td><?= $row['basiline']; ?></td>
                    <td><?= $row['nbrbeneficiaries']; ?></td>
                    <td><?= $row['action_taken']; ?></td>
                    <td align='center'>
                        <?php if (1==1 /*! $challenge->planned*/) { ?> 
                            <span class='label label-success'>
                                <a href="<?= Url::to(['/plans/create?assessment='.$assessment])?>">
                                    <i style='color:white' class='fa fa-plus'></i>
                                </a>
                            </span>
                        <?php } else { ?>
                            <span class='label label-success'><i class='fa fa-check'></i></span>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>  
    </div>
</div>     
