<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Assessment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Assessment').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
        <?php 
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                        'attribute' => 'questions.question',
                        'label' => Yii::t('backend', 'Questions')
                    ],
                'current_situation',
                'desired_capacity',
                'capacity_building_action',
                [
                        'attribute' => 'level0.level',
                        'label' => Yii::t('backend', 'Level')
                    ],
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]); 
        ?>
    </div>
</div>
