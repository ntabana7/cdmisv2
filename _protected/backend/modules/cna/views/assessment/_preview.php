   <?php 
        use yii\helpers\Url;
   ?>
    <div class="col-lg-12">
        <div class="table-responsive">
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th colspan="2" > 
                    <a href="<?= Url::to(['/cna/assessment/pre-submitted?level='.$level])?>" style='margin-left: 300px' class='btn btn-md  btn-warning  col-sm-4'> SUBMIT THIS ASSESSMENT</a>
                </th>
                <th colspan="4" style="text-align: center">Assessment</th>
                <th colspan="2" style="text-align: center">Submission</th>
            </tr>
            <tr>
                <td>#</td>
                <th>Question</th>
                <th>Current</th> 
                <th>Desired</th>
                <th>Challenge</th>
                <th>Action</th>
                <th>Pre-Submitted</th>
                <th>Submitted</th>
            </tr>                  
            </thead>
            <tbody>
                <?php foreach ($previews as $key => $assessment) { ?>
                    <tr>
                        <td><?= $key + 1;?></td>
                        <td><?= $assessment->questions->question; ?></td>

                        <td align='center'>
                            <?= ($assessment->current_status == 1) ? "<span class='label label-success'><i class='fa fa-check'></i></span>"
                                                                   : "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?>
                        </td>

                        <td align='center'>
                            <?= ($assessment->desired_status == 1) ? "<span class='label label-success'><i class='fa fa-check'></i></span>"
                                                                   : "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?>
                        </td>

                        <td align='center'>
                            <?= ($assessment->challenge_status == 1) ? "<span class='label label-success'><i class='fa fa-check'></i></span>"
                                                                   : "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?>
                        </td>

                        <td align='center'>
                            <?= ($assessment->action_status == 1) ? "<span class='label label-success'><i class='fa fa-check'></i></span>"
                                                                   : "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?>
                        </td>

                        <td align='center'>
                            <?= ($assessment->pre_submitted == 1) ? "<span class='label label-success'><i class='fa fa-check'></i></span>"
                                                                   : "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?>
                        </td>

                        <td align='center'>
                            <?= ($assessment->submission == 1) ? "<span class='label label-success'><i class='fa fa-check'></i></span>"
                                                                   : "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?>
                        </td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>       
