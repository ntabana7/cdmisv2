<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Assessment',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Assessment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="assessment-update">

    <h2><?= Html::encode($model->questions->question) ?></h2>
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
