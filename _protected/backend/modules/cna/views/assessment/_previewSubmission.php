   <?php 
        use yii\helpers\Url;
   ?>
    <div class="col-lg-12">
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th colspan="5" > 
                    <a href="<?= Url::to(['/cna/assessment/submit?level='.$level])?>" style='margin-left: 300px' class='btn btn-md  btn-success  col-sm-4'> SUBMIT THIS ASSESSMENT</a>
                </th>
            </tr>
            <tr>
                <td>#</td>
                <th>Question</th>
                <th>Current sistuation</th> 
                <th>Desired</th>
                <th>Challenge</th>
                <th>Action</th>
                <th>approved</th>
                <th>Submission</th>
            </tr>                  
            </thead>
            <tbody>
                <?php foreach ($previews as $key => $assessment) { ?>
                    <tr>
                        <td><?= $key + 1;?></td>
                        <td><?= $assessment->questions->question; ?></td>
                        <td><?= $assessment->current_situation; ?></td>
                        <td><?= $assessment->desired_capacity;?></td>
                        <td><?= $assessment->challenge;?></td>
                        <td><?= $assessment->capacity_building_action;?></td>
                        <td align='center'><span class='label label-success'><i class='fa fa-check'></i></span></td>
                        <td align='center'>
                            <?= ($assessment->submission) ? 
                                    "<span class='label label-success'><i class='fa fa-check'></i></span>" : 
                                    "<span class='label label-danger'><i class='fa fa-times'></i></span>"
                            ?>
                        </td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>       
