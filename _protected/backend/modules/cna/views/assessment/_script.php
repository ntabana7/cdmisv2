<?php
use yii\helpers\Url;
?>
<script>
    function addRow<?= $class ?>() { 
        var data = $('#add-<?= $rel_id?> :input').serializeArray();
        data.push({name: '_action', value : 'add' , name: 'question_id' , value: <?= $id?>});
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['add-'.$relID]); ?>',
            data: data,
            success: function (data) { alert(<?= $pos?>)
                $('#add-<?= $rel_id?>').html(data);
            }
        });
    }
    function delRow<?= $class ?>(id) {
        $('#add-<?= $id?> tr[data-key=' + id + ']').remove();
    }

    function addRowRelatedDesiredAnswers() {
        var data = $('#add-related-desired-answers :input').serializeArray();
        data.push({name: '_action', value : 'add'} );
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['add-related-desired-answers']); ?>',
            data: data,
            success: function (data) {
                $('#add-related-desired-answers').html(data);
            }
        });
    }
    function delRowRelatedDesiredAnswers(id) {
        $('#add-related-desired-answers tr[data-key=' + id + ']').remove();            
    }

    function addRowRelatedChallengeAnswers() {
        var data = $('#add-related-challenge-answers :input').serializeArray();
        data.push({name: '_action', value : 'add'} );
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['add-related-challenge-answers']); ?>',
            data: data,
            success: function (data) {
                $('#add-related-challenge-answers').html(data);
            }
        });
    }

    function delRowRelatedChallengeAnswers(id) {
        $('#add-related-challenge-answers tr[data-key=' + id + ']').remove();            
    }

    function addRowRelatedActionAnswers() {
        var data = $('#add-related-action-answers :input').serializeArray();
        data.push({name: '_action', value : 'add'} );
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['add-related-challenge-answers']); ?>',
            data: data,
            success: function (data) {
                $('#add-related-action-answers').html(data);
            }
        });
    }

    function delRowRelatedActionAnswers(id) {
        $('#add-related-action-answers tr[data-key=' + id + ']').remove();            
    }
</script>
