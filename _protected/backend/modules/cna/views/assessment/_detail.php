<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */

?>
<div class="container assessment-view">

    <div class="row">
        <div class="well col-sm-12">
            <b><?= Html::encode($model->questions->question) ?></b>
        </div>
    </div>

    <div class="row">
        <?php 
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                'current_situation',
                'desired_capacity',
                'challenge',
                'capacity_building_action',
                'comments',
                [
                    'attribute' => 'level0.level',
                    'label' => Yii::t('backend', 'Level'),
                ],
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn
            ]); 
        ?>
    </div>
</div>