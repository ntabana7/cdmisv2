<?php 
    use yii\helpers\Url;
?>
<div class="well" style='background-color: white'>
    <div class='row'>
        <div class='col-lg-12'>
            <h1><center><u>List of Challenges</u></center></h1>
            <center><b><i>(Institutional , Organational ,Individual)</i></b></center>
        </div>
        <hr>
        <div class="col-lg-12">
            <table class='table table-bordered table-striped'>    
                <tr>
                    <td>#</td>
                    <th>Question</th>
                    <th>Current</th> 
                    <th>Desired</th>
                    <th>Challenge</th>
                    <th>Action</th>
                    <th>Plan</th>
                </tr>                  
                </thead>
                <tbody>
                    <?php foreach ($previews as $key => $assessment) {
                ?>
                        <tr>
                            <td><?= $key + 1; ?></td>
                            <td><?= $assessment->questions->question; ?></td>
                            <td><?= $assessment->current_situation; ?></td>
                            <td><?= $assessment->desired_capacity; ?></td>
                            <td><?= $assessment->challenge; ?></td>
                            <td><?= $assessment->capacity_building_action; ?></td>
                            <td align='center'>
                                <?php if (! $assessment->planned) {
                    ?> 
                                <span class='label label-warning'>
                                    <a href="<?= Url::to(['/plans/create?assessment='.$assessment->id])?>">
                                        <i style='color:white' class='fa fa-plus'></i>
                                    </a>
                                </span>
                                <?php
                } else {
                    ?>
                                <span class='label label-success'><i class='fa fa-check'></i></span>
                                <?php
                } ?>
                            </td>

                        </tr>
                    <?php
            } ?>
                </tbody>
            </table>
        </div>  
    </div>
</div>     
