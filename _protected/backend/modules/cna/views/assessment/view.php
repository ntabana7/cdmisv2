<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Assessment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well" style='background-color: white'>

    <div class="row">
        <div class="col-sm-12">
            <h2><?=  Html::encode($model->questions->question) ?></h2>
        </div>
    </div>

    <hr>

    <div class="well">

        <p><b>Current situation :</b> <?= $model->current_situation?></p>
        <?php
        $providerRelatedCurrentAnswers->pagination  = false;

        if($providerRelatedCurrentAnswers->totalCount){
            $gridColumnRelatedCurrentAnswers = [
                ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'answer',
                    'total_number' ,
                    'baseline',
                    'qualificationArea.area',
            ];
            echo Gridview::widget([
                'dataProvider' => $providerRelatedCurrentAnswers,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dependent-answers']],
                'columns' => $gridColumnRelatedCurrentAnswers
            ]);

        }
        ?>
    </div>

    <div class="well">

        <p> <b>Desired Capacity :</b>  <?= $model->desired_capacity?></p>
        <?php
        if($providerRelatedDesiredAnswers->totalCount){
            $gridColumnRelatedDesiredAnswers = [
                ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'answer',
                    'qualificationArea.area',
                    'nbrbeneficiaries'
            ];
            echo Gridview::widget([
                'dataProvider' => $providerRelatedDesiredAnswers,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dependent-answers']],
                'export' => false,
                'columns' => $gridColumnRelatedDesiredAnswers
            ]);
        }
        ?>
    </div>

    <div class="well">

        <p> <b>Challenge :</b>  <?= $model->challenge?></p>
        <?php
        if($providerRelatedChallengeAnswers->totalCount){
            $gridColumnRelatedChallengeAnswers = [
                ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'qualificationArea.area',
                    'challenge'
            ];
            echo Gridview::widget([
                'dataProvider' => $providerRelatedChallengeAnswers,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dependent-answers']],
                'export' => false,
                'columns' => $gridColumnRelatedChallengeAnswers
            ]);
        }
        ?>
    </div>

    <div class="well">

        <p> <b>Action Taken :</b>  <?= $model->capacity_building_action?></p>
        <?php
        if($providerRelatedActionAnswers->totalCount){
            $gridColumnRelatedActionAnswers = [
                ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'qualificationArea.area',
                    'action_taken'
            ];
            echo Gridview::widget([
                'dataProvider' => $providerRelatedActionAnswers,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-dependent-answers']],
                'export' => false,
                'columns' => $gridColumnRelatedActionAnswers
            ]);
        }
        ?>
    </div>
    <div class='well'>
        <p> <b>Comment :</b>  <?= $model->comments?></p>
    </div>