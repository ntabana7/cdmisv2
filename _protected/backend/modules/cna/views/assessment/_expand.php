<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Assessment')),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Related Current Answers')),
        'content' => $this->render('_dataRelatedCurrentAnswers', [
            'model' => $model,
            'row' => $model->relatedCurrentAnswers,
        ]),
    ],

    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Related Desired Answers')),
        'content' => $this->render('_dataRelatedDesiredAnswers', [
            'model' => $model,
            'row' => $model->relatedDesiredAnswers,
        ]),
    ],

    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Related Challenges Answers')),
        'content' => $this->render('_dataRelatedChallengeAnswers', [
            'model' => $model,
            'row' => $model->relatedChallengeAnswers,
        ]),
    ],

    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Related Actions Answers')),
        'content' => $this->render('_dataRelatedActionAnswers', [
            'model' => $model,
            'row' => $model->relatedDesiredAnswers,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
