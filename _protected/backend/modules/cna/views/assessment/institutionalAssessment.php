<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cna\models\search\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Institutions;
use kartik\export\ExportMenu;
use yii\bootstrap\Progress;
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;

$institution = (isset($_GET['idInstitution'])) ? Institutions::InstitutionName($_GET['idInstitution']) : 'Assessment';
$this->title = Yii::t('backend', $institution);
$this->params['breadcrumbs'][] = $this->title;

$statistic = "$('.statistic-button').click(function(){
    $('.statistic-form').toggle(1000);
    return false;
});";

$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";

$preview = "$('.preview-button').click(function(){
    $('.preview-form').toggle(10);
    return false;
});";

$this->registerJs($search); 
$this->registerJs($preview); 
$this->registerJs($statistic); 
?>
<div class="well" style='background-color: white'>
    <?php if (Yii::$app->user->can('cluster')) { ?>
        <div class="row btn-toolbar">
            
            <a href="<?= Url::to(['/institutions/admin'])?>" class='primary col-lg-1'><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>

            <a href="<?= Url::to(['/cna/assessment/institutional','idInstitution'=>$_GET["idInstitution"]])?>" class='btn btn-lg btn-success col-lg-3'>     Institutional  
            </a>

            <a href="<?= Url::to(['/cna/assessment/organisational-assessment','idInstitution'=>$_GET["idInstitution"]])?>" class='btn btn-lg btn-info col-lg-3'> 
                Organisational
            </a>

            <a href="<?= Url::to(['/cna/assessment/individual-assessment','idInstitution'=>$_GET["idInstitution"]])?>" class='btn btn-lg btn-warning col-lg-3'> 
                Individual
            </a>
        </div>
        <hr>
    <?php }?>

<div class="row">

    <table class='table table-striped table-bordered'>
        <th>Current Capacity</th>
        <th>Desired Capacity</th>
        <th>Challenge</th>
        <th>Action Taken</th>
        <tr>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['current'].'%',
                            'percent' => $progress['percent']['current'],
                            'barOptions' => ['class' => $progress['class']['current']],
                            'options' => ['class' => $progress['active']['current'].' progress-striped']
                    ]);
                ?> 
            </td>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['desired'].'%',
                            'percent' => $progress['percent']['desired'],
                            'barOptions' => ['class' => $progress['class']['desired']],
                            'options' => ['class' => $progress['active']['desired'].' progress-striped']
                    ]);
                ?> 
            </td>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['challenge'].'%',
                            'percent' => $progress['percent']['challenge'],
                            'barOptions' => ['class' => $progress['class']['challenge']],
                            'options' => ['class' => $progress['active']['challenge'].' progress-striped']
                    ]);
                ?> 
            </td>
            <td>
                <?php
                    echo Progress::widget([ 
                            'label' => $progress['label']['action'].'%', 
                            'percent' => $progress['percent']['action'],
                            'barOptions' => ['class' => $progress['class']['action']],
                            'options' => ['class' => $progress['active']['action'].' progress-striped']
                    ]);
                ?> 
            </td>
        </tr>
    </table>

    <p class='well'>
        <?php //Html::a(Yii::t('backend', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('backend', 'Preview'), ['preview'], ['class' => 'btn btn-info preview-button']) ?>
        <?= Html::a(Yii::t('backend', 'Statistics'), '#', ['class' => 'btn btn-danger statistic-button']) ?>
        <?= Html::a(Yii::t('backend', 'Search'), ['#'], ['class' => 'btn btn-warning search-button']) ?>
    </p>

    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="statistic-form" style="display:none">
        <?=  $this->render('_statistics', ['questions' => $questions]); ?>
    </div>

    <div class="preview-form" style="display:none">
        <?=  $this->render('_preview', ['previews' => $previews, 'level' => $level]); ?>
    </div>
</div>

<div class='row assessment-index'>
   
        <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_index',['model' => $model, 'key' => $key, 'index' => $index, 'widget' => $widget, 'view' => $this]);
        },
    ]) ?>

</div>
</div>

