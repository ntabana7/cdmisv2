<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use backend\models\assessmentForm;
use \backend\modules\cna\models\QuestionOptions;
/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\currentSituation */
/* @var $form yii\widgets\ActiveForm */
$name = 'current_situation';
?>

<div class="current-situation-form">
    <?php 
        \mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
        'viewParams' => [
            'class' => 'DependentAnswers', 
            'relID' => 'dependent-answers', 
            'value' => \yii\helpers\Json::encode($model->dependentAnswers),
            'isNewRecord' => ($model->isNewRecord) ? 1 : 0
        ]
        ]);
    ?>
    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?php  foreach($questions as $key=> $question){ ?>
    
        <div class='well col-lg-12'>
            <div>
                <span >
                    <b >
                        <?=  ($key+1).'. '.$question->question?>
                    </b>
                </span>
                        &nbsp;&nbsp;&nbsp;
                <span class="label label-success">
                    <?= $question->sections->section_name;?>
                </span>
            </div>
            <?php 

            echo $form->field($model, 'questions_id[]')->hiddenInput(['value' => $question->id])->label(false); 
            switch ($question->inputtype0->type) {

            case 'inputtext':
                echo assessmentForm::inputText($form, $model, $name,$question, $placeholder = 'Answer');
            break;
            
            case 'select':
                echo assessmentForm::dropDownList($form,    $model, $name, $question , $placeholder = 'Select');
            break;
            
            case 'textarea':
                echo assessmentForm::textarea($form, $model, $name, $question, $placeholder = '');
            break;
            
            case 'radio':
                    
                    $addDependentAnswer = [
                        [
                                'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'DependentAnswers')),
                                'content' => $this->render('_formDependentAnswers', [
                                    'row' => array_merge(\yii\helpers\ArrayHelper::toArray($model->dependentAnswers),[['question_id'=> $question->id]]),
                                ])
                        ]
                    ];

                echo assessmentForm::radioList($form, $model, $name, $question,   $placeholder = 'Select');
                echo '<div style="display:none" id="add_dependent_answer_'.$question->id.'">';
                echo '<span id="dependent_question_'.$question->id.'"></span>';
                echo assessmentForm::inputTextMultiple($question, $addDependentAnswer);
                echo '</div>';
            break;
            
            case 'checkbox':
                echo assessmentForm::checkboxList($form, $model, $name, $get->id ,$get->required_question, $placeholder = 'null');
            break;
            
            default:
                # code...
                break;
            }

            echo assessmentForm::inputTextHidden($form, $model, 'other', $question,  $placeholder = 'Specify');
            ?>

        </div>

    <?php    } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
