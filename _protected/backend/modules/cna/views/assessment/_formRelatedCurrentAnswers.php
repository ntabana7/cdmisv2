<div class="form-group" id="add-related-current-answers-<?=$assessmentid?>">
<?php
use backend\modules\cdproviders\models\ProviderQualificationarea;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'RelatedCurrentAnswers',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'answer' =>  [ 
                    'type' => ($answer) ? TabularForm::INPUT_TEXT: TabularForm::INPUT_HIDDEN, 
                    'label' => ($answer) ? 'Current situation' : '',  
                    'columnOptions' => ['hidden'=>!$answer]
        ],
        'specific_area' => [
            'type' => ($specific_area) ? TabularForm::INPUT_WIDGET : TabularForm::INPUT_HIDDEN ,
            'columnOptions' => ['hidden'=>!$specific_area],
            'widgetClass'=>\kartik\widgets\Select2::classname(), 
            'options' => [
                'data'=>ArrayHelper::map(ProviderQualificationarea::find()->orderBy('area')->asArray()->cache(7200)->all(), 'idQualifarea', 'area'),
            ],
        ],
        'total_number' => [ 
                    'type' => ($total_number) ? TabularForm::INPUT_TEXT: TabularForm::INPUT_HIDDEN, 
                    'label' => ($total_number) ?'How many do you have?':'',  
                    'columnOptions' => ['hidden'=>!$total_number]
        ],

        'baseline' => [
                    'type' => ($baseline) ? TabularForm::INPUT_TEXT : TabularForm::INPUT_HIDDEN ,  
                    'columnOptions' => ['hidden'=>!$baseline],
        ],
        
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend', 'Delete'), 'onClick' => 'delRowRelatedCurrentAnswers(' . $key . '); return false;', 'id' => 'related-current-answers-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend', 'Add Related Current Answers'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowRelatedCurrentAnswers('.$assessmentid.','.$level.')']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

<script>
    function addRowRelatedCurrentAnswers(id,level) {

        var data = $("#add-related-current-answers-"+id+" :input").serializeArray();
        data.push({name: 'assessmentid', value : id} );
        data.push({name: '_action', value : 'add'} );
        data.push({name: 'level', value : level} );
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['add-related-current-answers']); ?>',
            data: data,
            success: function (data) {
                $('#add-related-current-answers-'+id).html(data);
            }
        });
    }

    function delRowRelatedCurrentAnswers(id) {
        $('#add-related-current-answers-<?= $assessmentid?> tr[data-key=' + id + ']').remove();            
    }
</script>


