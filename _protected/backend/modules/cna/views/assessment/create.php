<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */

$this->title = Yii::t('backend', 'Create Assessment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Assessment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formAssessment', [
        'model' => $model,
        'questions' => $questions
    ]) ?>

</div>
