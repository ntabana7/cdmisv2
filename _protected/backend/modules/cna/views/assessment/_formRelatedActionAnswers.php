<div class="form-group" id="add-related-action-answers-<?=$assessmentid?>">
<?php
use backend\modules\cdproviders\models\ProviderQualificationarea;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'RelatedActionAnswers',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
         'specific_area' => [
            'type' => ($specific_area) ? TabularForm::INPUT_WIDGET : TabularForm::INPUT_HIDDEN ,
            'columnOptions' => ['hidden'=>!$specific_area],
            'widgetClass'=>\kartik\widgets\Select2::classname(), 
            'options' => [
               'data'=>ArrayHelper::map(ProviderQualificationarea::find()->orderBy('area')->asArray()->cache(7200)->all(), 'idQualifarea', 'area'),
            ],
        ],
        'action_taken' =>  [ 
                    'type' => ($action_taken) ? TabularForm::INPUT_TEXT: TabularForm::INPUT_HIDDEN, 
                    'label' => ($action_taken) ? 'Action Taken' : '',  
                    'columnOptions' => ['hidden'=>!$action_taken]
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend', 'Delete'), 'onClick' => 'delRowRelatedActionAnswers(' . $key . '); return false;', 'id' => 'related-action-answers-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend', 'Add Related Action Answers'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowRelatedActionAnswers('.$assessmentid.','.$level.')']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>


<script>
    function addRowRelatedActionAnswers(id,level) {

        var data = $("#add-related-action-answers-"+id+" :input").serializeArray();
        data.push({name: 'assessmentid', value : id} );
        data.push({name: '_action', value : 'add'} );
        data.push({name: 'level', value : level} );
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['add-related-action-answers']); ?>',
            data: data,
            success: function (data) {
                $('#add-related-action-answers-'+id).html(data);
            }
        });
    }

    function delRowRelatedActionAnswers(id) {
        $('#add-related-action-answers-<?= $assessmentid?> tr[data-key=' + id + ']').remove();            
    }
</script>



