<style>
    .cdnm-form{background-color:#f1f1f1;padding:0.01em 16px;margin:55px 0;box-shadow:0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important}
</style>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Labels;
use kartik\popover\PopoverX;
use backend\models\actionForm;
use backend\models\currentForm;
use backend\models\desiredForm;
use backend\models\challengeForm;
/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Assessment */
/* @var $form yii\widgets\ActiveForm */

$addCurrentDependentAnswer = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'Related Current Answers')),
        'content' => $this->render('_formRelatedCurrentAnswers', [
            'row' => \yii\helpers\ArrayHelper::toArray($model->relatedCurrentAnswers),
            'assessmentid'      => $model->id,
            'level'             => $model->level,
            'answer'            => $answer,
            'total_number'      => $total_number,
            'baseline'          => $baseline,
            'specific_area'     => $specific_area       
        ])
    ]
];  

$addDesiredDependentAnswer = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'Related Desired Answers')),
        'content' => $this->render('_formRelatedDesiredAnswers', [
            'row' => \yii\helpers\ArrayHelper::toArray($model->relatedDesiredAnswers),
            'assessmentid'      => $model->id,
            'level'             => $model->level,
            'answer'            => $answer,
            'specific_area'     => $specific_area,
            'nbrbeneficiaries'  => $nbrbeneficiaries
        ])
    ]
]; 

$addChallengeDependentAnswer = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'Related Challenge Answers')),
        'content' => $this->render('_formRelatedChallengeAnswers', [
            'row' => \yii\helpers\ArrayHelper::toArray($model->relatedChallengeAnswers),
            'assessmentid' => $model->id,
            'level'        => $model->level,
            'specific_area'=> $specific_area,
            'challenge'    => $challenge,
        ])
    ]
];

$addActionDependentAnswer = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'Related Action Answers')),
        'content' => $this->render('_formRelatedActionAnswers', [
            'row' => \yii\helpers\ArrayHelper::toArray($model->relatedActionAnswers),
            'assessmentid' => $model->id,
            'specific_area'   => $specific_area,
            'action_taken' => $action_taken,
            'level'        => $model->level
        ])
    ]
]; 
    
   $script = <<< JS
     $(function () { 
            $("[data-toggle='tooltip']").tooltip(); 
        });

        $(function () { 
            $("[data-toggle='popover']").popover(); 
        });
JS;
$this->registerJs($script);


?>

<div >

    <?php $form = ActiveForm::begin(['action' =>[$url.'?id='.$model->id]]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="container">
        <div class='col-sm-8'  style="background-color:#e1e1e1;border: 1px solid #d1d1d1;margin-bottom: 20px">
            <?php currentForm::assessment($form, $model, 'current_situation', $addCurrentDependentAnswer); ?>
        </div>
        <div class='col-sm-1'>
            <span style="margin-top: 10px" class='pull-righht'>
                 <?=
                    // right
                     PopoverX::widget([
                        'header' => 'Definition',
                        'placement' => PopoverX::ALIGN_LEFT,
                        'content' => Labels::$current,
                        'footer' => '',
                        'toggleButton' => ['label'=>'help', 'class'=>'btn btn-sm btn-primary'],
                    ]);
                ?>
            </span>
        </div>      
    </div>

    <div class="container">
        <div class='col-sm-8'  style="background-color:#e1e1e1;border: 1px solid #d1d1d1;margin-bottom: 20px">
            <?php desiredForm::assessment($form, $model, 'desired_capacity', $addDesiredDependentAnswer); ?>
        </div>
        <div class='col-sm-1'>
            <span style="margin-top: 10px" class='pull-righht'>
                 <?=
                    // right
                     PopoverX::widget([
                        'header' => 'Definition',
                        'placement' => PopoverX::ALIGN_LEFT,
                        'content' => Labels::$desired,
                        'footer' => '',
                        'toggleButton' => ['label'=>'help', 'class'=>'btn btn-sm btn-primary'],
                    ]);
                ?>
            </span>
        </div>      
    </div>

    <div class="container">
        <div class='col-sm-8'  style="background-color:#e1e1e1;border: 1px solid #d1d1d1;margin-bottom: 20px">
             <?php challengeForm::assessment($form, $model, 'challenge', $addChallengeDependentAnswer); ?>
        </div>
        <div class='col-sm-1'>
            <span style="margin-top: 10px" class='pull-righht'>
                 <?=
                    // right
                     PopoverX::widget([
                        'header' => 'Definition',
                        'placement' => PopoverX::ALIGN_LEFT,
                        'content' => Labels::$cbchallenge,
                        'footer' => '',
                        'toggleButton' => ['label'=>'help', 'class'=>'btn btn-sm btn-primary'],
                    ]);
                ?>
            </span>
        </div>      
    </div>

    <div class="container">
        <div class='col-sm-8'  style="background-color:#e1e1e1;border: 1px solid #d1d1d1;margin-bottom: 20px">
            <?php actionForm::assessment( $form, $model, 'capacity_building_action', $addActionDependentAnswer); ?>
        </div>
        <div class='col-sm-1'>
            <span style="margin-top: 10px" class='pull-righht'>
                 <?=
                    // right
                     PopoverX::widget([
                        'header' => 'Definition',
                        'placement' => PopoverX::ALIGN_LEFT,
                        'content' => Labels::$actionplan,
                        'footer' => '',
                        'toggleButton' => ['label'=>'help', 'class'=>'btn btn-sm btn-primary'],
                    ]);
                ?>
            </span>
        </div>      
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Send'), 
                ['class' => $model->isNewRecord ? 'btn btn-success col-sm-3' : 'btn btn-primary']) 
        ?>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>

    <?php ActiveForm::end(); ?>
</div>


