<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\QuestionOptions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Question Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-options-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Question Options').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'question_option',
        [
                'attribute' => 'questions.id',
                'label' => Yii::t('backend', 'Questions')
            ],
        'dependent_question_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
