<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\QuestionOptions */

$this->title = Yii::t('backend', 'Create Question Options');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Question Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
