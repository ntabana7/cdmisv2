<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\QuestionOptions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Question Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-options-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Question Options').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'question_option',
        [
            'attribute' => 'questions.id',
            'label' => Yii::t('backend', 'Questions'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Questions<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnQuestions = [
        ['attribute' => 'id', 'visible' => false],
        'sections_id',
        'question',
        'dependent_question_needed',
        'inputtype',
        'required_question',
        'status',
        'position',
    ];
    echo DetailView::widget([
        'model' => $model->questions,
        'attributes' => $gridColumnQuestions    ]);
    ?>
</div>
