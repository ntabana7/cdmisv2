<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Sections */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sections-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Sections').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'section_name',
        [
                'attribute' => 'levels.id',
                'label' => Yii::t('backend', 'Levels')
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerQuestions->totalCount){
    $gridColumnQuestions = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'question',
        'if_applicable',
                'parent_id',
        'position',
        'status',
        [
                'attribute' => 'inputtype0.id',
                'label' => Yii::t('backend', 'Inputtype')
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerQuestions,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend', 'Questions')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnQuestions
    ]);
}
?>
    </div>
</div>
