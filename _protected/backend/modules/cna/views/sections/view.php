<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Sections */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sections-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Sections').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'section_name',
        [
            'attribute' => 'levels.level',
            'label' => Yii::t('backend', 'Levels'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerQuestions->totalCount){
    $gridColumnQuestions = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'question',
            'dependent_question_needed',
            [
                'attribute' => 'dependentQuestion.dependent_question',
                'label' => Yii::t('backend', 'Dependent Question')
            ],
            [
                'attribute' => 'inputtype0.id',
                'label' => Yii::t('backend', 'Inputtype')
            ],
            'required_question',
            'status',
            'position',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerQuestions,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-questions']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend', 'Questions')),
        ],
        'export' => false,
        'columns' => $gridColumnQuestions
    ]);
}
?>
