<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Questions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Questions').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'sections.id',
                'label' => Yii::t('backend', 'Sections')
            ],
        'question',
        'dependent_question_needed',
        'dependent_question_id',
        [
                'attribute' => 'inputtype0.id',
                'label' => Yii::t('backend', 'Inputtype')
            ],
        [
                'attribute' => 'actionType.id',
                'label' => Yii::t('backend', 'Action Type')
            ],
        [
                'attribute' => 'desiredType.id',
                'label' => Yii::t('backend', 'Desired Type')
            ],
        'required_question',
        'status',
        'position',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAssessment->totalCount){
    $gridColumnAssessment = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'current_situation',
        'desired_capacity',
        'capacity_building_action',
        [
                'attribute' => 'level0.id',
                'label' => Yii::t('backend', 'Level')
            ],
        'institution_id',
        'current_status',
        'desired_status',
        'action_status',
        'submission',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAssessment,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend', 'Assessment')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAssessment
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerQuestionOptions->totalCount){
    $gridColumnQuestionOptions = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'question_option',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerQuestionOptions,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend', 'Question Options')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnQuestionOptions
    ]);
}
?>
    </div>
</div>
