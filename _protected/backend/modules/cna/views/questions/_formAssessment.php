<div class="form-group" id="add-assessment">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Assessment',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'current_situation' => ['type' => TabularForm::INPUT_TEXT],
        'desired_capacity' => ['type' => TabularForm::INPUT_TEXT],
        'capacity_building_action' => ['type' => TabularForm::INPUT_TEXT],
        'level' => [
            'label' => 'Levels',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\CbLevels::find()->orderBy('idLevel')->asArray()->all(), 'idLevel', 'idLevel'),
                'options' => ['placeholder' => Yii::t('backend', 'Choose Levels')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'institution_id' => ['type' => TabularForm::INPUT_TEXT],
        'current_status' => ['type' => TabularForm::INPUT_TEXT],
        'desired_status' => ['type' => TabularForm::INPUT_TEXT],
        'action_status' => ['type' => TabularForm::INPUT_TEXT],
        'submission' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend', 'Delete'), 'onClick' => 'delRowAssessment(' . $key . '); return false;', 'id' => 'assessment-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend', 'Add Assessment'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAssessment()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

