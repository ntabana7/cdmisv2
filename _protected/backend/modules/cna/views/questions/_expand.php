<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Questions')),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Question Options')),
        'content' => $this->render('_dataQuestionOptions', [
            'model' => $model,
            'row' => $model->questionOptions,
        ]),
    ],

    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode(Yii::t('backend', 'Assessment')),
        'content' => $this->render('_dataAssessment', [
            'model' => $model,
            'row' => $model->assessments,
        ]),
    ],
   
                    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
