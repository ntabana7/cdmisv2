<?php

use yii\helpers\url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Questions */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Assessment', 
        'relID' => 'assessment', 
        'value' => \yii\helpers\Json::encode($model->assessments),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'QuestionOptions', 
        'relID' => 'question-options', 
        'value' => \yii\helpers\Json::encode($model->questionOptions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

if(isset($model->sections->levels->idLevel)){
   $model->level = $model->sections->levels->idLevel;
}
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
    
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <div class="well">
        <?= $form->field($model, 'level')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\models\CbLevels::find()->orderBy('level')->asArray()->all(), 'idLevel', 'level'),
            'options'=>[
                        'placeholder'=> Yii::t('backend', 'Choose Level'),
                        'onchange'=>'
                        $("#section_div").show();
                        $.post( "'.Url::to(['/cna/sections/lists', 'id' => '']).'"+$(this).val(),function(data){
                         $("select#questions-sections_id" ).html(data);
                        });'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <div id='section_div' style='display: block'>
            <?= $form->field($model, 'sections_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\Sections::find()->orderBy('id')->asArray()->all(), 'id', 'section_name'),
                'options' => ['placeholder' => Yii::t('backend', 'Choose Section')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <div class='well'>
    
        <?= $form->field($model, 'question')->textInput(['maxlength' => true, 'placeholder' => 'Question']) ?>

        <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'placeholder' => 'Slug']) ?>


        <?= $form->field($model, 'dependent_question_needed',  [
                    'options'=> [
                        'onchange'=>'
                            var checkbox = document.getElementById("questions-dependent_question_needed");
               
                            if( checkbox.checked ){
                                $("#dependent_div").show();
                            }else{
                                $("#dependent_div").hide(); 
                            }
                       '
                    ],])->checkbox() 
        ?>

        <div id="dependent_div" style="display: none">
        <?= $form->field($model, 'dependent_question_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\DependentQuestion::find()->orderBy('id')->asArray()->all(), 'id', 'dependent_question'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Dependent question')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        </div>

        <?= $form->field($model, 'desired_dependent_question_needed',  [
                    'options'=> [
                        'onchange'=>'
                            var checkbox = document.getElementById("questions-desired_dependent_question_needed");
               
                            if( checkbox.checked ){
                                $("#desired_dependent_div").show();
                            }else{
                                $("#desired_dependent_div").hide(); 
                            }
                       '
                    ],])->checkbox() 
        ?>

        <div id="desired_dependent_div" style="display: none">
        <?= $form->field($model, 'desired_dependent_question_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\DesiredDependentQuestion::find()->orderBy('id')->asArray()->all(), 'id', 'dependent_question'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Desired Dependent question')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>

        <?= $form->field($model, 'challenge_dependent_question_needed',  [
                    'options'=> [
                        'onchange'=>'
                            var checkbox = document.getElementById("questions-challenge_dependent_question_needed");
               
                            if( checkbox.checked ){
                                $("#challenge_dependent_div").show();
                            }else{
                                $("#challenge_dependent_div").hide(); 
                            }
                       '
                    ],])->checkbox() 
        ?>

        <div id="challenge_dependent_div" style="display: none">
        <?= $form->field($model, 'challenge_dependent_question_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\ChallengeDependentQuestion::find()->orderBy('id')->asArray()->all(), 'id', 'dependent_question'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose challenge Dependent question')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
    
        <?= $form->field($model, 'action_dependent_question_needed',  [
                    'options'=> [
                        'onchange'=>'
                            var checkbox = document.getElementById("questions-action_dependent_question_needed");
               
                            if( checkbox.checked ){
                                $("#action_dependent_div").show();
                            }else{
                                $("#action_dependent_div").hide(); 
                            }
                       '
                    ],])->checkbox() 
        ?>

        <div id="action_dependent_div" style="display: none">
        <?= $form->field($model, 'action_dependent_question_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\ActionDependentQuestion::find()->orderBy('id')->asArray()->all(), 'id', 'dependent_question'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Action Dependent question')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
    </div>

    <div class="well">

        <?= $form->field($model, 'inputtype')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\InputTypes::find()->orderBy('title')->asArray()->all(), 'id', 'title'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Input types')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'desired_type')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\InputTypes::find()->orderBy('id')->asArray()->all(), 'id', 'title'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Input types')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'challenge_type')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\InputTypes::find()->orderBy('id')->asArray()->all(), 'id', 'title'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Input types')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'action_type')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\backend\modules\cna\models\InputTypes::find()->orderBy('id')->asArray()->all(), 'id', 'title'),
            'options' => ['placeholder' => Yii::t('backend', 'Choose Input types')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="well">
        <?= $form->field($model, 'required_question')->checkbox() ?>

        <?= $form->field($model, 'status')->checkbox() ?>

        <?= $form->field($model, 'position')->textInput(['placeholder' => 'Position']) ?>
    </div>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend', 'QuestionOptions')),
            'content' => $this->render('_formQuestionOptions', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->questionOptions),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
