<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->assessments,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'current_situation',
        'desired_capacity',
        'capacity_building_action',
        [
                'attribute' => 'level0.level',
                'label' => Yii::t('backend', 'Level')
        ],
        'institution_id',
        [
                'attribute' => 'level',
                'label' => Yii::t('backend', 'Current Status'),
                'value' => function($model){                   
                    return ($model->current_status == 1) ? 'Yes' : 'No';                   
                },
        ],
         [
                'attribute' => 'level',
                'label' => Yii::t('backend', 'Desired Status'),
                'value' => function($model){                   
                    return ($model->desired_status == 1) ? 'Yes' : 'No';                   
                },
        ],
        [
                'attribute' => 'level',
                'label' => Yii::t('backend', 'Action Status'),
                'value' => function($model){                   
                    return ($model->action_status == 1) ? 'Yes' : 'No';                   
                },
        ],

        [
                'attribute' => 'level',
                'label' => Yii::t('backend', 'Submission'),
                'value' => function($model){                   
                    return ($model->submission == 1) ? 'Yes' : 'No';                   
                },
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'width:80%;margin:auto;overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
