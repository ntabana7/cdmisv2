<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Questions */

?>
<div style='width:80%;margin:auto;'>

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'sections.section_name',
            'label' => Yii::t('backend', 'Sections'),
        ],
        'question',
        [
                'attribute' => 'dependent_question_needed',
                'label' => Yii::t('backend', 'Other Question'),
                'value' => function($model){                   
                    return ($model->dependent_question_needed == 1) ? 'Yes' : 'No';                   
                },
        ],

        [
            'attribute' => 'inputtype0.type',
            'label' => Yii::t('backend', 'Current Capacity Situation Type'),
        ],
        [
            'attribute' => 'desiredType.type',
            'label' => Yii::t('backend', 'Desired Capacity Type'),
        ],
        [
            'attribute' => 'actionType.type',
            'label' => Yii::t('backend', 'Action Type'),
        ],
        
        [
                'attribute' => 'required_question',
                'label' => Yii::t('backend', 'Required'),
                'value' => function($model){                   
                    return ($model->required_question == 1) ? 'Yes' : 'No';                   
                },
        ],
        [
                'attribute' => 'status',
                'label' => Yii::t('backend', 'Status'),
                'value' => function($model){                   
                    return ($model->status == 1) ? 'Available' : 'No Available';                   
                },
        ],
        'position',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>