<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Questions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container questions-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Questions').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
           
            <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'sections.section_name',
            'label' => Yii::t('backend', 'Sections'),
        ],
        'question',
        //'slug',
        [
            'attribute' => 'dependent_question_needed',
            'label' => Yii::t('backend', 'Dependent question'),
            'value' => function($model){                   
                return ($model->dependent_question_needed) ? 'Yes' : 'No';                   
            },
        ],
        [
            'attribute' => 'desired_dependent_question_needed',
            'label' => Yii::t('backend', 'Dependent question'),
            'value' => function($model){                   
                return ($model->desired_dependent_question_needed) ? 'Yes' : 'No';                   
            },
        ],
        [
            'attribute' => 'challenge_dependent_question_needed',
            'label' => Yii::t('backend', 'Dependent question'),
            'value' => function($model){                   
                return ($model->challenge_dependent_question_needed) ? 'Yes' : 'No';                   
            },
        ],
        [
            'attribute' => 'action_dependent_question_needed',
            'label' => Yii::t('backend', 'Dependent question'),
            'value' => function($model){                   
                return ($model->action_dependent_question_needed) ? 'Yes' : 'No';                   
            },
        ],

        'dependentQuestion.dependent_question',
        'desiredDependentQuestion.dependent_question',
        'challengeDependentQuestion.dependent_question',
        'actionDependentQuestion.dependent_question',
        [
            'attribute' => 'inputtype0.type',
            'label' => Yii::t('backend', 'Inputtype'),
        ],
       
        [
            'attribute' => 'desiredType.type',
            'label' => Yii::t('backend', 'Desired Type'),
        ],

        [
            'attribute' => 'challengeType.type',
            'label' => Yii::t('backend', 'Challenge Type'),
        ],

        [
            'attribute' => 'actionType.type',
            'label' => Yii::t('backend', 'Action Type'),
        ],
        'required_question',
        'status',
        'position',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerAssessment->totalCount){
    $gridColumnAssessment = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'current_situation',
            'desired_capacity',
            'capacity_building_action',
            [
                'attribute' => 'level0.idLevel',
                'label' => Yii::t('backend', 'Level')
            ],
            'institution_id',
            'current_status',
            'desired_status',
            'action_status',
            'submission',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAssessment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-assessment']],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend', 'Assessment')),
        ],
        'columns' => $gridColumnAssessment
    ]);
}
?>

    </div>
    
    <div class="row">
        <?php
        if($providerQuestionOptions->totalCount){
            $gridColumnQuestionOptions = [
                ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'id', 'visible' => false],
                    'question_option',
                        ];
            echo Gridview::widget([
                'dataProvider' => $providerQuestionOptions,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-question-options']],
                'columns' => $gridColumnQuestionOptions
            ]);
        }
        ?>

    </div>
    <div class="row">
        <h4>Sections</h4>
      
        <?php 
        $gridColumnSections = [
            ['attribute' => 'id', 'visible' => false],
            'section_name',
            'levels.level',
        ];
        echo DetailView::widget([
            'model' => $model->sections,
            'attributes' => $gridColumnSections    ]);
        ?>  
    </div> 
</div>
