<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\Questions */

$this->title = Yii::t('backend', 'Create Questions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-create">

    <div class="well"><h1><?= Html::encode($this->title) ?></h1></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
