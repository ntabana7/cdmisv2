<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\DependentQuestion */

$this->title = Yii::t('backend', 'Create Dependent Question');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Dependent Question'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dependent-question-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
