<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\DependentQuestion */

$this->title = $model->dependent_question;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Dependent Question'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dependent-question-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend', 'Dependent Question').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'condition_answer',
        'dependent_question',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerQuestions->totalCount){
    $gridColumnQuestions = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'sections.id',
                'label' => Yii::t('backend', 'Sections')
            ],
        'question',
        'dependent_question_needed',
                [
                'attribute' => 'inputtype0.id',
                'label' => Yii::t('backend', 'Inputtype')
            ],
        'required_question',
        'status',
        'position',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerQuestions,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend', 'Questions')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnQuestions
    ]);
}
?>
    </div>
</div>
