<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cna\models\DependentQuestion */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Dependent Question',
]) . ' ' . $model->dependent_question;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Dependent Question'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dependent_question, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="dependent-question-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
