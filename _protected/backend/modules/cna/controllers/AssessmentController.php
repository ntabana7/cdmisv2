<?php

namespace backend\modules\cna\controllers;

use Yii;
use backend\modules\cna\models\search\AssessmentSearch;
use backend\modules\cna\models\base\RelatedChallengeAnswers;
use backend\modules\cna\models\base\RelatedCurrentAnswers;
use backend\modules\cna\models\base\RelatedDesiredAnswers;
use backend\modules\cna\models\base\RelatedActionAnswers;
use \backend\modules\cna\models\Questions;
use backend\modules\cna\models\Assessment;
use backend\modules\cna\models\Sections;
use yii\web\NotFoundHttpException;
use backend\models\base\Model;
use backend\models\CbLevels;
use yii\filters\VerbFilter;
use yii\web\Controller;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;


/**
 * AssessmentController implements the CRUD actions for Assessment model.
 */
class AssessmentController extends Controller
{
    public function behaviors()
    {
        return [
            // [
            //     'class' => 'yii\filters\PageCache',
            //     'only' => ['institutional'],
            //     'duration' => 60,
            //     'variations' => [
            //         \Yii::$app->language,
            //     ],
            // ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 
                            'view',
                            'create',
                            'update',
                            'submit',
                            'delete',
                            'submitted',
                            'instruction',
                            'pre-submitted',
                            'institutional',
                            'individual-index',
                            'update-individual',
                            'create-individual',
                            'plan-individual',
                            'plan-institutional',
                            'plan-organisational',
                            'approve-individual',
                            'approve-instruction',
                            'organisational-index',
                            'update-organisational',
                            'individual-assessment',
                            'create-organisational',
                            'add-dependent-answers',
                            'approve-institutional',
                            'approve-organisational',
                            'plan-challenge-individual',
                            'organisational-assessment',
                            'add-related-action-answers',
                            'add-related-current-answers',
                            'add-related-desired-answers',
                            'add-related-challenge-answers',
                        ],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Guidance note on how to use the tool.
     * @return mixed
     */
    public function actionInstruction()
    {
        return $this->render('instruction');
    }

    /**
     * Lists institutional Assessment models.
     * @return mixed
     * @author Uwamahoro Denyse <udenyse@gmail.com>
     */
    public function actionIndex()
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isInstitutional()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists organisational Assessment models.
     * @return mixed
     * @author Uwamahoro Denyse <udenyse@gmail.com>
     */
    public function actionOrganisationalIndex()
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isOrganisational()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel);

        return $this->render('indexOrganisational', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists individual Assessment models.
     * @return mixed
     * @author Uwamahoro Denyse <udenyse@gmail.com>
     */
    public function actionIndividualIndex()
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isIndividual()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel);

        return $this->render('indexIndividual', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    *
    * Assessment of institution on institutional Level
    * @return different arrays
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
    public function actionInstitutional($idInstitution = null)
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isInstitutional()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel,$idInstitution);

        return $this->render('institutionalAssessment', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'level'        => $idLevel,
            'questions'    => Assessment::numberQuestionBySection($idLevel),
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution($idInstitution)->isCurrent()->all(),
            'progress'     => Assessment::progress($idLevel,$idInstitution)
        ]);
    }

    /**
    *
    * Assessment of institution on organasitional Level
    * @return different arrays
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
    public function actionOrganisationalAssessment($idInstitution = null)
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isOrganisational()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel,$idInstitution);

        return $this->render('organisationalAssessment', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'level'        => $idLevel,
            'questions'    => Assessment::numberQuestionBySection($idLevel),
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution($idInstitution)->isCurrent()->all(),
            'progress'     => Assessment::progress($idLevel,$idInstitution )
        ]);
    }

    /**
    *
    * Assessment of institution on individual Level
    * @return different arrays
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
    public function actionIndividualAssessment($idInstitution=null)
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isIndividual()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel,$idInstitution);

        return $this->render('individualAssessment', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'level'        => $idLevel,
            'questions'    => Assessment::numberQuestionBySection($idLevel),
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution($idInstitution)->isCurrent()->all(),
            'progress'     => Assessment::progress($idLevel,$idInstitution )
        ]);
    }

    /**
     * Displays a single Assessment model.
     * @param integer $id
     * @return mixed
     * @author Uwamahoro Denyse <udenyse@gmail.com>
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerRelatedCurrentAnswers = new \yii\data\ArrayDataProvider([
            'allModels' => $model->relatedCurrentAnswers,
        ]);

        $providerRelatedDesiredAnswers = new \yii\data\ArrayDataProvider([
            'allModels' => $model->relatedDesiredAnswers,
        ]);

        $providerRelatedChallengeAnswers = new \yii\data\ArrayDataProvider([
            'allModels' => $model->relatedChallengeAnswers,
        ]);

        $providerRelatedActionAnswers = new \yii\data\ArrayDataProvider([
            'allModels' => $model->relatedActionAnswers,
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerRelatedCurrentAnswers'   => $providerRelatedCurrentAnswers,
            'providerRelatedDesiredAnswers'   => $providerRelatedDesiredAnswers,
            'providerRelatedChallengeAnswers' => $providerRelatedChallengeAnswers,
            'providerRelatedActionAnswers'    => $providerRelatedActionAnswers,
        ]);
    }

    /**
     * Creates a new Assessment model.(Institutional Level)
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model            =  new Assessment();
        $idLevel          = CbLevels::find()->isInstitutional()->one()->idLevel;
        $questions        = Questions::find()->byLevel($idLevel)->available()->all();
        $isExist          = $model->find()->byInstitution()->byLevel($idLevel)->isCurrent()->count(); //TODO Year 

        if ( $isExist == 0) {

            $transaction = \Yii::$app->db->beginTransaction();
            
            foreach($questions AS $id => $question){ 

                    $model = new Assessment();
                    $model->questions_id      = $question->id;
                    $model->slug_question     = $question->slug;
                    $model->level             = $idLevel;
                    $model->institution_id    = Yii::$app->user->identity->userFrom;
                    $model->current_situation = null;
                    
                    if(!$model->save(false)){        
                        $transaction->rollback();
                    }
            }

            $transaction->commit();
        } 

        return $this->redirect('institutional');
    }

    /**
     * Creates a new Assessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateOrganisational()
    {
        $model            =  new Assessment();
        $idLevel          = CBLevels::find()->isOrganisational()->one()->idLevel;
        $questions        = Questions::find()->byLevel($idLevel)->available()->all();
        $isExist          = $model->find()->byInstitution()->byLevel($idLevel)->isCurrent()->count(); //TODO Year 

        if ( $isExist == 0) {

            $transaction = \Yii::$app->db->beginTransaction();
            
            foreach($questions AS $id => $question){ 

                    $model = new Assessment();
                    $model->questions_id      = $question->id;
                    $model->slug_question     = $question->slug;
                    $model->level             = $idLevel;
                    $model->institution_id    = Yii::$app->user->identity->userFrom;
                    $model->current_situation = null;
                    
                    if(!$model->save(false)){                       
                        $transaction->rollback();
                    }
            }

            $transaction->commit();
        } 

        return $this->redirect('organisational-assessment');
    }


/**
     * Creates a new Assessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateIndividual()
    {
        $model            =  new Assessment();
        $idLevel          = CBLevels::find()->isIndividual()->one()->idLevel;
        $questions        = Questions::find()->byLevel($idLevel)->available()->all();
        $isExist          = $model->find()->byInstitution()->byLevel($idLevel)->isCurrent()->count();

        if ( $isExist == 0) {

            $transaction = \Yii::$app->db->beginTransaction();
            
            foreach($questions AS $id => $question){ 

                    $model = new Assessment();
                    $model->questions_id      = $question->id;
                    $model->slug_question     = $question->slug;
                    $model->level             = $idLevel;
                    $model->institution_id    = Yii::$app->user->identity->userFrom      ;
                    $model->current_situation = null;
                    
                    if(!$model->save(false)){                       
                        $transaction->rollback();
                    }
            }

            $transaction->commit();
        } 

        return $this->redirect('individual-assessment');
    }

    /**
     * Updates an existing Institutional Assessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post())) {

            //replace other by his value
            $model->current_situation         = $this->other($model->current_situation, $model->other_current);
            $model->desired_capacity          = $this->other($model->desired_capacity, $model->other_desired);
            $model->challenge                 = $this->other($model->challenge, $model->other_challenge);                   
            $model->capacity_building_action  = $this->other($model->capacity_building_action, $model->other_action);
               
            $model->current_status   = $this->answered($model->current_situation);//Current situation
            $model->desired_status   = $this->answered($model->desired_capacity);//Desired Capacity    
            $model->challenge_status = $this->answered($model->challenge);//challenge Capacity         
            $model->action_status    = $this->answered($model->capacity_building_action);//Current situation

            if($model->saveAll()){
                return $this->redirect(['institutional']);                
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Changes status when a question is answered.
     * @param string $question
     * @return integer
     */
    private function answered($question ){
        if(!is_null($question) && !empty($question) && $question !== ''){
            return 1;
        }

        return 0;
    }

     /**
     * Changes status when a question is answered.
     * @param string $question
     * @return integer
     */
    private function other($question ,$other){

        if($question == 'Other'){
            return $other; 
        }

        return $question;
    }

    /**
     * Updates an existing Organisational Assessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateOrganisational($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post())) {

             //replace other by his value
            $model->current_situation         = $this->other($model->current_situation, $model->other_current);
            $model->desired_capacity          = $this->other($model->desired_capacity, $model->other_desired);  
            $model->challenge                 = $this->other($model->challenge, $model->other_challenge);                
            $model->capacity_building_action  = $this->other($model->capacity_building_action, $model->other_action);
               
            $model->current_status   = $this->answered($model->current_situation);//Current situation
            $model->desired_status   = $this->answered($model->desired_capacity);//Desired Capacity   
            $model->challenge_status = $this->answered($model->challenge);//challenge Capacity          
            $model->action_status    = $this->answered($model->capacity_building_action);//Current situation

            if($model->saveAll()){
                return $this->redirect(['organisational-assessment']);                
            }

        } else {
            return $this->render('update-organisational', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Individual Assessment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateIndividual($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post())) {

             //replace other by his value
            $model->current_situation         = $this->other($model->current_situation, $model->other_current);
            $model->desired_capacity          = $this->other($model->desired_capacity, $model->other_desired); 
            $model->challenge                 = $this->other($model->challenge, $model->other_challenge);                 
            $model->capacity_building_action  = $this->other($model->capacity_building_action, $model->other_action);
               
            $model->current_status   = $this->answered($model->current_situation);//Current situation
            $model->desired_status   = $this->answered($model->desired_capacity);//Desired Capacity   
            $model->challenge_status = $this->answered($model->challenge);//challenge Capacity              
            $model->action_status    = $this->answered($model->capacity_building_action);//Current situation

            if($model->saveAll()){
                $this->updateRelatedCurrentAnswers($id);
                $this->updateRelatedDesiredAnswers($id);
                $this->updateRelatedChallengeAnswers($id);
                $this->updateRelatedActionAnswers($id);
                return $this->redirect(['individual-assessment']);            
            }

        } else {
            return $this->render('update-individual', [
                'model' => $model,
            ]);
        }
    }

    private function updateRelatedCurrentAnswers($id){

        $model = $this->findModel($id);
        if($model->questions->dependent_question_needed){                        
            if(strtolower($model->current_situation) !== strtolower($model->questions->dependentQuestion->condition_answer)){
                foreach($model->relatedCurrentAnswers as $current){
                    RelatedCurrentAnswers::findOne($current->id)->delete();
                }
            }
        }
    }

    private function updateRelatedDesiredAnswers($id){

        $model = $this->findModel($id);
        if($model->questions->desired_dependent_question_needed){                        
            if(strtolower($model->desired_capacity) !== strtolower($model->questions->desiredDependentQuestion->condition_answer)){
                foreach($model->relatedDesiredAnswers as $desired){
                    RelatedDesiredAnswers::findOne($desired->id)->delete();
                }
            }
        }
    }

    private function updateRelatedChallengeAnswers($id){

        $model = $this->findModel($id);
        if($model->questions->challenge_dependent_question_needed){                        
            if(strtolower($model->challenge) !== strtolower($model->questions->challengeDependentQuestion->condition_answer)){
                foreach($model->relatedChallengeAnswers as $related){
                    RelatedChallengeAnswers::findOne($related->id)->delete();
                }
            }
        }
    }

    private function updateRelatedActionAnswers($id){

        $model = $this->findModel($id);
        if($model->questions->action_dependent_question_needed){                        
            if(strtolower($model->capacity_building_action) !== strtolower($model->questions->actionDependentQuestion->condition_answer)){
                foreach($model->relatedActionAnswers as $related){
                    RelatedActionAnswers::findOne($related->id)->delete();
                }
            }
        }
    }
    /**
     * Deletes an existing Assessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Assessment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Assessment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assessment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for RelatedCurrentAnswers
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddRelatedCurrentAnswers()
    {

        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('RelatedCurrentAnswers');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')

            $row[] = [];

            return $this->renderAjax('_formRelatedCurrentAnswers', [
                'row' => $row,
                'assessmentid'  => Yii::$app->request->post('assessmentid'),
                'level'         =>Yii::$app->request->post('level'),
                'answer'        => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? false: true,
                'total_number'  => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false,
                'baseline'      => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false,
                'specific_area' => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false,   
            ]);

        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for RelatedDesiredAnswers
    *
    * @return mixed
    */
    public function actionAddRelatedDesiredAnswers()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('RelatedDesiredAnswers');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
                return $this->renderAjax('_formRelatedDesiredAnswers',[
                    'row' => $row,
                    'assessmentid'     => Yii::$app->request->post('assessmentid'),
                    'level'            =>Yii::$app->request->post('level'),
                    'answer'           => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? false : true,
                    'specific_area'    => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false, 
                    'nbrbeneficiaries' => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false,
                ]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for RelatedChallengeAnswers
    *
    * @return mixed
    */
    public function actionAddRelatedChallengeAnswers()
    {

        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('RelatedChallengeAnswers');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
            $row[] = [];

            return $this->renderAjax('_formRelatedChallengeAnswers', [
                'row'           => $row,
                'assessmentid'  => Yii::$app->request->post('assessmentid'),
                'level'         =>Yii::$app->request->post('level'),
                'specific_area' => (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false,
                'challenge'     => true
            ]);

        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for RelatedActionAnswers
    *
    * @return mixed
    */
    public function actionAddRelatedActionAnswers()
    {

        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('RelatedActionAnswers');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
            $row[] = [];

            return $this->renderAjax('_formRelatedActionAnswers', [
                'row' => $row,
                'assessmentid' => Yii::$app->request->post('assessmentid'),
                'level'        =>Yii::$app->request->post('level'),
                'answer'       => false,
                'specific_area'=> (Yii::$app->request->post('level') == CbLevels::find()->isIndividual()->one()->idLevel) ? true : false,
                'action_taken' => true
            ]);
            
        } else {
            throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
        }
    }

    /**
     * Pre submitted.
     * Submit assessment to approver.(eg:DG)
     * @param integer $id
     * @return mixed
     */
    public function actionPreSubmitted($level)
    {   
        $model = new Assessment();
        //TODO FIND BETTER WHERE TO PRESUBMIT AND AVOIDE SQL INJECTION
        $connection = \Yii::$app->db;

        $query = 'UPDATE assessment SET 
                    pre_submitted = '.Assessment::PRE_SUBMITTED.' 
                WHERE 
                institution_id='.Yii::$app->user->identity->userFrom.' AND 
                submission <> '.Assessment::SUBMITTED.' AND 
                pre_submitted <>'.Assessment::PRE_SUBMITTED.' AND 
                level ='.$level.' AND 
                challenge_status = 1 AND 
                action_status = 1';

        $command = $connection->createCommand($query);

        $command->execute();
        //Email Notifications
        $model->trigger(Assessment::EVENT_UNIT_ASSESSMENT_SUBMITTED); 
        $model->trigger(Assessment::EVENT_DG_ASSESSMENT_RECEIVED);

        if($level == CbLevels::find()->isInstitutional()->one()->idLevel){
            return $this->redirect(['institutional']);
        }
        if($level == CbLevels::find()->isOrganisational()->one()->idLevel){ 
            return $this->redirect(['organisational-assessment']);
        }
        if($level == CbLevels::find()->isIndividual()->one()->idLevel){
            return $this->redirect(['individual-assessment']);
        }
    }  

    /**
     * Pre submitted.
     * Submit assessment to approver.(eg:DG)
     * @param integer $id
     * @return mixed
     */
    public function actionSubmit($level)
    {   
        $model = new Assessment();
         //TODO FIND BETTER WHERE TO PRESUBMIT AND AVOIDE SQL INJECTION
        $connection = \Yii::$app->db;

        $query = 'UPDATE assessment SET 
                    submission= '.Assessment::SUBMITTED.' 
                WHERE 
                institution_id='.Yii::$app->user->identity->userFrom.' AND 
                submission <> '.Assessment::SUBMITTED.' AND 
                approved = '.Assessment::APPROVED.' AND 
                level ='.$level;

        $command = $connection->createCommand($query);
        $command->execute();
        
        $model->trigger(Assessment::EVENT_DG_ASSESSMENT_SUBMITTED); 
        $model->trigger(Assessment::EVENT_UNIT_ASSESSMENT_RECEIVED); 

        if($level == CbLevels::find()->isInstitutional()->one()->idLevel){
            return $this->redirect(['approve-institutional']);
        }
        if($level == CbLevels::find()->isOrganisational()->one()->idLevel){ 
            return $this->redirect(['approve-organisational']);
        }
        if($level == CbLevels::find()->isIndividual()->one()->idLevel){
            return $this->redirect(['approve-individual']);
        }
    }  

    /**
     * Guidance note on how to approve assessment.
     * @return mixed
    */
    public function actionApproveInstruction()
    {
        return $this->render('approve-instruction');
    }

    /**
    *
    * Assessment of institution on organasitional Level
    * @return different arrays
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
    public function actionApproveInstitutional()
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isInstitutional()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel);

        if(Yii::$app->request->post('hasEditable')){
        $id = Yii::$app->request->post('editableKey');
        $model = Assessment::findOne($id);

            $post = [];
            $posted = current($_POST['Assessment']);
            $post['Assessment'] = $posted;
           
           $out = Json::encode(['output'=>'Done', 'message'=>'']);

            if($model->load($post)){
                $model->save(false);
            }
                echo $out;die;
        }

        return $this->render('index', [
            'level'        => $idLevel,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'progress'     => Assessment::progressApproval($idLevel),
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution()->isCurrent()->isApproved()->all()
        ]);
    }

    /**
    *
    * Assessment of institution on organasitional Level
    * @return different arrays
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
    public function actionApproveOrganisational()
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isOrganisational()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel);

        if(Yii::$app->request->post('hasEditable')){
        $id = Yii::$app->request->post('editableKey');
        $model = Assessment::findOne($id);

            $post = [];
            $posted = current($_POST['Assessment']);
            $post['Assessment'] = $posted;
           
            $out = Json::encode(['output'=>'Done','message'=>'']);

            if($model->load($post)){
                $model->save(false);
            }
                echo $out;
                die;
        }

        return $this->render('index', [
            'level'        => $idLevel,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'progress'     => Assessment::progressApproval($idLevel),
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution()->isCurrent()->isApproved()->all()
        ]);
    }

    /**
    *
    * Assessment of institution on organasitional Level
    * @return different arrays
    * @author Uwamahoro Denyse <udenyse@gmail.com>
    */
    public function actionApproveIndividual()
    {
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isIndividual()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel);

        if(Yii::$app->request->post('hasEditable')){
        $id = Yii::$app->request->post('editableKey');
        $model = Assessment::findOne($id);

            $post = [];
           
            $out = Json::encode(['output'=>'','message'=>'']);
            $posted = current($_POST['Assessment']);
            $post['Assessment'] = $posted;

            if($model->load($post)){
                $model->save(false);
            }
                echo $out;
                return;
        }

        return $this->render('index', [
            'level'        => $idLevel,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'progress'     => Assessment::progressApproval($idLevel),
            'previews'      => Assessment::find()->byLevel($idLevel)->byInstitution()->isCurrent()->isApproved()->all(),
        ]);
    }

    public function actionPlanInstitutional(){
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isInstitutional()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel );

        return $this->render('planInstitutional', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution()->isSubmitted()->isCurrent()->all()
        ]);
    }

    public function actionPlanOrganisational(){
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isOrganisational()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel );

        return $this->render('planInstitutional', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution()->isSubmitted()->isCurrent()->all()
        ]);
    }

    public function actionPlanIndividual(){
        $searchModel = new AssessmentSearch();
        $idLevel = CbLevels::find()->isIndividual()->one()->idLevel;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idLevel );

        return $this->render('planIndividual', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'previews'     => Assessment::find()->byLevel($idLevel)->byInstitution()->isSubmitted()->isCurrent()->all()
        ]);
    }

    public function actionPlanChallengeIndividual($idassessment){
   
        return $this->render('planChallengeIndividual', [
            'challenges' => Assessment::challenges($idassessment)
        ]);
    }

}
