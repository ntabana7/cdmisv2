<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_complab}}".
 *
 * @property integer $idLab
 * @property string $complab
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderComplab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_complab}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['complab'], 'required'],
            [['complab'], 'string', 'max' => 10],
            [['complab'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLab' => 'Id Lab',
            'complab' => 'Complab',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idLab' => 'idLab']);
    }
}
