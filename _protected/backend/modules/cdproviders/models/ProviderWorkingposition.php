<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_workingposition}}".
 *
 * @property integer $idWorkingposition
 * @property string $position
 * @property string $iscoposition
 * @property string $iscocode
 *
 * @property ProviderBeneficiariesenrollment[] $providerBeneficiariesenrollments
 */
class ProviderWorkingposition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_workingposition}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'required'],
            [['position', 'iscoposition', 'iscocode'], 'string', 'max' => 100],
            [['position'], 'unique'],
            [['iscocode'], 'unique'],
            [['iscoposition'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idWorkingposition' => Yii::t('app', 'Id Workingposition'),
            'position' => Yii::t('app', 'Position'),
            'iscoposition' => Yii::t('app', 'Iscoposition'),
            'iscocode' => Yii::t('app', 'Iscocode'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiariesenrollments()
    {
        return $this->hasMany(ProviderBeneficiariesenrollment::className(), ['idWorkingposition' => 'idWorkingposition']);
    }
}
