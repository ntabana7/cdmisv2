<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_provider}}".
 *
 * @property integer $idProvider
 * @property string $providername
 * @property integer $idAffiliated
 * @property string $affiliatedto
 * @property integer $idProviderCat
 * @property integer $idProviderType
 * @property integer $idProviderBase
 * @property integer $idCountry
 * @property string $physicaladdress
 * @property string $email
 * @property string $phone
 * @property string $pobox
 * @property string $website
 * @property string $nationalIDorPassport
 * @property integer $idRegrw
 * @property string $registeredelsewhere
 * @property integer $idRegbody
 * @property string $registeredon
 * @property string $tinorregistrationnumber
 * @property string $status
 *
 * @property ProviderBeneficiariesenrollment[] $providerBeneficiariesenrollments
 * @property ProviderProvidercategory $idProviderCat0
 * @property ProviderProviderbase $idProviderBase0
 * @property ProviderProvidertype $idProviderType0
 * @property ProviderCountry $idCountry0
 * @property ProviderAffiliated $idAffiliated0
 * @property ProviderRegbody $idRegbody0
 * @property ProviderRegrwanda $idRegrw0
 * @property ProviderServices[] $providerServices
 * @property ProviderTrainingareasbyprovider[] $providerTrainingareasbyproviders
 * @property ProviderWhotrainwhat[] $providerWhotrainwhats
 */
class ProviderProvider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_provider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['providername', 'idAffiliated', 'idProviderCat', 'idProviderType', 'idProviderBase', 'idCountry', 'physicaladdress', 'email', 'phone', 'idRegrw', 'idRegbody', 'registeredon', 'tinorregistrationnumber'], 'required'],
            [['idAffiliated', 'idProviderCat', 'idProviderType', 'idProviderBase', 'idCountry', 'idRegrw', 'idRegbody'], 'integer'],
            [['providername', 'affiliatedto', 'physicaladdress', 'email', 'phone', 'pobox', 'website', 'registeredelsewhere', 'registeredon', 'tinorregistrationnumber', 'status'], 'string', 'max' => 100],
            [['nationalIDorPassport'], 'string', 'max' => 16],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['phone'], 'unique'],
            [['idProviderCat'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvidercategory::className(), 'targetAttribute' => ['idProviderCat' => 'idProviderCat']],
            [['idProviderBase'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProviderbase::className(), 'targetAttribute' => ['idProviderBase' => 'idProviderBase']],
            [['idProviderType'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvidertype::className(), 'targetAttribute' => ['idProviderType' => 'idProviderType']],
            [['idCountry'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderCountry::className(), 'targetAttribute' => ['idCountry' => 'idCountry']],
            [['idAffiliated'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderAffiliated::className(), 'targetAttribute' => ['idAffiliated' => 'idAffiliated']],
            [['idRegbody'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderRegbody::className(), 'targetAttribute' => ['idRegbody' => 'idRegbody']],
            [['idRegrw'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderRegrwanda::className(), 'targetAttribute' => ['idRegrw' => 'idRegrw']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProvider' => 'Id Provider',
            'providername' => 'Provider name',
            'idAffiliated' => 'Affiliated',
            'affiliatedto' => 'Affiliated to',
            'idProviderCat' => 'Provider Category',
            'idProviderType' => 'Provider Type',
            'idProviderBase' => 'Provider geo-location',
            'idCountry' => 'Country',
            'physicaladdress' => 'Physical address',
            'email' => 'Email',
            'phone' => 'Phone',
            'pobox' => 'P.o.Box',
            'website' => 'Website',
            'nationalIDorPassport' => 'National ID/Passport',
            'idRegrw' => 'Registered in Rwanda',
            'registeredelsewhere' => 'Registered elsewhere by',
            'idRegbody' => 'Registration body',
            'registeredon' => 'Registered on',
            'tinorregistrationnumber' => 'TIN/Registration number',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiariesenrollments()
    {
        return $this->hasMany(ProviderBeneficiariesenrollment::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProviderCat0()
    {
        return $this->hasOne(ProviderProvidercategory::className(), ['idProviderCat' => 'idProviderCat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProviderBase0()
    {
        return $this->hasOne(ProviderProviderbase::className(), ['idProviderBase' => 'idProviderBase']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProviderType0()
    {
        return $this->hasOne(ProviderProvidertype::className(), ['idProviderType' => 'idProviderType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCountry0()
    {
        return $this->hasOne(ProviderCountry::className(), ['idCountry' => 'idCountry']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAffiliated0()
    {
        return $this->hasOne(ProviderAffiliated::className(), ['idAffiliated' => 'idAffiliated']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRegbody0()
    {
        return $this->hasOne(ProviderRegbody::className(), ['idRegbody' => 'idRegbody']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRegrw0()
    {
        return $this->hasOne(ProviderRegrwanda::className(), ['idRegrw' => 'idRegrw']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderServices()
    {
        return $this->hasMany(ProviderServices::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderTrainingareasbyproviders()
    {
        return $this->hasMany(ProviderTrainingareasbyprovider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderWhotrainwhats()
    {
        return $this->hasMany(ProviderWhotrainwhat::className(), ['idProvider' => 'idProvider']);
    }
}
