<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderCountry;

/**
 * ProviderCountrySearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderCountry`.
 */
class ProviderCountrySearch extends ProviderCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCountry'], 'integer'],
            [['country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderCountry::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idCountry' => $this->idCountry,
        ]);

        $query->andFilterWhere(['like', 'country', $this->country]);

        return $dataProvider;
    }
}
