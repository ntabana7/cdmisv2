<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_country}}".
 *
 * @property integer $idCountry
 * @property string $country
 *
 * @property ProviderBeneficiariesenrollment[] $providerBeneficiariesenrollments
 * @property ProviderProvider[] $providerProviders
 */
class ProviderCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country'], 'required'],
            [['country'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCountry' => Yii::t('app', 'Id Country'),
            'country' => Yii::t('app', 'Country'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiariesenrollments()
    {
        return $this->hasMany(ProviderBeneficiariesenrollment::className(), ['idCountry' => 'idCountry']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idCountry' => 'idCountry']);
    }
}
