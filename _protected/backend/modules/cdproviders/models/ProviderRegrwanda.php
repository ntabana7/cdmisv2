<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_regrwanda}}".
 *
 * @property integer $idRegrw
 * @property string $registered
 *
 * @property ProviderProvider[] $providerProviders
 */
class ProviderRegrwanda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_regrwanda}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registered'], 'required'],
            [['registered'], 'string', 'max' => 10],
            [['registered'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRegrw' => 'Id Regrw',
            'registered' => 'Registered',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idRegrw' => 'idRegrw']);
    }
}
