<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_affiliated}}".
 *
 * @property integer $idAffiliated
 * @property string $affiliated
 *
 * @property ProviderProvider[] $providerProviders
 */
class ProviderAffiliated extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_affiliated}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliated'], 'required'],
            [['affiliated'], 'string', 'max' => 10],
            [['affiliated'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idAffiliated' => 'Id Affiliated',
            'affiliated' => 'Affiliated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idAffiliated' => 'idAffiliated']);
    }
}
