<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderInfrastructure;

/**
 * ProviderInfrastructureSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderInfrastructure`.
 */
class ProviderInfrastructureSearch extends ProviderInfrastructure
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInfrast', 'idProvider', 'idClassroom', 'howmanyclass', 'classcapacity', 'idInternet', 'idLab', 'howmanylab', 'idComputa', 'howmanycomputa', 'idHostel', 'hostelcapacity', 'idLibrary', 'idLabdigital'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        if(Yii::$app->user->can('admin')){
            $query = ProviderInfrastructure::find();
        }else{
           $query = ProviderInfrastructure::find()->where('idProvider=:u',['u'=>Yii::$app->user->identity->userFrom]);  
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInfrast' => $this->idInfrast,
            'idProvider' => $this->idProvider,
            'idClassroom' => $this->idClassroom,
            'howmanyclass' => $this->howmanyclass,
            'classcapacity' => $this->classcapacity,
            'idInternet' => $this->idInternet,
            'idLab' => $this->idLab,
            'howmanylab' => $this->howmanylab,
            'idComputa' => $this->idComputa,
            'howmanycomputa' => $this->howmanycomputa,
            'idHostel' => $this->idHostel,
            'hostelcapacity' => $this->hostelcapacity,
            'idLibrary' => $this->idLibrary,
            'idLabdigital' => $this->idLabdigital,
        ]);

        return $dataProvider;
    }
}
