<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_providercategory}}".
 *
 * @property integer $idProviderCat
 * @property string $category
 *
 * @property ProviderProvider[] $providerProviders
 */
class ProviderProvidercategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_providercategory}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['category'], 'string', 'max' => 100],
            [['category'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProviderCat' => Yii::t('app', 'Id Provider Cat'),
            'category' => Yii::t('app', 'Providers Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idProviderCat' => 'idProviderCat']);
    }
}
