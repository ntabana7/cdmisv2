<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderBeneficiariesenrollment;

/**
 * ProviderBeneficiariesenrollmentSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderBeneficiariesenrollment`.
 */
class ProviderBeneficiariesenrollmentSearch extends ProviderBeneficiariesenrollment
{
    public function rules()
    {
        return [
            [['idEnrollment', 'idProvider', 'idBeneficiary', 'idInstit', 'idWorkingposition', 'idSubLevel', 'idTraining', 'idQualif', 'idCountry', 'status'], 'integer'],
            [['startingdate', 'endingdate', 'costoftraining', 'funder', 'reasonnotcompleted', 'yearofcd'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        if(Yii::$app->user->can('admin')){
            $query = ProviderBeneficiariesenrollment::find();
        }else{
           $query = ProviderBeneficiariesenrollment::find()->where('idProvider=:u',['u'=>Yii::$app->user->identity->userFrom]);  
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idEnrollment' => $this->idEnrollment,
            'idProvider' => $this->idProvider,
            'idBeneficiary' => $this->idBeneficiary,
            'idInstit' => $this->idInstit,
            'idWorkingposition' => $this->idWorkingposition,
            'idSubLevel' => $this->idSubLevel,
            'idTraining' => $this->idTraining,
            'idQualif' => $this->idQualif,
            'idCountry' => $this->idCountry,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'startingdate', $this->startingdate])
            ->andFilterWhere(['like', 'endingdate', $this->endingdate])
            ->andFilterWhere(['like', 'costoftraining', $this->costoftraining])
            ->andFilterWhere(['like', 'funder', $this->funder])
            ->andFilterWhere(['like', 'reasonnotcompleted', $this->reasonnotcompleted])
            ->andFilterWhere(['like', 'yearofcd', $this->yearofcd]);

        return $dataProvider;
    }
}
