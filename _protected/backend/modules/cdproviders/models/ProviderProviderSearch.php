<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderProvider;

/**
 * ProviderProviderSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderProvider`.
 */
class ProviderProviderSearch extends ProviderProvider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvider', 'idAffiliated', 'idProviderCat', 'idProviderType', 'idProviderBase', 'idCountry', 'idRegrw', 'idRegbody'], 'integer'],
            [['providername', 'affiliatedto', 'physicaladdress', 'email', 'phone', 'pobox', 'website', 'tinorregistrationnumber', 'nationalIDorPassport', 'registeredelsewhere', 'registeredon', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderProvider::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idProvider' => $this->idProvider,
            'idAffiliated' => $this->idAffiliated,
            'idProviderCat' => $this->idProviderCat,
            'idProviderType' => $this->idProviderType,
            'idProviderBase' => $this->idProviderBase,
            'idCountry' => $this->idCountry,
            'idRegrw' => $this->idRegrw,
            'idRegbody' => $this->idRegbody,
        ]);

        $query->andFilterWhere(['like', 'providername', $this->providername])
            ->andFilterWhere(['like', 'affiliatedto', $this->affiliatedto])
            ->andFilterWhere(['like', 'physicaladdress', $this->physicaladdress])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'pobox', $this->pobox])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'tinorregistrationnumber', $this->tinorregistrationnumber])
            ->andFilterWhere(['like', 'nationalIDorPassport', $this->nationalIDorPassport])
            ->andFilterWhere(['like', 'registeredelsewhere', $this->registeredelsewhere])
            ->andFilterWhere(['like', 'registeredon', $this->registeredon])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
