<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_methodology}}".
 *
 * @property integer $idMethodology
 * @property string $methodology
 *
 * @property ProviderTrainingarea[] $providerTrainingareas
 */
class ProviderMethodology extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_methodology}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['methodology'], 'required'],
            [['methodology'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idMethodology' => Yii::t('app', 'Id Methodology'),
            'methodology' => Yii::t('app', 'Teaching methodology'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderTrainingareas()
    {
        return $this->hasMany(ProviderTrainingarea::className(), ['idMethodology' => 'idMethodology']);
    }
}
