<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\CbQualifications;

/**
 * This is the model class for table "{{%provider_trainer}}".
 *
 * @property integer $idTrainer
 * @property string $name
 * @property integer $idGender
 * @property string $dob
 * @property string $phone
 * @property string $email
 * @property string $nida
 * @property integer $idQualif
 * @property integer $idQualifarea
 * @property string $degree
 * @property string $whendoyoustarttraining
 *
 * @property CbQualifications $idQualif0
 * @property ProviderQualificationarea $idQualifarea0
 * @property ProviderHumangender $idGender0
 * @property ProviderTrainerexpetise[] $providerTrainerexpetises
 * @property ProviderWhotrainwhat[] $providerWhotrainwhats
 */
class ProviderTrainer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_trainer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'idGender', 'dob', 'phone', 'email', 'nida', 'idQualif', 'idQualifarea', 'degree', 'whendoyoustarttraining'], 'required'],
            [['idGender', 'idQualif', 'idQualifarea'], 'integer'],
            [['name', 'dob', 'phone', 'email', 'whendoyoustarttraining'], 'string', 'max' => 100],
            [['nida'], 'string', 'max' => 16],
            [['degree'], 'file'],
            [['phone'], 'unique'],
            [['email'], 'unique'],
            [['nida'], 'unique'],
            [['idQualif'], 'exist', 'skipOnError' => true, 'targetClass' => CbQualifications::className(), 'targetAttribute' => ['idQualif' => 'idQualif']],
            [['idQualifarea'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderQualificationarea::className(), 'targetAttribute' => ['idQualifarea' => 'idQualifarea']],
            [['idGender'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderHumangender::className(), 'targetAttribute' => ['idGender' => 'idGender']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTrainer' => Yii::t('app', 'Id Trainer'),
            'name' => Yii::t('app', 'Name'),
            'idGender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Date of bith'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'nida' => Yii::t('app', 'National ID/Passport Number'),
            'idQualif' => Yii::t('app', 'Qualification'),
            'idQualifarea' => Yii::t('app', 'Qualification area'),
            'degree' => Yii::t('app', 'Degree'),
            'whendoyoustarttraining' => Yii::t('app', 'When did you start to train'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualif0()
    {
        return $this->hasOne(CbQualifications::className(), ['idQualif' => 'idQualif']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualifarea0()
    {
        return $this->hasOne(ProviderQualificationarea::className(), ['idQualifarea' => 'idQualifarea']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGender0()
    {
        return $this->hasOne(ProviderHumangender::className(), ['idGender' => 'idGender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderTrainerexpetises()
    {
        return $this->hasMany(ProviderTrainerexpetise::className(), ['idTrainer' => 'idTrainer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderWhotrainwhats()
    {
        return $this->hasMany(ProviderWhotrainwhat::className(), ['idTrainer' => 'idTrainer']);
    }
}
