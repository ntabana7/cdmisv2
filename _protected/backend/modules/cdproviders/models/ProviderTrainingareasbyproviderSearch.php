<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderTrainingareasbyprovider;

/**
 * ProviderTrainingareasbyproviderSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderTrainingareasbyprovider`.
 */
class ProviderTrainingareasbyproviderSearch extends ProviderTrainingareasbyprovider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTrainingprovider', 'idSkills', 'idInstitSector', 'idCbType', 'idProvider', 'idTraining', 'methodology', 'language', 'evaluation'], 'integer'],
            [['startingdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->can('admin')){
            $query = ProviderTrainingareasbyprovider::find();
        }else{
           $query = ProviderTrainingareasbyprovider::find()->where('idProvider=:u',['u'=>Yii::$app->user->identity->userFrom]);  
        }
        // $query = ProviderTrainingareasbyprovider::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTrainingprovider' => $this->idTrainingprovider,
            'idSkills' => $this->idSkills,
            'idInstitSector' => $this->idInstitSector,
            'idCbType' => $this->idCbType,
            'idProvider' => $this->idProvider,
            'idTraining' => $this->idTraining,
            'methodology' => $this->methodology,
            'language' => $this->language,
            'evaluation' => $this->evaluation,
        ]);

        $query->andFilterWhere(['like', 'startingdate', $this->startingdate]);

        return $dataProvider;
    }
}
