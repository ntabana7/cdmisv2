<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_regbody}}".
 *
 * @property integer $idRegbody
 * @property string $register
 * @property string $abbravation
 *
 * @property ProviderProvider[] $providerProviders
 */
class ProviderRegbody extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_regbody}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['register', 'abbravation'], 'required'],
            [['register'], 'string', 'max' => 100],
            [['abbravation'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRegbody' => 'Id Regbody',
            'register' => 'Register',
            'abbravation' => 'Abbravation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idRegbody' => 'idRegbody']);
    }
}
