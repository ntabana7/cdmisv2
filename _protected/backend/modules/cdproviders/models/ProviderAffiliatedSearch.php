<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderAffiliated;

/**
 * ProviderAffiliatedSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderAffiliated`.
 */
class ProviderAffiliatedSearch extends ProviderAffiliated
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idAffiliated'], 'integer'],
            [['affiliated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderAffiliated::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idAffiliated' => $this->idAffiliated,
        ]);

        $query->andFilterWhere(['like', 'affiliated', $this->affiliated]);

        return $dataProvider;
    }
}
