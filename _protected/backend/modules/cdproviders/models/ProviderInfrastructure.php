<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_infrastructure}}".
 *
 * @property integer $idInfrast
 * @property integer $idProvider
 * @property integer $idClassroom
 * @property integer $howmanyclass
 * @property integer $classcapacity
 * @property integer $idInternet
 * @property integer $idLab
 * @property integer $howmanylab
 * @property integer $idComputa
 * @property integer $howmanycomputa
 * @property integer $idHostel
 * @property integer $hostelcapacity
 * @property integer $idLibrary
 * @property integer $idLabdigital
 *
 * @property ProviderProvider $idProvider0
 * @property ProviderClassroom $idClassroom0
 * @property ProviderInternet $idInternet0
 * @property ProviderComplab $idLab0
 * @property ProviderComputa $idComputa0
 * @property ProviderHostel $idHostel0
 * @property ProviderLibrary $idLibrary0
 */
class ProviderInfrastructure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_infrastructure}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvider', 'idClassroom', 'idInternet', 'idLab', 'idComputa', 'idHostel', 'idLibrary'], 'required'],
            [['idProvider', 'idClassroom', 'howmanyclass', 'classcapacity', 'idInternet', 'idLab', 'howmanylab', 'idComputa', 'howmanycomputa', 'idHostel', 'hostelcapacity', 'idLibrary', 'idLabdigital'], 'integer'],
            [['idProvider'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvider::className(), 'targetAttribute' => ['idProvider' => 'idProvider']],
            [['idClassroom'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderClassroom::className(), 'targetAttribute' => ['idClassroom' => 'idClassroom']],
            [['idInternet'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderInternet::className(), 'targetAttribute' => ['idInternet' => 'idInternet']],
            [['idLab'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderComplab::className(), 'targetAttribute' => ['idLab' => 'idLab']],
            [['idComputa'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderComputa::className(), 'targetAttribute' => ['idComputa' => 'idComputa']],
            [['idHostel'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderHostel::className(), 'targetAttribute' => ['idHostel' => 'idHostel']],
            [['idLibrary'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderLibrary::className(), 'targetAttribute' => ['idLibrary' => 'idLibrary']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInfrast' => 'Id Infrast',
            'idProvider' => 'Provider',
            'idClassroom' => 'Do you have classrooms?',
            'howmanyclass' => 'How many classes',
            'classcapacity' => 'Class capacity',
            'idInternet' => 'Do you have internet connection?',
            'idLab' => 'Do you have computer Lab?',
            'howmanylab' => 'How many computer labs you have?',
            'idComputa' => 'Do you have Computers?',
            'howmanycomputa' => 'How many computers',
            'idHostel' => 'Do you have Hostel?',
            'hostelcapacity' => 'Hostel capacity',
            'idLibrary' => 'Do you have Library?',
            'idLabdigital' => 'Is it digitalized?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvider0()
    {
        return $this->hasOne(ProviderProvider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClassroom0()
    {
        return $this->hasOne(ProviderClassroom::className(), ['idClassroom' => 'idClassroom']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInternet0()
    {
        return $this->hasOne(ProviderInternet::className(), ['idInternet' => 'idInternet']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLab0()
    {
        return $this->hasOne(ProviderComplab::className(), ['idLab' => 'idLab']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdComputa0()
    {
        return $this->hasOne(ProviderComputa::className(), ['idComputa' => 'idComputa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdHostel0()
    {
        return $this->hasOne(ProviderHostel::className(), ['idHostel' => 'idHostel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLibrary0()
    {
        return $this->hasOne(ProviderLibrary::className(), ['idLibrary' => 'idLibrary']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLabdigital0()
    {
        return $this->hasOne(ProviderLibdigital::className(), ['idLabdigital' => 'idLabdigital']);
    }
}
