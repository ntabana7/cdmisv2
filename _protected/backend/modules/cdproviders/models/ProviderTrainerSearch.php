<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderTrainer;

/**
 * ProviderTrainerSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderTrainer`.
 */
class ProviderTrainerSearch extends ProviderTrainer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTrainer', 'idGender', 'idQualif', 'idQualifarea'], 'integer'],
            [['name', 'dob', 'phone', 'email', 'nida', 'degree', 'whendoyoustarttraining'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderTrainer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTrainer' => $this->idTrainer,
            'idGender' => $this->idGender,
            'idQualif' => $this->idQualif,
            'idQualifarea' => $this->idQualifarea,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nida', $this->nida])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'whendoyoustarttraining', $this->whendoyoustarttraining]);

        return $dataProvider;
    }
}
