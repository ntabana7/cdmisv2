<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\Institutions;
use backend\models\CbSublevels;
use backend\models\TrainingAreas;
use backend\models\CbQualifications;
use backend\models\Funders;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%provider_beneficiariesenrollment}}".
 *
 * @property integer $idEnrollment
 * @property integer $idProvider
 * @property integer $idBeneficiary
 * @property integer $idInstit
 * @property integer $idWorkingposition
 * @property integer $idSubLevel
 * @property integer $idTraining
 * @property string $startingdate
 * @property string $endingdate
 * @property integer $idQualif
 * @property string $costoftraining
 * @property integer $idCountry
 * @property string $funder
 * @property string $yearofcd
 *
 * @property ProviderProvider $idProvider0
 * @property ProviderBeneficiaries $idBeneficiary0
 * @property ProviderWorkingposition $idWorkingposition0
 * @property CbSublevels $idSubLevel0
 * @property TrainingAreas $idTraining0
 * @property CbQualifications $idQualif0
 * @property ProviderCountry $idCountry0
 * @property Institutions $idInstit0
 */
class ProviderBeneficiariesenrollment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $names;
    public static function tableName()
    {
        return '{{%provider_beneficiariesenrollment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvider', 'idBeneficiary', 'idInstit', 'idWorkingposition', 'idSubLevel', 'idTraining', 'startingdate', 'endingdate', 'idQualif', 'costoftraining', 'idCountry', 'yearofcd'], 'required'],
            [['idProvider', 'idBeneficiary', 'idInstit', 'idWorkingposition', 'idSubLevel', 'idTraining', 'idQualif','status','idCountry'], 'integer'],
            [['startingdate', 'endingdate', 'costoftraining', 'yearofcd'], 'string', 'max' => 100],
            [['reasonnotcompleted'], 'string', 'max' => 200],
            [['idProvider'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvider::className(), 'targetAttribute' => ['idProvider' => 'idProvider']],
            [['idBeneficiary'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderBeneficiaries::className(), 'targetAttribute' => ['idBeneficiary' => 'idBeneficiary']],
            [['idWorkingposition'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderWorkingposition::className(), 'targetAttribute' => ['idWorkingposition' => 'idWorkingposition']],
            [['idSubLevel'], 'exist', 'skipOnError' => true, 'targetClass' => CbSublevels::className(), 'targetAttribute' => ['idSubLevel' => 'idSubLevel']],
            [['idTraining'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingAreas::className(), 'targetAttribute' => ['idTraining' => 'idTraining']],
            [['idQualif'], 'exist', 'skipOnError' => true, 'targetClass' => CbQualifications::className(), 'targetAttribute' => ['idQualif' => 'idQualif']],
            [['idCountry'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderCountry::className(), 'targetAttribute' => ['idCountry' => 'idCountry']],
            [['idInstit'], 'exist', 'skipOnError' => true, 'targetClass' => Institutions::className(), 'targetAttribute' => ['idInstit' => 'idInstit']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEnrollment' => 'Id Enrollment',
            'idProvider' => 'Provider',
            'idBeneficiary' => 'Beneficiary',
            'idInstit' => 'Institution',
            'idWorkingposition' => 'Working position',
            'idSubLevel' => 'CD Approach',
            'idTraining' => 'Training area',
            'startingdate' => 'Starting date',
            'endingdate' => 'Ending date',
            'idQualif' => 'Qualification after CD',
            'costoftraining' => 'Cost of training',
            'idCountry' => 'Country',
            'funder' => 'Funder',
            'statuslabel' => Yii::t('app', 'Status'),
            'reasonnotcompleted' => Yii::t('app', 'The reason He/She did not complete'),
            'yearofcd' => 'Year of CD',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvider0()
    {
        return $this->hasOne(ProviderProvider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBeneficiary0()
    {
        return $this->hasOne(ProviderBeneficiaries::className(), ['idBeneficiary' => 'idBeneficiary']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdWorkingposition0()
    {
        return $this->hasOne(ProviderWorkingposition::className(), ['idWorkingposition' => 'idWorkingposition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubLevel0()
    {
        return $this->hasOne(CbSublevels::className(), ['idSubLevel' => 'idSubLevel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTraining0()
    {
        return $this->hasOne(TrainingAreas::className(), ['idTraining' => 'idTraining']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualif0()
    {
        return $this->hasOne(CbQualifications::className(), ['idQualif' => 'idQualif']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCountry0()
    {
        return $this->hasOne(ProviderCountry::className(), ['idCountry' => 'idCountry']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstit0()
    {
        return $this->hasOne(Institutions::className(), ['idInstit' => 'idInstit']);
    }
    
    public function getFundersListDropdown()
    {   
        $listfunders   = Funders::find()->select('funderName')->all();
        $listt   = ArrayHelper::map( $listfunders,'funderName','funderName');

        return $listt;
    }
    public function getStatuslabel()
    {

        if($this->status == 1)
            return 'In process';
        elseif($this->status == 2)
            return 'Completed';
        elseif($this->status == 3)
            return 'Not completed';
        else
            return 'Pending' ;

        
    }

    //  public function getFunder()
    // {   
    //     return Funders::find()->where('idfunder=:funderid',['funderid'=>Yii::$app->user->identity->userFrom])->one()->funderName;
    // }
}
