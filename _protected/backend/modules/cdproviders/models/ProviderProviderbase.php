<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_providerbase}}".
 *
 * @property integer $idProviderBase
 * @property string $base
 *
 * @property ProviderProvider[] $providerProviders
 */
class ProviderProviderbase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_providerbase}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['base'], 'required'],
            [['base'], 'string', 'max' => 100],
            [['base'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProviderBase' => Yii::t('app', 'Id Provider Base'),
            'base' => Yii::t('app', 'Base'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idProviderBase' => 'idProviderBase']);
    }
}
