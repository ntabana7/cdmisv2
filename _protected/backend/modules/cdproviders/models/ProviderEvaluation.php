<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_evaluation}}".
 *
 * @property integer $idEvaluation
 * @property string $method
 *
 * @property ProviderTrainingarea[] $providerTrainingareas
 */
class ProviderEvaluation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_evaluation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method'], 'required'],
            [['method'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idEvaluation' => Yii::t('app', 'Id Evaluation'),
            'method' => Yii::t('app', 'Evaluation methodology'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderTrainingareas()
    {
        return $this->hasMany(ProviderTrainingarea::className(), ['idEvaluation' => 'idEvaluation']);
    }
}
