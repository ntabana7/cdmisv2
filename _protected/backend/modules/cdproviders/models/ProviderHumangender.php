<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_humangender}}".
 *
 * @property integer $idGender
 * @property string $gender
 *
 * @property ProviderBeneficiaries[] $providerBeneficiaries
 */
class ProviderHumangender extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_humangender}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender'], 'required'],
            [['gender'], 'string', 'max' => 10],
            [['gender'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idGender' => Yii::t('app', 'Id Gender'),
            'gender' => Yii::t('app', 'Gender'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiaries()
    {
        return $this->hasMany(ProviderBeneficiaries::className(), ['idGender' => 'idGender']);
    }
}
