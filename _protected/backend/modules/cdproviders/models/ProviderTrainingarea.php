<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\Skills;
use backend\models\InstitSectors;
use backend\models\CbTypes;

/**
 * This is the model class for table "{{%provider_trainingarea}}".
 *
 * @property integer $idTrainingprovider
 * @property integer $idSkills
 * @property integer $idInstitSector
 * @property integer $idCbType
 * @property integer $idProvider
 * @property string $coursename
 * @property string $internationalstandardsISCED
 * @property string $internationalstandardsISCEDCode
 * @property integer $idMethodology
 * @property integer $idLanguage
 * @property integer $idEvaluation
 * @property string $startingdate
 *
 * @property ProviderBeneficiariesenrollment[] $providerBeneficiariesenrollments
 * @property Skills $idSkills0
 * @property InstitSectors $idInstitSector0
 * @property CbTypes $idCbType0
 * @property ProviderProvider $idProvider0
 * @property ProviderMethodology $idMethodology0
 * @property ProviderLanguage $idLanguage0
 * @property ProviderEvaluation $idEvaluation0
 * @property ProviderWhotrainwhat[] $providerWhotrainwhats
 */
class ProviderTrainingarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_trainingarea}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idSkills', 'idInstitSector', 'idCbType', 'idProvider', 'coursename', 'idMethodology', 'idLanguage', 'idEvaluation', 'startingdate'], 'required'],
            [['idSkills', 'idInstitSector', 'idCbType', 'idProvider', 'idMethodology', 'idLanguage', 'idEvaluation'], 'integer'],
            [['coursename', 'internationalstandardsISCED', 'internationalstandardsISCEDCode', 'startingdate'], 'string', 'max' => 100],
            [['idSkills'], 'exist', 'skipOnError' => true, 'targetClass' => Skills::className(), 'targetAttribute' => ['idSkills' => 'idSkills']],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
            [['idCbType'], 'exist', 'skipOnError' => true, 'targetClass' => CbTypes::className(), 'targetAttribute' => ['idCbType' => 'idCbType']],
            [['idProvider'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvider::className(), 'targetAttribute' => ['idProvider' => 'idProvider']],
            [['idMethodology'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderMethodology::className(), 'targetAttribute' => ['idMethodology' => 'idMethodology']],
            [['idLanguage'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderLanguage::className(), 'targetAttribute' => ['idLanguage' => 'idLanguage']],
            [['idEvaluation'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderEvaluation::className(), 'targetAttribute' => ['idEvaluation' => 'idEvaluation']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTrainingprovider' => Yii::t('app', 'Id Trainingprovider'),
            'idSkills' => Yii::t('app', 'Id Skills'),
            'idInstitSector' => Yii::t('app', 'Id Instit Sector'),
            'idCbType' => Yii::t('app', 'Id Cb Type'),
            'idProvider' => Yii::t('app', 'Id Provider'),
            'coursename' => Yii::t('app', 'Coursename'),
            'internationalstandardsISCED' => Yii::t('app', 'Internationalstandards Isced'),
            'internationalstandardsISCEDCode' => Yii::t('app', 'Internationalstandards Iscedcode'),
            'idMethodology' => Yii::t('app', 'Id Methodology'),
            'idLanguage' => Yii::t('app', 'Id Language'),
            'idEvaluation' => Yii::t('app', 'Id Evaluation'),
            'startingdate' => Yii::t('app', 'Startingdate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiariesenrollments()
    {
        return $this->hasMany(ProviderBeneficiariesenrollment::className(), ['idTrainingprovider' => 'idTrainingprovider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSkills0()
    {
        return $this->hasOne(Skills::className(), ['idSkills' => 'idSkills']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCbType0()
    {
        return $this->hasOne(CbTypes::className(), ['idCbType' => 'idCbType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvider0()
    {
        return $this->hasOne(ProviderProvider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMethodology0()
    {
        return $this->hasOne(ProviderMethodology::className(), ['idMethodology' => 'idMethodology']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLanguage0()
    {
        return $this->hasOne(ProviderLanguage::className(), ['idLanguage' => 'idLanguage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEvaluation0()
    {
        return $this->hasOne(ProviderEvaluation::className(), ['idEvaluation' => 'idEvaluation']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderWhotrainwhats()
    {
        return $this->hasMany(ProviderWhotrainwhat::className(), ['idTrainingprovider' => 'idTrainingprovider']);
    }
}
