<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderTrainingarea;

/**
 * ProviderTrainingareaSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderTrainingarea`.
 */
class ProviderTrainingareaSearch extends ProviderTrainingarea
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTrainingprovider', 'idSkills', 'idInstitSector', 'idCbType', 'idProvider', 'idMethodology', 'idLanguage', 'idEvaluation'], 'integer'],
            [['coursename', 'internationalstandardsISCED', 'internationalstandardsISCEDCode', 'startingdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderTrainingarea::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idTrainingprovider' => $this->idTrainingprovider,
            'idSkills' => $this->idSkills,
            'idInstitSector' => $this->idInstitSector,
            'idCbType' => $this->idCbType,
            'idProvider' => $this->idProvider,
            'idMethodology' => $this->idMethodology,
            'idLanguage' => $this->idLanguage,
            'idEvaluation' => $this->idEvaluation,
        ]);

        $query->andFilterWhere(['like', 'coursename', $this->coursename])
            ->andFilterWhere(['like', 'internationalstandardsISCED', $this->internationalstandardsISCED])
            ->andFilterWhere(['like', 'internationalstandardsISCEDCode', $this->internationalstandardsISCEDCode])
            ->andFilterWhere(['like', 'startingdate', $this->startingdate]);

        return $dataProvider;
    }
}
