<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_classroom}}".
 *
 * @property integer $idClassroom
 * @property string $classroom
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderClassroom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_classroom}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['classroom'], 'required'],
            [['classroom'], 'string', 'max' => 10],
            [['classroom'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idClassroom' => 'Id Classroom',
            'classroom' => 'Classroom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idClassroom' => 'idClassroom']);
    }
}
