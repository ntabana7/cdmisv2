<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderWhotrainwhat;

/**
 * ProviderWhotrainwhatSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderWhotrainwhat`.
 */
class ProviderWhotrainwhatSearch extends ProviderWhotrainwhat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idwho', 'idProvider', 'idTraining', 'idTrainer'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->can('admin')){
            $query = ProviderWhotrainwhat::find();
        }else{
           $query = ProviderWhotrainwhat::find()->where('idProvider=:u',['u'=>Yii::$app->user->identity->userFrom]);  
        }
        // $query = ProviderWhotrainwhat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idwho' => $this->idwho,
            'idProvider' => $this->idProvider,
            'idTraining' => $this->idTraining,
            'idTrainer' => $this->idTrainer,
        ]);

        return $dataProvider;
    }
}
