<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_trainerexpetise}}".
 *
 * @property integer $idTrainerexpertise
 * @property integer $idTrainer
 * @property string $areaofexpertise
 * @property string $certificate
 *
 * @property ProviderTrainer $idTrainer0
 */
class ProviderTrainerexpetise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_trainerexpetise}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['areaofexpertise', 'certificate'], 'required'],
            [['idTrainer'], 'integer'],
            [['areaofexpertise'], 'string', 'max' => 200],
            [['certificate'], 'string', 'max' => 100],
            [['idTrainer'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderTrainer::className(), 'targetAttribute' => ['idTrainer' => 'idTrainer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTrainerexpertise' => Yii::t('app', 'Id Trainerexpertise'),
            'idTrainer' => Yii::t('app', 'Id Trainer'),
            'areaofexpertise' => Yii::t('app', 'Area of expertise'),
            'certificate' => Yii::t('app', 'Certificate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTrainer0()
    {
        return $this->hasOne(ProviderTrainer::className(), ['idTrainer' => 'idTrainer']);
    }
}
