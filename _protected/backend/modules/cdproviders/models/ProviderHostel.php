<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_hostel}}".
 *
 * @property integer $idHostel
 * @property string $hostelavailability
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderHostel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_hostel}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hostelavailability'], 'required'],
            [['hostelavailability'], 'string', 'max' => 10],
            [['hostelavailability'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idHostel' => 'Id Hostel',
            'hostelavailability' => 'Hostelavailability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idHostel' => 'idHostel']);
    }
}
