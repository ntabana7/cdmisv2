<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_library}}".
 *
 * @property integer $idLibrary
 * @property string $libraryavailability
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderLibrary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_library}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libraryavailability'], 'required'],
            [['libraryavailability'], 'string', 'max' => 10],
            [['libraryavailability'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLibrary' => 'Id Library',
            'libraryavailability' => 'Libraryavailability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idLibrary' => 'idLibrary']);
    }
}
