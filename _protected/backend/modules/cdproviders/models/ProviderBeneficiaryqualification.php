<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\CbQualifications;

/**
 * This is the model class for table "{{%provider_beneficiaryqualification}}".
 *
 * @property integer $idBenefQualification
 * @property integer $idBeneficiary
 * @property integer $idQualif
 * @property integer $idQualifarea
 *
 * @property ProviderBeneficiaries $idBeneficiary0
 * @property CbQualifications $idQualif0
 * @property ProviderQualificationarea $idQualifarea0
 */
class ProviderBeneficiaryqualification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_beneficiaryqualification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idQualif', 'idQualifarea'], 'required'],
            [['idBeneficiary', 'idQualif', 'idQualifarea'], 'integer'],
            [['idBeneficiary'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderBeneficiaries::className(), 'targetAttribute' => ['idBeneficiary' => 'idBeneficiary']],
            [['idQualif'], 'exist', 'skipOnError' => true, 'targetClass' => CbQualifications::className(), 'targetAttribute' => ['idQualif' => 'idQualif']],
            [['idQualifarea'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderQualificationarea::className(), 'targetAttribute' => ['idQualifarea' => 'idQualifarea']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBenefQualification' => Yii::t('app', 'Id Benef Qualification'),
            'idBeneficiary' => Yii::t('app', 'Id Beneficiary'),
            'idQualif' => Yii::t('app', 'Qualification'),
            'idQualifarea' => Yii::t('app', 'Qualification area'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdBeneficiary0()
    {
        return $this->hasOne(ProviderBeneficiaries::className(), ['idBeneficiary' => 'idBeneficiary']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualif0()
    {
        return $this->hasOne(CbQualifications::className(), ['idQualif' => 'idQualif']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdQualifarea0()
    {
        return $this->hasOne(ProviderQualificationarea::className(), ['idQualifarea' => 'idQualifarea']);
    }
}
