<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_language}}".
 *
 * @property integer $idLanguage
 * @property string $language
 *
 * @property ProviderTrainingarea[] $providerTrainingareas
 */
class ProviderLanguage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_language}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language'], 'required'],
            [['language'], 'string', 'max' => 100],
            [['language'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLanguage' => Yii::t('app', 'Id Language'),
            'language' => Yii::t('app', 'Teaching language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderTrainingareas()
    {
        return $this->hasMany(ProviderTrainingarea::className(), ['idLanguage' => 'idLanguage']);
    }
}
