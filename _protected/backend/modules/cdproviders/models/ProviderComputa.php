<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_computa}}".
 *
 * @property integer $idComputa
 * @property string $computavailability
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderComputa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_computa}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['computavailability'], 'required'],
            [['computavailability'], 'string', 'max' => 10],
            [['computavailability'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idComputa' => 'Id Computa',
            'computavailability' => 'Computavailability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idComputa' => 'idComputa']);
    }
}
