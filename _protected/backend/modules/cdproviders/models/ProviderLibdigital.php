<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_libdigital}}".
 *
 * @property integer $idLabdigital
 * @property string $labdigital
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderLibdigital extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_libdigital}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['labdigital'], 'required'],
            [['labdigital'], 'string', 'max' => 10],
            [['labdigital'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLabdigital' => 'Id Labdigital',
            'labdigital' => 'Labdigital',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idLabdigital' => 'idLabdigital']);
    }
}
