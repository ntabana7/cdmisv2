<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_providertype}}".
 *
 * @property integer $idProviderType
 * @property string $type
 *
 * @property ProviderProvider[] $providerProviders
 */
class ProviderProvidertype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_providertype}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 100],
            [['type'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProviderType' => Yii::t('app', 'Id Provider Type'),
            'type' => Yii::t('app', 'Providers Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderProviders()
    {
        return $this->hasMany(ProviderProvider::className(), ['idProviderType' => 'idProviderType']);
    }
}
