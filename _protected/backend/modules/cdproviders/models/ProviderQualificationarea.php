<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_qualificationarea}}".
 *
 * @property integer $idQualifarea
 * @property string $area
 * @property string $internationstandard
 * @property string $internationstandardcode
 *
 * @property ProviderBeneficiaries[] $providerBeneficiaries
 * @property ProviderTrainer[] $providerTrainers
 */
class ProviderQualificationarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_qualificationarea}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area'], 'required'],
            [['area', 'internationstandard', 'internationstandardcode'], 'string', 'max' => 100],
            [['area'], 'unique'],
            [['internationstandard'], 'unique'],
            [['internationstandardcode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idQualifarea' => Yii::t('app', 'Id Qualifarea'),
            'area' => Yii::t('app', 'Qualification area'),
            'internationstandard' => Yii::t('app', 'Internation standard'),
            'internationstandardcode' => Yii::t('app', 'Internation standard code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiaries()
    {
        return $this->hasMany(ProviderBeneficiaries::className(), ['idQualifarea' => 'idQualifarea']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderTrainers()
    {
        return $this->hasMany(ProviderTrainer::className(), ['idQualifarea' => 'idQualifarea']);
    }
}
