<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\CbSublevels;

/**
 * This is the model class for table "{{%provider_services}}".
 *
 * @property integer $idService
 * @property integer $idProvider
 * @property integer $idSubLevel
 *
 * @property ProviderProvider $idProvider0
 * @property CbSublevels $idSubLevel0
 */
class ProviderServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_services}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvider', 'idSubLevel'], 'required'],
            [['idProvider', 'idSubLevel'], 'integer'],
            [['idProvider'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvider::className(), 'targetAttribute' => ['idProvider' => 'idProvider']],
            [['idSubLevel'], 'exist', 'skipOnError' => true, 'targetClass' => CbSublevels::className(), 'targetAttribute' => ['idSubLevel' => 'idSubLevel']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idService' => Yii::t('app', 'Id Service'),
            'idProvider' => Yii::t('app', 'Providers'),
            'idSubLevel' => Yii::t('app', 'Service'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvider0()
    {
        return $this->hasOne(ProviderProvider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubLevel0()
    {
        return $this->hasOne(CbSublevels::className(), ['idSubLevel' => 'idSubLevel']);
    }
}
