<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\Skills;
use backend\models\InstitSectors;
use backend\models\CbTypes;
use backend\models\TrainingAreas;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%provider_trainingareasbyprovider}}".
 *
 * @property integer $idTrainingprovider
 * @property integer $idSkills
 * @property integer $idInstitSector
 * @property integer $idCbType
 * @property integer $idProvider
 * @property integer $idTraining
 * @property integer $idMethodology
 * @property integer $idLanguage
 * @property integer $idEvaluation
 * @property string $startingdate
 *
 * @property ProviderBeneficiariesenrollment[] $providerBeneficiariesenrollments
 * @property Skills $idSkills0
 * @property InstitSectors $idInstitSector0
 * @property CbTypes $idCbType0
 * @property ProviderProvider $idProvider0
 * @property ProviderMethodology $idMethodology0
 * @property ProviderLanguage $idLanguage0
 * @property ProviderEvaluation $idEvaluation0
 * @property TrainingAreas $idTraining0
 * @property ProviderWhotrainwhat[] $providerWhotrainwhats
 */
class ProviderTrainingareasbyprovider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_trainingareasbyprovider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idSkills', 'idInstitSector', 'idCbType', 'idProvider', 'idTraining','startingdate'], 'required'],
            [['idSkills', 'idInstitSector', 'idCbType', 'idProvider', 'idTraining'], 'integer'],
            [['startingdate'], 'string', 'max' => 100],
            [['idSkills'], 'exist', 'skipOnError' => true, 'targetClass' => Skills::className(), 'targetAttribute' => ['idSkills' => 'idSkills']],
            [['idInstitSector'], 'exist', 'skipOnError' => true, 'targetClass' => InstitSectors::className(), 'targetAttribute' => ['idInstitSector' => 'idInstitSector']],
            [['idCbType'], 'exist', 'skipOnError' => true, 'targetClass' => CbTypes::className(), 'targetAttribute' => ['idCbType' => 'idCbType']],
            [['idProvider'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvider::className(), 'targetAttribute' => ['idProvider' => 'idProvider']],
            // [['idMethodology'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderMethodology::className(), 'targetAttribute' => ['idMethodology' => 'idMethodology']],
            // [['idLanguage'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderLanguage::className(), 'targetAttribute' => ['idLanguage' => 'idLanguage']],
            // [['idEvaluation'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderEvaluation::className(), 'targetAttribute' => ['idEvaluation' => 'idEvaluation']],
            [['idTraining'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingAreas::className(), 'targetAttribute' => ['idTraining' => 'idTraining']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTrainingprovider' => Yii::t('app', 'Id Trainingprovider'),
            'idSkills' => Yii::t('app', 'Skills type'),
            'idInstitSector' => Yii::t('app', 'Sector'),
            'idCbType' => Yii::t('app', 'CD Type'),
            'idProvider' => Yii::t('app', 'Provider'),
            'idTraining' => Yii::t('app', 'Training area'),
            'methodology' => Yii::t('app', 'Training Methodology'),
            'language' => Yii::t('app', 'Training Language'),
            'evaluation' => Yii::t('app', 'Evaluation methodology'),
            'startingdate' => Yii::t('app', 'Starting date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiariesenrollments()
    {
        return $this->hasMany(ProviderBeneficiariesenrollment::className(), ['idTrainingprovider' => 'idTrainingprovider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSkills0()
    {
        return $this->hasOne(Skills::className(), ['idSkills' => 'idSkills']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitSector0()
    {
        return $this->hasOne(InstitSectors::className(), ['idInstitSector' => 'idInstitSector']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCbType0()
    {
        return $this->hasOne(CbTypes::className(), ['idCbType' => 'idCbType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvider0()
    {
        return $this->hasOne(ProviderProvider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getIdMethodology0()
    // {
    //     return $this->hasOne(ProviderMethodology::className(), ['idMethodology' => 'idMethodology']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLanguage0()
    {
        return $this->hasOne(ProviderLanguage::className(), ['idLanguage' => 'idLanguage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getIdEvaluation0()
    // {
    //     return $this->hasOne(ProviderEvaluation::className(), ['idEvaluation' => 'idEvaluation']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTraining0()
    {
        return $this->hasOne(TrainingAreas::className(), ['idTraining' => 'idTraining']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderWhotrainwhats()
    {
        return $this->hasMany(ProviderWhotrainwhat::className(), ['idTrainingprovider' => 'idTrainingprovider']);
    }
    public function getMethodologiesListDropdown()
    {   
        $listmethodologies   = ProviderMethodology::find()->select('methodology')->all();
        $listt   = ArrayHelper::map( $listmethodologies,'methodology','methodology');

        return $listt;
    }

    public function getEvaluationsListDropdown()
    {   
        $listevaluations   = ProviderEvaluation::find()->select('method')->all();
        $list   = ArrayHelper::map( $listevaluations,'method','method');

        return $list;
    }
    public function getLanguagesListDropdown()
    {   
        $listlanguages   = ProviderLanguage::find()->select('language')->all();
        $lists   = ArrayHelper::map( $listlanguages,'language','language');

        return $lists;
    }
}
