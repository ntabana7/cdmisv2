<?php

namespace backend\modules\cdproviders\models;

use Yii;


/**
 * This is the model class for table "{{%provider_beneficiaries}}".
 *
 * @property integer $idBeneficiary
 * @property string $name
 * @property integer $idGender
 * @property string $dob
 * @property string $phone
 * @property string $email
 * @property string $nida
 *
 * @property ProviderHumangender $idGender0
 * @property ProviderBeneficiariesenrollment[] $providerBeneficiariesenrollments
 * @property ProviderBeneficiaryqualification[] $providerBeneficiaryqualifications
 */
class ProviderBeneficiaries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_beneficiaries}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'idGender', 'dob', 'phone', 'email', 'nida'], 'required'],
            [['idGender'], 'integer'],
            [['name', 'dob', 'phone', 'email'], 'string', 'max' => 100],
            [['nida'], 'string', 'max' => 16],
            [['phone'], 'unique'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['nida'], 'unique'],
            [['idGender'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderHumangender::className(), 'targetAttribute' => ['idGender' => 'idGender']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBeneficiary' => Yii::t('app', 'Id Beneficiary'),
            'name' => Yii::t('app', 'Name'),
            'idGender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Date of birth'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'nida' => Yii::t('app', 'National ID number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGender0()
    {
        return $this->hasOne(ProviderHumangender::className(), ['idGender' => 'idGender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiariesenrollments()
    {
        return $this->hasMany(ProviderBeneficiariesenrollment::className(), ['idBeneficiary' => 'idBeneficiary']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderBeneficiaryqualifications()
    {
        return $this->hasMany(ProviderBeneficiaryqualification::className(), ['idBeneficiary' => 'idBeneficiary']);
    }
}
