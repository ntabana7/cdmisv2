<?php

namespace backend\modules\cdproviders\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\cdproviders\models\ProviderInternet;

/**
 * ProviderInternetSearch represents the model behind the search form about `backend\modules\cdproviders\models\ProviderInternet`.
 */
class ProviderInternetSearch extends ProviderInternet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInternet'], 'integer'],
            [['internetavailability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProviderInternet::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idInternet' => $this->idInternet,
        ]);

        $query->andFilterWhere(['like', 'internetavailability', $this->internetavailability]);

        return $dataProvider;
    }
}
