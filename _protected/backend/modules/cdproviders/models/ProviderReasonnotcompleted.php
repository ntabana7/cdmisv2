<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_reasonnotcompleted}}".
 *
 * @property integer $idreason
 * @property string $reason
 */
class ProviderReasonnotcompleted extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_reasonnotcompleted}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason'], 'required'],
            [['reason'], 'string', 'max' => 200],
            [['reason'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idreason' => Yii::t('app', 'Idreason'),
            'reason' => Yii::t('app', 'Reason'),
        ];
    }
}
