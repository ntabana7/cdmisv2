<?php

namespace backend\modules\cdproviders\models;

use Yii;
use backend\models\TrainingAreas;

/**
 * This is the model class for table "{{%provider_whotrainwhat}}".
 *
 * @property integer $idwho
 * @property integer $idProvider
 * @property integer $idTraining
 * @property integer $idTrainer
 *
 * @property ProviderProvider $idProvider0
 * @property TrainingAreas $idTraining0
 * @property ProviderTrainer $idTrainer0
 */
class ProviderWhotrainwhat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_whotrainwhat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvider', 'idTraining', 'idTrainer'], 'required'],
            [['idProvider', 'idTraining', 'idTrainer'], 'integer'],
            [['idProvider'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderProvider::className(), 'targetAttribute' => ['idProvider' => 'idProvider']],
            [['idTraining'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingAreas::className(), 'targetAttribute' => ['idTraining' => 'idTraining']],
            [['idTrainer'], 'exist', 'skipOnError' => true, 'targetClass' => ProviderTrainer::className(), 'targetAttribute' => ['idTrainer' => 'idTrainer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idwho' => Yii::t('app', 'Idwho'),
            'idProvider' => Yii::t('app', 'Provider'),
            'idTraining' => Yii::t('app', 'Training area'),
            'idTrainer' => Yii::t('app', 'Trainer'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvider0()
    {
        return $this->hasOne(ProviderProvider::className(), ['idProvider' => 'idProvider']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTraining0()
    {
        return $this->hasOne(TrainingAreas::className(), ['idTraining' => 'idTraining']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTrainer0()
    {
        return $this->hasOne(ProviderTrainer::className(), ['idTrainer' => 'idTrainer']);
    }
}
