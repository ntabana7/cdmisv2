<?php

namespace backend\modules\cdproviders\models;

use Yii;

/**
 * This is the model class for table "{{%provider_internet}}".
 *
 * @property integer $idInternet
 * @property string $internetavailability
 *
 * @property ProviderInfrastructure[] $providerInfrastructures
 */
class ProviderInternet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%provider_internet}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['internetavailability'], 'required'],
            [['internetavailability'], 'string', 'max' => 10],
            [['internetavailability'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInternet' => 'Id Internet',
            'internetavailability' => 'Internetavailability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderInfrastructures()
    {
        return $this->hasMany(ProviderInfrastructure::className(), ['idInternet' => 'idInternet']);
    }
}
