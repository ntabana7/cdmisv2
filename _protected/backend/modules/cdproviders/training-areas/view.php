<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingAreas */
?>
<div class="training-areas-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTraining',
            'idSkills'.'value'=>'idSkills0.skills',
            'idInstitSector'.'value'=>'idInstitSector0.sectName',
            'idCbType'.'value'=>'idCbType0.cbType',
            'courseName',
        ],
    ]) ?>

</div>
