<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiaryqualification */
?>
<div class="provider-beneficiaryqualification-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idBenefQualification',
            'idBeneficiary',
            'idQualif',
            'idQualifarea',
        ],
    ]) ?>

</div>
