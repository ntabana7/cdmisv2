<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiaryqualification */
?>
<div class="provider-beneficiaryqualification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
