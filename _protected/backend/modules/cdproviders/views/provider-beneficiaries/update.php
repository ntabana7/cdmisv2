<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiaries */
?>
<div class="provider-beneficiaries-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsBeneficiaryqualification'=>$modelsBeneficiaryqualification
    ]) ?>

</div>
