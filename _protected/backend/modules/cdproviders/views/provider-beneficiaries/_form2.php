<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use backend\modules\cdproviders\models\ProviderHumangender;
use backend\models\CbQualifications;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiaries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-beneficiaries-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
    <div class="col-md-6">
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nida')->textInput(['maxlength' => true]) ?>

    
    <?= $form->field($model, 'idGender')->dropDownList(ArrayHelper::map(ProviderHumangender::find()->all(),'idGender','gender'),[ 'prompt'=>'Select gender',
                            'language' => 'en',
                            ]);

    ?>  
    </div>  
    
    <div class="col-md-6">
        
    
    <?= $form->field($model, 'dob')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?> 

    </div>        
    
    <div class="col-md-6">
        
    <?= $form->field($model, 'idQualif')->dropDownList(ArrayHelper::map(CbQualifications::find()->all(),'idQualif','qualif'),[ 'prompt'=>'Select Qualification',
                            'language' => 'en',
                            ]);

    ?>
    </div>
    <div class="col-md-6">
    
    <?= $form->field($model, 'idQualifarea')->dropDownList(ArrayHelper::map(ProviderQualificationarea::find()->all(),'idQualifarea','area'),[ 'prompt'=>'Select Qualification area',
                            'language' => 'en',
                            ]);

    ?>

    </div>        
    </div>  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
