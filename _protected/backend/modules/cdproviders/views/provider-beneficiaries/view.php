<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiaries */
?>
<div class="provider-beneficiaries-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idBeneficiary',
            'name',
            'idGender0.gender',
            'dob',
            'phone',
            'email:email',
            'nida',
        ],
    ]) ?>

</div>
