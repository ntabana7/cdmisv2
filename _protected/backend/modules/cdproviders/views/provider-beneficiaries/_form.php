<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use backend\modules\cdproviders\models\ProviderHumangender;
use backend\models\CbQualifications;
use dosamigos\datepicker\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiaries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-beneficiaries-form">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

    <div class='row'>
    <div class="col-md-6">
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nida')->textInput(['maxlength' => true]) ?>

    
    <?= $form->field($model, 'idGender')->dropDownList(ArrayHelper::map(ProviderHumangender::find()->all(),'idGender','gender'),[ 'prompt'=>'Select gender',
                            'language' => 'en',
                            ]);

    ?>  
    </div>  
    
    <div class="col-md-6">
        
    
    <?= $form->field($model, 'dob')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?> 

    </div>   
    </div>


    <div class='row'>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-"></i>Benefiaciaries Qualifications</h4></div>
        <!-- envelope -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsBeneficiaryqualification[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'idQualif',
                    'idQualifarea',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsBeneficiaryqualification as $i => $modelBeneficiaryqualification): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Benefiaciaries Qualifications</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelBeneficiaryqualification->isNewRecord) {
                                echo Html::activeHiddenInput($modelBeneficiaryqualification, "[{$i}]id");
                            }
                        ?>                        
                        <div class="row">
                            <div class="col-sm-6">                             

                            <?= $form->field($modelBeneficiaryqualification, "[{$i}]idQualif")->dropDownList(ArrayHelper::map(CbQualifications::find()->all(),'idQualif','qualif'),[ 'prompt'=>'Select gender',
                            'language' => 'en',
                            ]);

                             ?>  
                            </div>
                            <div class="col-sm-6">
                            <?= $form->field($modelBeneficiaryqualification, "[{$i}]idQualifarea")->dropDownList(ArrayHelper::map(ProviderQualificationarea::find()->all(),'idQualifarea','area'),[ 'prompt'=>'Select gender',
                            'language' => 'en',
                            ]);

                             ?> 
                            </div>
                        </div><!-- .row -->
 
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>        
    </div>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
