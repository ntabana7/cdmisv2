<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainer */
?>
<div class="provider-trainer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTrainer',
            'name',
            'idGender0.gender',
            'dob',
            'phone',
            'email:email',
            'nida',
            'idQualif0.qualif',
            'idQualifarea0.area',
            'degree',
            'whendoyoustarttraining',
        ],
    ]) ?>

</div>
