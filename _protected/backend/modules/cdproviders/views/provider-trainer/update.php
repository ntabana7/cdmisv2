<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainer */
?>
<div class="provider-trainer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
