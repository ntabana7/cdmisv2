<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainer */

?>
<div class="provider-trainer-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelsTrainerexpertise'=>$modelsTrainerexpertise,
    ]) ?>
</div>
