<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use backend\modules\cdproviders\models\ProviderHumangender;
use backend\models\CbQualifications;
use dosamigos\datepicker\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-trainer-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class='row'>
    <div class="col-md-4">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>        
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'idGender')->dropDownList(ArrayHelper::map(ProviderHumangender::find()->all(),'idGender','gender'),[ 'prompt'=>'Select gender',
                            'language' => 'en',
                            ]);

    ?>
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'dob')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>
        
    </div>
    </div>
    <div class='row'>
    <div class="col-md-4">
       <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?> 
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>  
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'nida')->textInput(['maxlength' => true]) ?>  
    </div>
    </div>

    <div class='row'>
    <div class="col-md-4"> 
    <?= $form->field($model, 'idQualif')->dropDownList(ArrayHelper::map(CbQualifications::find()->all(),'idQualif','qualif'),[ 'prompt'=>'Select Qualification',
                            'language' => 'en',
                            ]);

    ?>       
    </div>    
    <div class="col-md-4"> 
    <?= $form->field($model, 'idQualifarea')->dropDownList(ArrayHelper::map(ProviderQualificationarea::find()->all(),'idQualifarea','area'),[ 'prompt'=>'Select Qualification area',
                            'language' => 'en',
                            ]);

    ?>       
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'degree')->fileInput() ?>
     </div>
    </div>

    <?= $form->field($model, 'whendoyoustarttraining')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>



    <div class='row'>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-"></i>Trainer Expertise</h4></div>
        <!-- envelope -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsTrainerexpertise[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'areaofexpertise',
                    'certificate',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsTrainerexpertise as $i => $modelTrainerexpertise): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Trainer Expertise</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelTrainerexpertise->isNewRecord) {
                                echo Html::activeHiddenInput($modelTrainerexpertise, "[{$i}]id");
                            }
                        ?>                        
                        <div class="row">
                            <div class="col-sm-6">
                            <?= $form->field($modelTrainerexpertise, "[{$i}]areaofexpertise")->textInput(['maxlength' => true]) ?>   
                            </div>
                            <div class="col-sm-6">
                            <?= $form->field($modelTrainerexpertise, "[{$i}]certificate")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
 
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>        
    </div>   
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
