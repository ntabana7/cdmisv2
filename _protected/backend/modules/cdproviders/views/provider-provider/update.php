<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvider */
?>
<div class="provider-provider-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
