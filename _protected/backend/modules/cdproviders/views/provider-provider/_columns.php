<?php
use yii\helpers\Url;
use yii\helpers\Html;
if(Yii::$app->user->can('admin')){    
 $_column =   [

     // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idProvider',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'providername',
    ],    
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idAffiliated',
        'value'=>'idAffiliated0.affiliated',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'affiliatedto',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idProviderCat',
        'value'=>'idProviderCat0.category',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idProviderType',
        'value'=>'idProviderType0.type',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idProviderBase',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idCountry',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'physicaladdress',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'email',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'phone',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'pobox',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'website',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'tinorregistrationnumber',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'nationalIDorPassport',
    // ],    
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idRegrw',
        'value'=>'idRegrw0.registered',
    ],
    [
        'label'=>Yii::t('app','Create').' '.Yii::t('app','User'),
        'format' => 'raw',
        
        'value'=>function ($data) {
            return Html::a(Yii::t('app','Create login'),['../user/create','iduser'=>$data["idProvider"],'email'=>$data["email"]]);
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idRegbody',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'registeredon',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'status',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

 ];   
}else{
     $_column =   [


];
}
 return $_column;

