<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\cdproviders\models\ProviderProvidercategory;
use backend\modules\cdproviders\models\ProviderProvidertype;
use backend\modules\cdproviders\models\ProviderProviderbase;
use backend\modules\cdproviders\models\ProviderCountry;
use backend\modules\cdproviders\models\ProviderAffiliated;
use backend\modules\cdproviders\models\ProviderRegbody;
use backend\modules\cdproviders\models\ProviderRegrwanda;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-provider-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
    <div class="col-md-4">
     <?= $form->field($model, 'providername')->textInput(['maxlength' => true]) ?>   
    </div>

    <div class="col-md-4">
    <?= $form->field($model, 'idAffiliated')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderAffiliated::find()->all(),'idAffiliated','affiliated'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerprovider-idaffiliated").value;
                                   if(id == 1){
                                    $("#affiliate" ).show();
                                   }
                                    else{
                                    $("#affiliate" ).hide();
                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?> 
    </div>
    <div class="col-md-4" id='affiliate' style='display:none'>
      <?= $form->field($model, 'affiliatedto')->textInput(['maxlength' => true]) ?>
    </div>
    </div>

    <div class='row'>
    <div class="col-md-4">
    <?= $form->field($model, 'idProviderCat')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderProvidercategory::find()->all(),'idProviderCat','category'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select your category',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerprovider-idprovidercat").value;
                                   if(id == 3){
                                    $("#nationalid" ).show();
                                   }
                                    else{
                                    $("#nationalid" ).hide();
                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>  
    </div>
    <div class="col-md-4" id='nationalid' style='display:none'>
    <?= $form->field($model, 'nationalIDorPassport')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'idProviderType')->dropDownList(ArrayHelper::map(ProviderProvidertype::find()->all(),'idProviderType','type'),[ 'prompt'=>'Select provider type',
                            'language' => 'en',
                            ]);

    ?>   
    </div>

    <div class="col-md-4">
    <?= $form->field($model, 'idRegrw')->dropDownList(ArrayHelper::map(ProviderRegrwanda::find()->all(),'idRegrw','registered'),[ 'prompt'=>'Select the answer',
                            'language' => 'en',
                            ]);

    ?> 
    
    </div>
    </div>

    <div class='row'>
    <div class="col-md-4"> 
    <?= $form->field($model, 'idRegbody')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderRegbody::find()->all(),'idRegbody','abbravation'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerprovider-idregbody").value;
                                   if(id == 4){
                                    $("#regbody" ).show();
                                   }
                                    else{
                                    $("#regbody" ).hide();                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>    
    </div>
    <div class="col-md-4" id='regbody' style='display:none'>
    <?= $form->field($model, 'registeredelsewhere')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'registeredon')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>     
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'tinorregistrationnumber')->textInput(['maxlength' => true]) ?> 
    </div>

    </div>

    <div class='row'>
    <div class="col-md-4">
     <?= $form->field($model, 'idProviderBase')->dropDownList(ArrayHelper::map(ProviderProviderbase::find()->all(),'idProviderBase','base'),[ 'prompt'=>'Select provider geo-location',
                            'language' => 'en',
                            ]);

    ?>   
    </div>
    <div class="col-md-4">
       <?= $form->field($model, 'idCountry')->dropDownList(ArrayHelper::map(ProviderCountry::find()->all(),'idCountry','country'),[ 'prompt'=>'country',
                            'language' => 'en',
                            ]);

    ?>  
    </div>
    <div class="col-md-4">
      <?= $form->field($model, 'physicaladdress')->textInput(['maxlength' => true]) ?>  
    </div>
    </div>

    <div class='row'>
    <div class="col-md-4">
     <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>   
    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>   
    </div>
    <div class="col-md-4">
          <?= $form->field($model, 'pobox')->textInput(['maxlength' => true]) ?>  
    </div>
    </div>

    <div class='row'>
    <div class="col-md-12">
     <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>  
    </div>     
    </div>
   <!--  <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?> -->
     
  <?php if (!Yii::$app->request->isAjax){ ?>
      <div class="form-group">
          <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
      </div>
  <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
