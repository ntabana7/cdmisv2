<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvider */
?>
<div class="provider-provider-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProvider',
            'providername',            
            'idAffiliated0.affiliated',
            'affiliatedto',
            'idProviderCat0.category',
            'idProviderType0.type',
            'idProviderBase0.base',
            'idCountry0.country',
            'physicaladdress',
            'email:email',
            'phone',
            'pobox',
            'website',
            'tinorregistrationnumber',
            'nationalIDorPassport',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idRegrw',
            'value'=>$model->idRegrw0->registered,            
            ],
            'idRegbody0.register',
            'registeredelsewhere',
            'registeredon',
            'status',
        ],
    ]) ?>

</div>
