<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderLibrary */

?>
<div class="provider-library-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
