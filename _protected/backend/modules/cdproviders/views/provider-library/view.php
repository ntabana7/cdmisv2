<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderLibrary */
?>
<div class="provider-library-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLibrary',
            'libraryavailability',
        ],
    ]) ?>

</div>
