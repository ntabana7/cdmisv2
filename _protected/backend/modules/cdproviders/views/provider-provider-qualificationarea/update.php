<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderQualificationarea */
?>
<div class="provider-qualificationarea-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
