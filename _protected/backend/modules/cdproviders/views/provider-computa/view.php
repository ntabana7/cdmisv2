<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderComputa */
?>
<div class="provider-computa-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idComputa',
            'computavailability',
        ],
    ]) ?>

</div>
