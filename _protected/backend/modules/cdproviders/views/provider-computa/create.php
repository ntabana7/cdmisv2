<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderComputa */

?>
<div class="provider-computa-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
