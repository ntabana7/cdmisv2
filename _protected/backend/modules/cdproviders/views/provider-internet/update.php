<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderInternet */
?>
<div class="provider-internet-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
