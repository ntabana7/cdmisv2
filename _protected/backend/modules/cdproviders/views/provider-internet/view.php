<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderInternet */
?>
<div class="provider-internet-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idInternet',
            'internetavailability',
        ],
    ]) ?>

</div>
