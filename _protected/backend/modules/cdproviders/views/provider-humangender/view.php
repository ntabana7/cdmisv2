<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderHumangender */
?>
<div class="provider-humangender-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idGender',
            'gender',
        ],
    ]) ?>

</div>
