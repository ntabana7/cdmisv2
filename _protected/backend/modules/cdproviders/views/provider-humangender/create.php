<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderHumangender */

?>
<div class="provider-humangender-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
