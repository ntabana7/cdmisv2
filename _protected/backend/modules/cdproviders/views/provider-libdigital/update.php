<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderLibdigital */
?>
<div class="provider-libdigital-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
