<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderLibdigital */
?>
<div class="provider-libdigital-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLabdigital',
            'labdigital',
        ],
    ]) ?>

</div>
