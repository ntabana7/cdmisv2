<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Skills;
use backend\models\InstitSectors;
use backend\models\CbTypes;
use backend\modules\cdproviders\models\ProviderProvider;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainingarea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-trainingarea-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idSkills')->dropDownList(ArrayHelper::map(Skills::find()->all(),'idSkills','skills'),[ 'prompt'=>'Select skills type',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'idInstitSector')->dropDownList(ArrayHelper::map(InstitSectors::find()->all(),'idInstitSector','sectName'),[ 'prompt'=>'Select sector',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'idCbType')->dropDownList(ArrayHelper::map(CbTypes::find()->all(),'idCbType','cbType'),[ 'prompt'=>'Select course type',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'idProvider')->dropDownList(ArrayHelper::map(ProviderProvider::find()->all(),'idProvider','providername'),[ 'prompt'=>'Select provider',
                            'language' => 'en',
                            ]);

    ?>

    <?= $form->field($model, 'coursename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'internationalstandardsISCED')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'internationalstandardsISCEDCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idMethodology')->textInput() ?>

    <?= $form->field($model, 'idLanguage')->textInput() ?>

    <?= $form->field($model, 'idEvaluation')->textInput() ?>

    <?= $form->field($model, 'startingdate')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
