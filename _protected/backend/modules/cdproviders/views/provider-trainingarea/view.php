<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainingarea */
?>
<div class="provider-trainingarea-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTrainingprovider',
            'idSkills',
            'idInstitSector',
            'idCbType',
            'idProvider',
            'coursename',
            'internationalstandardsISCED',
            'internationalstandardsISCEDCode',
            'idMethodology',
            'idLanguage',
            'idEvaluation',
            'startingdate',
        ],
    ]) ?>

</div>
