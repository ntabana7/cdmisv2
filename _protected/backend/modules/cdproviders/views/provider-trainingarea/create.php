<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainingarea */

?>
<div class="provider-trainingarea-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
