<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainingarea */
?>
<div class="provider-trainingarea-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
