<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiariesenrollment */
?>
<div class="provider-beneficiariesenrollment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
