<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Plans */
?>
<div class="beneficiariesenrollment-update">

    <?= $this->render('_decisionform', [
        'model' => $model,
    ]) ?>

</div>