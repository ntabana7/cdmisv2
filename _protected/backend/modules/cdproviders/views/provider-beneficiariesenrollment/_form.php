<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2; 
use backend\models\Institutions;
use backend\models\CbSublevels;
use backend\models\TrainingAreas;
use backend\models\CbQualifications;
use backend\models\Funders;
use backend\modules\cdproviders\models\ProviderProvider;
use backend\modules\cdproviders\models\ProviderBeneficiaries;
use backend\modules\cdproviders\models\ProviderWorkingposition;
use backend\modules\cdproviders\models\ProviderCountry;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiariesenrollment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-beneficiariesenrollment-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
    <div class="col-md-4">
    <?= $form->field($model, 'idBeneficiary')->textInput(
            [
            'maxlength' => true,
            'onchange'=>'   
                $.post("'.Url::to(['provider-beneficiaries/lists', 'id'=> '']).'"+$(this).val(),function(data){
                    $("div#name").html(data)
                });'
            ]);
    ?>
    </div>
    <div class="col-md-4" id='name'>
     </div>
    <div class="col-md-4">
     <!-- <?= $form->field($model, 'idProvider')->dropDownList(ArrayHelper::map(ProviderProvider::find()->all(),'idProvider','providername'),[ 'prompt'=>'Select provider',
                            'language' => 'en',
                            ]);

    ?>   --> 
    </div>
    
     
    </div>
    <div class='row'>
    <div class="col-md-12">
     
    <?= $form->field($model, 'idInstit')->widget(select2::classname(),[
        'data'=>ArrayHelper::map(Institutions::find()->all(),'idInstit','institName'),
        'theme' => Select2::THEME_KRAJEE, 
        'options'=>[
        'placeholder'=>'Select beneficiary institution',
        // 'onchange'=>'
        //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
        //          $("select#select2-plans-idsubprogram-container" ).html(data);
        //     });'
        ],
        'language' => 'en',
        'pluginOptions'=>['alloweClear'=>true],
        ]);

        ?>


    </div>        
    </div>
      <div class='row'>
    <div class="col-md-4">

        <?= $form->field($model, 'idWorkingposition')->widget(select2::classname(),[
        'data'=>ArrayHelper::map(ProviderWorkingposition::find()->all(),'idWorkingposition','position'),
        'theme' => Select2::THEME_KRAJEE, 
        'options'=>[
        'placeholder'=>'Select beneficiary position',
        // 'onchange'=>'
        //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
        //          $("select#select2-plans-idsubprogram-container" ).html(data);
        //     });'
        ],
        'language' => 'en',
        'pluginOptions'=>['alloweClear'=>true],
        ]);

        ?>


    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'idSubLevel')->dropDownList(ArrayHelper::map(CbSublevels::find()->where(['IN', 'idSubLevel', [1,2,3]])->all(),'idSubLevel','subLevel'),[ 'prompt'=>'Select service',
                            'language' => 'en',
                            ]);

    ?>  
    </div>
    <div class="col-md-4"> 

    <?= $form->field($model, 'idTraining')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(TrainingAreas::trainings(),'idTraining','courseName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the course name',
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]); 
                            ?> 
    </div>
    </div>
    <div class='row'>
    <div class="col-md-4">

    
    <?= $form->field($model, 'idCountry')->widget(select2::classname(),[
        'data'=>ArrayHelper::map(ProviderCountry::find()->all(),'idCountry','country'),
        'theme' => Select2::THEME_KRAJEE, 
        'options'=>[
        'placeholder'=>'Country where CD took place',
        // 'onchange'=>'
        //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
        //          $("select#select2-plans-idsubprogram-container" ).html(data);
        //     });'
        ],
        'language' => 'en',
        'pluginOptions'=>['alloweClear'=>true],
        ]);

        ?>





    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'startingdate')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>   
    </div>

    <div class="col-md-4">
     <?= $form->field($model, 'endingdate')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>   
    </div>
    </div>
    <div class='row'>
    <div class="col-md-4">
     <?= $form->field($model, 'idQualif')->dropDownList(ArrayHelper::map(CbQualifications::find()->all(),'idQualif','qualif'),[ 'prompt'=>'Select the qualification after CD',
                            'language' => 'en',
                            ]);

    ?>      
    </div>

    <div class="col-md-4">
     <?= $form->field($model, 'costoftraining')->textInput(['maxlength' => true]) ?>   
    </div>
    <div class="col-md-4">
     <!-- <?= $form->field($model, 'yearofcd')->textInput(['maxlength' => true]) ?> -->   
    </div>
    </div>
    <!-- <?= $form->field($model, 'funder')->textInput(['maxlength' => true]) ?> -->
    
    <?= $form->field($model, 'funder[]')            
                            ->dropDownList($model->FundersListDropdown,
                            [
                            'multiple'=>'multiple'
                            //'class'=>'chosen-select input-md required',              
                            ]             
                            )->label("Funders"); 
    ?> 

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
