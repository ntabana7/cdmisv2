<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idEnrollment',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idProvider',
        'value'=>'idProvider0.providername',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idBeneficiary',
        'value'=>'idBeneficiary0.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInstit',
        'value'=>'idInstit0.institName',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idWorkingposition',
        'value'=>'idWorkingposition0.position',
    ],
    'statuslabel',
    [
        'label'=>Yii::t('app','Report benecifiaries status'),
        'format' => 'raw',
        
        'value'=>function ($data) {
            return Html::a(Yii::t('app','Report'),['provider-beneficiariesenrollment/decision','id'=>$data["idEnrollment"],]);
        },
    ],

    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idSubLevel',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idTraining',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'startingdate',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'endingdate',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idQualif',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'costoftraining',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idCountry',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'funder',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'yearofcd',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'],
        

    ],

];   