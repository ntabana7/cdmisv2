<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiariesenrollment */
?>
<div class="provider-beneficiariesenrollment-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idEnrollment',
            'idProvider0.providername',
            'idBeneficiary0.name',
            'idInstit0.institName',
            'idWorkingposition0.position',
            'idSubLevel0.subLevel',
            'idTraining0.courseName',
            'startingdate',
            'endingdate',
            'idQualif0.qualif',
            'costoftraining',
            'idCountry0.country',
            'funder',
            'statuslabel',
            'reasonnotcompleted',
            'yearofcd',
        ],
    ]) ?>

</div>
