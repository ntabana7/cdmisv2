<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderBeneficiariesenrollment */

?>
<div class="provider-beneficiariesenrollment-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
