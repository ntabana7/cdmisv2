<?php
use yii\helpers\Url;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],
    //     [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idInfrast',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idProvider',
        'value'=>'idProvider0.providername',            
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idClassroom',
        'value'=>'idClassroom0.classroom',           
    ],    
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'howmanyclass',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'classcapacity',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idInternet',
        'value'=>'idInternet0.internetavailability',            
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idLab',
        'value'=>'idLab0.complab',            
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'howmanylab',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'idComputa',
        'value'=>'idComputa0.computavailability',            
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'howmanycomputa',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idHostel',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'hostelcapacity',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'idLibrary',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'idLabdigital',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   