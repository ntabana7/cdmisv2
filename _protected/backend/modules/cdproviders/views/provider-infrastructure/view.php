<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderInfrastructure */
?>
<div class="provider-infrastructure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idInfrast',
            'idProvider0.providername',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idClassroom',
            'value'=>$model->idClassroom0->classroom,            
            ],
            'howmanyclass',
            'classcapacity',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idInternet',
            'value'=>$model->idInternet0->internetavailability,            
            ],
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idLab',
            'value'=>$model->idLab0->complab,            
            ],
            'howmanylab',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idComputa',
            'value'=>$model->idComputa0->computavailability,            
            ],
            'howmanycomputa',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idHostel',
            'value'=>$model->idHostel0->hostelavailability,            
            ],
            'hostelcapacity',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idLibrary',
            'value'=>$model->idLibrary0->libraryavailability,            
            ],
            'idLabdigital0.labdigital',
        ],
    ]) ?>

</div>
