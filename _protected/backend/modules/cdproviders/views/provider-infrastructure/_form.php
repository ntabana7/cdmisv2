<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\cdproviders\models\ProviderProvider;
use backend\modules\cdproviders\models\ProviderClassroom;
use backend\modules\cdproviders\models\ProviderInternet;
use backend\modules\cdproviders\models\ProviderComplab;
use backend\modules\cdproviders\models\ProviderComputa;
use backend\modules\cdproviders\models\ProviderHostel;
use backend\modules\cdproviders\models\ProviderLibrary;
use backend\modules\cdproviders\models\ProviderLibdigital;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderInfrastructure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-infrastructure-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class='row'>
    <!-- <div class="col-md-6">   
    <?= $form->field($model, 'idProvider')->dropDownList(ArrayHelper::map(ProviderProvider::find()->all(),'idProvider','providername'),[ 'prompt'=>'Select provider',
                            'language' => 'en',
                            ]);

    ?>     
    </div> -->
    <div class="col-md-6"> 
    <?= $form->field($model, 'idInternet')->dropDownList(ArrayHelper::map(ProviderInternet::find()->all(),'idInternet','internetavailability'),[ 'prompt'=>'Select the answer',
                            'language' => 'en',
                            ]);

    ?>         
    </div>    
    </div>

    <div class='row'>
    <div class="col-md-4">
    <?= $form->field($model, 'idClassroom')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderClassroom::find()->all(),'idClassroom','classroom'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerinfrastructure-idclassroom").value;
                                   if(id == 1){
                                    $("#classroom" ).show();
                                    $("#classroomcapacity" ).show();
                                   }
                                    else{
                                    $("#providerinfrastructure-howmanyclass" ).val(0);
                                    $("#providerinfrastructure-classcapacity" ).val(0);
                                    $("#classroom" ).hide();
                                    $("#classroomcapacity" ).hide();
                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>          
    </div>
    <div class="col-md-4" id='classroom' style='display:none'>
    <?= $form->field($model, 'howmanyclass')->textInput() ?>        
    </div>
    <div class="col-md-4" id='classroomcapacity' style='display:none'>
    <?= $form->field($model, 'classcapacity')->textInput() ?>        
    </div> 
    <div class="col-md-4">
    <?= $form->field($model, 'idLab')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderComplab::find()->all(),'idLab','complab'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerinfrastructure-idlab").value;
                                   if(id == 1){
                                    $("#computerlabnumber" ).show();
                                   }
                                    else{
                                    $("#computerlabnumber" ).hide();
                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>          
    </div>
    <div class="col-md-4" id='computerlabnumber' style='display:none'> 
    <?= $form->field($model, 'howmanylab')->textInput() ?>       
    </div> 
    <div class="col-md-4">
    <?= $form->field($model, 'idComputa')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderComputa::find()->all(),'idComputa','computavailability'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerinfrastructure-idcomputa").value;
                                   if(id == 1){
                                    $("#computernumber" ).show();
                                   }
                                    else{
                                    $("#computernumber" ).hide();
                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>       
    </div>     
    </div>
    <div class='row'>    
    <div class="col-md-4" id='computernumber' style='display:none'> 
    <?= $form->field($model, 'howmanycomputa')->textInput() ?>       
    </div>
    </div>  

    <div class='row'>    
    <div class="col-md-4">
    <?= $form->field($model, 'idHostel')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderHostel::find()->all(),'idHostel','hostelavailability'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerinfrastructure-idhostel").value;
                                   if(id == 1){
                                    $("#hostelcapacity" ).show();
                                   }
                                    else{
                                    $("#hostelcapacity" ).hide();
                                   
                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>       
    </div>
    <div class="col-md-4" id='hostelcapacity' style='display:none'>    
    <?= $form->field($model, 'hostelcapacity')->textInput() ?>    
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'idLibrary')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(ProviderLibrary::find()->all(),'idLibrary','libraryavailability'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the answer',
                                'onchange'=>'

                                // To get where to type your affiliation if you are affiliated

                                   var id = document.getElementById("providerinfrastructure-idlibrary").value;
                                   if(id == 1){
                                    $("#digitallab").show();
                                   }
                                    else{
                                    $("#digitallab").hide();

                                    };'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>  
       
    </div>
    <div class="col-md-4" id='digitallab' style='display:none'>
    <?= $form->field($model, 'idLabdigital')->dropDownList(ArrayHelper::map(ProviderLibdigital::find()->all(),'idLabdigital','labdigital'),[ 'prompt'=>'Select the answer',
                            'language' => 'en',
                            ]);

    ?>      
    </div>  
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
