<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderInfrastructure */
?>
<div class="provider-infrastructure-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
