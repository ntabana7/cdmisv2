<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderAffiliated */
?>
<div class="provider-affiliated-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
