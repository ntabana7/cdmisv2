<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderAffiliated */
?>
<div class="provider-affiliated-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idAffiliated',
            'affiliated',
        ],
    ]) ?>

</div>
