<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderQualificationarea */
?>
<div class="provider-qualificationarea-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idQualifarea',
            'area',
            'internationstandard',
            'internationstandardcode',
        ],
    ]) ?>

</div>
