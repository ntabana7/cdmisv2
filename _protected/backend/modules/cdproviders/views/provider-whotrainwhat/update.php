<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderWhotrainwhat */
?>
<div class="provider-whotrainwhat-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
