<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderWhotrainwhat */
?>
<div class="provider-whotrainwhat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idwho',
            'idProvider',
            'idTraining',
            'idTrainer',
        ],
    ]) ?>

</div>
