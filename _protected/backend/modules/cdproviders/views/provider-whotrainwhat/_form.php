<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2; 
use backend\modules\cdproviders\models\ProviderProvider;
use backend\modules\cdproviders\models\ProviderTrainingareasbyprovider;
use backend\modules\cdproviders\models\ProviderTrainer;
use backend\models\TrainingAreas;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderWhotrainwhat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-whotrainwhat-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <!-- <?= $form->field($model, 'idProvider')->dropDownList(ArrayHelper::map(ProviderProvider::find()->all(),'idProvider','providername'),[ 'prompt'=>'Select provider',
                            'language' => 'en',
                            ]);

    ?> -->

    


    <?= $form->field($model, 'idTraining')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(TrainingAreas::trainings(),'idTraining','courseName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select the course name',
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]); 
    ?> 

    <?= $form->field($model, 'idTrainer')->dropDownList(ArrayHelper::map(ProviderTrainer::find()->all(),'idTrainer','name'),[ 'prompt'=>'Select trainer name',
                            'language' => 'en',
                            ]);

    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
