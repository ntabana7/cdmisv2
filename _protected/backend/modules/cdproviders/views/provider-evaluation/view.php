<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderEvaluation */
?>
<div class="provider-evaluation-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idEvaluation',
            'method',
        ],
    ]) ?>

</div>
