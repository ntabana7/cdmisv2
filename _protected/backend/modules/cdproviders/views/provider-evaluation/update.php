<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderEvaluation */
?>
<div class="provider-evaluation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
