<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderMethodology */

?>
<div class="provider-methodology-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
