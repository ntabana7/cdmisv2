<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderMethodology */
?>
<div class="provider-methodology-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idMethodology',
            'methodology',
        ],
    ]) ?>

</div>
