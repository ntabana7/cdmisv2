<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderRegrwanda */
?>
<div class="provider-regrwanda-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idRegrw',
            'registered',
        ],
    ]) ?>

</div>
