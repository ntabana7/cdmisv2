<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderRegrwanda */

?>
<div class="provider-regrwanda-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
