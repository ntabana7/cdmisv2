<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvidertype */
?>
<div class="provider-providertype-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProviderType',
            'type',
        ],
    ]) ?>

</div>
