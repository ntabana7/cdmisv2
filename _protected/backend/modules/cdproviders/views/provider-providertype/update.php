<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvidertype */
?>
<div class="provider-providertype-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
