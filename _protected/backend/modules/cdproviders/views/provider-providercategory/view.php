<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvidercategory */
?>
<div class="provider-providercategory-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProviderCat',
            'category',
        ],
    ]) ?>

</div>
