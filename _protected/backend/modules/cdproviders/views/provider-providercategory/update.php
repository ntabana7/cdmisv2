<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProvidercategory */
?>
<div class="provider-providercategory-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
