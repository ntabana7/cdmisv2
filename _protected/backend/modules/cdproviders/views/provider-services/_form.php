<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\cdproviders\models\ProviderProvider;
use backend\models\CbSublevels;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderServices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-services-form">

    <?php $form = ActiveForm::begin(); ?>

   <!--  <?= $form->field($model, 'idProvider')->dropDownList(ArrayHelper::map(ProviderProvider::find()->all(),'idProvider','providername'),[ 'prompt'=>'Select provider',
                            'language' => 'en',
                            ]);

    ?> -->

    <?= $form->field($model, 'idSubLevel')->dropDownList(ArrayHelper::map(CbSublevels::find()->where(['IN', 'idSubLevel', [1,2,3,8,9]])->all(),'idSubLevel','subLevel'),[ 'prompt'=>'Select service',
                            'language' => 'en',
                            ]);

    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
