<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderServices */
?>
<div class="provider-services-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
