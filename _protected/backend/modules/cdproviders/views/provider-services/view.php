<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderServices */
?>
<div class="provider-services-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idService',
            'idProvider0.providername',
            'idSubLevel0.subLevel',
        ],
    ]) ?>

</div>
