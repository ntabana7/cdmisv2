<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
?>
<div class="user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email:email',
            'password_hash',
            'status',
            'auth_key',
            'password_reset_token',
            'account_activation_token',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
