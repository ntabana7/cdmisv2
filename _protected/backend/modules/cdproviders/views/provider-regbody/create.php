<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderRegbody */

?>
<div class="provider-regbody-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
