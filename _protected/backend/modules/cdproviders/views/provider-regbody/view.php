<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderRegbody */
?>
<div class="provider-regbody-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idRegbody',
            'register',
            'abbravation',
        ],
    ]) ?>

</div>
