<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderRegbody */
?>
<div class="provider-regbody-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
