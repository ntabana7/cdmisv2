<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderCountry */
?>
<div class="provider-country-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCountry',
            'country',
        ],
    ]) ?>

</div>
