<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderCountry */

?>
<div class="provider-country-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
