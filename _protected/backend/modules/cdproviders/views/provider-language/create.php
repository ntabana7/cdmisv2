<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderLanguage */

?>
<div class="provider-language-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
