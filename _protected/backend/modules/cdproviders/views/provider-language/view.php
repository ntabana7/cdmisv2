<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderLanguage */
?>
<div class="provider-language-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLanguage',
            'language',
        ],
    ]) ?>

</div>
