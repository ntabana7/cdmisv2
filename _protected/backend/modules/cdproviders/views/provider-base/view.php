<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProviderbase */
?>
<div class="provider-providerbase-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProviderBase',
            'base',
        ],
    ]) ?>

</div>
