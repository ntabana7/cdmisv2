<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderProviderbase */
?>
<div class="provider-providerbase-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
