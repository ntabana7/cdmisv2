<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderComplab */
?>
<div class="provider-complab-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idLab',
            'complab',
        ],
    ]) ?>

</div>
