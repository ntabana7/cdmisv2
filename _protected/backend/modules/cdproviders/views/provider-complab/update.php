<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderComplab */
?>
<div class="provider-complab-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
