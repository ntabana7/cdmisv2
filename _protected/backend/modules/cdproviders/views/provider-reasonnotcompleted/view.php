<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderReasonnotcompleted */
?>
<div class="provider-reasonnotcompleted-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idreason',
            'reason',
        ],
    ]) ?>

</div>
