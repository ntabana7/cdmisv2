<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderReasonnotcompleted */
?>
<div class="provider-reasonnotcompleted-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
