<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainingareasbyprovider */
?>
<div class="provider-trainingareasbyprovider-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'idTrainingprovider',
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idProvider',
            'value'=>$model->idProvider0->providername,
            ],
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idInstitSector',
            'value'=>$model->idInstitSector0->sectName,
            ],
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idCbType',
            'value'=>$model->idCbType0->cbType,
            ],            
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idSkills',
            'value'=>$model->idSkills0->skills,
            ],
            [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'idTraining',
            'value'=>$model->idTraining0->courseName,
            ],
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'idMethodology',
            // 'value'=>$model->idMethodology0->methodology,
            // ],
            'methodology',
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'idLanguage',
            // 'value'=>$model->idLanguage0->language,
            // ],
            'language',
            // [
            // 'class'=>'\kartik\grid\DataColumn',
            // 'attribute'=>'idEvaluation',
            // 'value'=>$model->idEvaluation0->method,
            // ],
            'evaluation',
            'startingdate',
        ],
    ]) ?>

</div>

   