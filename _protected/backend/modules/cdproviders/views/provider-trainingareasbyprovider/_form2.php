<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use backend\models\Skills;
use backend\models\InstitSectors;
use backend\models\CbTypes;
use backend\models\TrainingAreas;
use backend\modules\cdproviders\models\ProviderProvider;
use backend\modules\cdproviders\models\ProviderMethodology;
use backend\modules\cdproviders\models\ProviderLanguage;
use backend\modules\cdproviders\models\ProviderEvaluation;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainingareasbyprovider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-trainingareasbyprovider-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <!-- <?= $form->field($model, 'idProvider')->dropDownList(ArrayHelper::map(ProviderProvider::find()->all(),'idProvider','providername'),[ 'prompt'=>'Select provider',
                            'language' => 'en',
                            ]);

    ?>     -->
    <div class='row'>
    <div class="col-md-4">
      <?= $form->field($model, 'idInstitSector')->dropDownList(ArrayHelper::map(InstitSectors::find()->all(),'idInstitSector','sectName'),[ 'prompt'=>'Select sector',
                            'language' => 'en',
                            ]);

    ?>  
    </div>
    <div class="col-md-4">
       <?= $form->field($model, 'idCbType')->dropDownList(ArrayHelper::map(CbTypes::find()->all(),'idCbType','cbType'),[ 'prompt'=>'Select course type',
                            'language' => 'en',
                            ]);

    ?> 
    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'idSkills')->widget(select2::classname(),[
                                                    'data'=>ArrayHelper::map(Skills::find()->all(),'idSkills','skills'),
                                                    'theme' => Select2::THEME_KRAJEE, 
                                                    'options'=>[
                                                        'placeholder'=>'Select Skills type',
                                                        'onchange'=>'
                                                            
                                                            $.post("'.Url::to(['../training-areas/lists', 'id'=> '']).'"+$(this).val()+","+$("#providertrainingareasbyprovider-idcbtype").val()+","+$("#providertrainingareasbyprovider-idinstitsector").val(),function(data){
                                                                 $("select#providertrainingareasbyprovider-idtraining" ).html(data);
                                                              
                                                            
                                                            });'
                                                    ],
                                                    'language' => 'en',
                                                    'pluginOptions'=>['alloweClear'=>true],
                                                    ]);

                               
                            ?>   
    </div>
    </div>

    <div class='row'>
    <div class="col-md-4">
      <?= $form->field($model, 'idTraining')->widget(select2::classname(),[
                            'data'=>ArrayHelper::map(TrainingAreas::find()->where('idTraining=0')->all(),'idTraining','courseName'),
                            'theme' => Select2::THEME_KRAJEE, 
                            'options'=>[
                                'placeholder'=>'Select training area',
                                // 'onchange'=>'
                                //     $.post("'.Url::to(['subprograms/lists', 'id'=> '']).'"+$(this).val(),function(data){
                                //          $("select#select2-plans-idsubprogram-container" ).html(data);
                                //     });'
                            ],
                            'language' => 'en',
                            'pluginOptions'=>['alloweClear'=>true],
                            ]);
       
                            ?>  
    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'idMethodology')->dropDownList(ArrayHelper::map(ProviderMethodology::find()->all(),'idMethodology','methodology'),[ 'prompt'=>'Select teaching methodology',
                            'language' => 'en',
                            ]);

    ?>   
    </div>
    <div class="col-md-4">
     <?= $form->field($model, 'idLanguage')->dropDownList(ArrayHelper::map(ProviderLanguage::find()->all(),'idLanguage','language'),[ 'prompt'=>'Select teaching language',
                            'language' => 'en',
                            ]);

    ?>   
    </div>
    </div> 

    <div class='row'>
    <div class="col-md-6"> 
    <?= $form->field($model, 'idEvaluation')->dropDownList(ArrayHelper::map(ProviderEvaluation::find()->all(),'idEvaluation','method'),[ 'prompt'=>'Select evaluation method',
                            'language' => 'en',
                            ]);

    ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'idEvaluation')->dropDownList(ArrayHelper::map(ProviderEvaluation::find()->all(),'idEvaluation','method'),[ 'prompt'=>'Select evaluation method',
                            'language' => 'en',
                            ]);

    ?> 
    </div>
    </div>

    <?= $form->field($model, 'startingdate')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
