<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainerexpetise */
?>
<div class="provider-trainerexpetise-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTrainerexpertise',
            'idTrainer',
            'areaofexpertise',
            'certificate',
        ],
    ]) ?>

</div>
