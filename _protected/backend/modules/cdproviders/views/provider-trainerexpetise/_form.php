<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainerexpetise */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-trainerexpetise-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idTrainer')->textInput() ?>

    <?= $form->field($model, 'areaofexpertise')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'certificate')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
