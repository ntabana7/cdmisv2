<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderTrainerexpetise */

?>
<div class="provider-trainerexpetise-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
