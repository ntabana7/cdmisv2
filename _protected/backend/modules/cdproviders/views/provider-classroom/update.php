<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderClassroom */
?>
<div class="provider-classroom-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
