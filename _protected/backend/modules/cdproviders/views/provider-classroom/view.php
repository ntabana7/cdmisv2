<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderClassroom */
?>
<div class="provider-classroom-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idClassroom',
            'classroom',
        ],
    ]) ?>

</div>
