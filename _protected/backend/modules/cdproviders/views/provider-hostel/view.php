<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderHostel */
?>
<div class="provider-hostel-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idHostel',
            'hostelavailability',
        ],
    ]) ?>

</div>
