<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderHostel */
?>
<div class="provider-hostel-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
