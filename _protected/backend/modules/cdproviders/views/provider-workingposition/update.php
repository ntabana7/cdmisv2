<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderWorkingposition */
?>
<div class="provider-workingposition-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
