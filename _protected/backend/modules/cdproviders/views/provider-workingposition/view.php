<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\cdproviders\models\ProviderWorkingposition */
?>
<div class="provider-workingposition-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idWorkingposition',
            'position',
            'iscoposition',
            'iscocode',
        ],
    ]) ?>

</div>
