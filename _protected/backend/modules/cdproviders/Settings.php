<?php

namespace backend\modules\cdproviders;

/**
 * cdproviders module definition class
 */
class Settings extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\cdproviders\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
