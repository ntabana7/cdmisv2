<?php

namespace backend\controllers;

use Yii;
use backend\models\Commentonprogress;
use backend\models\CommentonprogressSearch;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use backend\models\Plans;

/**
 * CommentonprogressController implements the CRUD actions for Commentonprogress model.
 */
class CommentonprogressController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Commentonprogress models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new CommentonprogressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idplan' => $_GET['id'],
        ]);
    }


    /**
     * Displays a single Commentonprogress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Commentonprogress #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Commentonprogress model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Commentonprogress();  
        //create object to get data from plan table

        $dataModel = new Plans;
        $dataFromPlan = $dataModel->find()->where('idplan='.$_GET['idplan'])->one();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Commentonprogress",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'dataFromPlan'=>$dataFromPlan, //passing array containing data from Plan to the create page
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];
                }else if($model->load($request->post()) ){
                $model->id = Yii::$app->user->identity->id;
                $model->createdon=date('Y-m-d h:i:s');

                //get instance of the upload file 

                $usename=$model->institName=Yii::$app->user->identity->username;
                $reportname=$dataFromPlan->actionDescription;
                $model->file = UploadedFile::getInstance($model,'file');

                if($model->file){

                    $imageName = Yii::$app->user->identity->username.'_'.$reportname.'_'.'finalreport'.'_'.$model->createdon; 
                    // echo  Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension;die;                      
                    $model->file->saveAs(Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension);
                    $extension = $model->file->extension;
                 
                    $model->finalreport=$imageName.'.'.$extension;

                }else{

                    echo "File not attached";

                }       


                if($model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Commentonprogress",
                    'content'=>'<span class="text-success">Create Indivimplementationreport success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create?idplan='.$_GET['idplan']],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
                }         
                    
            }else{           
                return [
                    'title'=> "Create new Commentonprogress",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idcomment]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Commentonprogress model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $dataModel = new Plans;
        $dataFromPlan = $dataModel->find()->where('idplan='.$model->idplan)->one();       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Commentonprogress #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'dataFromPlan' =>$dataFromPlan
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) ){
                $model->id = Yii::$app->user->identity->id;
                $model->createdon=date('Y-m-d h:i:s');

                //get instance of the upload file 

                $usename=$model->institName=Yii::$app->user->identity->username;
                $reportname=$dataFromPlan->actionDescription;
                $model->file = UploadedFile::getInstance($model,'file');

                if($model->file){

                    $imageName = Yii::$app->user->identity->username.'_'.$reportname.'_'.'finalreport'.'_'.$model->createdon; 
                    // echo  Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension;die;                      
                    $model->file->saveAs(Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension);
                    $extension = $model->file->extension;
                 
                    $model->finalreport=$imageName.'.'.$extension;

                }else{

                    echo "File not attached";

                }       

                
                if($model->save()){
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Commentonprogress #".$id,
                        'content'=>$this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ]; 
                }       
            }else{
                 return [
                    'title'=> "Update Commentonprogress #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idcomment]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Commentonprogress model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Commentonprogress model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Commentonprogress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Commentonprogress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Commentonprogress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
