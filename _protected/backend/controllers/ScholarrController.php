<?php

namespace backend\controllers;

use Yii;
use frontend\models\Scholarr;
use frontend\models\ScholarrSearch;
use frontend\models\Scholardetails;
use backend\modules\cdproviders\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ScholarrController implements the CRUD actions for Scholarr model.
 */
class ScholarrController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Scholarr models.
     * @return mixed
     */
    // public function actionIndex()
    // {    
    //     $searchModel = new ScholarrSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }
    public function actionIndex()
    {
        $model = new Scholarr;
        $modelseducationdetail = [new Scholardetails]; 
        //if(!Yii::$app->user->isGuest){

            $searchModel = new ScholarrSearch;
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        //}
    }


    /**
     * Displays a single Scholarr model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Scholarr #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Scholarr model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Scholarr();
        $modelseducationdetail = [new Scholardetails];   

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Scholarr",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){


            $modelseducationdetail = Model::createMultiple(Scholardetails::classname());
            Model::loadMultiple($modelseducationdetail, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelseducationdetail) && $valid;
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // $model->applicationdate=date('Y-m-d H:i:s');
                    // $model->idInstit = Yii::$app->user->identity->userFrom;
                    if ($flag = $model->save(false)) {
                        // $total = 0;
                        foreach ($modelseducationdetail as $modeleducationdetail) 
                        {
                            $modeleducationdetail->scholar_id = $model->id;
                            // $total+= $modeleducationdetail->requestednumber;
                            if (! ($flag = $modeleducationdetail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                  
                        // $this->updatedemandednumber($model->idRequest,$total);
                    }
                    if ($flag) {
                        $transaction->commit();
                    return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Scholarr",
                    'content'=>'<span class="text-success">Create scholarr success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];  
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
            else{           
                return [
                    'title'=> "Create new Scholarr",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail 
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }


        }


        }else{
            /*
            *   Process for non-ajax request
            */
            // if ($model->load($request->post()) && $model->save()) {
            //     return $this->redirect(['view', 'id' => $model->id]);
            // }
            if($model->load($request->post())){


            $modelseducationdetail = Model::createMultiple(Scholardetails::classname());
            Model::loadMultiple($modelseducationdetail, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelseducationdetail) && $valid;
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // $model->applicationdate=date('Y-m-d H:i:s');
                    // $model->idInstit = Yii::$app->user->identity->userFrom;
                    if ($flag = $model->save(false)) {
                        // $total = 0;
                        foreach ($modelseducationdetail as $modeleducationdetail) 
                        {
                            $modeleducationdetail->scholar_id = $model->id;
                            // $total+= $modeleducationdetail->requestednumber;
                            if (! ($flag = $modeleducationdetail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                  
                        // $this->updatedemandednumber($model->idRequest,$total);
                    }
                    if ($flag) {
                        $transaction->commit();
                     return $this->redirect(['view', 'id' => $model->id]);  
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
            else{           
                return [
                    'title'=> "Create new Scholarr",
                    'content'=>$this->render('create', [
                        'model' => $model,
                        'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail 
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }


        }

             else {
                return $this->render('create', [
                    'model' => $model,
                    'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail 
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Scholarr model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Scholarr #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail 
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Scholarr #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Scholarr #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail 
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'modelseducationdetail'=>(empty($modelseducationdetail)) ? [new Scholardetails ]: $modelseducationdetail 
                ]);
            }
        }
    }

    /**
     * Delete an existing Scholarr model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Scholarr model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Scholarr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Scholarr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Scholarr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
