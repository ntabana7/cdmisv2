<?php

namespace backend\controllers;

use Yii;
use backend\models\Plans;
use backend\models\PlansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use backend\models\Institutions;
use backend\models\Fiscalyear;

/**
 * PlansController implements the CRUD actions for Plans model.
 */
class PlansController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Plans models.
     * @return mixed
     */
    public function actionIndex()
    {   
       
        if(Yii::$app->user->can('institution') || Yii::$app->user->can('funder') || Yii::$app->user->can('funderboss')){ 
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    public function actionClusterindex()
    {   

        if(Yii::$app->user->can('institution')){ 
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('clusterplan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

     public function actionApprovedplan()
    {   

        if(Yii::$app->user->can('institution')){ 
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->searchApprovedPlan(Yii::$app->request->queryParams);

        return $this->render('approvedplan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }

    /**
     * Lists all Plans models.
     * @return mixed
     */
    public function actionReport()
    {  
        if(Yii::$app->user->can('institution')){  
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.');
    }
    }
        //Report Budget by CB Level
    
    public function actionReport2()
    {    
        if(Yii::$app->user->can('institution')){ 
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
       throw new NotFoundHttpException('Your session has been expired, you need to login again.'); 
    }
    }
    public function actionBycritical()
    {  
        if(Yii::$app->user->can('institution')){   
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bycritical', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.'); 
    }
    }
    


    /**
     * Displays a single Plans model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    { 
        if(Yii::$app->user->can('institution') || Yii::$app->user->can('funder') || Yii::$app->user->can('funderboss')){  
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Plans #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

    }else{
        throw new NotFoundHttpException('Your session has been expired, you need to login again.'); 
    }

    }

    /**
     * Creates a new Plans model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        // if(!isset(Yii::$app->user->identity->userfrom)){
        //    return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        // }

        if(Yii::$app->user->can('admin')){ 
            throw new NotFoundHttpException('You are not supposed to submit the CB plan');
        }elseif(Yii::$app->user->can('cluster')){ 
        $request = Yii::$app->request;
        $model = new Plans();  

            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {                  
                if(Yii::$app->user->can('cluster')){ 
                $model->idInstit=$model->idInstit;
                $model->idInstitSector=$model->idInstitSector;
                $model->funders = implode(",",$_POST['Plans']['funders']);
                $model->datasource = 2;
                $model->grantedAmount=$model->cost;
                }

                $model->status=2;
                $model->submittedon=date('Y-m-d h:i:s');
                $model->stakeholders = implode(",",$_POST['Plans']['stakeholders']);
                
                if($model->idSubLevel == 2){                    
                    $model->quarters = $model->male1.','.$model->female1;
                    $model->quarters2 = $model->male2.','.$model->female2;
                    $model->quarters3 = $model->male3.','.$model->female3;
                    $model->quarters4 = $model->male4.','.$model->female4;
                    
                    $totalmale = $model->male1+$model->male2+$model->male3+$model->male4;;
                    $totalfemale = $model->female1+$model->female2+$model->female3+$model->female4;

                    if($model->nbrbeneficiaries != ($totalfemale+$totalmale)){
                         Yii::$app->session->setFlash('beneficiaire', "The number of beneficiaries is different from the number you gave in quarters");

                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                }else{
                    $model->quarters =  $model->qter1;
                    $model->quarters2 = $model->qter2;
                    $model->quarters3 = $model->qter3;
                    $model->quarters4 = $model->qter4;
                }
                 #var_dump($model->qter1,$model->qter2,$model->qter3,$model->qter4);die;
                

                //get instance of the upload file            

                $model->institName=Yii::$app->user->identity->username;
                $model->file = UploadedFile::getInstance($model,'file');
                if(empty($model->file)){
                    //$imageName = 'noprofile';
                   // $extension = 'jpg';
                    $model->actionplan =$model->find()->select('actionplan')->where('idInstit='.Yii::$app->user->identity->userFrom)->orderBy('idplan DESC')->LIMIT(1)->one()->actionplan;
                }else{
                  
                    $imageName = Yii::$app->user->identity->username.'_'.'Actionplan'.'_'.$model->submittedon; 
                    // echo  Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension;die;                      
                    $model->file->saveAs(Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension);
                    $extension = $model->file->extension;
                 
                    $model->actionplan=$imageName.'.'.$extension;         
                }
                                
                if( $model->save()){

                return $this->redirect(['view', 'id' => $model->idplan]);
                }
            } else {

                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        
       }elseif(Yii::$app->user->can('institution') || Yii::$app->user->can('funder')){ 
        $request = Yii::$app->request;
        $model = new Plans();  

            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {    

                $model->datasource = 1;
                if(Yii::$app->user->can('institution')){ 
                $model->idInstit=Yii::$app->user->identity->userFrom;
                $model->idInstitSector = Institutions::find()->where('idInstit=:u',['u'=>Yii::$app->user->identity->userFrom])->one()->idInstitSector;
                $model->funders = implode(",",$_POST['Plans']['funders']);
                $model->datasource = 0;
                }

                $model->status=0;
                $model->submittedon=date('Y-m-d h:i:s');
                $model->stakeholders = implode(",",$_POST['Plans']['stakeholders']);
                
                if($model->idSubLevel == 2){                    
                    $model->quarters = $model->male1.','.$model->female1;
                    $model->quarters2 = $model->male2.','.$model->female2;
                    $model->quarters3 = $model->male3.','.$model->female3;
                    $model->quarters4 = $model->male4.','.$model->female4;
                    
                    $totalmale = $model->male1+$model->male2+$model->male3+$model->male4;;
                    $totalfemale = $model->female1+$model->female2+$model->female3+$model->female4;

                    if($model->nbrbeneficiaries != ($totalfemale+$totalmale)){
                         Yii::$app->session->setFlash('beneficiaire', "The number of beneficiaries is different from the number you gave in quarters");

                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                }else{
                    $model->quarters =  $model->qter1;
                    $model->quarters2 = $model->qter2;
                    $model->quarters3 = $model->qter3;
                    $model->quarters4 = $model->qter4;
                }
                 #var_dump($model->qter1,$model->qter2,$model->qter3,$model->qter4);die;
                

                //get instance of the upload file            

                $model->institName=Yii::$app->user->identity->username;
                $model->file = UploadedFile::getInstance($model,'file');
                if(empty($model->file)){
                    //$imageName = 'noprofile';
                   // $extension = 'jpg';
                    $model->actionplan =$model->find()->select('actionplan')->where('idInstit='.Yii::$app->user->identity->userFrom)->orderBy('idplan DESC')->LIMIT(1)->one()->actionplan;
                }else{
                  
                    $imageName = Yii::$app->user->identity->username.'_'.'Actionplan'.'_'.$model->submittedon; 
                    // echo  Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension;die;                      
                    $model->file->saveAs(Yii::getAlias('@upload').'/'. $imageName . '.' . $model->file->extension);
                    $extension = $model->file->extension;
                 
                    $model->actionplan=$imageName.'.'.$extension;         
                }
                                
                if( $model->save()){

                return $this->redirect(['view', 'id' => $model->idplan]);
                }
            } else {

                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        
       }else{
            throw new NotFoundHttpException('Your session has been expired, you need to login again.');
       }

    }

    /**
     * Updates an existing Plans model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('institution') || Yii::$app->user->can('funder')){ 
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        
            /*
            *   Process for non-ajax request
            */
            // echo "<pre>";
            // print_r($model);die;
            if ($model->load($request->post())){ 
                $this->deleteUpdate($id);
                $this->actionCreate();

               $transaction->commit();
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
    

        }else{
            throw new NotFoundHttpException('Your session has been expired, you need to login again.');
        }
    }

      public function actionDecision($id)
    {
        if(Yii::$app->user->can('cluster')){ 
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        
            /*
            *   Process for non-ajax request
            */
            // echo "<pre>";
            // print_r($model);die;
            if ($model->load($request->post()) && $model->save(false)){ 
                    return $this->redirect(['view', 'id' => $model->idplan]);
               
            } else {
                return $this->render('decision', [
                    'model' => $model,
                ]);
            }
    

        }else{
            throw new NotFoundHttpException('Your session has been expired, you need to login again.');
        }
    }

    /**
     * Delete an existing Plans model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    public function deleteUpdate($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

    }

     /**
     * Delete multiple existing Plans model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Plans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Plans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Plans::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



//     /* Access posts from a category*/
// $category = common\models\Category::find()->where(['category_id'=>$category_id])->one();
// foreach($category->posts as $post) // iterate over all associated posts of category
// {
//     $postTitle = $post->title; //access data of Post model
// }



// /* Access category from post */

// /* Access sector from plan */
// $post = common\models\Post::find()->where(['id'=>$post_id])->one();
// $categoryName = $post->category->category_name;




}
