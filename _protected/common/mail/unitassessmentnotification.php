<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">    
    .c-std {
    border: 1px solid #ddd;
    border-radius: 3px;
    background-color: #f7f7f7;
    box-shadow: 1px 1px 1px #eee;
    padding: 2px 5px 2px;
    margin-bottom: 2px;
    float: right;
    width:90%;
    }
</style>
</head>
<body>
    <?php $this->beginBody() ?>

    <div class='table-responsive c-std'>
        <p>
            Thank You for Submitting Your Assessment
        </p>
        <p>
            We will carefully review and consider each request, and will be in touch with you with any question.
        </p>
        <p>
            Thank you
        </p>        
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>




