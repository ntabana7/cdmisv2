<?php
ini_set('date.timezone','Africa/Kigali');
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('appRoot', '/'.basename(dirname(dirname(dirname(__DIR__)))));
Yii::setAlias('upload', '/var/www/html/advanced/backend/uploads/');
Yii::setAlias('uploadImgUrl', 'http://localhost:82/backend/uploads/');


