<?php
namespace common\grid;

use Yii;
use Closure;
use yii\helpers\Html;
use yii\helpers\Json;
use kartik\base\Config;
use yii\grid\DataColumn;
use kartik\grid\GridView;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use kartik\editable\Editable;
use yii\base\InvalidConfigException;
use backend\modules\cna\models\Assessment;
/*
 *
 * @author denyse Uwamahoro
 * 
 */
class ProgressColumn extends DataColumn
{
 
    public $editableOptions = [];

    /**
     * @var boolean whether to refresh the grid on successful submission of editable
     */
    public $refreshGrid = false;


    public $readonly = false;



    /**
     * @var string the css class to be appended for the editable inputs in this column
     */
    protected $_css;

    /**
     * @inheritdoc
     */
    public $hAlign = GridView::ALIGN_CENTER;

    /**
     * @inheritdoc
     */
    public $width = '90px';

    /**
     * @inheritdoc
     */
    public $format = 'raw';

    /**
     * @var string label for the 0 value. Defaults to `Pending`.
     */
    public $pendingLabel;

    /**
     * @var string label for the 1 value. Defaults to `Approved`.
     */
    public $approvedLabel;

    /**
     * @var string label for the 2 value. Defaults to `Rejected`.
     */
    public $rejectedLabel;

    /**
     * @var string icon/indicator for the pending value. If this is not set, it will use the value from `trueLabel`. If
     * GridView `bootstrap` property is set to true - it will default to [[GridView::ICON_COLLAPSE]].
     */
    public $pendingIcon;

    /**
     * @var string icon/indicator for the approved value. If this is null, it will use the value from `falseLabel`. If
     * GridView `bootstrap` property is set to true - it will default to [[GridView::ICON_ACTIVE]].
     */
    public $approvedIcon;

    /**
     * @var string icon/indicator for the rejected value. If this is null, it will use the value from `falseLabel`. If
     * GridView `bootstrap` property is set to true - it will default to [[GridView::ICON_INACTIVE]].
     */
    public $rejectedIcon;

    /**
     * @var boolean whether to show null value as a false icon.
     */
    public $showNullAsFalse = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->_css = 'kv-edcol-' . hash('crc32', uniqid(rand(1, 100), true));
        if ($this->refreshGrid) {
            EditableColumnAsset::register($this->_view);
        }

        if (empty($this->pendingLabel)) {
            $this->pendingLabel = Yii::t('kvgrid', 'Pending');
        }
        if (empty($this->approvedLabel)) {
            $this->approvedLabel = Yii::t('kvgrid', 'Approved');
        }
        if (empty($this->rejectedLabel)) {
            $this->rejectedLabel = Yii::t('kvgrid', 'Rejected');
        }
        $this->filter = [0 => $this->pendingLabel, 1 => $this->approvedLabel , 1 => $this->rejectedLabel];

        if (empty($this->pendingIcon)) {
            /** @noinspection PhpUndefinedFieldInspection */
            $this->pendingIcon = ($this->grid->bootstrap) ? GridView::ICON_COLLAPSE : $this->pendingLabel;
        }

        if (empty($this->approvedIcon)) {
            /** @noinspection PhpUndefinedFieldInspection */
            $this->approvedIcon = ($this->grid->bootstrap) ? GridView::ICON_ACTIVE : $this->approvedLabel;
        }

        if (empty($this->rejectedIcon)) {
            /** @noinspection PhpUndefinedFieldInspection */
            $this->rejectedIcon = ($this->grid->bootstrap) ? GridView::ICON_INACTIVE : $this->rejectedLabel;
        }

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);

        if ($value !== null) {
            if($value == 3){
                $percentages = Assessment::percentages($value, $model->idInstit);
                $percentage  = ($percentages > 0 ) ? $percentages.'%' : '';
                return Progress::widget([ 
                            'label' => '<span style="color:black">'.$percentage.'</span>',
                            'percent' => $percentages,
                            'barOptions' => ['class' => Assessment::progressClass($percentages)],
                            'options' => ['class' => 'progress-striped']
                           
                ]);
            }elseif($value == 2){
                $percentages = Assessment::percentages($value, $model->idInstit);
                $percentage  = ($percentages > 0 ) ? $percentages.'%' : '';
                return Progress::widget([ 
                            'label' => '<span style="color:black">'.$percentage.'</span>',
                            'percent' => $percentages,
                            'barOptions' => ['class' => Assessment::progressClass($percentages)],
                            'options' => ['class' => 'progress-striped']
                ]);
            }else{
                $percentages = (Assessment::percentages($value, $model->idInstit)) ;
                $percentage  = ($percentages > 0 ) ? $percentages.'%' : '';
                return Progress::widget([ 
                            'label' => '<span style="color:black">'.$percentage.'</span>',
                            'percent' => $percentages,
                            'barOptions' => ['class' => Assessment::progressClass($percentages)],
                ]);
            }

        }

    }
}
