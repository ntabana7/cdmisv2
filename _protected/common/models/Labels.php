<?php
namespace common\models;
use yii\helpers\Html;

class Labels {

	//Definitions are written here

	public static $current= '<p class="text-justify">What is the current capacity situation? </p>';

	public static $desired='<p class="text-justify">What is the desired capacity situation to be able to deliver on the policy priority?</p>';

	public static$program = '<p class="text-justify"><b>Programmes:</b> are the major pillars, strategic orientations or targets each institution is delivering on, these are based on your sector and/or institutional roles and responsibilities. Your sector/organisation’s main programs pulled from the Annual Action Plan as per the Budget Call Circular format for public institutions.</p>';

	public static $subprogram = '<p class="text-justify"><b>Sub programs:</b> are sub pillars or smaller targets that are linked to the big programs that all institutions are delivering on, they are more specific to what sectors/institutions are delivering on in line with their mandate. Your sector/organisation’s sub program pulled from the Annual Action Plan as per the Budget Call Circular format for public institutions.</p>';

	public static $output = '<p class="text-justify">' .
	'<b>Output:</b> this is the short term result/achievement trough the implementation of the capacity building activity you are planning.  It’s taken directly from your Annual Action Plan.' . 
	'</p>';

	public static $cbchallenge = '<p class="text-justify">' .
	'<b>Capacity Challenge:</b> This is the capacity problem, at different levels (individual, organizational or /and institutional) you currently experience and that prevent you from achieving performance targets. It is that issue at hand that may impede you from achieving your annual targets as institutions or individuals.' . 
	'</p>';

	public static $baseline = '<p class="text-justify">' .
	'<b>CD Baseline:</b> This refers to the starting point or current situation in the organization. The point from where implementation begins, improvement is judged, or comparison is made.' . 
	'</p>';

	public static $level = '<p class="text-justify">' .
	'A CD level includes either Institutional, Organizational or Individual.<br><br>

	<b>INSTITUTIONAL LEVEL OR ENABLING ENVIRONMENT:</b>refers to the policy, legal and regulatory environment within which organizations and sectors function. These may be described as the formal and informal “rules of the game” that dictate what organizations and sectors can or cannot do, as well as the level of authority and discretion under their mandates.<br><br>

	<b>ORGANIZATIONAL CAPABILITIES:</b> refers to the ability of organizations to deliver on their mandates and meet performance targets. Key drivers of organizational capacity include internal policies, organizational arrangements/ structures, operating procedures, processes, work flows and internal frameworks that allow an organization if it is well resourced and aligned to operate and deliver on its mandate as well as soft attributes such as attitudes and shared and values.<br><br>

	<b>INDIVIDUAL COMPETENCIES:</b> refers to sufficiency and quality of the workforce i.e. the right numbers of people with the required knowledge, skills, experience, exposure, aptitudes, attitudes and mind-set. ' . 
	'</p>';

	public static $sublevel = '<p class="text-justify">' .
	'<b>CD Approach:</b> refers to how (process) to develop the capacity of an Institution, organization or individuals . 
	'  . 
	'</p>';

	public static $indicator = '<p class="text-justify">' .
	'<b>CD Indicator:</b> An indicator in general is a specific measure that when tracked systematically over time, indicates progress (or not) toward a specific target. Indicators are traditionally numerical.  An outcome indicator answers the question:<br><br> How will we know success when we see it?<br><br>

	<b>QUANTITATIVE INDICATORS</b> can be expressed as a number. For example, the number of civil servants trained by Rwanda Management Institute (RMI), the percentage of staff who understand the vision and mission of the organization among others.<br><br> 
	<b>QUALITATIVE INDICATORS</b> on the other hand, indicate the quality of something, and they cannot normally be expressed as a number; for example, ‘improved working relations among staff’; improved staff participation in learning clinics.<br><br>
	<b>PERFORMANCE INDICATORS</b> are measures of inputs, processes, outputs, outcomes, and impacts for development projects, programs, or strategies.  Only beneficial when supported with sound data collection, perhaps involving formal surveys, analysis, and review.  Indicators enable us to track progress, demonstrate results, and take corrective action to improve service delivery. 

	' . 
	'</p>';

	public static $cbtype = '<p class="text-justify">' .
	'<b>SPECIALIZED</b> - short courses composed of curriculum units or training modules - after a formal academic training in Civil Engineering, one could then undertake specialized training in Railway; Oil & Gas<br><br>
	<b>GENERIC/ SOFT SKILLS</b> – Generic/ Soft skills are ‘transferable’ skills and are seen as part of a suite of skills which in combination optimize an individual’s productivity. They underpin other technical skills, as well as drawing on personal attributes, which affect how effectively skills can be learnt.<br><br>
	<b>PROFESSIONAL</b> – training to earn or maintain professional credentials – usually acquired through Continuous Professional Development (CPD) that takes one through different levels.<br><br>
	<b>EXPERT</b> – Advanced training level as agreed upon by the Sector e.g. Certified Microsoft IT Expert.<br><br>
	<b>ASSOCIATE</b> - Basic training level as agreed upon by the Sector e.g. Associate Microsoft IT Expert

	' . 
	'</p>';

	public static $skills = '<p class="text-justify">
	<b>Skills:</b> is a learned capacity to obtain pre-determined results. E.g.: Technical Skills, Cognitive Skills and Inter-personal skills
	</p>';

	public static $training = '<p class="text-justify">
	<b>Training areas:</b> 
	</p>';

	public static $qualification = '<p class="text-justify">
	<b>Qualification:</b>
	</p>';

	public static $beneficiaries = '<p class="text-justify">
	<b>Beneficiaries</b>: Individuals, groups, and or organizations whose situation is supposed to improve through the project activities.  OR the individuals, groups, or organizations, whether targeted or not, that benefit, directly or indirectly, from the development intervention.  Other terms that can be used are <b>reach</b>, <b>target group</b> 
	</p>';

	public static $cbaction = '<p class="text-justify">
	<b>CD action:</b> Actions taken or work performed through which inputs, such as training, capacity building funds, technical assistance for example international expert coaches under the strategic capacity building initiative and other types of CD resources are mobilized and applied to produce specific outputs. 
	</p>';

	public static $responsable = '<p class="text-justify">
	<b>Responsable:</b> refers to individual or organization that has the responsibility to implement the planned CD action. 
	</p>';

	public static $stakeholders = '<p class="text-justify">
	<b>Key Stakeholder:</b> Key Institutions or other partners who will have a role to play, or will impact, either directly or indirectly, the implementation and effectiveness of the CB action.
	</p>';

	public static $funders = '<p class="text-justify">
	<b>Source of fund:</b> Refers to the current or expected source of funding. It is not necessary to have identified sources of funding for all interventions at this stage. E.g JICA, KOICA, SIDA Fund.
	</p>';

	public static $budget = '<p class="text-justify"><b>Budget:</b> These are the estimated financial resources required to implement the CB action</p>';

	public static $actionplan = '<p class="text-justify"><b>Action plan:</b> Is a document that lists what steps must be taken in order to achieve a specific goal. Its purpose is to clarify what resources are required to reach the goal, formulate a timeline for when specific tasks need to be completed and determine what resources are required</p>';

	public static function renderLabelHelp($label, $help) {
		/*
		* Html::label(Labels::renderLabelHelp($model->getAttributeLabel('capacity_building_action'),Labels::$actionplan));
		*/
	    return Html::tag('span', $label, [
	        'data-toggle'=>'popover',
	        'data-trigger' => 'click hover',
	        'data-placement' => 'auto right',
	        'data-html' => 'true',    // allow html tags
	        'data-title'=> 'Field Help',
	        'data-content'=>$help,
	        'style'=>'text-decoration: underline; cursor:help;'
	    ]);
}
}
?>