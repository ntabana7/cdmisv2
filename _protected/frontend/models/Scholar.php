<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%scholar}}".
 *
 * @property integer $id
 * @property string $FirstName
 * @property string $LastName
 * @property string $gender
 * @property string $passport_id
 * @property string $email
 * @property string $birthdate
 * @property string $universityattended
 * @property string $universitycountry
 * @property string $latestqualification
 * @property string $areastudy
 * @property string $startingyear
 * @property string $endingyear
 * @property string $sponsorship
 * @property string $levelsponsored
 * @property string $emproymentstatus
 * @property string $instutition
 * @property string $location
 */
class Scholar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%scholar}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FirstName', 'LastName', 'gender', 'passport_id', 'email', 'birthdate', 'universityattended', 'universitycountry', 'latestqualification', 'areastudy', 'startingyear', 'endingyear', 'sponsorship', 'levelsponsored', 'emproymentstatus'], 'required'],
            [['FirstName', 'LastName', 'sponsorship', 'levelsponsored', 'instutition'], 'string', 'max' => 200],
            [['gender'], 'string', 'max' => 11],
            [['passport_id', 'email', 'birthdate', 'universityattended', 'universitycountry', 'latestqualification', 'areastudy', 'startingyear', 'endingyear', 'emproymentstatus', 'location'], 'string', 'max' => 100],
            [['passport_id'], 'unique'],
            [['email'], 'unique'],
            [['email'],'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'FirstName' => Yii::t('app', 'First Name'),
            'LastName' => Yii::t('app', 'Last Name'),
            'gender' => Yii::t('app', 'Gender'),
            'passport_id' => Yii::t('app', 'Passport or ID Number'),
            'email' => Yii::t('app', 'Email'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'universityattended' => Yii::t('app', 'University attended'),
            'universitycountry' => Yii::t('app', 'University located'),
            'latestqualification' => Yii::t('app', 'Latest qualification'),
            'areastudy' => Yii::t('app', 'Area of study'),
            'startingyear' => Yii::t('app', 'Starting year'),
            'endingyear' => Yii::t('app', 'Ending year'),
            'sponsorship' => Yii::t('app', 'Sponsorship'),
            'levelsponsored' => Yii::t('app', 'Level sponsored'),
            'emproymentstatus' => Yii::t('app', 'Employment status'),
            'instutition' => Yii::t('app', 'Instutition'),
            'location' => Yii::t('app', 'Where is the Institution?'),
        ];
    }

    private function getcountries(){
        return array("Select country","Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

    }

    public function countries(){
        $countries = [];
        foreach($this->getcountries() as $row){
            $countries[$row]=$row;
        }

        return $countries;
    }

    public function getYears($startyear,$range){
       $year =[];
       for($i=$startyear;$i<$startyear+$range;$i++){
            $year[$i] = $i;
       }

       return $year;
    }

    public function sendEmail($email)
    {

        return Yii::$app->mailer->compose(['html'=>'notification'], ['model' => $this])
            ->setTo($email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Notification #' . $this->id)
            ->send();
    }
    private function getlevels(){
        return array("Select education level","PhD","PG Diploma","Masters","Bachelor","Diploma");

    }
    public function levels(){
        $levels = [];
        foreach($this->getlevels() as $row){
            $levels[$row]=$row;
        }

        return $levels;
    }

    private function getemploymentstatus(){
        return array("Select employment status","Employed","Not employed");

    }
    public function employmentstatus(){
        $employmentstatus = [];
        foreach($this->getemploymentstatus() as $row){
            $employmentstatus[$row]=$row;
        }

        return $employmentstatus;
    }

    private function getsponsorship(){
        return array("Select ponsorship program","Presidential Scholarship","Government of Rwanda","Other");

    }
    public function sponsorship(){
        $sponsorship = [];
        foreach($this->getsponsorship() as $row){
            $sponsorship[$row]=$row;
        }

        return $sponsorship;
    }
}
