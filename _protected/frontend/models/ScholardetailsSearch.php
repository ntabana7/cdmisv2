<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Scholardetails;

/**
 * ScholardetailsSearch represents the model behind the search form about `frontend\models\Scholardetails`.
 */
class ScholardetailsSearch extends Scholardetails
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'scholar_id'], 'integer'],
            [['universityattended', 'universitycountry', 'sponsorhip', 'othersponsorship', 'qualification', 'qualificationarea', 'startingdate', 'endingdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Scholardetails::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'scholar_id' => $this->scholar_id,
        ]);

        $query->andFilterWhere(['like', 'universityattended', $this->universityattended])
            ->andFilterWhere(['like', 'universitycountry', $this->universitycountry])
            ->andFilterWhere(['like', 'sponsorhip', $this->sponsorhip])
            ->andFilterWhere(['like', 'othersponsorship', $this->othersponsorship])
            ->andFilterWhere(['like', 'qualification', $this->qualification])
            ->andFilterWhere(['like', 'qualificationarea', $this->qualificationarea])
            ->andFilterWhere(['like', 'startingdate', $this->startingdate])
            ->andFilterWhere(['like', 'endingdate', $this->endingdate]);

        return $dataProvider;
    }
}
