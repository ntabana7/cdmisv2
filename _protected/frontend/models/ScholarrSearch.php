<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Scholarr;

/**
 * ScholarrSearch represents the model behind the search form about `frontend\models\Scholarr`.
 */
class ScholarrSearch extends Scholarr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['passport_id', 'familyname', 'firstname', 'fathername', 'mothername', 'gender', 'birthdate', 'emailaddress', 'telephone', 'emproymentstatus', 'position', 'instutition', 'location'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Scholarr::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'passport_id', $this->passport_id])
            ->andFilterWhere(['like', 'familyname', $this->familyname])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'fathername', $this->fathername])
            ->andFilterWhere(['like', 'mothername', $this->mothername])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'birthdate', $this->birthdate])
            ->andFilterWhere(['like', 'emailaddress', $this->emailaddress])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'emproymentstatus', $this->emproymentstatus])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'instutition', $this->instutition])
            ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }
}
