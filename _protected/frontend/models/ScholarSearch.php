<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Scholar;

/**
 * ScholarSearch represents the model behind the search form about `frontend\models\Scholar`.
 */
class ScholarSearch extends Scholar
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['FirstName', 'LastName', 'gender', 'passport_id', 'email', 'birthdate', 'universityattended', 'universitycountry', 'latestqualification', 'areastudy', 'startingyear', 'endingyear', 'sponsorship', 'levelsponsored', 'emproymentstatus', 'instutition', 'location'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Scholar::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'FirstName', $this->FirstName])
            ->andFilterWhere(['like', 'LastName', $this->LastName])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'passport_id', $this->passport_id])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'birthdate', $this->birthdate])
            ->andFilterWhere(['like', 'universityattended', $this->universityattended])
            ->andFilterWhere(['like', 'universitycountry', $this->universitycountry])
            ->andFilterWhere(['like', 'latestqualification', $this->latestqualification])
            ->andFilterWhere(['like', 'areastudy', $this->areastudy])
            ->andFilterWhere(['like', 'startingyear', $this->startingyear])
            ->andFilterWhere(['like', 'endingyear', $this->endingyear])
            ->andFilterWhere(['like', 'sponsorship', $this->sponsorship])
            ->andFilterWhere(['like', 'levelsponsored', $this->levelsponsored])
            ->andFilterWhere(['like', 'emproymentstatus', $this->emproymentstatus])
            ->andFilterWhere(['like', 'instutition', $this->instutition])
            ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }
}
