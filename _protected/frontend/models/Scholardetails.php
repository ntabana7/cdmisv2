<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%scholardetails}}".
 *
 * @property integer $id
 * @property integer $scholar_id
 * @property string $universityattended
 * @property string $universitycountry
 * @property string $sponsorhip
 * @property string $othersponsorship
 * @property string $qualification
 * @property string $qualificationarea
 * @property string $startingdate
 * @property string $endingdate
 *
 * @property Scholarr $scholar
 */
class Scholardetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%scholardetails}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['universityattended', 'universitycountry', 'sponsorhip', 'qualification', 'qualificationarea', 'startingdate', 'endingdate'], 'required'],
            [['scholar_id'], 'integer'],
            [['universityattended', 'universitycountry'], 'string', 'max' => 100],
            [['sponsorhip', 'othersponsorship'], 'string', 'max' => 200],
            [['qualification', 'qualificationarea'], 'string', 'max' => 255],
            [['startingdate', 'endingdate'], 'string', 'max' => 50],
            [['scholar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scholarr::className(), 'targetAttribute' => ['scholar_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
   public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'scholar_id' => Yii::t('app', 'Scholar ID'),
            'universityattended' => Yii::t('app', 'Name of University'),
            'universitycountry' => Yii::t('app', 'Where the University is located'),
            'sponsorhip' => Yii::t('app', 'Sponsorhip'),
            'othersponsorship' => Yii::t('app', 'Other sponsorship'),
            'qualification' => Yii::t('app', 'Qualification'),
            'qualificationarea' => Yii::t('app', 'Qualification area'),
            'startingdate' => Yii::t('app', 'Starting date'),
            'endingdate' => Yii::t('app', 'Ending date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScholar()
    {
        return $this->hasOne(Scholarr::className(), ['id' => 'scholar_id']);
    }
}
