<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var frontend\models\Scholar $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Scholars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scholar-view">
    
    <?= DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'id',
            'FirstName',
            'LastName',
            'gender',
            'passport_id',
            'email:email',
            'birthdate',
            'universityattended',
            'universitycountry',
            'latestqualification',
            'areastudy',
            'startingyear',
            'endingyear',
            'sponsorship',
            'levelsponsored',
            'emproymentstatus',
            'instutition',
            'location',
        ],
        
        'enableEditMode' => false,
    ]) ?>

</div>
