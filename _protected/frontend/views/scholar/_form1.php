<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var frontend\models\Scholar $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="scholar-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'FirstName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter First Name...', 'maxlength' => 200]],

            'LastName' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Last Name...', 'maxlength' => 200]],

            'gender' => ['type'=>Form::INPUT_RADIO_LIST, 'items'=>['M'=>'Male', 'F'=>'Female'],'options'=>['inline'=>true]],

            'passport_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Passport ID...', 'maxlength' => 100]],

            'email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email...', 'maxlength' => 100]],

            'birthdate' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>$model->getYears(1930,100), 'placeholder'=>'Type and select year'],

            'universityattended' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Universityattended...', 'maxlength' => 100]],

            'universitycountry' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>$model->countries(), 'placeholder'=>'Select Country'],

            'latestqualification' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>$model->levels(), 'placeholder'=>'Select education level'],

            'areastudy' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Areastudy...', 'maxlength' => 100]],

            'startingyear' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>$model->getYears(1950,100), 'placeholder'=>'Type and select year'],

            'endingyear' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>$model->getYears(1950,100), 'placeholder'=>'Type and select year'],

            'sponsorship' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>$model->sponsorship(), 'placeholder'=>'Select ponsorship program'],

            'levelsponsored' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>$model->levels(), 'placeholder'=>'Select education level'],

            'emproymentstatus' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>$model->employmentstatus(), 'placeholder'=>'Select employement status'],

            'instutition' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Instutition...', 'maxlength' => 200]],

            'location' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>$model->countries(), 'placeholder'=>'Select location'],
        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
