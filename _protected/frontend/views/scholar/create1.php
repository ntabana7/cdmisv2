<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var frontend\models\Scholar $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Scholar',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Scholars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scholar-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
