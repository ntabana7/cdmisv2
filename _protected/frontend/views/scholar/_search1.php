<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\ScholarSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="scholar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'FirstName') ?>

    <?= $form->field($model, 'LastName') ?>

    <?= $form->field($model, 'gender') ?>

    <?= $form->field($model, 'passport_id') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'birthdate') ?>

    <?php // echo $form->field($model, 'universityattended') ?>

    <?php // echo $form->field($model, 'universitycountry') ?>

    <?php // echo $form->field($model, 'latestqualification') ?>

    <?php // echo $form->field($model, 'areastudy') ?>

    <?php // echo $form->field($model, 'startingyear') ?>

    <?php // echo $form->field($model, 'endingyear') ?>

    <?php // echo $form->field($model, 'sponsorship') ?>

    <?php // echo $form->field($model, 'levelsponsored') ?>

    <?php // echo $form->field($model, 'emproymentstatus') ?>

    <?php // echo $form->field($model, 'instutition') ?>

    <?php // echo $form->field($model, 'location') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
