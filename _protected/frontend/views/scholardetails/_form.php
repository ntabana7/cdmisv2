<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Scholardetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scholardetails-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'scholar_id')->textInput() ?>

    <?= $form->field($model, 'universityattended')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'universitycountry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sponsorhip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'othersponsorship')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qualification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qualificationarea')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'startingdate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endingdate')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
