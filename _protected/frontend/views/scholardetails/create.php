<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Scholardetails */

?>
<div class="scholardetails-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
