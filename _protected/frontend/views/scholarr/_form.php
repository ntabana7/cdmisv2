<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Scholarr */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scholarr-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

<div class='row'>
<div class="col-md-4">
<?= $form->field($model, 'passport_id')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
<?= $form->field($model, 'familyname')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
<?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
</div>
</div>

<div class='row'>
<div class="col-md-4">
<?= $form->field($model, 'fathername')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
 <?= $form->field($model, 'mothername')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
 <?= $form->field($model, 'gender')->dropDownList($model->gender())?>
</div>
</div>

<div class='row'>
<div class="col-md-4">
<?= $form->field($model, 'birthdate')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
        ]);

    ?>
</div>
<div class="col-md-4">
 <?= $form->field($model, 'emailaddress')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
 <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
</div>
</div>

<div class='row'>
<div class="col-md-3">
<?= $form->field($model, 'emproymentstatus')->dropDownList($model->employmentstatus(),
            [
            'onchange'=>'
               var id = document.getElementById("scholarr-emproymentstatus").value;
               if(id == "Employed"){
                $("#position" ).show();
                $("#institution" ).show();
                $("#location" ).show();
               }
                else{
                $("#position" ).hide();
                $("#institution" ).hide();
                $("#location" ).hide();
               
                };'
            ]) ?>
</div>
<div class="col-md-3" id='position' style='display:none'>
<?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>    
</div>
<div class="col-md-3" id='institution' style='display:none'>
 <?= $form->field($model, 'instutition')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-3" id='location' style='display:none'>
 <?= $form->field($model, 'location')->dropDownList($model->countries(),
            [
            // 'prompt'=>'Select a country',
            // 'onchange'=>'
            // $.post( "'.Url::to(['normalprice', 'id' => '']).'"+$(this).val(),function(data){
            //  $("td#collectionproducts-price" ).html(data);
            // });'
            ])
    ?>
</div>
</div>
    
<div class='row'>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-"></i>Education details</h4></div>
        <!-- envelope -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelseducationdetail[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'universityattended',
                    'universitycountry',
                    'sponsorhip',
                    'othersponsorship',
                    'qualification',
                    'qualificationarea',
                    'startingdate',
                    'endingdate',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelseducationdetail as $i => $modeleducationdetail): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Education details</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modeleducationdetail->isNewRecord) {
                                echo Html::activeHiddenInput($modeleducationdetail, "[{$i}]id");
                            }
                        ?>                        
                        <div class="row">
                            <div class="col-sm-4">
                            <?= $form->field($modeleducationdetail, "[{$i}]universityattended")->textInput(['maxlength' => true]) ?>   
                            </div>    
                            <div class="col-sm-4">
                            <?= $form->field($modeleducationdetail, "[{$i}]universitycountry")->dropDownList($model->countries(),
                                [
                                // 'prompt'=>'Select a country',
                                // 'onchange'=>'
                                // $.post( "'.Url::to(['normalprice', 'id' => '']).'"+$(this).val(),function(data){
                                //  $("td#collectionproducts-price" ).html(data);
                                // });'
                                ]) 
                            ?>
                            </div>
                            <div class="col-sm-4">
                            <?= $form->field($modeleducationdetail, "[{$i}]sponsorhip")->dropDownList($model->sponsorship(),
                                [
                                // 'onchange'=>'
                                //    var id = document.getElementById("scholardetails-0-sponsorhip").value;
                                //    if(id == "Other"){
                                //     $("#other" ).show();
                                //    }
                                //     else{
                                //     $("#other" ).hide();
                                   
                                //     };'
                                ]) 
                            ?>
                            
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-md-4">
                            <?= $form->field($modeleducationdetail, "[{$i}]othersponsorship")->textInput(['maxlength' => true]) ?>                                
                            </div>                        
                             <div class="col-sm-4">
                             <?= $form->field($modeleducationdetail, "[{$i}]qualification")->dropDownList($model->levels(),
                                [
                                // 'prompt'=>'Select a country',
                                // 'onchange'=>'
                                // $.post( "'.Url::to(['normalprice', 'id' => '']).'"+$(this).val(),function(data){
                                //  $("td#collectionproducts-price" ).html(data);
                                // });'
                                ]) 
                            ?>  
                            </div>
                            <div class="col-sm-4">
                            <?= $form->field($modeleducationdetail, "[{$i}]qualificationarea")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                            <?= $form->field($modeleducationdetail, "[{$i}]startingdate")->widget(
                            DatePicker::className(), [
                            // inline too, not bad
                            'inline' => false, 
                            // modify template for custom rendering
                            // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                            'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                            ]
                            ]);

                            ?>                               
                            </div>                        
                             <div class="col-sm-6">
                             <?= $form->field($modeleducationdetail, "[{$i}]endingdate")->widget(
                            DatePicker::className(), [
                            // inline too, not bad
                            'inline' => false, 
                            // modify template for custom rendering
                            // 'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                            'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                            ]
                            ]);

                            ?> 
                            </div>                            
                        </div>
                        </div><!-- .row -->
 
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>   

   	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
