<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Scholarr */
?>
<div class="scholarr-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelseducationdetail'=>$modelseducationdetail,
    ]) ?>

</div>
