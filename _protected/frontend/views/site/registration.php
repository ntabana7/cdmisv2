<?php
use nenad\passwordStrength\PasswordInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
use backend\models\CbQualifications;
use backend\models\Districts;
use backend\modules\cdproviders\models\ProviderQualificationarea;
use backend\modules\internship\models\InternshipGrade;
use backend\modules\internship\models\InternshipGender;
use backend\modules\internship\models\InternshipBank;
use backend\modules\internship\models\InternshipUniversities;
use backend\modules\internship\models\Internshipinternapplication;


// added these classes
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

//This Class is included to allow display of definition
use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */


$this->title = Yii::t('app', 'Registration');
$this->params['breadcrumbs'][] = $this->title;
?>


<script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>


<div class="site-signup">
   <div class="container" style="width:100%">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-12 well bs-component">

        <p><?= Yii::t('app', 'Please fill out the following fields to signup:') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->widget(PasswordInput::classname(), []) ?>

            <hr>
         
            <div class="row">
                <section>
                <div class="wizard">
                    <div class="wizard-inner">
                        <div class="connecting-line"></div>
                        <ul class="nav nav-tabs" role="tablist">

                            <!-- <li role="presentation" class="active">
                                <a href="#step0" data-toggle="tab" aria-controls="step0" role="tab" title="Step 0">
                                    <span class="round-tab">
                                        <i class="glyphicon glyphicon-user"></i>
                                    </span>
                                </a>
                            </li> -->

                            <li role="presentation" class="active">
                                <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                    <span class="round-tab">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </span>
                                </a>
                            </li>

                            <li role="presentation" class="disabled">
                                <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                    <span class="round-tab">
                                        <i class="glyphicon glyphicon-info-sign"></i>
                                    </span>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                    <span class="round-tab">
                                        <i class="glyphicon glyphicon-question-sign"></i>
                                    </span>
                                </a>
                            </li>

                            <li role="presentation" class="disabled">
                                <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                    <span class="round-tab">
                                        <i class="glyphicon glyphicon-ok"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>

               
                        <div class="tab-content">

                        <!-- Start step 0 -->


                <!-- End step 0 -->

                    <!-- Start step 1 -->
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="step1">
                            <div class="row">
                            <div class="col-md-4"> 
                            <?= $form->field($modelRegistration, 'nida')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'firstname')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'secondname')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">                            
                            <div class="col-md-6">
                            <?= $form->field($modelRegistration, 'email')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                            <?= $form->field($modelRegistration, 'telephone')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        </div>
                         <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                        <li><button type="button" class="btn btn-primary next-step">continue</button></li>
                        </ul>                      
                    

                    </div>
                    <!-- End Start step 1 -->



                    <!-- Start step 2 -->
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div class="step2">
                            <div class="step_21">
                            <div class="row">
                            <div class="col-md-3">
                            <?= $form->field($modelRegistration, 'dob')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-3">
                             <?= $form->field($modelRegistration, 'idGender')->dropDownList(ArrayHelper::map(InternshipGender::find()->all(),'idGender','gender'),[ 'prompt'=>'Select your gender',
                            'language' => 'en',
                            ]);

                            ?>
                            </div>
                             <div class="col-md-3"> 
                            <?= $form->field($modelRegistration, 'idQualif')->dropDownList(ArrayHelper::map(CbQualifications::find()->all(),'idQualif','qualif'),[ 'prompt'=>'Select your qualification',
                            'language' => 'en',
                            ]);

                            ?>
                            </div>
                             <div class="col-md-3">                              
                             <?= $form->field($modelRegistration, 'idQualifarea')->dropDownList(ArrayHelper::map(ProviderQualificationarea::find()->all(),'idQualifarea','area'),[ 'prompt'=>'Select your qualification area',
                            'language' => 'en',
                            ]);

                            ?>
                            </div>                           
                            </div>

                            <!-- Second row step 2 -->
                            <div class="row mar_ned">
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'idGrade')->dropDownList(ArrayHelper::map(InternshipGrade::find()->all(),'idGrade','grade'),[ 'prompt'=>'Select grade',
                            'language' => 'en',
                            ]);

                             ?> 
                            </div> 
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'degree')->fileInput()?>  
                            </div> 
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'idUniversity')->dropDownList(ArrayHelper::map(InternshipUniversities::find()->all(),'idUniversity','university'),[ 'prompt'=>'Select your University',
                            'language' => 'en',
                            ]);

                            ?>   
                            </div>                  
                            </div>

                            </div>
                            <div class="step-22">
                            
                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary next-step">continue</button></li>
                        </ul>
                    



                    </div>
                    <!-- End Start step 2 -->



                    <!-- Start step 3 -->
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <div class="step33">
                        <h5><strong><!-- CB Resources --></strong></h5>
                        <hr>  
                            <div class="row mar_ned">
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'yearofgraduation')->textInput(['maxlength' => true]) ?> 
                            </div> 
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'accountNbr')->textInput(['maxlength' => true]) ?> 
                            </div> 
                            <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'idBank')->dropDownList(ArrayHelper::map(InternshipBank::find()->all(),'idBank','bank'),[ 'prompt'=>'Select your bank',
                            'language' => 'en',
                            ]);

                            ?>  
                            

                            </div> 
                                
                            </div>                          
                            <div class="row mar_ned">
                            <div class="col-md-4">
                            <!-- ?= $form->field($modelRegistration, 'idDistrict')->textInput(['maxlength' => true]) ?>  -->
                            <?= $form->field($modelRegistration, 'idDistrict[]')            
                                                    ->dropDownList($modelRegistration->DistrictsListDropdown,
                                                    [
                                                    'multiple'=>'multiple'
                                                    //'class'=>'chosen-select input-md required',              
                                                    ]             
                                                    )->label("Preferably I may work from"); 
                            ?>  
                            </div>
                             <div class="col-md-4">
                            <?= $form->field($modelRegistration, 'idHome')->dropDownList(ArrayHelper::map(Districts::find()->all(),'idDistrict','distName'),[ 'prompt'=>'Select District',
                            'language' => 'en',
                            ]);

                            ?> 
                            </div>
                            </div>

                                                       
                            
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                            <li><button type="button" class="btn btn-primary btn-info-full next-step">continue</button></li>
                        </ul>
                    
                    </div>



                    </div>
                    <!-- End Start step 3 -->
                    
                    <!-- Last step for submitting data -->
                   
                    <!-- Last step for submitting data -->




                    <div class="clearfix"></div>  
                </div> 
        
        </div>
    </section>
   </div>
</div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>

        <?php if ($model->scenario === 'rna'): ?>
            <div style="color:#666;margin:1em 0">
                <i>*<?= Yii::t('app', 'We will send you an email with account activation link.') ?></i>
            </div>
        <?php endif ?>

    </div>
</div>




<script type="text/javascript">
    $(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {       
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');
    
    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});
    
    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');
    
    // The Accordion Effect
    $('.accordion-header').click(function () {
        if($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
        
        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });
    
    return false;
});
</script>

<!-- CSS -->

<style>
.length{width: 274px;}


/*added CSS codes*/

.wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #fff;
    border: 2px solid #5bc0de;
    
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 50px;
}

.wizard h3 {
    margin-top: 0;
}
.step1 .row {
    margin-bottom:10px;
      padding-right:10px;
        padding-left:10px;
}
.step_21 {
    border :1px solid #eee;
    border-radius:5px;
    padding:10px;
}
.step33 {
    border:1px solid #ccc;
    border-radius:5px;
    padding-left:10px;
    margin-bottom:10px;
    padding-right:10px;
}
.step44 {
   margin : -33px 0px 0px 242px;
}
.dropselectsec {
    width: 68%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
}
.dropselectsec1 {
    width: 74%;
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    outline: none;
    font-weight: normal;
}
.mar_ned {
    margin-bottom:10px;
}
.wdth {
    width:25%;
}
.birthdrop {
    padding: 6px 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    color: #333;
    margin-left: 10px;
    width: 16%;
    outline: 0;
    font-weight: normal;
}


/* according menu */
#accordion-container {
    font-size:13px
}
.accordion-header {
    font-size:13px;
    background:#ebebeb;
    margin:5px 0 0;
    padding:7px 20px;
    cursor:pointer;
    color:#fff;
    font-weight:400;
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border-radius:5px
}
.unselect_img{
    width:18px;
    -webkit-user-select: none;  
    -moz-user-select: none;     
    -ms-user-select: none;      
    user-select: none; 
}
.active-header {
    -moz-border-radius:5px 5px 0 0;
    -webkit-border-radius:5px 5px 0 0;
    border-radius:5px 5px 0 0;
    background:#F53B27;
}
.active-header:after {
    content:"\f068";
    font-family:'FontAwesome';
    float:right;
    margin:5px;
    font-weight:400
}
.inactive-header {
    background:#333;
}
.inactive-header:after {
    content:"\f067";
    font-family:'FontAwesome';
    float:right;
    margin:4px 5px;
    font-weight:400
}
.accordion-content {
    display:none;
    padding:20px;
    background:#fff;
    border:1px solid #ccc;
    border-top:0;
    -moz-border-radius:0 0 5px 5px;
    -webkit-border-radius:0 0 5px 5px;
    border-radius:0 0 5px 5px
}
.accordion-content a{
    text-decoration:none;
    color:#333;
}
.accordion-content td{
    border-bottom:1px solid #dcdcdc;
}



@media( max-width : 585px ) {

    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}
</style>